# RDFS로 무엇을 할 수 있나? <sup>[1](#footnote_1)</sup>

**스키마는 OWL 없이도 조금 더 멋지고 유용하게 사용할 수 있다.**

[RDFS란?](./what-is-rdfs.md)에서 RDF Schema 언어가 어떻게 RDF 어휘를 정의할 수 있는지, 그리고 그 정의 자체가 RDF triple인지에 대해 설명했다. 스키마의 간단한 클래스와 속성 이름 정의가 데이터 세트 구조에 대한 기계 판독 가능한 문서로서 동일한 도메인을 중심으로 구축된 데이터와 어플리케이션의 상호 운용성을 어떻게 향상시킬 수 있는지 살펴보았다. 여기에서는 지난번 샘플 스키마에서 보았던 것에 추가하여 RDF 스키마가 어떻게 추가적인 종류의 중요한 정보를 저장할 수 있는지 살펴본 다음, RDF 스키마를 통해 할 수 있는 몇 가지 멋진 작업들을 살펴보겠다.

## 더 많은 데이터 모델링
RDFS를 사용하여 클래스와 속성 이름을 정의할 때 이들 간의 관계도 정의할 수 있다. 다음은 [RDFS란?](./what-is-rdfs.md)에서 다룬 스키마를 확장하여 클래스 간, 속성 간, 클래스와 속성 간의 관계를 정의한다.

```t
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> . 
@prefix vcard:   <http://www.w3.org/2006/vcard/ns#> .
@prefix emp:     <http://www.snee.com/schema/employees/> .
@prefix ex:      <http://www.snee.com/example/> .
@prefix dcterms: <http://purl.org/dc/terms/> . 

emp:Person rdf:type rdfs:Class ;
          rdfs:label "person" . 

emp:Employee a rdfs:Class ; 
            rdfs:subClassOf emp:Person ;
            rdfs:label "employee" . 

vcard:given-name  rdf:type rdf:Property ;
                  rdfs:domain emp:Person ;
                  rdfs:label "given name".

vcard:family-name rdf:type rdf:Property ;
                  rdfs:domain emp:Person ;
                  rdfs:label "family name" ;
                  rdfs:label "apellido"@es . 

emp:hireDate a rdf:Property ;
            rdfs:domain  emp:Employee ;
            rdfs:label   "hire date" ;
            rdfs:comment "The first day an employee was on the payroll."  ;
            rdfs:subPropertyOf dcterms:date . 

emp:reportsTo a rdf:Property ; 
             rdfs:domain emp:Employee ;
             rdfs:range  emp:Employee ;
             rdfs:label  "reports to" .
```

이전 스키마에 없었던 이 스키마의 첫 번째 요소는 `ex:Employee`가 `ex:Person`의 서브클래스라는 triple이다. 추론 구문 분석기가 직원 `ex:id2` (Heidi Smith)와 `ex:id3` (Jane Berger)가 `ex:Employee` 클래스의 인스턴스라는 것을 알았다면, 이 두 인스턴스도 `emp:Person` 클래스의 인스턴스라는 것을 알 수 있다.

이제 클래스를 선언하고 어떤 클래스가 다른 클래스의 하위 클래스인지 표시하는 방법을 알았으므로 클래스 계층 구조를 구축할 수 있다. 대부분의 최신 프로그래밍 언어를 사용해 본 사람이라면 익숙할 것이다. 그러나 이러한 프로그래밍 언어 중 속성 계층구조도 구축할 수 있는 언어는 거의 없다. 위의 스키마는 `emp:hireDate` 속성을 널리 사용되는 [Dublin Core](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) 어휘의 `dcterms:date` 속성의 하위 프로퍼티로 선언한다.

이렇게 하면 무엇을 얻을 수 있을까? 우선, 이 인사 데이터에 대한 사용자 인터페이스를 생성하는 도구가 `emp:hireDate` 속성을 인식하지 못할 수도 있지만, 추론을 통해 이 속성이 표준 `dcterms:date` 하나의 특수 버전이라는 것을 알아낸다면 편집 양식에서 이 필드를 표현하는 데 일반 텍스트 상자보다 날짜 위젯이 더 적합하다는 것을 알 수 있을 것이다.

Dublin Core DCMI Metadata Terms 어휘에 대한 RDFS 스키마의 [turtle 버전](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/dublin_core_terms.ttl)에는 predicate와 object `rdfs:subPropertyOf <http://purl.org/dc/elements/1.1/date>`가 포함된 9개의 triple이 포함되어 있다. 이를 통해 `dcterms:available`, `dcterms:created`와 `dcterms:dateAccepted`같은 속성이 날짜라는 것을 알 수 있다. "dateAccepted"라는 속성에서 이를 추측할 수 있지만, 해당 속성의 의미를 설명하는 기계 판독 가능한 방법이 없었다면 “created” 속성에 대해 알 수 없었을 것이다. (“semantics”라는 용어를 저자는 거의 사용하지 않지만, 이 용어를 사용할 때는 진심이다.)

클래스 간의 관계와 속성 간의 관계를 정의하는 방법을 살펴봤으니 이제 이 스키마에서 주목해야 할 다음 사항은 이 스키마가 속성과 클래스 간의 관계를 정의하는 방법이다. 위의 첫 번째 `rdfs:domain` triple은 `vcard:given-name` 속성을 `emp:Person` 클래스와 연결한다. (`emp:Employee`가 `emp:Person`의 하위 클래스인 경우, 이 속성은 이제 `emp:Employee`와도 연결된다.) 표준 어휘로 정의된 속성을 내 어휘로 정의한 내 사물과 연관시키는 데 문제가 있을까? 전혀 그렇지 않다. 내 어플리케이션에 대해 정의하는 사물에 대한 표준 기반 컨텍스트를 제공하기 때문에 실제로는 좋은 일이다.

[W3C RDFS ㄱRecommendation](https://www.w3.org/TR/rdf-schema/#ch_domain)에 따르면 "`rdfs:domain`은 주어진 속성을 가진 리소스가 하나 이상의 클래스의 인스턴스임을 나타내는 데 사용되는 `rdf:Property`의 인스턴스이다."라고 나와 있다. 이를 감안할 때, 위의 스키마는 RDF 리소스에 `vcard:given-name` 속성이 있으면 해당 리소스가 `emp:Person`의 인스턴스라고 추론할 수 있다는 것을 말한다. (만약 이것이 사무실 개가 사람이라는 추론으로 이어진다면, 클래스 계층 구조와 어떤 속성이 어떤 클래스와 연관되어 있는지 다시 평가해야 한다.)

때때로 우리는 웹에서 발견되는 데이터에서 이러한 종류의 추론을 가능하게 하기 위해 RDFS와 OWL이 발명되었다는 사실을 잊어버리곤 한다. 데이터 구조를 정의하는 데 도움을 주기 위해 개발된 것은 아니지만, 앞서 살펴본 것처럼 RDFS는 최소한 데이터 구조를 문서화하는 데 유용하다. 사용자 인터페이스 생성 예제를 계속 이어서, Employee 인스턴스에 대한 편집 양식을 생성하는 시스템은 이 스키마의 `rdfs:domain` triplet을 통해 이 편집 양식에 `vcard:given-name`, `vcard:family-name`, `emp:hireDate`와 `emp:reportsTo` 필드가 포함되어야 한다는 것을 알 수 있다. (지난번에도 언급했듯이 이러한 필드에 실제 속성 이름이 아닌 속성의 `rdfs:label` 값으로 레이블을 지정하면 양식을 더 읽기 쉽다는 것을 알아야 한다.)

클래스 계층을 정의할 수 있다는 것을 알고 있는 소프트웨어 개발자는 RDFS에서 클래스와 속성 간의 관계에 대해 약간 혼란스러워할 수 있다. 표준 객체 지향 모델링에서는 클래스를 정의할 때 해당 클래스에서 사용하는 속성을 정의하고 일부는 수퍼클래스로부터 상속받을 수 있다. RDFS에서는 클래스와 프로퍼티를 별도로 정의한 다음 원하는 경우 `rdfs:domain` 프로퍼티와 연결한다. (속성이 자체 계층 구조를 가질 수 있다는 사실은 객체 지향 개발자가 익숙해지는 데 시간이 걸릴 수 있다.)

`emp:reportsTo` 클래스에 대해 정의된 `rdfs:range` 속성은 클래스와 속성 간의 관계를 정의하는 또 다른 방법이다. RDFS 권장 사항에 따르면 이 속성은 "속성 값이 하나 이상의 클래스의 인스턴스임을 명시하는 데 사용된다." `emp:reportsTo`의 `rdfs:domain`이 `emp:Employee`인 경우 "X가 Y에게 보고한다"는 것은 X가 `emp:Employee`라는 것을 의미하고, `emp:reportsTo`의 `rdfs:range`가 `emp:Employee`인 경우 같은 문장에서 Y가 `emp:Employee`, 즉 한 직원이 다른 직원에게 보고한다는 것을 추론할 수 있다. 이러한 종류의 추론을 `rdfs:range`로 수행할 계획이 없더라도 주어진 속성에 대해 어떤 종류의 값을 기대할 수 있는지 나타내는 것은 여전히 유용하다. 예를 들어, 직원 데이터를 편집하기 위한 양식을 생성하는 어플리케이션은 양식의 "reports to" 부분에 일반 텍스트 상자 대신 직원 이름의 드롭다운 목록을 생성할 수 있다.

## 흥미로운 애플리케이션에 대한 더 많은 지원
위에서 설명한 아이디어를 여러 유용한 프로젝트에 어떻게 적용했는 지에 대하여 작성한 다른 블로그 포스팅도 있다.

### (모바일!) 사용자 인터페이스 구동
[Using SPARQL queries from native Android apps](https://www.bobdc.com/blog/using-sparql-queries-from-nati/)에서는 사용자가 선택한 정보를 웹 서버로 보내기 전에 의류 제품과 해당 제품의 색상과 사이즈를 선택할 수 있는 네이티브 Android 앱을 만들기 위해 MIT App Inventor 툴킷을 사용한 방법을 설명한다. 선택한 제품, 색상과 사이즈는 모두 RDFS 모델에 저장되며, 제 휴대폰의 스크린샷은 모델을 저장하는 RDF 스키마에 새 색상을 추가한 후 색상 선택 목록이 어떻게 확장되었는지 보여준다. 이 게시물에서는 Android 앱을 변경하지 않고 RDFS 모델을 추가하면 앱에서 영어 이외의 다른 언어를 지원할 수 있는 방법도 설명한다.

### 데이터 통합
[Driving Hadoop data integration with standards-based models instead of code](http://www.bobdc.com/blog/driving-hadoop-data-integratio/)에서는 Microsoft의 SQL Server Northwind 샘플 데이터베이스의 데이터와 Oracle의 샘플 HR 데이터베이스의 데이터를 결합하는 데이터 통합 데모를 설명한다. 이 두 데이터베이스는 모두 인적 자원 데이터베이스를 설명하지만 유사한 속성에 대해 서로 다른 이름(예: `LastName`과 `last_name`)을 사용합한다. 이 데모는 Python과 SPARQL 쿼리를 사용하여 두 소스에서 데이터를 수집하고 공통 어휘를 사용하여 표현한다. 이 시스템은 RDFS 모델을 사용하여 이 어휘를 정의하고, 위에서 언급한 `rdfs:subPropertyOf` 속성을 사용하여 두 데이터 소스에서 이 어휘로 매핑을 정의하는 데 이 부분이 매우 중요하다. 데모를 실행하고 더 많은 입력을 포함하도록 RDFS 모델을 확장한 후, 데모를 다시 실행하면 해당 확장된 모델만으로 더 많은 소스 데이터가 통합된다. Python 스크립트를 변경할 필요가 없었다.

지금까지 이 프로젝트에 대해 설명한 모든 아이디어는 매우 간단하다. 이 글의 참신함은 이 모든 것이 여러 시스템에 분산된 Hadoop 클러스터에서 이루어지도록 설정했다는 점이다. 당시에는 이 클러스터가 특히 인기가 많았기 때문이다.

이 글은 IBM Data Magazine에 기고한 글과 함께 작성되었기 때문에 RDF에 대해 잘 알고 있다고 가정하지 않아 RDF를 처음 접하는 개발자들에게 도움이 될 수 있다.

### 부분 스키마로 데이터 변환
[Transforming data with inferencing and (partial!) schemas](https://www.bobdc.com/blog/partialschemas)에서는 필요한 것보다 많은 데이터가 복잡하게 얽혀 있는 경우, 실제로 원하는 데이터의 하위 집합에 대한 RDF 스키마가 매우 유용할 수 있는 방법을 설명한다. 추론을 사용하여 데이터를 변환할 때 특히 그렇다. 여기서는 해당 게시물의 첫 번째 단락 전체를 인용하겠다.

> *원래는 "부분 스키마!"라는 제목을 붙일 계획이었지만, 예를 조립하면서 점진적으로 구축된 부분 스키마의 가치를 보여줄 뿐만 아니라, 아래 단계는 스키마를 사용한 추론이 데이터 통합에 매우 유용한 변환을 구현하는 방법도 보여준다는 사실을 깨달았다. 적절한 상황에서는 절차적이든 선언적이든 코드를 사용하는 대신 데이터 모델 자체에 의해 변환이 이루어지기 때문에 SPARQL보다 훨씬 더 우수할 수 있다.*

다음은 데모를 살펴본 이후 또 다른 문단이다.

> *데이터와 그 스키마를 보다 유연한 방식으로 진화시킬 수 있다는 이 아이디어는 데이터 통합 프로젝트에 특히 유용할 것이다. 여기서는 (다소) 엉망인 RDF로 시작했지만, 둘 이상의 RDF 데이터 집합으로 작업하는 경우(일부는 JSON 또는 관계형 데이터베이스와 같은 다른 형식에서 변환된 것일 수도 있음), RDFS를 사용하여 해당 데이터 집합의 작은 하위 집합을 식별하고 해당 하위 집합의 구성 요소 간의 관계를 지정하면 지식 그래프와 이를 사용하는 어플리케이션이 훨씬 더 빨리 유용해질 수 있다.*

오늘 설명한 많은 기법들이 해당 프로젝트에서 유용하게 활용되는 것을 다시 한 번 확인할 수 있다.

### 조금 더 유용한 배경
특정 표준을 사용할 때 표준 자체가 긴 기술 전문 용어의 집합이라고 생각하기 쉽다. W3C RDF 스키마 권장사항은 [RDFS: The primary document](http://www.bobdc.com/blog/rdfs-the-primary-document/)에 쓴 것처럼 그리 길지 않고 실제로 가독성이 높기 때문에 추천한다. [RDF Schema Wikipedia page](https://en.wikipedia.org/wiki/RDF_Schema)에도 RDFS가 무엇을 제공하고 어떤 종류의 작업을 할 수 있는지 잘 요약되어 있다.

"데이터 통합"과 "부분 스키마로 데이터 변환" 예에서는 추론을 실제로 실행하는 방법에 대해 더 자세히 설명하지만, 저자는 추론을 아주 자연스럽게 언급했다. RDFS 추론의 잠재적 역할과 메커니즘을 다루는 [Living in a materialized world](https://www.bobdc.com/blog/materializing)도 유용할 수 있다.

그리고 [Hidden gems included with Jena’s command line utilities](https://www.bobdc.com/blog/jenagems)에서는 오픈 소스 멀티플랫폼 Apache Jena 도구가 어떻게 RDFS 추론을 수행할 수 있는지 설명한다.


<a name="footnote_1">1</a> 이 페이지는 [What else can I do with RDFS?](https://www.bobdc.com/blog/whatisrdf2/)을 편역한 것임.
