# RDF triple 생성(Creating RDF triples)

## 노드 생성(Creating Nodes)
RDF 데이터는 노드가 URI 참조, 공백 노드 또는 리터럴인 그래프이다. RDFLib에서 [**`URIRef`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.URIRef), [**`BNode`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.BNode)와 [**`Literal`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Literal) 클래스로 이러한 노드 타입을 표시한다. URIRef와 BNode는 모두 사람, 회사, 웹사이트 등과 같은 리소스로 볼 수 있다.

- `BNode`는 정확한 URI를 알 수 없는 노드이다. 일반적으로 다른 노드와 관련해서만 ID가 있는 노드이다.
- `URIRef`는 정확한 URI가 알려진 노드이다. RDF 그래프에서 일부 subject와 predicate를 나타내는 것 외에도 `URIRef`는 항상 속성/술어를 나타내는 데 사용된다.
- `Literal`은 이름, 날짜, 숫자 등과 같은 객체 값을 나타낸다. 가장 일반적인 리터럴 값은 XML 데이터 타입입니다. string, int, ... 그러나 사용자 정의 타입도 선언할 수 있다.

노드 클래스의 생성자는 노드를 생성될 수 있다.

```python
from rdflib import URIRef, BNode, Literal

bob = URIRef("http://example.org/people/Bob")
linda = BNode()  # a GUID is generated

name = Literal("Bob")  # passing a string
age = Literal(24)  # passing a python int
height = Literal(76.5)  # passing a python float
```

Literal을 Python 객체에서 생성할 수 있으며, 이는 `data-typed literals`을 생성한다. 매핑에 대한 자세한 내용은 [Literals](https://rdflib.readthedocs.io/en/stable/rdf_terms.html#rdflibliterals)을 참조한다.

동일한 `namespace`에 많은 `URIRefs`를 생성하기 위해, 즉 동일한 접두사를 갖는 URI를 위해 RDFLib에는 [**`rdflib.namespace.Namespace`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.namespace.html#rdflib.namespace.Namespace) 클래스가 있습니다.

```python
from rdflib import Namespace

n = Namespace("http://example.org/people/")

n.bob  # == rdflib.term.URIRef("http://example.org/people/bob")
n.eve  # == rdflib.term.URIRef("http://example.org/people/eve")
```

이는 모든 속성과 클래스에 동일한 URI 접두사가 있는 스키마에서 매우 유용하다. RDFLib은 대부분의 W3C 스키마를 포함하여 몇 가지 일반적인 RDF/OWL 스키마에 대한 Nmaespace를 정의한다.

```python
from rdflib.namespace import CSVW, DC, DCAT, DCTERMS, DOAP, FOAF, ODRL2, ORG, OWL, \
                           PROF, PROV, RDF, RDFS, SDO, SH, SKOS, SOSA, SSN, TIME, \
                           VOID, XMLNS, XSD

RDF.type
# == rdflib.term.URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")

FOAF.knows
# == rdflib.term.URIRef("http://xmlns.com/foaf/0.1/knows")

PROF.isProfileOf
# == rdflib.term.URIRef("http://www.w3.org/ns/dx/prof/isProfileOf")

SOSA.Sensor
# == rdflib.term.URIRef("http://www.w3.org/ns/sosa/Sensor")
```

## 그래프에 triple 추가(Adding Triples to a graph)
[RDF 로딩과 저장](load-save.md)에서 이미 [**`parse()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.parse) 함수를 사용하여 파일 및 온라인 위치에서 트리플을 추가하는 방법을 보았다.

[**`add()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.add) 함수를 사용하여 Python 코드 내에서 triple을 직접 추가할 수도 있다.

> `Graph.**add**(*triple*)` 

>   - `self`를 컨텍스트로 사용하여 트리플 추가

>   - **Parameters:: triple(Tuple[Node, Node, Node])** -

[**`add()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.add)는 RDFLib 노드의 3-튜플("triple")을 사용한다. 이전에 정의한 노드와 네임스페이스를 사용한다.

```python
from rdflib import Graph, URIRef, Literal, BNode
from rdflib.namespace import FOAF, RDF

g = Graph()
g.bind("foaf", FOAF)

bob = URIRef("http://example.org/people/Bob")
linda = BNode()  # a GUID is generated

name = Literal("Bob")
age = Literal(24)

g.add((bob, RDF.type, FOAF.Person))
g.add((bob, FOAF.name, name))
g.add((bob, FOAF.age, age))
g.add((bob, FOAF.knows, linda))
g.add((linda, RDF.type, FOAF.Person))
g.add((linda, FOAF.name, Literal("Linda")))

print(g.serialize())
```

출력은 다음과 같다.

```
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

<http://example.org/people/Bob> a foaf:Person ;
    foaf:age 24 ;
    foaf:knows [ a foaf:Person ;
            foaf:name "Linda" ] ;
    foaf:name "Bob" .

```

일부 속성의 경우 리소스당 하나의 값만 의미가 있다 (즉, *함수 속성(functional property)*이거나 최대 카디널리티가 1인 경우). [**`set()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.set) 메서드는 다음과 같은 경우에 유용하다.

```python
from rdflib import Graph, URIRef, Literal
from rdflib.namespace import FOAF

g = Graph()
bob = URIRef("http://example.org/people/Bob")

g.add((bob, FOAF.age, Literal(42)))
print(f"Bob is {g.value(bob, FOAF.age)}")
# prints: Bob is 42

g.set((bob, FOAF.age, Literal(43)))  # replaces 42 set above
print(f"Bob is now {g.value(bob, FOAF.age)}")
# prints: Bob is now 43
```

[**`rdflib.graph.Graph.value()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.value)는 일치 쿼리 메서드입니다. 속성에 대한 단일 값을 반환하고 선택적으로 보다 많으면 예외를 발생시킨다.

전체 그래프를 결합하여 triple을 추가할 수도 있다. [RDFLib 그래프에서 집합 연산](navigating-graphs.md#set-operations-graphs)을 참조한다.

## Triple 제거(Removing triples)
마찬가지로, [**`remove()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.remove)를 호출하여 triple을 제거할 수 있다.

> `Graph.**remove**(*triple*)` 

>   - 그래프에서 triple을 제거

>   - triple에 컨텍스트 속성을 제공하지 않으면 모든 컨텍스트에서 triple을 제거한다.

제거할 때 트리플의 일부를 지정하지 않은 상태로 둘 수 있다 (즉, `None` 전달). 그러면 일치하는 모든 triple을 제거한다.

```python
g.remove((bob, None, None))  # remove all triples about bob
```

## 예(An example)
LiveJournal은 사용자를 위해 FOAF 데이터를 생성하지만 사람의 전체 이름에 `foaf:member_name`을 사용하는 것 같지만 `foaf:member_name`은 FOAF의 namespace에 없으며 아마도 `foaf:name`을 사용해야 할 것이다.

일부 LiveJournal 데이터를 검색하려면 모든 `foaf:member_name`에 대해 `foaf:name`을 추가한 다음 `foaf:member_name` 값을 제거하여 데이터가 실제로 다른 FOAF 데이터와 정렬되도록 다음과 같이 할 수 있다.

```python
from rdflib import Graph
from rdflib.namespace import FOAF

g = Graph()
# get the data
g.parse("http://danbri.livejournal.com/data/foaf")

# for every foaf:member_name, add foaf:name and remove foaf:member_name
for s, p, o in g.triples((None, FOAF['member_name'], None)):
    g.add((s, FOAF['name'], o))
    g.remove((s, FOAF['member_name'], o))
```

> **NOTE**

> rdflib 5.0.0부터 FOAF는 닫힌 멤버 집합을 가진 [**`ClosedNamespace()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.namespace.html#rdflib.namespace.ClosedNamespace) 클래스 인스턴스로 선언되었고 `foaf:member_name`은 그 중 하나가 아니기 때문에 RDFlib에서 `foaf:member_name`을 사용하지 않는다! LiveJournal이 RDFlib 5.0.0을 사용했다면 triple이 생성될 때 `foaf:member_name`에 대해 오류가 발생했을 것이다.

## 컨테이너와 컬렉션 생성(Creating Containers & Collections)
컨테이너 또는 컬렉션의 각 triple을 개별적으로 선언하는 대신 사용할 수 있는 RDF 컨테이너와 컬렉션을 위한 두 가지 편의 클래스가 있다.

- [**`Container()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.container.Container)(또한 `Bag`, `Seq` 및 `Alt`)와
- [**`Collection()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.collection.Collection)

방법은 해당 문서를 참조한다.
