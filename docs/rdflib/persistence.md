# 지속성(Persistence)
RDFLib는 RDF 및 Notation 3의 지속성을 위해 [**`추상화된 Store API`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.store.Store)를 제공한다. [**`Graph`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph) 클래스는 가비지 컬렉션, 트랜잭션 관리, 업데이트패턴 일치, 제거, 길이 및 데이터베이스 관리([**`open()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.open) / [**`close()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.close) / [**`destroy()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.destroy))를 포함한 RDF 저장소의 triple 기반 관리를 위해 이 API의 인스턴스(생성자의 첫 번째 인수로)와 함께 작동한다.

다른 저장소에 대해 이 API를 구현하여 추가 지속성 메커니즘을 지원할 수 있다.

## 현재 코어 RDFLib에서 제공하는 저장소(Stores currently shipped with core RDFLib)

- [**`Memory`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.plugins.stores.html#rdflib.plugins.stores.memory.Memory)- 영구적이지 않다!
- [**`BerkeleyDB`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.plugins.stores.html#rdflib.plugins.stores.berkeleydb.BerkeleyDB) - Python의 berkeleydb 패키지를 통한 디스크를 이용한 지속성
- [**`SPARQLStore`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.plugins.stores.html#rdflib.plugins.stores.sparqlstore.SPARQLStore) - 원격 SPARQL 쿼리 엔드포인트 주변의 읽기 전용 래퍼
- [**`SPARQLUpdateStore`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.plugins.stores.html#rdflib.plugins.stores.sparqlstore.SPARQLUpdateStore) - 원격 SPARQL 쿼리/업데이트 엔드포인트 쌍을 둘러싼 읽기-쓰기 래퍼

## 사용법(Usage)
대부분의 경우 저장소 이름을 Graph 생성자에 전달하는 것으로 충분하다.

```python
from rdflib import Graph

graph = Graph(store='BerkeleyDB')
```

디스크로 지속성을 제공하는 대부분의 저장소는 읽거나 쓰기 전에 열어야 한다. ConjuntiveGraph 쿼드 저장소가 아닌 triple 저장소에 저장할 때 그래프를 열 수 있는 식별자를 지정해야 한다.

```python
graph = Graph('BerkeleyDB', identifier='mygraph')

# first time create the store:
graph.open('/home/user/data/myRDFLibStore', create=True)

# work with the graph:
data = """
       PREFIX : <https://example.org/>

       :a :b :c .
       :d :e :f .
       :d :g :h .
       """
graph.parse(data=data, format="ttl")

# when done!
graph.close()
```

완료되면 저장소와 연관된 자원을 해제하기 위해 [**`close()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.close)를 호출해야 한다.

## Additional store plugin
RDFLib 확장 프로젝트에서 더 많은 저장소 구현을 사용할 수 있다.

- [**`rdflib-sqlalchemy`**](https://github.com/RDFLib/rdflib-sqlalchemy) - 다양한 RDBM 백엔드에 저장을 지원 
- [**`rdflib-leveldb`**](https://github.com/RDFLib/rdflib-leveldb) - Google의 [LevelDB](https://code.google.com/p/leveldb/) 키-값 저장소 SW를 사용하는 저장소.
- [**`rdflib-kyotocabinet`**](https://github.com/RDFLib/rdflib-kyotocabinet) - [Kyoto Cabinet](http://fallabs.com/kyotocabinet/) 키-값 저장소 SW를 사용하는 저장소.

## Example

- [**`examples.berkeleydb_example`**](https://rdflib.readthedocs.io/en/stable/apidocs/examples.html#module-examples.berkeleydb_example)에는 BerkeleyDB 저장소를 사용하는 예제가 있다.
- [**`examples.sparqlstore_example`**](https://rdflib.readthedocs.io/en/stable/apidocs/examples.html#module-examples.sparqlstore_example)에 SPARQLSStore를 사용하는 예제가 있다.
