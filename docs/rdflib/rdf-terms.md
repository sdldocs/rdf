# rdflib에서의 RDF 용어(RDF terms in rdflib)
용어(terms)는 RDFLib의 그래프의 triple에 나타날 수 있는 객체의 종류이다. `IRI`, `Blank Node`와 `Literal`이 핵심 RDF 개념의 부분이며, 후자는 리터럴 값과 데이터 타입 또는 [**RFC 3066**](https://datatracker.ietf.org/doc/html/rfc3066.html) 언어 태그로 구성된다.

> **NOTE**

> IRI/URI를 표현하기 위한 RDFLib의 클래스를 "URIRef"라고 합니다. 구현 당시 그것이 당시의 현재 RDF 사양을 URI/IRI라고 불렀기 때문이다. 여기서는 그 클래스 이름을 유지하지만 RDF 객체를 "IRI"로 사용한다.

## 클래스 계층(Class hierarchy)
RDFLib의 모든 용어는 [**`rdflib.term.Identifier`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Identifier) 클래스의 하위 클래스이다. 다양한 용어의 클래스 다이어그램은 다음과 같다.

![Fig. 1](../images/rdflib/kroki-4f49976b7105d910cc1507fd5903a43901542f27.svg)
<center>Fig. 1 Term Class Hierarchy</center> <a id="fig-1"></a>

노드는 기본 저장소에 실제로 저장되어 있는 용어의 하위 집합이다.

이러한 용어 집합은 저장소가 형식 인식 여부에 의존한다. 형식을 인식하지 않는 저장소는 RDF 모델의 핵심 용어만 유지하지만 형식을 인식하는 스토어는 N3 확장도 저장한다. 그러나 용어 패턴으로 노드를 일치시키는 용도로만 사용되는 유틸리티 용어는 노드가 아닌 용어일 것이다.

## Python Classes
주요 세 RDF 객체인 *IRI*, *Blank Node*와 *Literal*을 RDFLib에서 다음 주요 세 Python 클래스로 표현한다.

### URIRedf
IRI(Internationalized Resource Identifier)는 URIRef 클래스를 사용하여 RDFLib 내에서 표현된다. 다음은 [RDF 1.1 사양의 IRI 섹션](https://www.w3.org/TR/rdf11-concepts/#section-IRIs)로부터 *URIRef* 클래스를 자동 빌드한 것이다.

*`class rdflib.term.`* **`URIRef`** *`(value: str, base: Optional[str] = None)`*

[RDF 1.1’s IRI Section](https://www.w3.org/TR/rdf11-concepts/#section-IRIs)

> **NOTE**

> RDFLib 외부의 RDF에 대한 문서에서는 IRI 또는 URI라는 용어를 사용하지만 여기에서는 이 클래스를 URIRef라고 한다. 이는 RDF 사양의 첫 번째 버전이 최신 버전일 때 만들어져 *URIRef*라는 용어를 사용했기 때문이다 ([RDF 1.0 URIRef](http://www.w3.org/TR/rdf-concepts/#section-Graph-URIref) 참조).

RDF 그래프 내의 IRI(Internationalized Resource Identifier)는 RFC 3987에 정의된 구문을 준수하는 유니코드 문자열이다.

RDF 추상 구문의 IRI는 절대적**이어야** 하고 단편 식별자를 포함**할 수** 있다.

IRI는 더 넓은 범위의 유니코드 문자를 허용하는 URI[RFC3986]의 일반화이다.

```python
>>> from rdflib import URIRef
>>> uri = URIRef()  
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: __new__() missing 1 required positional argument: 'value'
>>> uri = URIRef('')
>>> uri
rdflib.term.URIRef('')
>>> uri = URIRef('http://example.com')
>>> uri
rdflib.term.URIRef('http://example.com')
>>> uri.n3()
'<http://example.com>'
```

### BNodes
RDF에서 공백 노드(BNode라고도 함)는 IRI 또는 리터럴이 제공되지 않은 리소스를 나타내는 RDF 그래프의 노드이다. 공백 노드로 표시되는 리소스를 익명 리소스라고도 한다. RDF 표준에 따르면, Notation 3과 같은 일부 구문에서는 공백 노드를 predicate로 사용하는 것이 허용되지만 공백 노드는 트리플의 subject 또는 object으로만 사용할 수 있다. 공백 노드에 노드 ID가 있는 경우 (모든 공백 노드는 RDF 직렬화 과정에서 레이블이 지정되지는 않음), 범위가 RDF 그래프의 특정 직렬화로 제한된다. 즉, 한 그래프의 노드 p1이 다른 그래프에서 p1이라는 노드와 동일한 노드를 나타내지 않는다. 

다음은 *BNode* 클래스의 자동 빌드 문서이다.

*`class rdflib.term.`* **`BNode`** *`(value: ~typing.Optional[str] = None, _sn_gen: ~typing.Callable[[], str] = <function _serial_number_generator.<locals>._generator>, _prefix: str = 'N')`*

[RDF 1.1’s Blank Section](https://www.w3.org/TR/rdf11-concepts/#section-blank-nodes)

공백 노드는 일부 구체적인 RDF 구문 또는 RDF 저장소 구현에서 사용되는 RDF 그래프의 명명되지 않은 노드에 대한 로컬 식별자이다. 항상 파일 또는 RDF 저장소로 로컬 범위가 지정되며 공백 노드는 영구적이거나 포터블한 식별자가 아니다. 공백 노드의 식별자는 RDF 추상 구문의 일부는 아니지만 특정한 구체적인 구문 또는 구현(예: Turtle, JSON-LD)에 전적으로 의존한다.

--

RDF 데이터로부터 적재하는 것처럼 비RDFLib 소스.로부터 다시 생성된다면 RDFLib의 `BNode` 클래스는 그래프의 모든 공백 노드에 대해 고유한 ID를 만들지만 BNode의 ID가 그래프 전체에서 일치하거나 동일한 그래프의 여러 복사본에 대해 일치할 것으로 기대하거나 의존하지 **않아야** 한다. 

```python
>>> from rdflib import BNode
>>> bn = BNode()
>>> bn  
rdflib.term.BNode('AFwALAKU0')
>>> bn.n3()  
'_:AFwALAKU0'
```

### Literals
리터럴은 예를 들어 사람의 이름, 생년월일, 키 등과 같은 RDF의 속성 값이며, `string`, `double`, `dateTime` 등 간단한 데이터 타입을 사용하여 저장된다.  일반적으로 다음과 같다.

```python
name = Literal("Nicholas")  # the name 'Nicholas', as a string

age = Literal(39, datatype=XSD.integer)  # the number 39, as an integer
```

언어 태그가 있는 문자열인 `langString`은 약간 특별한 경우이다. 

```python
name = Literal("Nicholas", lang="en")  # the name 'Nicholas', as an English string
imie = Literal("Mikołaj", lang="pl")  # the Polish version of the name 'Nicholas'
```

리터럴의 데이터 타입 값에 대해 사용자 정의 IRI를 사용하여 표시되는 특수 리터럴 타입, 예를 들어 [GeoSPARQL RDF 표준](https://opengeospatial.github.io/ogc-geosparql/geosparql11/spec.html#_geometry_serializations)에서 다음과 같은 [GeoJSON 기하학 직렬화](https://opengeospatial.github.io/ogc-geosparql/geosparql11/spec.html#_rdfs_datatype_geogeojsonliteral)를 나타내기 위해 사용자 정의 데이터 타입인 `geoJSONLiteral`을 정의하였다.

```python
GEO = Namespace("http://www.opengis.net/ont/geosparql#")

geojson_geometry = Literal(
    '''{"type": "Point", "coordinates": [-83.38,33.95]}''',
    datatype=GEO.geoJSONLiteral)
```

다음은 `Literal` 클래스의 자동 빌드 문서이며, [RDF 1.1 사양 'Literals' 섹션]()은 Literal에 대한 노드가 이어서 진다.

*`class rdflib.term.`* **`Literal`** *`(lexical_or_value: Any, lang: Optional[str] = None, datatype: Optional[str] = None, normalize: Optional[bool] = None)`*

[RDF 1.1’s Literal Section](http://www.w3.org/TR/rdf-concepts/#section-Graph-Literal)

리터럴은 문자열, 숫자 및 날짜와 같은 값에 사용된다.

RDF 그래프의 리터럴은 두 개 또는 세 개의 요소로 구성된다.

- 정규 형식 C이어야 하는 유니코드 문자열인 어휘 형식
- 데이터 타입 IRI, 어휘 형식이 리터럴 값에 매핑되는 방법을 표시하는 데이터 타입을 식별하는 IRI
- 데이터 타입 IRI가 `http://www.w3.org/1999/02/22-rdf-syntax-ns#langString`인 경우에는 언어 태그가 지정되어야 한다. 언어 태그는 [언어 식별용 태그](http://tools.ietf.org/html/bcp47) 섹션 2.2.9에 따라 잘 구성**되어야** 한다.

세 번째 요소가 있는 경우 리터럴은 언어 태그가 지정된 문자열이다. 언어 태그의 어휘 표현은 소문자로 변환될 **수** 있다. 언어 태그의 값 범위은 항상 소문자이다.

--

유효한 XSD 데이터 타입의 경우 어휘 형식은 구성 시 선택적으로 정규화된다. 기본 동작은 `rdflib.NORMALIZE_LITERALS`에 의해 설정되며, `__new__`에 대한 `normalize` 매개변수로 재정의될 수 있다.`

리터럴의 동치과 해싱은 다음과 같은 어휘 형식을 기반으로 수행된다.

```
>>> from rdflib.namespace import XSD
```

```
>>> Literal('01') != Literal('1')  # clear - strings differ
True
```

그러나 데이터 타입을 사용하면 정규화된다.

```
>>> Literal('01', datatype=XSD.integer) != Literal('1', datatype=XSD.integer)
False
```

비활성화되지 않은 경우

```
>>> Literal('01', datatype=XSD.integer, normalize=False) != Literal('1', datatype=XSD.integer)
True
```

값 기반으로 비교할 수 있다.

```
>>> Literal('01', datatype=XSD.integer).eq(Literal('1', datatype=XSD.float))
True
```

`eq` 메서드는 기본 파이썬 타입에 대한 제한된 지원도 제공한다.

```
>>> Literal(1).eq(1) # fine - int compatible with xsd:integer
True
>>> Literal('a').eq('b') # fine - str compatible with plain-lit
False
>>> Literal('a', datatype=XSD.string).eq('a') # fine - str compatible with xsd:string
True
>>> Literal('a').eq(1) # not fine, int incompatible with plain-lit
NotImplemented
```

`Greater-than/less-than 순서 비교는 호환 가능한 데이터 타입이 사용되는 경우 값 공간에서도 수행된다. 호환되지 않는 데이터 타입은 DT 또는 lang-tag로 정렬된다. 다른 노드의 경우 순서는 None < BNode < URIRef < Literal 이다.

비 rdflib 노드와의 모든 비교는 PY3에서 "NotImplemented" 오류이다.

```
>>> from rdflib import Literal, XSD
>>> lit2006 = Literal('2006-01-01',datatype=XSD.date)
>>> lit2006.toPython()
datetime.date(2006, 1, 1)
>>> lit2006 < Literal('2007-01-01',datatype=XSD.date)
True
>>> Literal(datetime.utcnow()).datatype
rdflib.term.URIRef(u'http://www.w3.org/2001/XMLSchema#dateTime')
>>> Literal(1) > Literal(2) # by value
False
>>> Literal(1) > Literal(2.0) # by value
False
>>> Literal('1') > Literal(1) # by DT
True
>>> Literal('1') < Literal('1') # by lexical form
False
>>> Literal('a', lang='en') > Literal('a', lang='fr') # by lang-tag
False
>>> Literal(1) > URIRef('foo') # by node-type
True
```

`> <` 연산자는 `NotImplemented`이 아니고, `TypeError (py3k)` 예외를 발생시킨다.

```
>>> Literal(1).__gt__(2.0)
NotImplemented
```

RDF 그래프의 리터럴은 하나 또는 두 개의 명명된 구성 요소를 포함한다.

모든 리터럴은 정규 형식(Noraml Form) C이어야 하는 유니코드 문자열인 어휘 형식을 갖는다.

일반 리터럴에는 사전 형식이 있으며 소문자로 정규화된 [**RFC 3066**](https://datatracker.ietf.org/doc/html/rfc3066.html)에 정의된 언어 태그가 선택적으로 있다. 잘못된 언어 태그가 **₩rdflib.term.Literal.__init__()₩**에 전달되면 예외가 발생한다.

타입이 정의된 리터럴은 어휘 형식과 RDF URI 참조인 데이터 타입 URI를 갖는다.

> **Note**:

> 언어 태그를 사용하지 않는 경우에는 로케일을 사용하지 않는다. 언어 태그는 인간 언어 텍스트와 관련이 있다. 프레젠테이션은 최종 사용자 응용 프로그램에서 사용할 수 있다.

> **Note**:

> 언어 태그의 대소문자 정규화는 추상 구문에 대한 설명의 일부이며 결과적으로 RDF 어플리케이션에서의 추상 동작이다. 사례를 정규화하기 위해 실제로 RDF 구현을 제한하지 않는다. 결정적으로 두 언어 태그를 비교한 결과는 원래 입력의 대소문자에 민감하지 않아야 한다. – [RDF 개념 및 추상 구문](http://www.w3.org/TR/rdf-concepts/#section-Graph-URIref)

#### Common XSD datatypes
*string* 또는 *integer*와 같은 대부분의 간단한 리터럴을 XML 스키마(XSD) 데이터 타입으로 정의하고 있다. 아래 그림을 참조한다. 또한 이러한 XSD 데이터 타입은 RDFLib와 함께 제공되는 [**₩XSD Namespace class₩**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.XSD)에 나열되어 있으므로 많은 Python 코드 편집기에서 이를 사용할 때 자동 완성 기능을 사용할 수 있다.

XSD 데이터 타입을 사용할 필요가 *없으며* 위에서 설명한 대로 GeoSPARQL과 같이 고유한 데이터 타입을 항상 정의할 수 있음을 기억하자.

![Fig. 2](../images/rdflib/datatype_hierarchy.png)

#### Python conversions
RDFLib 리터럴은 기본적으로 XML 스키마 데이터 타입 또는 언어 속성이 있는 유니코드 문자처럼 작동한다.

이 클래스는 Python 리터럴(과 time/date/datetime과 같이 내장된)을 동등한 RDF 리터럴로 변환하고 (역으로) 리터럴을 해당하는 Python 리터럴로 변환하는 메커니즘을 제공한다. Python 리터럴과의 이 매핑은 다음과 같다.

| **XML Datatype** | **Python type** |
|------------------|-----------------|
| None | None | 
| xsd:time | time | 
| xsd:date | date |
| xsd:dateTime | datetime |
| xsd:string | None |
| xsd:normalizedString | None |
| xsd:token | None |
| xsd:language | None |
| xsd:boolean | boolean |
| xsd:decimal | Decimal |
| xsd:integer | long |
| xsd:nonPositiveInteger | int |
| xsd:long | long | |
| xsd:nonNegativeInteger | int |
| xsd:negativeInteger | int |
| xsd:int | long |
| xsd:unsignedLong | long |
| xsd:positiveInteger | int |
| xsd:short | int |
| xsd:unsignedInt | long |
| xsd:byte | int |
| xsd:unsignedShort | int |
| xsd:unsignedByte | int |
| xsd:float | float |
| xsd:double | float |
| xsd:base64Binary | base64 |
| xsd:anyURI | None |
| rdf:XMLLiteral | **xml.dom.minidom.Document** |
| rdf:HTML | **xml.dom.minidom.DocumentFragment** |

적절한 데이터 타입과 어휘 표현은 다음을 사용하여 찾을 수 있다.

*`rdflib.term.`* **`_castPythonToLiteral`** *`(obj, datatype)`*

파이썬 타입의 튜플과 특수 데이터 타입 URI를 어휘 값과 데이터 타입 URI(또는 None)의 튜플로 캐스팅한다.

**Parameters::**

- **obj**`(`[**`Any`**](https://docs.python.org/3.7/library/typing.html#typing.Any)`)` –
- **datatype**`(`[**`Optional`**](https://docs.python.org/3.7/library/typing.html#typing.Optional)`[`[**`str`**](https://docs.python.org/3.7/library/stdtypes.html#str)`])` –

**Return type::**

- [`**Tuple**`]()`[`[**`Any`**](https://docs.python.org/3.7/library/typing.html#typing.Any)`,` [**`Optional`**](https://docs.python.org/3.7/library/typing.html#typing.Optional)`[`[**`str`**](https://docs.python.org/3.7/library/stdtypes.html#str)`]]`

그리고 역으로

*`rdflib.term.`* **`_castLexicalToPython`** *`(lexical, datatype)`*

주어진 데이터 타입 :rtype: Any :returns: 값 공간의 어휘 형식을 파이썬 객체러 값 또는 `None`으로 매핑한다.  

**Parameters::**

- **lexical**`(`[**`Union`**](https://docs.python.org/3.7/library/typing.html#typing.Union)`[`[**`str`**](https://docs.python.org/3.7/library/stdtypes.html#str)`,` [**`bytes`**](https://docs.python.org/3.7/library/stdtypes.html#bytes)`])` –
- **datatype**`(`[**`Optional`**](https://docs.python.org/3.7/library/typing.html#typing.Optional)`[`[**`str`**](https://docs.python.org/3.7/library/stdtypes.html#str)`])` –

이 모든 것은 Python 객체를 생성자에 전달하여 `Literal` 객체를 생성할 때 자동으로 일어나므로 수동으로 수행할 필요없다.

[**`rdflib.term.bind()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.bind)를 사용하여 사용자 지정 데이터 타입을 추가할 수 있다. [**`examples.custom_datatype`**](https://rdflib.readthedocs.io/en/stable/apidocs/examples.html#module-examples.custom_datatype)도 참조한다.
