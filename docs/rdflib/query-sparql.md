# SPARQL로 쿼리(Querying with SPARQL)

## 쿼리 수행(Run a Query)
RDFLib는 SPARQL 1.1 쿼리와 SPARQL 1.1 업데이트 쿼리 언어의 구현을 함께 제공한다.

그래프에 대해 [**`rdflib.graph.Graph.query()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.query) 메서드를 사용하여 쿼리를 수행하고 [**`rdflib.graph.Graph.update()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.update)로 그래프를 갱신할 수 있다.

쿼리 메서드는 [**`rdflib.query.Result`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.query.Result) 인스턴스를 반환한다. SELECT 쿼리의 경우 이를 반복하면 각각 변수의 바인딩 집합을 포함하는 **`rdflib.query.ResultRow`** 인스턴스가 반환된다. CONSTRUCT/DESCRIBE 쿼리의 경우 결과 개체를 반복하여 triple을 제공한다. ASK 쿼리의 경우 반복이 단일 부울 응답을 생성하거나 부울 컨텍스트(예: `bool(result)`)에서 결과 개체를 수행한다.

예를 들어

```python
import rdflib
g = rdflib.Graph()
g.parse("http://danbri.org/foaf.rdf#")

knows_query = """
SELECT DISTINCT ?aname ?bname
WHERE {
    ?a foaf:knows ?b .
    ?a foaf:name ?aname .
    ?b foaf:name ?bname .
}"""

qres = g.query(knows_query)
for row in qres:
    print(f"{row.aname} knows {row.bname}")
```

결과는 `SELECT` 인수와 동일한 순서인 튜플 값이다. 또는 값은 애트리뷰트나 항목을 변수 이름으로 액세스할 수 있다. `row.b`와 `row["b"]`는 동일하다. 위의 적절한 데이터가 주어지면 다음과 같이 풀력된다.

```
Timothy Berners-Lee knows Edd Dumbill
Timothy Berners-Lee knows Jennifer Golbeck
Timothy Berners-Lee knows Nicholas Gibbins
...

```

`SPARQL PREFIX`를 사용하는 대신 `initNs` kwarg를 사용하여 네임스페이스 바인딩을 전달할 수 있다 ([Namespaces and Bindings](https://rdflib.readthedocs.io/en/stable/namespaces_and_bindings.html) 참조).

변수는 초기 바인딩 `dict`을 전달할 수 있는 `initBindings` kwarg를 사용하여 미리 바인딩할 수도 있다. 이는 아래에 설명할 [준비된 쿼리](#prepared-queris)에서 특히 유용하다.

## 갱신 쿼리(Update Queries)
갱신 쿼리는 검색 쿼리와 동일하지만 [**`rdflib.graph.Graph.update()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.update) 메서드를 사용하여 수행됩니다. 예:

```python
from rdflib import Graph

# Create a Graph, add in some test data
g = Graph()
g.parse(
    data="""
        <x:> a <c:> .
        <y:> a <c:> .
    """,
    format="turtle"
)

# Select all the things (s) that are of type (rdf:type) c:
qres = g.query("""SELECT ?s WHERE { ?s a <c:> }""")

for row in qres:
    print(f"{row.s}")
# prints:
# x:
# y:

# Add in a new triple using SPATQL UPDATE
g.update("""INSERT DATA { <z:> a <c:> }""")

# Select all the things (s) that are of type (rdf:type) c:
qres = g.query("""SELECT ?s WHERE { ?s a <c:> }""")

print("After update:")
for row in qres:
    print(f"{row.s}")
# prints:
# x:
# y:
# z:

# Change type of <y:> from <c:> to <d:>
g.update("""
         DELETE { <y:> a <c:> }
         INSERT { <y:> a <d:> }
         WHERE { <y:> a <c:> }
         """)
print("After second update:")
qres = g.query("""SELECT ?s ?o WHERE { ?s a ?o }""")
for row in qres:
    print(f"{row.s} a {row.o}")
# prints:
# x: a c:
# z: a c:
# y: a d:
```

## 원격 서비스 쿼리(Querying a Remote Service)
SPARQL 1.1의 `SERVICE` 키워드는 원격 SPARQL 엔드포인트(endpoint)에 쿼리를 보낼 수 있다.

```python
import rdflib

g = rdflib.Graph()
qres = g.query(
    """
    SELECT ?s
    WHERE {
      SERVICE <http://dbpedia.org/sparql> {
        ?s a ?o .
      }
    }
    LIMIT 3
    """
)

for row in qres:
    print(row.s)
```

이 예에서 [DBPedia](https://dbpedia.org/)의 SPARQL 엔드포인트 서비스에 쿼리를 보내 쿼리를 실행한 다음 그 결과를 얻을 수 있다.

```
<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.openlinksw.com/schemas/virtcxml#FacetCategoryPattern>
<http://www.w3.org/2001/XMLSchema#anyURI> <http://www.w3.org/2000/01/rdf-schema#Datatype>
<http://www.w3.org/2001/XMLSchema#anyURI> <http://www.w3.org/2000/01/rdf-schema#Datatype>
```

## 준비된 쿼리(Prepared Queries) <a id="prepared-queries"></A>
RDFLib를 사용하면 실행 전에 쿼리를 *준비*할 수 있으므로 매번 쿼리를 다시 구문 분석하여 SPARQL 대수학으로 변환할 필요가 없다.

**`rdflib.plugins.sparql.prepareQuery()`** 메서드는 쿼리를 문자열로 받아 [**`rdflib.plugins.sparql.sparql.Query`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.plugins.sparql.html#rdflib.plugins.sparql.sparql.Query) 객체를 반환한다. 그런 다음 [**`rdflib.graph.Graph.query()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.query) 메서드에 전달할 수 있다.

`initBindings` kwarg를 초기 바인딩 `dict`으로 전달하는 데 사용할 수 있다.

```python
q = prepareQuery(
    "SELECT ?s WHERE { ?person foaf:knows ?s .}",
    initNs = { "foaf": FOAF }
)

g = rdflib.Graph()
g.parse("foaf.rdf")

tim = rdflib.URIRef("http://www.w3.org/People/Berners-Lee/card#i")

for row in g.query(q, initBindings={'person': tim}):
    print(row)
```

## 맞춤형 평가 함수(Custom Evaluation Functions)
전문가의 경우 SPARQL 대수 연산자를 평가하는 방식을 재정의할 수 있다. setuptools 엔트리포인트(entry-point) `rdf.plugins.sparqleval`을 사용하거나 단순히 [**`rdflib.plugins.sparql.CUSTOM_EVALS`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.plugins.sparql.html#rdflib.plugins.sparql.CUSTOM_EVALS)에 항목을 추가하여 사용자 정의 함수를 등록할 수 있다. 함수는 각 대수 구성 요소를 호출하며, 기본 구현이 이 부분을 처리하여야 함을 나타내기 위해 `NotImplementedError`를 발생시킬 수 있다.

[examples/custom_eval.py](examples.md/#custom_eval) 참조