# 그래프 병합(Merging graphs)

    문서 또는 다른 RDF 그래프 간에 공백 노드 공유를 명시적으로 제공하는 기타 구조(예: RDF 데이터 세트)가 표현하는 그래프에서 파생된 경우에만 그래프가 공백 노드를 공유한다. 단순히 웹 문서를 다운로드한다고 해서 결과 RDF 그래프의 공백 노드가 동일한 문서의 다른 다운로드 또는 동일한 RDF 소스로부터 공백 노드와 동일하다는 의미는 아니다.

공백 노드 식별자를 사용하는 RDF의 구체적인 구문을 조작하는 RDF 어플리케이션은 식별하는 공백 노드의 identity를 추적하도록 주의해야 한다. 공백 노드 식별자는 종종 로컬 범위를 가지므로 다른 소스의 RDF와 결합될 때 고유한 공백 노드의 우발적인 병합을 피하기 위해 식별자를 변경해야 할 수 있다.

> 예를 들어, 두 문서 모두 공백 노드 식별자 "\_:x"를 사용하여 공백 노드를 식별할 수 있지만 이러한 문서가 공유 식별자 범위에 있지 않거나 공통 소스에서 파생되지 않았다면, 문서에서 "\_:x"를 다른 문서에서 설명하는 그래프의 노드와 다른 공백 노드로 식별한다. 여러 소스의 RDF를 결합하여 그래프를 구성하는 경우 공백 노드 식별자를 다른 문서에 없는 다른 것으로 대체하여 표준화하는 것이 필요할 수 있다.(?)

> (*copied directly from https://www.w3.org/TR/rdf11-mt/#shared-blank-nodes-unions-and-merges*)

RDFLib에서는 파싱할 때 공백 노드에 고유한 ID가 부여되므로 여러 파일을 동일한 그래프로 읽어 들이면 그래프 병합을 수행할 수 있다.

```python
from rdflib import Graph

graph = Graph()

graph.parse(input1)
graph.parse(input2)
```

`graph`는 이제 `input1`과 `input2`의 병합된 그래프를 포함한다.

> **NOTE**

> 그러나 RDFLib에서 집합 그래프 연산은 더 큰 데이터베이스(예: [**`ConjunctiveGraph`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.ConjunctiveGraph)의 컨텍스트에서)의 하위 그래프에서 수행되고 공유된 공백 노드 ID를 가정하므로 병합을 정확히 수행하지 않는다. 

```python
from rdflib import Graph

g1 = Graph()
g1.parse(input1)

g2 = Graph()
g2.parse(input2)

graph = g1 + g2
```
> `graph`에서 공백 노드의 원치 않는 충돌이 발생할 수 있다.
