# 유틸리티와 편이 함수(Utilities & convenience functions)
RDF 프로그래밍에서 RDFLib와 Python이 가장 빠른 도구는 아닐 수 있지만 사용하기 가장 쉽고 편리하여 전반적으로 *가장 빠른* 도구로 만들기 위해 열심히 노력하고 있다!

여기에 번거로움 없는 RDF 코딩을 위한 힌트와 포인터를 소개한다.

## 함수 속성(Functional properties)
[**`value()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.value)와 [**`set()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.set)을 사용하여 *함수 속성* 인스턴스, 즉 리소스에 대해 한 번만 발생할 수 있는 속성으로 수행한다.

```python
from rdflib import Graph, URIRef, Literal, BNode
from rdflib.namespace import FOAF, RDF

g = Graph()
g.bind("foaf", FOAF)

# Add demo data
bob = URIRef("http://example.org/people/Bob")
g.add((bob, RDF.type, FOAF.Person))
g.add((bob, FOAF.name, Literal("Bob")))
g.add((bob, FOAF.age, Literal(38)))

# To get a single value, use 'value'
print(g.value(bob, FOAF.age))
# prints: 38

# To change a single of value, use 'set'
g.set((bob, FOAF.age, Literal(39)))
print(g.value(bob, FOAF.age))
# prints: 39
```

## 그래프 슬라이싱(Slicing graphs)
Python은 `start`, `stop`과 `step-size` triple을 사용하는 `slice` 객체를 사용하여 배열을 슬라이싱할 수 있다.

```python
for i in range(20)[2:9:3]:
    print(i)
# prints:
# 2, 5, 8
```

RDFLib 그래프는 `__getitem__`을 재정의하고 대신 슬라이스 triple을 RDF triple로 변환한다. 이를 통해[**`triples()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.triples), [**`subject_predicates()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.subject_predicates), **`contains()`** 및 기타 그래프 쿼리 메서드 등 슬라이스 구문을 단축키로 사용할 수 있다.

```python
from rdflib import Graph, URIRef, Literal, BNode
from rdflib.namespace import FOAF, RDF

g = Graph()
g.bind("foaf", FOAF)

# Add demo data
bob = URIRef("http://example.org/people/Bob")
bill = URIRef("http://example.org/people/Bill")
g.add((bob, RDF.type, FOAF.Person))
g.add((bob, FOAF.name, Literal("Bob")))
g.add((bob, FOAF.age, Literal(38)))
g.add((bob, FOAF.knows, bill))

print(g[:])
# same as
print(iter(g))

print(g[bob])
# same as
print(g.predicate_objects(bob))

print(g[bob: FOAF.knows])
# same as
print(g.objects(bob, FOAF.knows))

print(g[bob: FOAF.knows: bill])
# same as
print((bob, FOAF.knows, bill) in g)

print(g[:FOAF.knows])
# same as
print(g.subject_objects(FOAF.knows))
```

 완전한 예는 [**`examples.slice`**](https://rdflib.readthedocs.io/en/stable/apidocs/examples.html#module-examples.slice)를 참조한다.

> **NOTE**

> 슬라이싱은 Python `REPL`에서 작업하기 위한 1회성 실행 스크립트로는 편리하지만 슬라이싱은 바인딩된 슬라이스 부분에 따라 다양한 길이의 튜플을 반환하므로 더 복잡한 프로그램에서 사용하는 데 주의해야 한다. 변수를 전달하고 None 또는 False이면 갑자기 예상했던 것과 다른 길이의 튜플 생성기를 다시 얻을 수도 있다.

## SPARQL 경로(SPARQL Paths)
SPARQL 속성 경로를 URIRef에 재정의하여 연산자를 사용할 수 있다. [**`examples.foafpaths`**](https://rdflib.readthedocs.io/en/stable/apidocs/examples.html#module-examples.foafpaths)와 [**`rdflib.paths`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#module-rdflib.paths)를 참조.

## 단일 용어를 N3 구문으로 직렬화(Serializing a single term to N3)
간단한 출력 또는 간단한 직렬화할 때, 용어의 읽기 쉬운 표현이 필요한 경우가 종종 있다. 모든 용어(URIRef, Literal 등)를 적절한 N3 형식으로 반환하는 `n3` 메서드를 지원한다.

```python
from rdflib import Graph, URIRef, Literal
from rdflib.namespace import FOAF

# A URIRef
person = URIRef("http://xmlns.com/foaf/0.1/Person")
print(person.n3())
# prints: <http://xmlns.com/foaf/0.1/Person>

# Simplifying the output with a namespace prefix:
g = Graph()
g.bind("foaf", FOAF)

print(person.n3(g.namespace_manager))
# prints foaf:Person

# A typed literal
l = Literal(2)
print(l.n3())
# prints "2"^^<http://www.w3.org/2001/XMLSchema#integer>

# Simplifying the output with a namespace prefix
# XSD is built in, so no need to bind() it!
l.n3(g.namespace_manager)
# prints: "2"^^xsd:integer
```

## Parsing data from a string
`data` 매개변수를 사용하여 문자열에서 데이터를 구문 분석할 수 있다.

```python
from rdflib import Graph

g = Graph().parse(data="<a:> <p:> <p:>.")
for r in g.triples((None, None, None)):
    print(r)
# prints: (rdflib.term.URIRef('a:'), rdflib.term.URIRef('p:'), rdflib.term.URIRef('p:'))
```

## 명령어 도구(Command Line tools)
RDFLib에는 몇 가지 명령어 도구가 포함되어 있다 ([**1rdflib.tools1**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.tools.html#module-rdflib.tools) 참조).