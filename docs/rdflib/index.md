# rdflib 6.2.0
> **NOTE**

> [rdflin 6.2.0](https://rdflib.readthedocs.io/en/stable/index.html)을 편역한 것임

RDFLib은 RDF 작업을 위한 순수한 Python 패키지이며 다음을 포함하고 있다.

- **Parsers & Serializers**
    - RDF/XML, N3, NTriples, N-Quads, Turtle, TriX, JSON-LD, HexTuples, RDFa 및 Microdata
- **Store implementations**
    - 메모리 스토어
    - BerkeleyDB와 같은 데이터베이스를 사용하는 지속성 있는 디스크 스토어
    - 원격 SPARQL endpoints
- **그래프 인터페이스**
    - 하나의 단일 그래프
    - 한 데이터세트내의 다중 Named 그래프
- **SPARQL 1.1 구현**
    - Queries와 Updates 모두를 지원

## 시작하기
RDFLib를 처음 사용한다면 다음에서부터 시작하는 데 도움이 될 것이다.

- [RDFLib를 시작하기](getting-started.md)
- [RDF 로딩과 저장](load-save.md)
- [RDF triple 생성](create-rdf.md)
- [Graph 탐색](navigating-graphs.md)
- [SPARQL로 쿼리](query-sparql.md)
- [유틸리티와 편이 함수](util-function.md)
- [예](exmaples.md)

## In depth
RDF에 익숙하고 RDFLib가 RDF를 처리하는 방법에 대한 세부 정보가 필요하면 다음이 적합할 것이다.

- [rdflib에서의 RDF 용어(RDF terms in rdflib)](rdf-terms.md)
- [내임스페이스와 바인딩](namespaces-bindings.md)
- [지속성](persistence.md)
- [그래프 병합](merging-graph.md)
- [Upgrading 5.0.0 to 6.0.0](https://rdflib.readthedocs.io/en/stable/upgrade5to6.html)
- [Upgrading 4.2.2 to 5.0.0](https://rdflib.readthedocs.io/en/stable/upgrade4to5.html)

## Reference
모든 것의 핵심 세부 사항.

- [rdflib](https://rdflib.readthedocs.io/en/stable/apidocs/modules.html)
- [Plugins](https://rdflib.readthedocs.io/en/stable/plugins.html)
    - [Plugin parsers](https://rdflib.readthedocs.io/en/stable/plugin_parsers.html)
    - [Plugin serializers](https://rdflib.readthedocs.io/en/stable/plugin_serializers.html)
    - [Plugin stores](https://rdflib.readthedocs.io/en/stable/plugin_stores.html)
    - [Plugin query results](https://rdflib.readthedocs.io/en/stable/plugin_query_results.html)

## For developers

> **NOTE**

> 이 부분은 지금 상황에서 불필요하여 편역에서 일단 제외하였다.

- [RDFLib developers guide](https://rdflib.readthedocs.io/en/stable/index.html#:~:text=RDFLib%20developers%20guide)
- [Contributor Covenant Code of Conduct](https://rdflib.readthedocs.io/en/stable/CODE_OF_CONDUCT.html)
- [Writing RDFLib Documentation](https://rdflib.readthedocs.io/en/stable/docs.html)
- [Persisting Notation 3 Terms](https://rdflib.readthedocs.io/en/stable/persisting_n3_terms.html)
- [Type Hints](https://rdflib.readthedocs.io/en/stable/type_hints.html)

## Source Code
GitHub `https://github.com/RDFLib/rdflib`에서 rdflib 소스 코드를 호스팅하며 여기에서 이 커뮤니티 프로젝트를 개선하는 데 도움이 되도록 issue를 제출하고 풀 요청을 생성할 수 있다.

GitHub의 RDFlib 조직(https://github.com/RDFLib)은 이 패키지와 유용하다고 생각할 수 있는 기타 여러 RDF 및 RDFlib 관련 패키지를 유지 관리한다.

## Further help & Contact
rdflib를 개발하는 것보다 사용하는 데 더 많은 도움이 필요하면 `[rdflib]` 태그를 사용하여 StackOverflow에 질문을 게시한다. 기존 `[rdflib]` 태그가 지정된 질문 목록은 [https://stackoverflow.com/questions/tagged/rdflib](https://stackoverflow.com/questions/tagged/rdflib)에 보관되어 있다.

rdflib의 개발자 메일링 리스트에 가입하고 싶다면 [https://groups.google.com/group/rdflib-dev](https://groups.google.com/group/rdflib-dev)에 등록하시오.

[gitter](https://gitter.im/RDFLib/rdflib) 또는 [matrix](https://matrix.to/#/#RDFLib_rdflib:gitter.im)를 통해 채팅할 수 있다.
