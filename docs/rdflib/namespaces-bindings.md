# 내임스페이스와 바인딩(Namespaces and Bindings)
동일 네임스페이스에서 다수 URI를 작업할 수 있도록 RDFLib는 여러 축약(short-cut)을 제공한다.

[**`rdflib.namespace`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.namespace.html#module-rdflib.namespace)는 [**`rdflib.namespace.Namespace`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.namespace.html#rdflib.namespace.Namespace) 클래스를 정의하여 네임스페이스에 URI를 쉽게 생성할 수 있도록 한다.

```python
from rdflib import Namespace

EX = Namespace("http://example.org/")
EX.Person  # a Python attribute for EX. This example is equivalent to rdflib.term.URIRef("http://example.org/Person")

# use dict notation for things that are not valid Python identifiers, e.g.:
EX['first%20name']  # as rdflib.term.URIRef("http://example.org/first%20name")
```

이 두 가지 스타일의 네임스페이스 생성(객체 속성과 사전)은 동일하며 유효한 RDF 네임스페이스와 유효한 Python 식별자가 아닌 URI를 허용하기 위해서만 사용할 수 있다. 이것은 위의 `first%20name`의 예와 같이 공백과 같은 구문적인 것뿐만 아니라 `class` 또는 `while`과 같은 Python 예약어에 대한 것이므로 URI `http://example.org/class`에 대해 `EX.class`가 아닌 `EX["class"]`를 생성하시오. 

## 공통 네임스페이스(Common Namespaces)
`namespace` 모듈은 RDF, RDFS, OWL, FOAF, SKOS, PROF 등과 같은 많은 공통 네임스페이스를 정의한다. 제공된 네임스페이스 목록은 RDFLib에 대한 사용자 기여로 커지고 있다.

이러한 네임스페이스와 사용자가 정의하는 다른 모든 네임스페이스는 [**`rdflib.namespace.NamespaceManager`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.namespace.html#rdflib.namespace.NamespaceManager)를 사용하여 접두사와 연결할 수도 있다. `http://xmlns.com/foaf/0.1/`에 대해 `foaf`를 사용한다.

각 RDFLib 그래프에는 접두사 매핑을 위한 네임스페이스 목록을 유지하는 [**`namespace_manager`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.namespace_manager)가 있다. 네임스페이스 관리자는 RDF에서 읽을 때 채워지며 이러한 접두사는 RDF를 직렬화하거나 SPARQL 쿼리를 구문 분석할 때 사용된다. 접두사는 **`rdflib.graph.bind()`** 메서드로 바인딩할 수 있다.

```python
from rdflib import Graph, Namespace
from rdflib.namespace import FOAF

EX = Namespace("http://example.org/")

g = Graph()
g.bind("foaf", FOAF)  # bind an RDFLib-provided namespace to a prefix
g.bind("ex", EX)      # bind a user-declared namespace to a prefix
```

실제로 [**`rdflib.namespace.NamespaceManager`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.namespace.html#rdflib.namespace.NamespaceManager) 클래스가 **`rdflib.graph.bind()`** 메서드를 제한니다. 다음을 참조하여라.

## NamespaceManager
각 RDFLib 그래프는 **`namespace_manager`** 필드에 [**`rdflib.namespace.NamespaceManager`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.namespace.html#rdflib.namespace.NamespaceManager) 인스턴스와 함께 제공된다. 위와 같이 이 인스턴스의 **`bind`** 메서드를 사용하여 접두사를 네임스페이스 URI에 바인딩할 수 있지만 **`NamespaceManager`**는 선택한 전략에 따라 일부 바인딩을 자동으로 수행한다.

네임스페이스 바인딩을 **`NamespaceManager`** 인스턴스의 **`bind_namespaces`** 입력 매개변수로 표시하여 `Graph`를 통해 설정할 수도 있다.

```python
from rdflib import Graph
from rdflib.namespace import NamespaceManager
import pprint

g = Graph(bind_namespaces="rdflib")  # bind via Graph
namespace_manager = NamespaceManager(g)
g.namespace_manager = namespace_manager
all_ns = [n for n in g.namespace_manager.namespaces()]
pprint.pprint(all_ns)

g2 = Graph()
nm = NamespaceManager(g2, bind_namespaces="rdflib")  # bind via NamespaceManager
namespace_manager = NamespaceManager(g2)
g2.namespace_manager = namespace_manager
all_ns = [n for n in g2.namespace_manager.namespaces()]
pprint.pprint(all_ns)
```

유효한 전략은 다음과 같다.

- core: 
    - 여러 핵심 RDF 접두사만 바인딩
    - NAMESPACE_PREFIXES_CORE 개체로 부터 owl, rdf, rdfs, xsd, xml
    - 기본이다
- rdflib:
    - DefinedNamespace 인스턴스로 RDFLib와 함께 제공된 모든 네임스페이스를 바인딩한다.
    - 모든 핵심 네임스페이스와 brick, csvw, dc, dcat, dcmitype, cdterms, dcam, doap, foaf, 지역, odrl, org, prof, prov, qb, sdo, sh, skos, sosa, ssn, 시간, vann, void
    - 최신 목록은 [**`rdflib.namespace`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.namespace.html#module-rdflib.namespace)의 NAMESPACE_PREFIXES_RDFLIB 객체를 참조하십시오.
- none:
    - 접두사에 네임스페이스를 바인딩하지 않는다,
    - 이는 기본 동작이 **아니다**.
- cc:
    - 온라인 접두사 데이터베이스인 `prefix.cc`에서 접두사 바인딩을 사용
    - 아직 구현되지 않았고 바라는 것이다.

### 재바이딩(Rebinding)
사용된 전략에 관계없이 네임스페이스의 접두사를 사용자가 선호하는 접두사로 덮어쓸 수 있다. 예를 들면 다음과 같다.

```python
from rdflib import Graph
from rdflib.namespace import GEO  # imports GeoSPARQL's namespace
import pprint

g = Graph(bind_namespaces="rdflib")  # binds GeoSPARQL's namespace to prefix 'geo'
namespace_manager = NamespaceManager(g)
g.namespace_manager = namespace_manager
all_ns = [n for n in g.namespace_manager.namespaces()]
pprint.pprint(all_ns)

g.bind('geosp', GEO, override=True)
namespace_manager = NamespaceManager(g)
g.namespace_manager = namespace_manager
all_ns = [n for n in g.namespace_manager.namespaces()]
pprint.pprint(all_ns)
```

**`NamespaceManager`**에는 주어진 URL을 정규화하는 방법도 있다.

```python
from rdflib import Graph
from rdflib.namespace import NamespaceManager

import pprint

g = Graph()
namespace_manager = NamespaceManager(g)
g.namespace_manager = namespace_manager
all_ns = [n for n in g.namespace_manager.namespaces()]
pprint.pprint(all_ns)

nm.normalizeUri('owl')
```

간단한 출력 또는 간단한 직렬화의 경우, 용어의 읽기 쉬운 표현이 필요한 경우가 있다. 모든 RDFLib 용어에는 적절한 N3 형식을 반환하고 접두사를 제공하기 위해 NamespaceManager 인스턴스가 제공할 수 있는 `.n3()` 메서드가 있다 (예: .n3(namespace_manager=some_nm).

```python
from rdflib import Graph, URIRef, Literal, BNode
from rdflib.namespace import FOAF, NamespaceManager

person = URIRef("http://xmlns.com/foaf/0.1/Person")
print(person.n3())

g = Graph()
g.bind("foaf", FOAF)

print(person.n3(g.namespace_manager))

l = URIRef('http://www.w3.org/2001/XMLSchema#')
print(l.n3())

l = Literal(2)
print(l.n3())

print(l.n3(g.namespace_manager))
```

내임스페이스 관리에는 URI를 가져와서 여러 부분으로 분해하는 유용한 메서드 `compute_qname` `g.namespace_manager.compute_qname(x)` (또는 `g.compute_qname(x))`도 있다.

```python
from rdflib import Graph, URIRef

g = Graph()
print(g.compute_qname(URIRef("http://foo/bar#baz")))
# ('ns1', rdflib.term.URIRef('http://foo/bar#'), 'baz')
```

## SPARQL 쿼리에서의 네임스페이스(Namespaces in SPARQL Queries)
[**`query()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.query)에 제공된 `initNs` 인수는 쿼리 문자열에서 확장할 내임임스페이스의 사전이다. `initNs` 인수를 전달하지 않으면 그래프 namespace_manager에 등록된 내임스페이스를 사용한다.

```python
from rdflib.namespace import FOAF

graph.query('SELECT * WHERE { ?p a foaf:Person }', initNs={'foaf': FOAF})
```

빈 접두사(예: `?a :knows ?b`)를 사용하려면 SPARQL 쿼리에서 접두사가 없는 `PREFIX` 지시문을 사용하여 기본 내임스페이스를 설정한다.

```python
PREFIX : <http://xmlns.com/foaf/0.1/>
```