# Graph 탐색
RDF 그래프는 RDF triple의 집합이며 이것을 RDFLib에서 정확히 미러링하려고 한다. Python [**`Graph()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph)는 컨테이너(container) 타입을 에뮬레이트하려고 한다.

## Itrator인 Graph(Graph as Iterators)
RDFLib 그래프는 포함된 triple에 대한 반복(iteration)을 지원하기 위해 [**`__iter__()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.__iter__)를 재정의한다.

```python
for s, p, o in someGraph:
    if not (s, p, o) in someGraph:
        raise Exception("Iterator / Container Protocols are Broken!!")
```

이 루프는 someGraph의 모든 subject(s), predicate(p) 및 object(o)를 반복한다.

## Contains 검사(Contains check)
그래프는 [**`__contains__()`**]()를 구현하므로 triple이 있는 `graph` 구문에서 그래프에 트리플이 있는지 확인할 수 있다.

```python
from rdflib import URIRef
from rdflib.namespace import RDF

bob = URIRef("http://example.org/people/bob")
if (bob, RDF.type, FOAF.Person) in graph:
    print("This graph knows that Bob is a person!")
```

이 triple이 완전히 바인딩될 필요는 없다.

```python
if (bob, None, None) in graph:
    print("This graph contains triples about Bob!")
```

## RDFLib Graphs에서 집한 연산(Set Operations on RDFLib Graphs) <a id="set-operations-graphs"></a>
그래프는 [**`__iadd__()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.__iadd__), [**`__isub__()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.__isub__) 등의 여러 파이썬 연산자를 재정의한다. 이는 그래프에서 합집합, 차집합 및 기타 집합 연산을 지원한다.

| 연산 | 결과 |
|  G1 + G2 | return new graph with union (triples on both) |
| G1 += G2 | in place union / addition | 
| G1 - G2 | return new graph with difference (triples in G1, not in G2) |
| G1 -= G2 | in place difference / subtraction |
| G1 & G2 | intersection (triples in both graphs) |
| G1 ^ G2 | xor (triples in either G1 or G2, but not in both) |

> **Warning** 

> 그래프에 대한 집합 연산은 그래프 간에 빈 노드를 공유한다고 가정한다. 이는 기대하는 것일 수도 있고 아닐 수도 있다. 자세한 내용은 그래프 병합(merge)을 참조한다.

## 기본 triple 매칭(Basic Triple Matching)
모든 트리플을 반복하는 대신 RDFLib 그래프는 [**`triples()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.triples) 함수를 사용하여 기본 triple 패턴 일치(matching)를 지원한다. 이 함수는 인수에 의해 주어진 패턴과 일치하는 triple의 생성기이다. 즉, 인수는 반환되는 triple에 조건을 지정한다. [**`None`**](https://docs.python.org/3.7/library/constants.html#None)은 와일드카드로 처리된다. 예를 들어:

```python
g.parse("some_foaf.ttl")
# find all subjects (s) of type (rdf:type) person (foaf:Person)
for s, p, o in g.triples((None, RDF.type, FOAF.Person)):
    print(f"{s} is a person")

# find all subjects of any type
for s, p, o in g.triples((None,  RDF.type, None)):
    print(f"{s} is a {o}")

# create a graph
bobgraph = Graph()
# add all triples with subject 'bob'
bobgraph += g.triples((bob, None, None))
```

전체 트리플에 관심이 없다면 [**`objects()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.objects), [**`subject()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.subjects), [**`predicates()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.predicates), [**`predicate_objects()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.predicate_objects) 등의 메서드를 사용하여 원하는 부분만 얻을 수 있다.

```python
for person in g.subjects(RDF.type, FOAF.Person):
    print("{} is a person".format(person))
```

마지막으로 일부 속성의 경우 리소스당 하나의 값만 의미가 있다 (즉, *함수 속성*이거나 최대 카디널리티가 1인 경우). [**`value()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.value) 메서드는 생성기가 아닌 단일 노드만 반환하므로 유용하다.

```python
# get any name of bob
name = g.value(bob, FOAF.name)
# get the one person that knows bob and raise an exception if more are found
mbox = g.value(predicate = FOAF.name, object=bob, any=False)
```

## triple 접근을 위한 [**`Graph`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph) 메소드(Graph methods for accessing triples)
다음은 그래프 쿼리를 위한 모든 편리한 메소드 목록이다.

`Graph.`**`triples`**(*`triple: _TriplePatternType`*) → [`Generator`](https://docs.python.org/3.7/library/typing.html#typing.Generator)`[_TripleType,`[`None`](https://docs.python.org/3.7/library/constants.html#None)`,`[`None`](https://docs.python.org/3.7/library/constants.html#None)`]` [[source]](https://rdflib.readthedocs.io/en/stable/_modules/rdflib/graph.html#Graph.triples)

`Graph.`**`triples`**(*`triple:`*[*`Tuple`*](https://docs.python.org/3.7/library/typing.html#typing.Tuple)*`[`*[*`Optional`*](https://docs.python.org/3.7/library/typing.html#typing.Optional)*`[_SubjectType],`*[*`Path`*](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.paths.Path)*`,`*[*`Optional`*](https://docs.python.org/3.7/library/typing.html#typing.Optional)*`[_ObjectType]]`*) → [`Generator`](https://docs.python.org/3.7/library/typing.html#typing.Generator)`[`[`Tuple`](https://docs.python.org/3.7/library/typing.html#typing.Tuple)`[_SubjectType,`[`Path`](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.paths.Path)`, _ObjectType],`[`None`](https://docs.python.org/3.7/library/constants.html#None)`,`[`None`](https://docs.python.org/3.7/library/constants.html#None)`]` 

`Graph.`**`triples`**(*`triple:`*[*`Tuple`*](https://docs.python.org/3.7/library/typing.html#typing.Tuple)*`[`*[*`Optional`*](https://docs.python.org/3.7/library/typing.html#typing.Optional)*`[_SubjectType],`*[*`Union`*](https://docs.python.org/3.7/library/typing.html#typing.Union)*`[`*[`None`](https://docs.python.org/3.7/library/constants.html#None)*`,`* [*`Path`*](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.paths.Path)*`, _PredicateType],`*[*`Optional`*](https://docs.python.org/3.7/library/typing.html#typing.Optional)*`[_ObjectType]]`*) → [`Generator`](https://docs.python.org/3.7/library/typing.html#typing.Generator)`[`[`Tuple`](https://docs.python.org/3.7/library/typing.html#typing.Tuple)`[_SubjectType,`[`Union`](https://docs.python.org/3.7/library/typing.html#typing.Union)`[_PredicateType,`[`Path`](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.paths.Path)`], _ObjectType],`[`None`](https://docs.python.org/3.7/library/constants.html#None)`,`[`None`](https://docs.python.org/3.7/library/constants.html#None)`]` 

triple 스토어를 통한 생성기

주어진 triple 패턴과 일치하는 triple을 반환한다. triple 패턴이 컨텍스트를 제공하지 않으면 모든 컨텍스트를 검색한다.

**Parameters**::
**triple (**[**`Tuple`**](https://docs.python.org/3.7/library/typing.html#typing.Tuple)**[** [**`Optional`**](https://docs.python.org/3.7/library/typing.html#typing.Optional)**[** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**,** [**`Union`**](https://docs.python.org/3.7/library/typing.html#typing.Union)**[** [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**,** [**`Path`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.paths.Path)**,** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**],** [**`Optional`**](https://docs.python.org/3.7/library/typing.html#typing.Optional)**[** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**]])** –

**Return type**::
[**`Generator`**](https://docs.python.org/3.7/library/typing.html#typing.Generator)**[** [**`Tuple`**](https://docs.python.org/3.7/library/typing.html#typing.Tuple)**[** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**,** [**`Union`**](https://docs.python.org/3.7/library/typing.html#typing.Union)**[** [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**,** [**`Path`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.paths.Path)**],** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**],** [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**,** [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**]**

`Graph.`**value**(`subject=None, predicate=rdflib.term.URIRef('http://www.w3.org/1999/02/22-rdf-syntax-ns#value'), object=None, default=None, any=True`)

두 범위의 쌍에 대한 값 가져오기

subject, redicate, object 중 정확히 하나는 None이어야 한다. 값이 하나만 있을 수 있다는 것을 알고 있는 경우에 유용하다.

많이 발생하는 상황 중 하나이므로 이 '매크로'는 유틸리티와 유사하다.

**Parameters**::

- subject, redicate, object – 정확히 하나는 None이어야 한다. 
- default - 반환되는 값 값이 발견되지 않은 경우 
– True면 둘 이상이 있는 경우 값을 반환하고, 그렇지 않으면 UniquenessError를 발생시킨다.

`Graph.`**subjects**(`predicate=None, object=None, unique=False`)
주어진 predicate와 object를 갖는 (선택적으로 고유한) subject 생성기

**Parameters**::

- **predicate** ([**`Union`**](https://docs.python.org/3.7/library/typing.html#typing.Union)**[** [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**,** [**`Path`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.paths.Path)**,** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**]**) –
- **object** ([**`Optional`**](https://docs.python.org/3.7/library/typing.html#typing.Optional)**[** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**]**) –
- **unique** ([**`bool`**](https://docs.python.org/3.7/library/functions.html#bool)) –

**Return type**:: [**`Generator`**](https://docs.python.org/3.7/library/typing.html#typing.Generator)**[** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**,**  [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**,** [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**]**

`Graph.`**objects**(`subject=None, predicate=None, unique=False`)
주어진 subject와 predicate를 갖는 (선택적으로 고유한) object 생성기

**Parameters**::

- **subject** ([**`Optional`**](https://docs.python.org/3.7/library/typing.html#typing.Optional)**[** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**]**) –
- **predicate** ([**`Union`**](https://docs.python.org/3.7/library/typing.html#typing.Union)**[** [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**,** [**`Path`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.paths.Path)**,** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**]**) –
- **unique** ([**`bool`**](https://docs.python.org/3.7/library/functions.html#bool)) –

**Return type**:: [**`Generator`**](https://docs.python.org/3.7/library/typing.html#typing.Generator)**[** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**,**  [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**,** [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**]**

`Graph.`**predicates**(`subject=None, predicate=None, unique=False`)
주어진 subject와 object를 갖는 (선택적으로 고유한) predicate 생성기

**Parameters**::

- **subject** ([**`Optional`**](https://docs.python.org/3.7/library/typing.html#typing.Optional)**[** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**]**) –
- **object** ([**`Optional`**](https://docs.python.org/3.7/library/typing.html#typing.Optional)**[** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**]**) –
- **unique** ([**`bool`**](https://docs.python.org/3.7/library/functions.html#bool)) –

**Return type**:: [**`Generator`**](https://docs.python.org/3.7/library/typing.html#typing.Generator)**[** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**,**  [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**,** [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**]**

`Graph.`**subject-objects**(`predicate=None, unique=False`)
주어진 predicate를 갖는 (선택적으로 고유한) (subject, object) 튜플 생성기

**Parameters**::

- **predicate** ([**`Union`**](https://docs.python.org/3.7/library/typing.html#typing.Union)**[** [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**,** [**`Path`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.paths.Path)**,** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**]**) –
- **unique** ([**`bool`**](https://docs.python.org/3.7/library/functions.html#bool)) –

**Return type**:: [**`Generator`**](https://docs.python.org/3.7/library/typing.html#typing.Generator)**[** [**`Tuple`**](https://docs.python.org/3.7/library/typing.html#typing.Tuple)**[** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**,** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**],**  [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**,** [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**]**

`Graph.`**subject-predicates**(`object=None, unique=False`)
주어진 object를 갖는 (선택적으로 고유한) (subject, predicate) 튜플 생성기

**Parameters**::

- **object** ([**`Optional`**](https://docs.python.org/3.7/library/typing.html#typing.Optional)**[** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**]**) –
- **unique** ([**`bool`**](https://docs.python.org/3.7/library/functions.html#bool)) –

**Return type**:: [**`Generator`**](https://docs.python.org/3.7/library/typing.html#typing.Generator)**[** [**`Tuple`**](https://docs.python.org/3.7/library/typing.html#typing.Tuple)**[** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**,** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**],**  [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**,** [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**]**

`Graph.`**predicate-objects**(`subject=None, unique=False`)
주어진 subbject를 갖는 (선택적으로 고유한) (object, predicate) 튜플 생성기

**Parameters**::

- **subject** ([**`Optional`**](https://docs.python.org/3.7/library/typing.html#typing.Optional)**[** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**]**) –
- **unique** ([**`bool`**](https://docs.python.org/3.7/library/functions.html#bool)) –

**Return type**:: [**`Generator`**](https://docs.python.org/3.7/library/typing.html#typing.Generator)**[** [**`Tuple`**](https://docs.python.org/3.7/library/typing.html#typing.Tuple)**[** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**,** [**`Node`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.term.Node)**],**  [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**,** [**`None`**](https://docs.python.org/3.7/library/constants.html#None)**]**
