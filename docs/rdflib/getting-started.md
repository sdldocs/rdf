# RDF 시작하기

## 설치
RDFLib는 오픈 소스이며 [GitHub](http://github.com/RDFLib/rdflib/) 리포지토리에서 유지 관리된다. 현재와 이전 버전의 RDFLib 릴리스를 [PyPi](http://pypi.python.org/pypi/rdflib/)에서 찾아 볼 수 있다.

RDFLib를 설치하는 가장 좋은 방법은 `pip`(필요한 경우 sudo)를 사용하는 것이다.

```shell
$ pip install rdflib
```

최신 코드를 실행하려면 GitHub 리포지토리의 마스터 브랜치를 복제하고 이를 사용하거나 GitHub에서 직접 `pip install` 할 수 있다.

```shell
$ pip install git+https://github.com/RDFLib/rdflib.git@master#egg=rdflib
```

## 지원

## 작동 원리
*패키지는 이전에 RDF를 처음 사용하는 Python 프로그래머에게 RDF를 소개하는 적절한 방법을 알려주는 다양한 Python 관용구를 사용한다.*

**Graph**는 RDFLib이 RDF 작업을 위해 필요한 기본 인터페이스이다.

RDFLib graph는 정렬되지 않은 컨테이너이다. 이는 일반적인 파이썬 `set` 연산(예: 트리플을 추가하기 위한 **`add()`**)과 triple을 검색하고 임의의 순서로 이를 반환하는 메서드를 포함하고 있다.

RDFLib graph는 또한 예측 가능한 방식으로 작동하기 위해 특정 내장 Python 메서드를 재정의한다. 그 메소드는 [컨테이너 타입을 에뮬레이트하여](https://docs.python.org/3.7/reference/datamodel.html#emulating-container-types) 이를 수행하며, 3 항목 튜플(RDF에서 "triple") 세트로 잘 취급된다.

```python
[
    (subject0, predicate0, object0),
    (subject1, predicate1, object1),
    ...
    (subjectN, predicateN, objectN)
 ]

```

## 작은 예

```python
from rdflib import Graph

# Create a Graph
g = Graph()

# Parse in an RDF file hosted on the Internet
g.parse("http://www.w3.org/People/Berners-Lee/card")

# Loop through each triple in the graph (subj, pred, obj)
for subj, pred, obj in g:
    # Check if there is at least one triple in the Graph
    if (subj, pred, obj) not in g:
       raise Exception("It better be!")

# Print the number of "triples" in the Graph
print(f"Graph g has {len(g)} statements.")
# Prints: Graph g has 86 statements.

# Print out the entire Graph in the RDF Turtle format
print(g.serialize(format="turtle"))
```

여기에서 [**`graph`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph)를 생성하고, Tim Berners-Lee의 소셜 네트워크 세부 정보가 저장되어 있는 온라인 RDF 파일을 해당 그래프에 표현할 수 있도록 구문 분석한다. `print()` 문은 `len()` 함수를 사용하여 그래프의 triple 갯수를 출력한다.

## 더 광범위한 예

```python
from rdflib import Graph, Literal, RDF, URIRef
# rdflib knows about quite a few popular namespaces, like W3C ontologies, schema.org etc.
from rdflib.namespace import FOAF , XSD

# Create a Graph
g = Graph()

# Create an RDF URI node to use as the subject for multiple triples
donna = URIRef("http://example.org/donna")

# Add triples using store's add() method.
g.add((donna, RDF.type, FOAF.Person))
g.add((donna, FOAF.nick, Literal("donna", lang="en")))
g.add((donna, FOAF.name, Literal("Donna Fales")))
g.add((donna, FOAF.mbox, URIRef("mailto:donna@example.org")))

# Add another person
ed = URIRef("http://example.org/edward")

# Add triples using store's add() method.
g.add((ed, RDF.type, FOAF.Person))
g.add((ed, FOAF.nick, Literal("ed", datatype=XSD.string)))
g.add((ed, FOAF.name, Literal("Edward Scissorhands")))
g.add((ed, FOAF.mbox, Literal("e.scissorhands@example.org", datatype=XSD.anyURI)))

# Iterate over triples in store and print them out.
print("--- printing raw triples ---")
for s, p, o in g:
    print((s, p, o))

# For each foaf:Person in the store, print out their mbox property's value.
print("--- printing mboxes ---")
for person in g.subjects(RDF.type, FOAF.Person):
    for mbox in g.objects(person, FOAF.mbox):
        print(mbox)

# Bind the FOAF namespace to a prefix for more readable output
g.bind("foaf", FOAF)

# print all the data in the Notation3 format
print("--- printing mboxes ---")
print(g.serialize(format='n3'))
```

## SPARQL 쿼리 예

```python
from rdflib import Graph

# Create a Graph, pare in Internet data
g = Graph().parse("http://www.w3.org/People/Berners-Lee/card")

# Query the data in g using SPARQL
# This query returns the 'name' of all ``foaf:Person`` instances
q = """
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>

    SELECT ?name
    WHERE {
        ?p rdf:type foaf:Person .

        ?p foaf:name ?name .
    }
"""

# Apply the query to the graph and iterate through results
for r in g.query(q):
    print(r["name"])

# prints: Timothy Berners-Lee
```

## 더 많은 예
소스 배포의 `examples` 폴더에서 더 많은 [예들](https://rdflib.readthedocs.io/en/stable/apidocs/examples.html)을 찾을 수 있다.