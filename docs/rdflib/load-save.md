# RDF 로딩과 저장(Loading and saving RDF)

## RDF 파일 로딩
RDF 데이터는 다양한 구문(`turtle`, `rdf/xml`, `n3`, `n-triples`, `trix`, `JSON-LD` 등)을 사용하여 표현할 수 있다. 가장 간단한 형식은 `n-triples`로, triple 형식이다.

현재 디렉토리에 다음 두 행이 있는 `demo.nt` 파일을 작성한다.

```
<http://example.com/drewp> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://xmlns.com/foaf/0.1/Person> .
<http://example.com/drewp> <http://example.com/says> "Hello World" .
```

첫 행에서 이 파일은 "`drewp is a FOAF Person:`. 둘째 행에서 "`drep says "Hello World"`"라고 한다.

RDFLib는 파일 익스텐션으로 파일 형식을 추측할 수 있으므로 ("`.nt`"는 일반적으로 n-triplr에 사용됨) [`**parse()**`](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.parse)를 사용하여 파일을 읽을 수 있다. 파일이 비표준 RDF 파일 익스텐션이면 키워드 매개변수 `format`을 설정하여 인터넷 미디어 타입 또는 형식 이름 ([사용 가능한 파서 목록](plugin-parsers.md)이 있음)을 지정할 수 있다.

대화형 파이썬 인터프리터에서 다음을 시도하시오.

```python
from rdflib import Graph

g = Graph()
g.parse("demo.nt")

print(len(g))
# prints: 2

import pprint
for stmt in g:
    pprint.pprint(stmt)
# prints:
# (rdflib.term.URIRef('http://example.com/drewp'),
#  rdflib.term.URIRef('http://example.com/says'),
#  rdflib.term.Literal('Hello World'))
# (rdflib.term.URIRef('http://example.com/drewp'),
#  rdflib.term.URIRef('http://www.w3.org/1999/02/22-rdf-syntax-ns#type'),
#  rdflib.term.URIRef('http://xmlns.com/foaf/0.1/Person'))
```

마지막 6 줄은 RDFLib이 파일의 두 명령문을 나타내는 방법을 보여준다. 명령문 자체는 길이가 3인 튜플("triple")이고 triple의 subject, predicate와 object는 모두 rdflib 타입이다.



## 원격 RDF 읽기
인터넷에서 그래프를 읽는 것은 쉽다.

```python
from rdflib import Graph

g = Graph()
g.parse("http://www.w3.org/People/Berners-Lee/card")
print(len(g))
# prints: 86
```

[**`rdflib.Graph.parse()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.Graph.parse)는 로컬 파일, 이 예에서와 같이 URL을 통한 원격 데이터 또는 문자열의 RDF 데이터(`data` 매개변수 사용)를 처리할 수 있다.

## RDF 저장
그래프를 파일에 저장하려면 [**`rdflib.Graph.serialize()`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.Graph.serialize) 함수를 사용한다.

```python
from rdflib import Graph

g = Graph()
g.parse("http://www.w3.org/People/Berners-Lee/card")
g.serialize(destination="tbl.ttl")
```

이는 [http://www.w3.org/People/Berners-Lee/card](http://www.w3.org/People/Berners-Lee/card)의 데이터를 구문 분석하고 기본 RDF 직렬화(rdflib 6.0.0부터)인 turtle 형식을 사용하여 이 디렉토리의 `tbl.ttl` 파일에 저장한다. 

동일한 데이터를 읽고 변수 `v`에 RDF/XML 형식 문자열로 저장하려면 다음을 수행한다.

```python
from rdflib import Graph

g = Graph()
g.parse("http://www.w3.org/People/Berners-Lee/card")
v = g.serialize(format="xml")
```

다음 표에는 rdflib를 사용하여 데이터를 직렬화할 수 있는 RDF 형식과 `serialize()`에서 형식을 지정하는 데 사용되는 `format=KEYWORD` 키워드를 나열하였다.

| **RDF 형식** | **Keyword** | **Notes** |
|-------------|-------------|-----------|
| Turtle | turtle, ttl or turtle2 | turtle2 is just turtle with more spacing & linebreaks |
| RDF/XML| xml or pretty-xml | Was the default format, rdflib < 6.0.0 |
| JSON-LD | json-ld | There are further options for compact syntax and other JSON-LD variants |
| N-Triples | ntriples, nt or nt11 | nt11 is exactly like nt, only utf8 encoded |
| Notation-3 | n3 | N3 is a superset of Turtle that also caters for rules and a few other things |
| Trig | trig | Turtle-like format for RDF triples + context (RDF quads) and thus multiple graphs |
| Trix | trix | RDF/XML-like format for RDF quads | 
| N-Quads | nquads | N-Triples-like format for RDF quads |

## 다중 그래프 작업(Working with multi-graphs)
컨텍스트를 인식하는 RDF 데이터인 다중 그래프를 읽고 쿼리하려면 rdflib의 [**`rdflib.ConjunctiveGraph`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.ConjunctiveGraph) 또는 [**`rdflib.Dataset`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.Dataset) 클래스를 사용해야 한다. 이는 쿼드(triple + 그래프 ID)에 대한 모든 것을 알고 있는 [**`rdflib.Graph`**](https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.Graph)의 확장이다.

아래와 같은 다중 그래프 데이터 파일이 있는 경우(이전 @prefix가 아닌) 새로운 스타일의 PREFIX 문을 사용한 `trig` 형식)

```python
PREFIX eg: <http://example.com/person/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>

eg:graph-1 {
    eg:drewp a foaf:Person .
    eg:drewp eg:says "Hello World" .
}

eg:graph-2 {
    eg:nick a foaf:Person .
    eg:nick eg:says "Hi World" .
}
```

파일을 구문 분석하고 다음과 같이 쿼리할 수 있다.

```python
from rdflib import Dataset
from rdflib.namespace import RDF

g = Dataset()
g.parse("demo.trig")

for s, p, o, g in g.quads((None, RDF.type, None, None)):
    print(s, g)
```

아는 다음과 같이 출력한다.

```
http://example.com/person/drewp http://example.com/person/graph-1
http://example.com/person/nick http://example.com/person/graph-2
```
