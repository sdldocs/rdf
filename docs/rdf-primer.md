# RDF Primer<sup>[1](#footnote_1)</sup>

## Abstract
이 페이지에서는 RDF를 효과적으로 사용하는 데 필요한 기본 지식을 설명한다. RDF의 기본 개념을 소개하고 RDF의 사용에 대한 구체적인 예를 보여준다. [RDF 데이터 모델](#rdf-data-model), [RDS 어휘](#rdf-vocabularies) 와 [RDF 그래프 작성](#writing-rdf-graph)는 RDF의 핵심 요소에 대한 최소한의 소개를 담고 있다. ["RDF 1.1의 새로운 기능"](https://www.w3.org/TR/rdf11-primer/#bib-RDF11-NEW)에 RDF 1.0(2004 버전)에서 RDF 1.1으로 변경 사항을 요약하였다.

## 목차

- [개요](#introduction)
- [RDF 사용의 필요성](#why-rdf)
- [RDF 데이터 모델](#rdf-data-model)
- [RDS 어휘](#rdf-vocabularies)
- [RDF 그래프 작성](#writing-rdf-graph)
- [RDF 그래프 시멘틱](#semantics-rdf-graphs)
- [RDF 데이터](#rdf-data)
- [유용한 정보](#more-information)

## 개요 <a id="introduction"></a>
RDF(Resource Description Framework)는 [리소스](http://www.w3.org/TR/2014/REC-rdf11-concepts-20140225/#resources-and-statements)에 대한 정보를 표현하기 위한 프레임워크이다. 리소스는 문서, 사람, 물리적 객체 및 추상 개념을 포함하며 무엇이든 될 수 있다.

RDF는 사람에게 보이기 위한 것 뿐아니라 어플리케이션이 웹 상의 정보를 처리해야 하는 상황을 위한 것이다. RDF는 이 정보를 표현하기 위한 공통 프레임워크를 제공하므로 의미를 잃지 않고 어플리케이션 간에 정보를 교환할 수 있다. 이는 공통 프레임워크이므로 어플리케이션 설계자는 공통 RDF 파서와 처리 도구를 활용할 수 있다. 서로 다른 어플리케이션 간에 정보를 교환할 수 있다는 것은 정보가 원래 작성된 어플리케이션이 아닌 다른 어플리케이션에서 사용 가능하게 될 수 있음을 의미한다.

특히 RDF는 웹에서 데이터를 게시하고 상호 연결하는 데 사용될 수 있다. 예를 들어, `http://www.example.org/bob#me`를 검색하면 Bob이 IRI에 의해 식별된 Alice를 알고 있다는 사실을 포함하여 Bob에 대한 데이터를 제공받을 수 있다. (IRI는 "International Resource Indentifier"이며, [IRIs](#iris)에서 설명한다). Alice의 IRI를 검색하면 친구, 관심사 등을 위한 다른 데이터세트에 대한 링크를 포함하여 Alice에 대한 더 많은 데이터를 제공받을 수 있다. 사용자나 자동화된 프로세스가 이같은 링크를 따라가서 이러한 다양한 것들에 대한 데이터를 수집할 수 있다. 이러한 RDF는 종종 [Linked Data](http://www.w3.org/DesignIssues/LinkedData.html)로 사용된다.

이 페이지는 규범적이지 않으며 RDF 1.1에 대한 완전한 설명을 제공하지 않는다. RDF의 규범적 사양은 다음 문서에서 찾을 수 있다.

- [RDF11-CONCEPTS](http://www.w3.org/TR/rdf11-concepts/) - RDF의 기본 개념과 추상 구문("RDF 개념 및 추상 구문")을 설명하는 문서
- [RDF11-MT](http://www.w3.org/TR/rdf11-mt/) - RDF의 형식적 모델 이론적 의미론을 설명하는 문서 ("RDF Semantics")
- RDF의 직렬화 형식 사양은 다음과 같다.
    - [Turtle](http://www.w3.org/TR/turtle/)과  [TriG](http://www.w3.org/TR/trig/)
    - [JSON-LD](http://www.w3.org/TR/json-ld/)(JSON 기반)
    - [RDFa](http://www.w3.org/TR/rdfa-primer/)(HTML 임베딩용)
    - [N-Triples](http://www.w3.org/TR/n-triples/) 및 [N-Quads](http://www.w3.org/TR/n-quads/)(라인 기반 교환 형식)
    - [RDF/XML](http://www.w3.org/TR/rdf-syntax-grammar/)(원래 2004 구문, RDF 1.1용으로 업데이트됨)
- [RDF Schema](http://www.w3.org/TR/rdf-schema/)와 RDF 데이터에 대한 데이터 모델링 어휘를 설명하는 문서


## RDF 사용의 필요성 <a id="why-rdf"></a>
다음은 여러 분야의 실행 커뮤니티가 목표로 하는 다양한 RDF의 사용법을 설명한다.

- 예를 들어, 널리 사용되는 [schema.org](http://schema.org/) 어휘를 사용하여 웹 페이지에 컴퓨터가 판독 가능한 (machine-readable) 정보를 추가하여 검색 엔진에 고급 형식으로 표시하거나 서드파티 어플리케이션에서 자동으로 처리할 수 있도록 한다.
- 서드파티 데이터세트에 연결하여 데이터세트를 풍부하게 한다. 예를 들어, 그림에 대한 데이터세트는 [Wikidata](http://www.wikidata.org/)의 해당 작가와 연결함으로써 더욱 풍부해질 수 있으며, 따라서 그림에 대한 광범위한 정보를 접근할 수 있다.
- API 피드를 상호 연결하여 클라이언트에게 더 많은 정보에 접근할 수 있는 방법을 쉽게 찾을 수 있도록 한다.
- 특정 주제에 대한 데이터 집계를 구축하는 등 [Linked Data](http://www.w3.org/DesignIssues/LinkedData.html)로 현재 게시된 데이터세트를 사용할 수 있다.
- 여러 웹 사이트에서 인물에 대한 RDF 설명을 상호 연결하여 분산 소셜 네트워크를 구축한다.
- 데이터베이스 간에 데이터를 교환하기 위한 표준을 제공한다.
- 조직 내에서 다양한 데이터세트를 상호 연결하여 [SPARQL](http://www.w3.org/TR/sparql11-overview/)을 사용하여 교차 데이터세트 쿼리를 수행할 수 있다.

## RDF 데이터 모델 <a id="rdf-data-model"></a>

### Triples
RDF를 사용하여 리소스에 대해 기술할 수 있다. 문장의 형식은 간단하다. 문장은 항상 다음과 같은 구조로 이루어져 있다.

```
<subject> <predicate> <object>
```

RDF 문은 두 리소스 간의 관계를 나타낸다. **subject**와 **object**는 관계가 있는 두 리소스을 나타내고, **predicare**는 관계의 본질을 나타낸다. 관계는 (주체에서 객체로) 방향 방식으로 표현되며 RDF **속성(property)**이라 불린다. RDF 문은 세 개의 요소로 구성되기 때문에 **triple**이라고 한다.

다음은 (비공식적으로 의사 코드로 표현한) RDF Triples 예이다

> Example 1: triples 샘플 (비정규)

```
<Bob> <is a> <person>.
<Bob> <is a friend of> <Alice>.
<Bob> <is born on> <the 4th of July 1990>. 
<Bob> <is interested in> <the Mona Lisa>.
<the Mona Lisa> <was created by> <Leonardo da Vinci>.
<the video 'La Joconde à Washington'> <is about> <the Mona Lisa>
```

동일한 리소스가 여러 triple에서 참조되는 경우가 많다. 위의 예에서, Bob은 네 triple의 subject이고, Mona Lisa는 한 triple subject이며, 두 triple의 object이다. 동일한 리소스를 한 트리플의 subject와 다른 triple의 object 위치에 둘 수 있는 능력은 RDF의 힘의 중요한 부분으로 triple 간의 연결을 찾는 것을 가능하게 한다.

triple은 연결된 **그래프**로 시각화할 수 있다. 그래프는 노드와 아크로 구성된다. triple의 subject와 object는 그래프에서 노드를 구성하고 predicate는 arc를 만든다. 아래 그림은 샘플 triples의 그래프를 보인다.

![informal graph](images/rdf-primer/example-graph.jpeg)
<center>Fig. 1 샘플 triples의 비정규 그패프</center> <a id="fig-1"></a>

위와 같은 그래프를 만든 후에는 [SPARQL](http://www.w3.org/TR/sparql11-overview/)를 사용하여 Leonardo da Vinci의 그림에 관심이 있는 사람을 조회할 수 있다.

이 절에서 RDF 데이터 모델을 "추상 문법(abstract syntax)"의 형태로 설명한다. 즉, 특정 문법(텍스트 파일에 저장된 triple을 나타내기 위해 사용되는 문법)과 독립적인 데이터 모델이다. 다른 구체적인 문법으로 추상 문법의 관점에서 정확히 동일한 그래프를 생성할 수 있다. [RDF 그래프의 의미](http://www.w3.org/TR/rdf11-mt/)는 이 추상 문법 측면에서 정의된다. [RDF 그래프 작성](#writing-rdf-graph)의 뒷부분에서 구체적인 RDF 문법을 소개된다.

다음 세 섹션에서는 triple에 나타나는 세 가지 유형의 RDF 데이터 즉 IRI, 리터럴(literal)과 빈 노드(blank node)에 대해 논의한다.

### IRIs <a id="iris"></a>
IRI는 "International Resource Identifier"의 약자이다. IRI는 리소스를 식별한다. 사람들이 웹 주소로 사용하는 URL(Uniform Resource Locator)은 IRI의 한 종류이다. 다른 형태의 IRI는 위치나 접근 방법을 암시하지 않고 리소스에 대한 식별자를 제공한다. IRI의 개념은 URI(Uniform Resource Identifier)를 일반화한 것으로 IRI 문자열에 비ASCII 문자 사용을 허용한다. [RFC 3987](http://www.ietf.org/rfc/rfc3987.txt)에서 IRI를 명시하고 있다.

IRI는 트리플의 **세 위치 모두**에 나타날 수 있다.

언급한 바와 같이, IRI는 문서, 사람, 물리적 객체 및 추상 개념과 같은 리소스를 식별하는 데 사용된다. 예를 들어 [DBpedia](http://dbpedia.org/)에서 Leonardo da Vinci의 IRI는 다음과 같다.

> [http://dbpedia.org/resource/Leonardo_da_Vinci](http://dbpedia.org/resource/Leonardo_da_Vinci)

[Europeana](http://www.europeana.eu/)에서 제목이 'La Joconde à Washington'인 Mona Lisa에 관한 [INA](http://www.ina.fr/) 비디오의 IRI는 다음과 같다.

 > [http://data.europeana.eu/item/04802/243FA8618938F4117025F17A8B813C5F9AA4D619](http://data.europeana.eu/item/04802/243FA8618938F4117025F17A8B813C5F9AA4D619)

IRI는 글로벌 식별자이므로 다른 사람들이 이 IRI를 재사용하여 동일한 것을 식별할 수 있다. 예를 들어, 사람 간의 친구 관계를 기술하기 위해 다음과 같은 IRI를 RDF 속성으로 사용한다.

> [http://xmlns.com/foaf/0.1/knows](http://xmlns.com/foaf/0.1/knows)

RDF는 IRI가 무엇을 나타내는지에 대해 인식하지 않는다. 그러나 IRI는 특정 단어 또는 관례에 의해 의미를 부여할 수도 있다. 예를 들어 [DBpedia](http://dbpedia.org/)는 `http://dbpedia.org/resource/Name` 형식의 IRI를 사용하여 해당 Wikipedia 문서에 설명된 내용을 나타낸다.

### 리터럴(literals)
[리터럴](http://www.w3.org/TR/2014/REC-rdf11-concepts-20140225/#section-Graph-Literal)은 IRI가 아닌 기본 값이다. 리터럴의 예로는 "La Joconde"와 같은 문자열, "the 4th of July, 1990"과 같은 날짜, "3.14159"와 같은 숫자가 있다. 리터럴은 이러한 값을 정확하게 분석하고 해석할 수 있도록 [데이터 유형(datatype)](http://www.w3.org/TR/2014/REC-rdf11-concepts-20140225/#section-Datatypes)에 연결되어 있다. 문자열 리터럴은 선택적으로 [언어 태그(language tag)](http://www.w3.org/TR/2014/REC-rdf11-concepts-20140225/#section-Graph-Literal)를 사용할 수 있다. 예를 들어 "Léonard de Vinci"와 "奥纳达····西文"는 각각 언어 태그로 "fr"와 "zh"를 갖는다.

리터럴은 triple의 **object 위치**에만 나타날 수 있다.

RDF 개념 문서는 (완전하지 않은) [데이터 유형 목록](http://www.w3.org/TR/2014/REC-rdf11-concepts-20140225/#section-Datatypes)을 제공한다. 여기에는 문자열, 부울, 정수, 십진수, 날짜 등과 같은 XML 스키마가 정의한 많은 데이터 타입이 있다.

### 빈 노드 (Blank nodes)
IRI와 리터럴은 함께 RDF 문을 작성하기 위한 기본 자료를 제공한다. 또한 글로벌 식별자를 사용하는 번거로움 없이 리소스를 정의할 수 있어 편리하다. 예를 들어, 우리는 모나리자 그림의 배경에 우리가 편백나무로 알고 있는 정체불명의 나무가 있다고 말하고 싶다면, 그림의 편백나무와 같이 글로벌 식별자가 없는 리소스를 [빈 노드](http://www.w3.org/TR/2014/REC-rdf11-concepts-20140225/#section-blank-nodes) RDF로 나타낼 수 있다. 빈 노드는 대수학에서 단순한 변수와 같다. 값이 무엇인지 정의하지 않고 단순히 어떤 것을 나타낸다.

빈 노드는 트리플의 **주체와 객체** 위치에 나타날 수 있다. IRI로 명시적으로 이름을 지정하지 않고 리소스를 나타내는 데 사용할 수 있다.


![Informal blank node example](images/rdf-primer/example-blank-node.jpeg) <a id="fig-2"></a>
<center>Fig. 2 비정규 빈 노드 예</center> 

### 다중 그래프 (Multiple graphs) <a id="multiple-graphs"></a>
RDF는 다중 그래프에서 RDF 문을 그룹화하고 이러한 그래프를 IRI와 연결하는 메커니즘을 제공한다. 최근에 RDF 데이터 모델에서 다중 그래프를 확장하였다. 실제로, RDF 도구 빌더와 데이터 관리자는 triple 집합의 부분집합에 대해 이야기할 수 있는 메커니즘이 필요했다. 다중 그래프는 RDF 쿼리 언어인 SPARQL에 처음 도입되었다. 따라서 RDF 데이터 모델은 SPARQL과 밀접하게 그리고 나란히 다중 그래프의 개념으로 확장되었다.

RDF 문서의 다중 그래프는 [RDF 데이터세트](http://www.w3.org/TR/2014/REC-rdf11-concepts-20140225/#section-dataset)를 구성한다. RDF 데이터세트는 명명된 그래프들과 최대 하나의 명명되지 않은("기본") 그래프로 구성될 수 있다.

예를 들어, 위의 Example 1을 두 개의 명명된 그래프로 그룹화할 수 있다. 소셜 네트워킹 사이트에서 첫번째 그래프를 제공되고 `http://example.org/bob`로 식별할 수 있다.

> Exampme 2: 샘플 데이터세트의 첫번째 그래프 
```
<Bob> <is a> <person>.
<Bob> <is a friend of> <Alice>.
<Bob> <is born on> <the 4th of July 1990>.
<Bob> <is interested in> <the Mona Lisa>.
```

그래프에 연결된 IRI를 [그래프 이름](http://www.w3.org/TR/2014/REC-rdf11-concepts-20140225/#section-dataset)이라 한다.

두번째 그래프는 [Wikidata](http://www.wikidata.org/)에서 제공하고 `https://www.wikidata.org/wiki/Special:EntityData/Q12418`로 식별된다.

> Exampme 3: 샘플 데이터세트의 두번째 그래프 
```
<Leonardo da Vinci> <is the creator of> <the Mona Lisa>.
<The video 'La Joconde à Washington'> <is about> <the Mona Lisa>.
```

아래는 이름 없는 그래프의 예이다. 그래프 이름 `<http://example.org/bob>`이 subject인 두 개의 triple을 포함하고 있다. triple은 게시자와 라이센스 정보를 다음 그래프 IRI와 연결한다.

> Exampme 4: 샘플 데이터세트의 이름 없는 그래프 
```
<http://example.org/bob> <is published by> <http://example.org>.
<http://example.org/bob> <has license> <http://creativecommons.org/licenses/by/3.0/>.
```

이 데이터세트 예에서 그래프 이름이 해당 그래프 내에 있는 RDF 데이터의 소스를 나타낸다고 가정한다. 즉, `http://example.org/bob`을 검색하면 해당 그래프의 네 triple을 액세스할 수 있다.

> **Note**: RDF는 이러한 의미론적 가정(즉, 그래프 이름이 RDF 데이터의 소스를 나타냄)이 데이터세트의 다른 사용자에게 전달하는 표준 방법인 것은 아니다. 이러한 사용자는 확립된 커뮤니티 관행처럼 데이터세트를 의도된 방식으로 해석하기 위해 대역외 지식에 의존해야 한다. 별도 [문서](http://www.w3.org/TR/rdf11-datasets/)에서 데이터세트의 가능한 의미론을 설명하고 있다.

![Informal graph of the sample dataset](images/rdf-primer/example-multiple-graphs.jpeg)
<center>Fig. 3 샘플 데이터세트의 비정규 그패프</center>

위 그림은 샘플 데이터세트를 보여준다. [TriG](#trig)은 이 데이터세트에 대한 문법의 구체적인 예를 제공한다.

## RDS 어휘 <a id="rdf-vocabularies"></a>
RDF 데이터 모델은 리소스를 서술하는 방법을 제공한다. 언급했듯이, 이 데이터 모델은 IRI가 무엇을 의미하는지 가정하지 않는다. 실제로 RDF는 일반적으로 이러한 리소스에 대한 의미 정보를 제공하는 어휘 또는 다른 규칙과 함께 사용된다.

어휘의 정의를 지원하기 위해 RDF는 [RDF 스키마 언어](http://www.w3.org/TR/rdf-schema/)를 제공한다. 이를 통해 RDF 데이터의 의미적 특성을 정의할 수 있다. 예를 들어, IRI `http://www.example.org/friendOf`을 속성으로 사용할 수 있으며 `http://www.example.org/friendOf` triple의 subject와 object는 클래스 `http://www.example.org/Person`의 리소스여야 한다고 명시할 수 있다.

RDF 스키마는 **클래스** 개념을 사용하여 리소스를 분류하는 데 사용할 수 있는 카테고리를 지정한다. **타입** 속성을 통해 인스턴스와 클래스간의 관계를 기술한다. RDF 스키마를 사용하면 클래스와 서브클래스간 및 속성과 하위 속성간의 계층 구조를 만들 수 있다. 특정 triple의 subject와 object에 대한 타입 제한은 **domain**과 **range** 제한을 통해 정의할 수 있다. domain 제한의 예는 위 단락에서 보였다. 즉 "friendOf" triple의 subject는 "Person" 클래스이어야 한다.

RDF 스키마가 제공하는 주요 모델링 구성체를 아래 표와 같이 요약할 수 있다.

<center>Table 1 RDF 스키마 구성요소</center> <a id="table-1"></a>

| 구성요소 | 구문 형식 | 설명 |
|------|---------|-----|
| [클래스](http://www.w3.org/TR/rdf-schema/#ch_classes) (a class) | C `rdf:type rdfs:Class` | C (a resource) is an RDF class |
| [속성](http://www.w3.org/TR/rdf-schema/#ch_property) (a class)] | P `rdf:type rdf:Property` | P (a resource) is an RDF property |
| [type](http://www.w3.org/TR/rdf-schema/#ch_type) (a property) | I `rdf:type` C | I (a resource) is an instance of C (a class) |
| [subClassOf](http://www.w3.org/TR/rdf-schema/#ch_subclassof) (a proprty) | C1 `rdfs:subClassOf` C2 | C1 (a class) is a subclass of C2 (a class) |
| [subPropertyOf](http://www.w3.org/TR/rdf-schema/#ch_subpropertyof) (a property) | P1 `rdfs:subPropertyOf` P2 | P1 (a property) is a sub-property of P2 (a property) |
| [domain](http://www.w3.org/TR/rdf-schema/#ch_domain) (a property) | P `rdfs:domain` C | domain of P (a property) is C (a class) |
| [range](http://www.w3.org/TR/rdf-schema/#ch_range) (a property) | P `rdfs:range` C | range of P (a property) is C (a class) |

> **Note**: 구문 형식(두 번째 열)은 [다음 절](#writing-rdf-graph)에서 더 자세히 논의할 접두어 표기법이다. 두 개의 다른 접두사(`rdf:`와 `rdfs:`)를 가지고 있다는 사실은 역호환성을 위한 다소 성가신 역사적 유물이다.

RDF 스키마의 도움으로 RDF 데이터의 모델을 만들 수 있다. 아래는 간단한 구술적인 예이다.

> Example 5: (구술적) RDF 스키마 triple
```
<Person> <type> <Class>
<is a friend of> <type> <Property>
<is a friend of> <domain> <Person>
<is a friend of> <range> <Person>
<is a good friend of> <subPropertyOf> <is a friend of>
```

`<is a friend of>`는 일반적으로 triple의 predicate로 사용되는 속성이지만 (Example 1에서와 같이), triple은 이같은 속성을 설명하거나 다른 리소스에 대한 설명에서 값을 제공할 수 있는 자원이다. 이 예에서, `<is a friend of>`는 type, domain 및 range 값을 할당하는 triple의 subject이며, `<is a friend of>` 속성에 대하여 설명하는 triple의 object이다.

소셜 네트워크를 설명하기 위한 "[Friend of a Friend](http://www.foaf-project.org/)" (FOAF)는 세계적으로 사용된 최초의 RDF 어휘중 하나이다. 아래는 RDF 어휘의 다른 예이다.

[Dublin Core](http://dublincore.org/documents/dcmi-terms/)<br>
Dublin Core Metadata Initiative는 광범위한 리소스를 기술하기 위한 메타데이터 요소 집합을 유지한다. 여기서 어휘는 "creator", "publisher", "title"과 같은 속성을 제공한다.

[schema.org](http://schema.org/)<br>
schema.org는 주요 검색 공급자 그룹에 의해 개발된 어휘 집합이다. 웹 마스터가 웹 페이지를 표시하기 위해 이러한 어휘를 사용할 수 있기 때문에 검색 엔진이 페이지에 대한 내용을 이해할 수 있다는 아이디어에서 왔다.

[SKOS](http://www.w3.org/2004/02/skos/)<br>
SKOS는 용어와 시소러스와 같은 분류 체계를 웹에 게시하기 위한 어휘이다. SKOS는 2009년부터 W3C 권고사항이며 도서관 분야에서 널리 사용되고 있다. 미국 의회도서관은 주제 제목을 SKOS 어휘로 출판하였다.

어휘는 재사용으로 가치를 창출한다. 즉 사람들이 더 많은 IRI 어휘를 재사용할수록 IRI 사용이 더 가치 있게 된다. 이는 새로운 IRI를 발명하는 대신에 다른 사람의 IRI를 재사용하도록 해야 한다는 것을 뜻하는 것이다.

[RDF 의미론 문서](http://www.w3.org/TR/rdf11-mt/)에서 RDF 스키마 구성의 의미론에 대한 공식적인 명세를 설명하고 있다. RDF 데이터의 보다 포괄적인 의미 모델링에 관심이 있는 사용자는 [OWL](http://www.w3.org/TR/owl2-overview/)의 사용을 고려할 수 있다. OWL은 RDF 어휘를 사용하므로 RDF 스키마와 함께 사용할 수 있다.

## RDF 그래프 작성 <a id="writing-rdf-graph"></a>
RDF 그래프를 작성하기 위한 여러 가지 직렬화(serialization) 형식이 존재한다. 그러나, 같은 그래프를 작성하는 다른 방법이라도 정확히 같은 triple들로 이루어지므로 논리적으로 동일하다.

이 절에서는 주석이 있는 예를 통해 다음과 같은 형식을 간략히 소개한다.

1. Turtle 패밀리의 RDF 언어 ([N-Triples](#n-triples), [Turtle](#turtle), [TriG](#trig)과 [N-Quads](#n-quards))
2. [JSON-LD](#json-ld) (JSON 기반 RDF 구문)
3. [RDFa](#rdfa) (HTML과 XML 내에서 사용을 위한)
4. [RDF/XML](https://www.w3.org/TR/rdf11-primer/#section-rdfxml) (RDF를 위한 XML 구문)

> **Tip**: [Turtle 패밀리의 RDF 언어](#turtle-family)(Turtle과 형제들)은 RDF를 직렬화하기 위한 모든 기본 개념을 설명하고 있다. 특정 RDF 사용에 관심이 있는 경우에만 JSON-LD, RDFa 및 RDF/XML을 읽을 것을 추천한다.

### Turtle 패밀리의 RDF 언어 <a id="turtle-family"></a>
이 섹션에서는 밀접하게 관련된 4 RDF 언어를 소개한다. RDF triple을 작성하는 기본 구문을 제공하기 때문에 N-triples부터 시작한다. Turtle 구문은 가독성을 향상시키기 위해 다양한 형태의 구문으로 기본 구문을 확장한다. 이후 여러 그래프를 인코딩하기 위한 Turtle과  N-Triples의 확장인 TriG와 N-Quads를 다룬다. 이 4 언어를 합쳐서 "Turtle 패밀리의 RDF 언어"라 부른다.

#### N-Triples <a id="n-triples"></a>
[N-Triples](http://www.w3.org/TR/n-triples/)는 RDF 그래프를 직렬화하기 위한 간단한 문법의 라인 기반 텍스트 구문을 제공한다. [Fig. 1](#fig-1)의 비정규 그래프는 다음과 같은 방법으로 N-Triples을 표현할 수 있다.

> Example 6: N-Triples

```
01    <http://example.org/bob#me> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://xmlns.com/foaf/0.1/Person> .
02    <http://example.org/bob#me> <http://xmlns.com/foaf/0.1/knows> <http://example.org/alice#me> .
03    <http://example.org/bob#me> <http://schema.org/birthDate> "1990-07-04"^^<http://www.w3.org/2001/XMLSchema#date> .
04    <http://example.org/bob#me> <http://xmlns.com/foaf/0.1/topic_interest> <http://www.wikidata.org/entity/Q12418> .
05    <http://www.wikidata.org/entity/Q12418> <http://purl.org/dc/terms/title> "Mona Lisa" .
06    <http://www.wikidata.org/entity/Q12418> <http://purl.org/dc/terms/creator> <http://dbpedia.org/resource/Leonardo_da_Vinci> .
07    <http://data.europeana.eu/item/04802/243FA8618938F4117025F17A8B813C5F9AA4D619> <http://purl.org/dc/terms/subject> <http://www.wikidata.org/entity/Q12418>
```

각 줄은 triple을 나타낸다. 전체 IRI는 꺾쇠괄호(`<>`)로 둘러싸여 있다. 줄의 끝의 마침표는 triple의 끝을 나타낸다. 3번째 줄에서 리터럴의 예를 볼 수 있는데, 이는 날짜이다. 데이터 타입은 `^^` 구분 기호를 통해 리터럴에 추가된다. 날짜 표현은 XML 스키마 데이터 타입 [date](http://www.w3.org/TR/xmlschema11-2/#date)의 규칙을 따른다.

문자열 리터럴은 어디에나 나타날 수 있기 때문에 N-Triples은 사용자가 문자열 리터럴을 쓸 때 데이터 타입을 생략할 수 있다. 따라서, 줄 5의 `"Mona Lisa"`는 `"Mona Lisa"^^xsd:string`과 같다. 언어 태그가 있는 문자열의 경우 태그는 문자열 바로 뒤에 나타나며 `@` 기호로 구분된다. 즉 `"La Joconde"@fr` (Mona Lisa의 프랑스 이름)처럼 작성한다.

**Note**: 기술적인 이유로 언어 태그 문자열의 데이터 타입은 `xsd:string`이 아니라 `rdf:langString`이다. 언어 태그 문자열의 데이터 타입을 명시적으로 지정하지 않았다.

아래 그림은 예의 결과인 triple을 보인다.

![RDF graph](images/rdf-primer/example-graph-iris.jpeg)
<center>Fig. 4 N-Triples 예의 결과 RDF 그래프</center> <a id="fig-4"></a>

N-Triples 예제의 7줄은 위 그림의 7개 아크(링크)와 일치한다.

N-Triples는 종종 많은 양의 RDF를 교환하고, 라인 지향 텍스트 처리 도구를 사용하여 큰 RDF 그래프를 처리하는 데 사용된다.

#### Turtle <a id="turtle"></a>
[Turtle](http://www.w3.org/TR/turtle/)는 **N-Triples의 확장**이다. Turtle은 기본적인 N-Triples 구문 외에도 네임스페이스 접두사, 목록 및 리터럴의 데이터 타입을 위한 줄임말(shorthand) 등 다수의 구문적 단축키(shortcut)를 도입하였다. Turtle은 쓰기 용이성, 구문 분석 용이성, 가독성 사이의 절충점을 제공한다. [Fig. 4](#fig-4)에 나타낸 그래프는 Turtle로 다음과 같이 나타낼 수 있다.

> Example 7: Turtle

```turtle
01    BASE   <http://example.org/>
02    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
03    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
04    PREFIX schema: <http://schema.org/>
05    PREFIX dcterms: <http://purl.org/dc/terms/>
06    PREFIX wd: <http://www.wikidata.org/entity/>
07 
08    <bob#me>
09        a foaf:Person ;
10        foaf:knows <alice#me> ;
11        schema:birthDate "1990-07-04"^^xsd:date ;
12        foaf:topic_interest wd:Q12418 .
13   
14    wd:Q12418
15        dcterms:title "Mona Lisa" ;
16        dcterms:creator <http://dbpedia.org/resource/Leonardo_da_Vinci> .
17  
18    <http://data.europeana.eu/item/04802/243FA8618938F4117025F17A8B813C5F9AA4D619>
19        dcterms:subject wd:Q12418 .
```

Turtle의 예는 논리적으로 [N-Triples](#fig-4) 예제와 동일하다. 1~6행은 IRI를 작성하기 위한 줄임말을 제공하는 지시문을 포함하고 있다. 상대 IRI(예: 8 행의 `bob#me`)는 1 행에서 지정한 기본 IRI로부터 확장된다. 2~6행은 IRI 접두사(`foaf:` 등)를 정의하며, 전체 IRI 대신 접두사 이름으로 사용할 수 있다(예: `foaf:Person`). 대응되는 IRI는 접두사를 사용하는 IRI로 대체함으로써 구성된다 (이 예에서 `foaf:Person`은 `<http://xmlns.com/foaf/0.1/Person>`을 나타낸다).

8~12행은 Turtle이 같은 subject를 갖는 triple들에 줄임말을 제공하는 방법을 보여준다. 9~12행은 `<http://example.org/bob#me>`을 subject로 하는 triple의 predicate-object 부분을 지정합니다. 9-11 행의 끝에 있는 세미콜론은 뒤에 나타나는 predicate-object 쌍이 데이터에 표시된 가장 최근의 subject를 사용하는 새로운 triple(이 경우 `bob#me`)의 일부임을 나타낸다.

9행은 특별한 종류의 구문 예를 보여준다. triple은 비정규적으로 "Bob은 사람이다"로 읽어야 한다. `a` predicate는 인스턴스 관계를 모델링하는 속성 `rdf:type`의 출임말이다([표 1](#table-1) 참조). 줄임말 `a`는 `rdf:type`에 대한 인간의 직관과 일치시키기 위한 것이다.

##### 빈 노드의 표현
아래는 이전의 편백나무 예제를 사용하여 빈 노드를 작성하기 위한 두 가지 구문 변형을 볼 수 있다.

> Example 8: 빈 노드
```
PREFIX lio: <http://purl.org/net/lio#> 

<http://dbpedia.org/resource/Mona_Lisa> lio:shows _:x .
_:x a <http://dbpedia.org/resource/Cypress> .
```

`_:x`라는 항은 빈 노드이다. Mona Lisa의 그림에서 묘사된 명명되지 않은 리소스 를나타낸다. 명명되지 않은 리소스는 `Cypress` 클래스의 인스탄스이다. 위의 예는 [Fig. 2](#fig-2)의 비정규 그래프에 대한 구체적인 구문을 제공한다.

Turtle은 빈 노드를 위한 대체 표기를 가지고 있어, `_:x`와 같은 구문을 사용할 필요가 없다.

> Example 9: 빈 노드 (대체 표기)
```
@prefix foaf: <http://xmlns.com/foaf/0.1/> .

# Some resource (blank node) is interested in some other resource
# entitled "Mona Lisa" and created by Leonardo da Vinci.

[] foaf:topic_interest [
          dcterms:title "Mona Lisa" ;
          dcterms:creator <http://dbpedia.org/resource/Leonardo_da_Vinci> ] .
```

대괄호는 여기서 빈 노드를 나타낸다. 대괄호 안의 predicate-object 쌍을 빈 노드를 subject로 하는 triple로 해석한다. `#`으로 시작하는 행은 주석이다.

[Turtle 사양](http://www.w3.org/TR/turtle/)에서 Turtle의 구문에 대한 자세한 내용을 참조할 수 있다.

#### TriG <a id="trig"></a>
Turtle의 구문은 단일 그래프에 이름을 붙이기 위한 기능을 제공하지 않아 단일 그래프만을 지원한다. [TriG](http://www.w3.org/TR/trig/)는 RDF 데이터세트 형태로 다중 그래프를 지정할 수 있는 **Turtle의 확장**이다.

> **Note**: RDF 1.1에서 모든 공식 Turtle 문서는 공식 TriG 문서이다. 그것을 다른 또 하나의 언어로 볼 수 있다. Turtle과 TriG라는 이름을 역사적 이유로 여전히 유지한다.

다음과 같이 [예의 다중 그래프 버전](#multiple-graphs)을 TriG에서 명시할 수 있다.

> Example 10: TriG

```
01    BASE   <http://example.org/> 
02    PREFIX foaf: <http://xmlns.com/foaf/0.1/> 
03    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 
04    PREFIX schema: <http://schema.org/> 
05    PREFIX dcterms: <http://purl.org/dc/terms/> 
06    PREFIX wd: <http://www.wikidata.org/entity/> 
07    
08    GRAPH <http://example.org/bob>
09      {
10        <bob#me>
11            a foaf:Person ;
12            foaf:knows <alice#me> ;
13            schema:birthDate "1990-07-04"^^xsd:date ;
14            foaf:topic_interest wd:Q12418 .
15      }
16  
17    GRAPH <https://www.wikidata.org/wiki/Special:EntityData/Q12418>
18      {
19        wd:Q12418
20            dcterms:title "Mona Lisa" ;
21            dcterms:creator <http://dbpedia.org/resource/Leonardo_da_Vinci> .
22    
23        <http://data.europeana.eu/item/04802/243FA8618938F4117025F17A8B813C5F9AA4D619>
24           dcterms:subject wd:Q12418 .
25      }
26  
27    <http://example.org/bob>
28        dcterms:publisher <http://example.org> ;
29        dcterms:rights <http://creativecommons.org/licenses/by/3.0/> .
```

이 RDF 데이터세트는 두 명명된 그래프를 포함하고 있다. 8행과 17행은 이 두 그래프의 이름을 명시한다. 명명된 그래프의 triple들은 중괄호(줄 9~15 행, 18~25 행) 사이에 넣는다. 선택적으로 그래프 이름 앞에 키워드 `GRAPH`를 사용할 수 있다. 이는 가독성을 향상시킬 수 있지만, 주로 [SPARQL Update](http://www.w3.org/TR/sparql11-update/)와 맞추기 위해 도입되었다.

상단의 선언문과 triple 구문은 Turtle 구문을 따른다.

27~29 행에 지정된 두 개의 triple은 명명된 그래프의 일부가 아니다. 이들은 이 RDF 데이터 세트의 명명되지 않은("기본") 그래프를 구성한다.

아래 그림은 이 예제의 결과로 triple들을 보여준다.

![Triples](images/rdf-primer/example-multiple-graphs-iris.jpeg)
<center>Fig. 5 TriG  예의 결과인 triple</center> <a id="fig-5"></a>

#### N-Quads <a id="n-quads"></a>
[N-Quads](http://www.w3.org/TR/n-quads/)는 RDF 데이터세트를 교환하기 위한 N-Triple에 대한 간단한 확장이다. N-Quads를 사용하면 행에 네 번째 요소를 추가하여 해당 행에 기술된 triple의 그래프 IRI를 캡처할 수 있다. 위의 TriG의 N-Quads 버전은 다음과 같다.

> Example 11: N-Quads
```
01    <http://example.org/bob#me> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://xmlns.com/foaf/0.1/Person> <http://example.org/bob> .
02    <http://example.org/bob#me> <http://xmlns.com/foaf/0.1/knows> <http://example.org/alice#me> <http://example.org/bob> .
03    <http://example.org/bob#me> <http://schema.org/birthDate> "1990-07-04"^^<http://www.w3.org/2001/XMLSchema#date> <http://example.org/bob> .
04    <http://example.org/bob#me> <http://xmlns.com/foaf/0.1/topic_interest> <http://www.wikidata.org/entity/Q12418> <http://example.org/bob> .
05    <http://www.wikidata.org/entity/Q12418> <http://purl.org/dc/terms/title> "Mona Lisa" <https://www.wikidata.org/wiki/Special:EntityData/Q12418> .
06    <http://www.wikidata.org/entity/Q12418> <http://purl.org/dc/terms/creator> <http://dbpedia.org/resource/Leonardo_da_Vinci> <https://www.wikidata.org/wiki/Special:EntityData/Q12418> .
07    <http://data.europeana.eu/item/04802/243FA8618938F4117025F17A8B813C5F9AA4D619> <http://purl.org/dc/terms/subject> <http://www.wikidata.org/entity/Q12418> <https://www.wikidata.org/wiki/Special:EntityData/Q12418> .
08    <http://example.org/bob> <http://purl.org/dc/terms/publisher> <http://example.org> .
09    <http://example.org/bob> <http://purl.org/dc/terms/rights> <http://creativecommons.org/licenses/by/3.0/> .
```

N-Quads 예제의 9개 행은 [Fig. 5](#fig-5)의 9개 아크에 대응된다. 1~7행은 quads를 나타내며, 여기서 첫 번째 요소는 그래프 IRI를 구성한다. 그래프 IRI 다음 쿼드 부분은 N-triples의 구문 규칙을 따라 문장의 subject, predicate와 object를 명시한다. 8행과 9행은 명명되지 않은(기본값) 그래프의 문을 나타내며, 네 번째 요소가 없으므로 정규 triple을 구성한다.

N-Triple과 마찬가지로 N-Quads는 일반적으로 대규모 RDF 데이터세트를 교환하고 라인 지향 텍스트 처리 도구를 사용하여 RDF를 처리하는 데 사용된다.

### JSON-LD <a id="json-ld"></a>
[JSON-LD](http://www.w3.org/TR/json-ld/)는 RDF 그래프 및 데이터세트에 대한 JSON 구문을 제공한다. JSON-LD를 사용하여 최소한의 변경만으로 JSON 문서를 RDF로 변환할 수 있다. JSON-LD는 JSON 객체에 대한 범용 식별자를 제공하며, JSON 문서가 데이터 타입과 언어 처리뿐만 아니라 웹 상의 다른 JSON 문서에 기술된 객체를 참조할 수 있는 메커니즘이다. JSON-LD는 또한 [@graph](http://www.w3.org/TR/json-ld/#named-graphs) 키워드를 사용하여 RDF 데이터세트를 직렬화하는 방법을 제공한다.

다음의 JSON-LD 예는 [Fig. 4](#fig-4) 그래프를 인코딩한다.

> Example 12: JSON-LD
```
01    {
02      "@context": "example-context.json",
03      "@id": "http://example.org/bob#me",
04      "@type": "Person",
05      "birthdate": "1990-07-04",
06      "knows": "http://example.org/alice#me",
07      "interest": {
08        "@id": "http://www.wikidata.org/entity/Q12418",
09        "title": "Mona Lisa",
10        "subject_of": "http://data.europeana.eu/item/04802/243FA8618938F4117025F17A8B813C5F9AA4D619",
11        "creator": "http://dbpedia.org/resource/Leonardo_da_Vinci"
12      }
13    }
```

2행의 `@context` 키는 문서가 RDF 그래프에 매핑되는 방법을 설명하는 JSON 문서를 가리킨다(아래 참조). 각 JSON 객체는 RDF 리소스에 대응한다. 이 예에서 기술하는 주 리소스는 `@id` 키워드를 사용하여 3행에 명시된 `<http://example.org/bob#me>`이다. `@id` 키워드는 JSON-LD 문서에서 키로 사용될 때 현재 JSON 객체에 해당하는 리소스를 식별하는 IRI를 가리킨다. 이 자원의 타입을 4행에, 생년월일을 5행에, 그리고 친구 중 하나를 6행에 기술하고 있다. 7행부터 12행까지 그의 관심사 중 하나인 Mona Lisa 그림을 기술한다.

이 그림을 설명하기 위해 7행에 새로운 JSON 객체를 만들고 8행에서 Wikidata의 Mona Lisa IRI와 연결시킨다. 그런 다음 9행부터 11행까지 그 그림의 다양한 특성을 설명한다.

이 예에서 사용된 JSON-LD 컨텍스트는 다음과 같다.

> Example 13: JSON-LD 컨텍스트 먕시
```
01    {
02      "@context": {
03        "foaf": "http://xmlns.com/foaf/0.1/", 
04        "Person": "foaf:Person",
05        "interest": "foaf:topic_interest",
06        "knows": {
07          "@id": "foaf:knows",
08          "@type": "@id"
09        },
10        "birthdate": {
11          "@id": "http://schema.org/birthDate",
12          "@type": "http://www.w3.org/2001/XMLSchema#date"
13        },
14        "dcterms": "http://purl.org/dc/terms/",
15        "title": "dcterms:title",
16        "creator": {
17          "@id": "dcterms:creator",
18          "@type": "@id"
19        },
20        "subject_of": {
21          "@reverse": "dcterms:subject",
22          "@type": "@id"
23        }
24      }
25    }
```

이 컨텍스트는 JSON-LD 문서를 RDF 그래프로 매핑하는 방법을 설명한다. 4행부터 9행까지는 3행에 정의된 FOAF 네임스페이스에서 `Person`, `interest`와 `knows`을 타입과 속성에 매핑하는 방법을 명시한다. 또한 8행에서 `@type`과 `@id` 키워드를 사용하여 `knows` 키에 IRI로 해석될 값이 있음을 명시한다.

10행에서 12행까지 `birthdate`를 schema.org 속성 IRI에 매핑하고 `xsd:date` 데이터 타입에 매핑할 수 있는 값을 명시한다.

14행부터 23행까지 `title`, `creator`와 `subject_of`를 Dublin Core 속성 IRI에 매핑하는 방법을 설명한다. 21행의 `@reverse` 키워드는 이 컨텍스트를 사용하여 JSON-LD 문서에서 `"subject_of": "x"`를 만날 때마다, 이것을 `x` IRI인 RDF triple에 매핑해야 하며, 그 속성은 `dcterms:subject`이고 그 객체는 부모 JSON 객체에 대응하는 리소스이다.

### RDFa <a id="rdfa"></a>
[RDFa](http://www.w3.org/TR/rdfa-primer/)는 HTML과 XML 문서 내에 RDF 데이터를 포함하기 위해 사용할 수 있는 RDF 구문이다. 이를 통해 검색 엔진은 웹을 탐색할 때 이 데이터를 집계하여 풍부한 검색 결과를 제공할 수 있다(예: [schema.org](http://schema.org/)와 [Rich Snippets](https://support.google.com/webmasters/answer/99170?hl=en) 참조).

아래의 HTML 예는 [Fig. 4](#fig-4)의 RDF 그래프를 인코딩한다.

> Example 14: RDFa
```
01  <body prefix="foaf: http://xmlns.com/foaf/0.1/
02                   schema: http://schema.org/
03                   dcterms: http://purl.org/dc/terms/">
04    <div resource="http://example.org/bob#me" typeof="foaf:Person">
05      <p>
06        Bob knows <a property="foaf:knows" href="http://example.org/alice#me">Alice</a>
07        and was born on the <time property="schema:birthDate" datatype="xsd:date">1990-07-04</time>.
08      </p>
09      <p>
10        Bob is interested in <span property="foaf:topic_interest"
11        resource="http://www.wikidata.org/entity/Q12418">the Mona Lisa</span>.
12      </p>
13    </div>
14    <div resource="http://www.wikidata.org/entity/Q12418">
15      <p>
16        The <span property="dcterms:title">Mona Lisa</span> was painted by
17        <a property="dcterms:creator" href="http://dbpedia.org/resource/Leonardo_da_Vinci">Leonardo da Vinci</a>
18        and is the subject of the video
19        <a href="http://data.europeana.eu/item/04802/243FA8618938F4117025F17A8B813C5F9AA4D619">'La Joconde à Washington'</a>.
20      </p>
21    </div>
22    <div resource="http://data.europeana.eu/item/04802/243FA8618938F4117025F17A8B813C5F9AA4D619">
23        <link property="dcterms:subject" href="http://www.wikidata.org/entity/Q12418"/>
24    </div>
25  </body>
```

위의 예는 HTML 내에서 RDF triple을 명시할 수 있는 `resource`, `property`, `type`과 `prefix` 네 특수한 RDFa 속성을 포함하고 있다.

1행의 `prefix` 속성은 Turtle 접두사와 유사한 방식으로 IRI 줄임말를 명시한다. 엄밀히 말하면, RDFa는 이 예에서 사용하는 접두사를 포함하는 [사전 정의된 접두사](http://www.w3.org/2011/rdfa-context/rdfa-1.1) 목록을 가지고 있기 때문에 이러한 특정 접두사를 생략할 수 있다.

4행과 14행의 `div` 요소는 이 HTML 요소 내에서 어떤 RDF 문을 만들 수 있는지에 대한 IRI를 명시하는 `resource` 속성을 가지고 있다. 4행의 `typeof` 속성의 의미는 Turtle의 줄임말 `(is) a`와 유사하다. subject `<http://example.org/bob#me>`는 클래스 `foaf:Person`의 인스턴스(`rdf:type`)이다.

6행에서 속성 `property`을 볼 수 있다. 이 속성 값(`foaf:knows`)dmf RDF 속성 IRI로 해석한다. `href` 속성 값(`http://example.org/alice#me`)을 triple의 객체로 해석한다. 따라서, 6행에서 오는 결과인 RDF 문장은 다음과 같다.

```
<http://example.org/bob#me> <http://xmlns.com/foaf/0.1/knows> <http://example.org/alice#me> .
```

7행에서 리터럴 값을 가진 triple을 객체로 본다. 속성 `property`는 HTML `time` 요소를 명시한다. HTML을 사용하려면 시간 요소의 내용이 유효한 [시간 값](http://www.w3.org/TR/html5/text-level-semantics.html#the-time-element)이어야 한다. 시간 요소의 내장된 HTML 의미론을 사용함으로써 RDFa는 명시적인 데이터 타입 선언 없이 `xsd:date`으로 값을 해석할 수 있다.

10~11행에서 triple의 객체를 명시하는 데에도 'resource' 속성이 사용되는 것을 볼 수 있다. 객체가 IRI이고 IRI 자체가 HTML 컨텐츠(예: `href` 속성)의 일부가 아닐 때 이 접근 방식을 사용한다. 16행에 `span` 속성으로 컨텐츠를 정의하는 리터럴("Mona Lisa")의 두 번째 예가 포함되어 있다. 만약 RDFa가 리터럴의 데이터 타입을 유추할 수 없다면, 데이터 터입을 `xsd:string`으로 가정한다.

RDF 문을 항상 HTML 문서 컨텐츠의 일부로 정의할 수 있는 것은 아니다. 이 경우 콘텐츠를 렌더링하지 않는 HTML 구문을 사용하여 triple을 지정할 수 있다. 예는 22~23행에서 찾아 볼 수 있다. 23행의 HTML `link` 요소를 여기서는 Europeana 비디오(22 행)의 주제를 명시하기 위하여 사용한다.

이 예에서는 RDFa의 사용을 [RDFa Lite](http://www.w3.org/TR/rdfa-lite/)로 제한하였다. [RDFA a Primer](http://www.w3.org/TR/rdfa-primer/)에서 RDFa에 대하여 보다 자세히 설명하고 있다.

### RDF/XML <a id="rdf-xml"></a>
[RDF/XML]()는 RDF 그래프에 대한 XML 구문을 제공한다. RDF가 1990년대 후반에 처음 개발되었을 때, 이것이 유일한 구문법이었고, 일부 사람들은 여전히 이 구문을 "RDF"라고 부른다. 2001년, "N3"라는 Turtle의 선구자가 제안되었고, 점차 여기에서 나열하고 있는 다른 언어들이 채택되고 표준화되었다.

아래의 RDF/XML 예는 [Fig. 4](#fig-4)에서 설명하는 RDF 그래프를 인코딩한 것이다.

> Example 15: RDF/XML
```
01    <?xml version="1.0" encoding="utf-8"?>
02    <rdf:RDF
03             xmlns:dcterms="http://purl.org/dc/terms/"
04             xmlns:foaf="http://xmlns.com/foaf/0.1/"
05             xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
06             xmlns:schema="http://schema.org/">
07       <rdf:Description rdf:about="http://example.org/bob#me">
08          <rdf:type rdf:resource="http://xmlns.com/foaf/0.1/Person"/>
09          <schema:birthDate rdf:datatype="http://www.w3.org/2001/XMLSchema#date">1990-07-04</schema:birthDate>
10          <foaf:knows rdf:resource="http://example.org/alice#me"/>
11          <foaf:topic_interest rdf:resource="http://www.wikidata.org/entity/Q12418"/>
12       </rdf:Description>
13       <rdf:Description rdf:about="http://www.wikidata.org/entity/Q12418">
14          <dcterms:title>Mona Lisa</dcterms:title>
15          <dcterms:creator rdf:resource="http://dbpedia.org/resource/Leonardo_da_Vinci"/>
16       </rdf:Description>
17       <rdf:Description rdf:about="http://data.europeana.eu/item/04802/243FA8618938F4117025F17A8B813C5F9AA4D619">
18          <dcterms:subject rdf:resource="http://www.wikidata.org/entity/Q12418"/>
19       </rdf:Description>
20    </rdf:RDF>
```

RDF/XML에서는 XML 요소 `rdf:RDF`를 사용하여 RDF triple를 명시한다(2행과 20행). `rdf:RDF`의 속성은 RDF 시작 태그(3-6 행)는 XML 요소 및 속성의 이름을 작성하기 위한 줄임말을 제공한다. XML 요소 `rdf:Description`(`http://www.w3.org/1999/02/22-rdf-syntax-ns#Description`의 줄임말)을 `about` 속성이 명시하는 IRI를 subject로 갖는 triple 집합을 정의하는 데 사용한다. 첫 번째 설명 블록(7-12 행)에는 네 개의 하위 요소가 있습니다. 하위 요소의 이름은 RDF 속성 즉, `rdf:type`을 나타내는 IRI이다(예:(line 8). 여기서 각 하위 요소는 하나의 triple을 표현한다. triple의 객체가 IRI인 경우 속성 하위 요소에는 컨텐츠가 없고 `rdf:resource` 속성(8, 10-11, 15, 18 행)을 사용하여 객체 IRI를 명시한다. 예를 들어, 10행은 3행과 일치한다.

```
<http://example.org/bob#me> <http://xmlns.com/foaf/0.1/knows> <http://example.org/alice#me> .
```

triple의 객체가 리터럴인 경우 속성 요소(9와 14행)의 컨텐츠로 리터럴 값을 입력한다. 데이터 타입을 속성 요소(9행)의 속성으로 지정한다. 데이터 타입이 생략되고(14행) 언어 태그가 없다면 리터럴은 `xsd:string` 데이터 타입으로 여겨진다.

위의 예는 기본 구문법을 보여주고 있다. [RDF/XML 문서](http://www.w3.org/TR/rdf-syntax-grammar/)에서 구문법에 대한 자세한 내용을 찾아 볼 수 있다. 일부 네임스페이스 접두사가 정의되었음에도 불구하고 속성 값에 전체 IRI가 포함되어 있는 것은 이상하게 보일 수 있다. XML 요소 및 속성 이름에만 이러한 접두사를 사용할 수 있기 때문이다.

## RDF 그래프 시멘틱 <a id="semantics-rdf-graphs"></a>
RDF의 가장 중요한 목표는 여러 소스의 유용한 정보를 자동으로 병합하여 여전히 일관적이고 유용한 더 큰 컬렉션을 형성하는 것이다. 이 병합의 시작점으로, 위에서 설명한 대로 동일하게 간단한 스타일인 subject-predicate-object triple로 모든 정보를 전한다. 그러나 정보를 일관되게 유지하기 위해서는 표준 구문 이상의 것이 필요하다. 또한 이러한 triple의 의미에 대한 동의가 필요하다.

이 시점에서 RDF의 의미를 직관적으로 파악할 수 있다.

- subject, predicate와 object를 명명하는 데 사용되는 IRI는 사용할 때마다 동일한 이름으로 명명하므로 범위 내에서는 "글로벌"이다.
- 각 triple은 subject와 object 사이에 predicate 관계가 실제로 존재할 때 정확히 "참"이다.
- RDF 그래프의 모든 triple이 모두 "참"일 때 RDF 그래프는 "참"이다.

이러한 개념들은 [RDF Semantics 문서](http://www.w3.org/TR/rdf11-mt)에 수학적으로 정밀하게 명시되어 있다.

이러한 선언적 의미론을 갖는 RDF의 이점 중 하나는 시스템이 논리적 추론을 할 수 있다는 것이다. 즉, 시스템이 참으로 받아들이는 특정 입력 triple 집합이 주어지면, 어떤 상황에서는 다른 triple도 논리적으로 참이어야 한다고 추론할 수 있다. 우리는 첫 번째 triple 세트가 추가 triple을 "포함"한다고 말한다. "추론자(reasoner)"라고 하는 이러한 시스템은 때때로 주어진 입력 triple이 서로 모순된다는 것도 추론할 수 있다.

사람들이 새로운 개념을 사용하고 싶을 때 새로운 용어를 만들 수 있는 RDF의 유연성을 고려할 때, 여러분이 하고 싶은 많은 종류의 추론이 있다. 특정한 종류의 추론이 많은 다른 응용 분야에서 유용해 보일 때, 그것을 [수반 체제(entailment regime)](http://www.w3.org/TR/rdf11-mt/#semantic-extensions-and-entailment-regimes)로 문서화할 수 있다. 몇몇 수반 체제는 RDF 시맨틱스에 명시되어 있다. 일부 다른 수반 체제와 SPARQL과 함께 그것을 사용하는 방법에 대하여 [SPARQL 1.1 Entailment Regimes](http://www.w3.org/TR/sparql11-entailment/)에서 기술적으로 설명하고 있다.  일부 수반 체제는 구현이 상당히 쉽고 추론이 신속하게 이루어질 수 있는 반면, 다른 것들은 효율적으로 구현하기 위해 정교한 기술을 요구한다.

예제로 다음 두 문장을 생각해보자.

```
ex:bob foaf:knows ex:alice .
foaf:knows rdfs:domain foaf:Person .
```

RDF 의미론 문서는 이 그래프에서 다음과 같은 triple을 유도할 수 있다고 한다.

```
ex:bob rdf:type foaf:Person .
```

위의 유도은 [RDF 스키마 수반](http://www.w3.org/TR/rdf11-mt)의 예이다.

RDF의 의미론도 triple은 다음과 같다.

```
ex:bob ex:age "forty"^^xsd:integer . 
```
리터럴이 XML 스키마의 정수 데이터 타입에 대해 정의된 제약 조건을 준수하지 않기 때문에 논리적 불일치가 발생한다.

RDF 도구는 일부 데이터 타입을 인식하지 못할 수 있다. 최소한 문자열 리터럴과 언어 태그 리터럴를 위한 데이터 타입을 지원하는 도구가 필요하다.

다른 많은 데이터 모델링 언어들과 달리, RDF 스키마는 모델링시 상당한 유연성을 제공한다. 예를 들어 동일한 엔터티를 클래스와 속성으로 모두 사용할 수 있다. 또한, "계급"의 세계와 "인스턴스"의 세계 사이에는 엄격한 구분이 없다. 따라서 RDF 시맨틱는 다음 그래프를 유효하다고 본다.

```
ex:Jumbo rdf:type ex:Elephant .
ex:Elephant rdf:type ex:Species .
```

따라서 `Elephant`는 (`Jumbo`를 샘플 인스턴스로 하는) 클래스와 (동물 종의 클래스) 인스턴스 모두될 수 있다.

이 절의 예는 RDF Semantics가 제공하는 것에 대한 느낌을 주기 위한 것이다. [RDF Semantics 문서](http://www.w3.org/TR/rdf11-mt)에서 보다 자세히 설명하고 있다.

## RDF 데이터 <a id="rdf-data"></a>
RDF를 사용하면 모든 소스의 triple을 그래프로 결합하여 RDF로 처리할 수 있다. 대량의 RDF 데이터를 [Linked Data](http://www.w3.org/DesignIssues/LinkedData.html)로부터 사용할 수 있습니다. 데이터세트는 RDF를 사용하여 웹에 게시와 연동되고 있으며, 그 중 상당수는 [SPARQL](https://sdldocs.gitlab.io/sparql/)를 통해 쿼리 기능을 제공한다. 위의 예에서 사용된 데이터세트의 예는 다음과 같다.

- [Wikidata](http://www.wikidata.org/), [Wikimedia Foundation](http://www.wikimedia.org/)이 운영하는 무료, 협업 및 다국어 데이터베이스
- [DBpedia](http://dbpedia.org/), [Eikipedia infodboxes](http://en.wikipedia.org/wiki/Help:Infobox)에서 추출한 데이터를 게시
- [WordNet](http://www.w3.org/2006/03/wn/wn20/), 영어 용어의 어휘 데이터베이스로 다양한 의미적 상호 관계를 가진 동의어 집합으로 그룹화하였으며 다른 언어에도 유사한 데이터베이스를 지원한다.
- [Europeana](http://www.europeana.eu/), 많은 유럽 기관으로부터 문화에 대한 데이터를 발행
- [VIAF](http://viaf.org/), 여러 국립 도서관 및 기타 기관에서 사람, 작품 및 지리적 위치에 대한 데이터를 게시

[datahub.io](http://datahub.io/dataset)는 Linked Data에서 사용할 수 있는 데이터세트 목록을 관리한다.

많은 어휘들이 RDF 데이터 소스간의 연결을 기록하는 데 널리 쓰이게 되었다. 예로 OWL 어휘에서 제공하는 `sameAs` 속성이다. 이 속성은 두 IRI가 실제로 동일한 리소스를 가리키고 있음을 나타내는 데 사용할 수 있다. 이는 다른 게시자가 각자 다른 식별자를 사용하여 동일한 것을 나타낼 수 있기 때문에 유용하다. 예를 들어 VIAF도 Leonardo da Vinci를 나타내는 IRI도 가지고 있다. `owl:sameAs`의 도움으로 이 정보를 기록할 수 있다.

> Example 16: 데이터세트간의 link
```
<http://dbpedia.org/resource/Leonardo_da_Vinci>
    owl:sameAs <http://viaf.org/viaf/24604287/> 
```

예를 들어 동일한 자원을 가리키는 IRI의 RDF 데이터를 병합하거나 비교함으로써 RDF 데이터 처리 소프트웨어가 이러한 링크를 사용할 수 있다.

## 유용한 정보 <a id="more-information"></a>
RDF에 대하여 간단히 소개하였다. [W3C Linked Data 페이지](http://www.w3.org/standards/semanticweb/data)를 살펴보는 것 또한 도움이 될 것이다.

<a name="footnote_1">1</a>: 이 페이지는 W3C의 [RDF 1.1 Primer](https://www.w3.org/TR/rdf11-primer/#section-rdfa)를 편역한 것이다.
