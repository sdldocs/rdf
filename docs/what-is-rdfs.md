# RDFS린? <sup>[1](#footnote_1)</sup>

**간단한 스키마로 얼마나 많은 이점을 얻을 수 있을까?**

RDFS 또는 [RDF Schema](https://www.w3.org/TR/rdf-schema/)는 RDF 어휘 및 데이터 모델을 설명하기 위한 W3C 표준 전문 어휘이다. 하지만 이에 대해 자세히 설명하기 전에 표준화된 전문 어휘(RDFS 자체 또는 다른 사람이 RDFS를 사용하여 설명하는 어휘)를 사용하는 것이 다른 사람과 어휘를 공유하여 상호 운용성을 쉽게 할 수 있다는 이점 외에 왜 유용한지 설명하고자 한다.

[RDF란](./what-is-rdf.md)에서 데이터 세트 예들에는 predicate가 W3C 표준 [vCard business card](https://www.w3.org/TR/vcard-rdf/) 온톨로지에서 가져온 triple이 포함되어 있었다. 또한 저자가 직접 도메인 이름으로 만든 네임스페이스의 triple도 포함되어 있었다. 특정 종류의 RDF 어플리케이션은 데이터를 검토하고, 그러한 어플리케이션을 위해 설계된 특수 어휘를 사용하는 predicate를 찾으면 해당 어휘를 중심으로 설계된 특수 작업을 실행한다. 예를 들어, `http://www.opengis.net/def/function/geosparql/` 네임스페이스에서 술어를 찾는 GeoSPARQL 응용 프로그램은 "뉴욕 현대미술관에서 1마일 이내에 어떤 박물관이 있는가?"와 같은 질문에 답하는 지리 공간 수학을 [GeoSPARQL queries on OSM Data in GraphDB](https://www.bobdc.com/blog/geosparqlgraphdb/)에서 설명한 대로 수행할 수 있다.

RDF를 사용하는 데는 스키마가 필요하지 않다. 그러나 RDFS 어휘를 이해할 수 있는 상용과 오픈 소스 도구(반드시 사용자가 정의한 어휘가 아니라 RDFS 어휘 자체를 의미함)를 사용하려면 어플리케이션에서 RDF 기반 어플리케이션을 중심으로 사용자 인터페이스를 구축하고 서로 다른 데이터 세트의 데이터를 통합하는 등의 작업을 더 쉽게 수행할 수 있다. 하지만 그 전에 RDF Schema의 예와 이를 사용하는 몇 가지 데이터를 살펴보도록 하자.

다음 RDFS Schema는 turtle 구문을 사용하여 몇 가지 클래스와 속성을 기술한다.

```t
# Employee schema version 1
# Pound sign lets you add comments to Turtle.
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> . 
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> . 
@prefix vcard: <http://www.w3.org/2006/vcard/ns#> .
@prefix emp:   <http://www.snee.com/schema/employees/> .

emp:Person   rdf:type rdfs:Class .
emp:Employee a        rdfs:Class .

vcard:given-name  a rdf:Property .
vcard:family-name a rdf:Property .
emp:hireDate      a rdf:Property .
emp:reportsTo     a rdf:Property .
```

가장 먼저 주목해야 할 점은 스키마 자체가 몇 가지 RDF 구조를 설명하기 위해 turtle RDF를 사용하는 triple이라는 점이다. 즉, SPARQL과 기타 RDF 도구를 사용하여 스키마 자체와 스키마 모음으로 작업할 수 있다.

두 번째로 주목해야 할 점은 스키마가 얼마나 간단할 수 있는가 하는 것이다. 이 경우에는 "다음은 잠재적으로 사용할 수 있는 몇 가지 클래스와 속성이다"라고 말하는 6개의 triple만 있으면 된다.

`rdf:type` predicate는 "다음 클래스의 인스턴스"를 의미하므로 위의 첫 번째 triple은 `emp:Person` 자체가 클래스라는 것을 나타낸다. (아래에서 `emp:Person`의 인스턴스를 만드는 방법을 살펴보겠다.) 이 스키마의 다음 triple은 `emp:Employee`도 클래스라고 한다. 이 줄에서는 `rdf:type` predicate 대신 "`a`"라는 단축키를 사용한다. 이는 같은 의미이지만, triple을 영어 표현인 "`emp:Employee` is a class"에 더 가깝게 만드는 구문을 사용한다.

이 예제에서 나머지 4개의 triple은 사용 가능한 몇 가지 속성을 나열한다. vCard 어휘에서 두 개를 복사하고 두 개는 직접 만들었다.

## 스키마 사용
다음 인스턴스 데이터는 위에서 선언한 클래스와 속성(properties)을 사용한다.

```t
@prefix vcard: <http://www.w3.org/2006/vcard/ns#> .
@prefix emp:   <http://www.snee.com/schema/employees/> .
@prefix ex:    <http://www.snee.com/example/> .

ex:id1 a emp:Person ; 
       vcard:given-name  "Francis" ;
       vcard:family-name "Jones" .

ex:id2 a emp:Employee ;
       vcard:given-name  "Heidi" ;
       vcard:family-name "Smith" ;
       emp:hireDate      "2015-01-13" .

ex:id3 a emp:Employee ; 
       vcard:given-name  "Jane" ;
       vcard:family-name "Berger" ;
       emp:reportsTo     ex:id2 . 
```

이 triple은 다른 Turtle 구문을 사용한다. "`;`"은 "다음 triple이 최종 triple과 동일한 subject를 갖음"을 의미gks다. 예를 들어, 이 샘플 데이터의 prefix 선언 뒤의 처음 세 줄은 resource `sn:id1`이 `Person` 클래스의 인스턴스이며, 이름이 `Francis`이고, 성이 `Jones`임을 나타낸다.

위의 스키마는 많은 것을 말해주지는 않지만 적어도 관계형 테이블의 열 목록만큼은 유용하다. 이 스키마를 가지고 있고 이 데이터로 작업하는 사람은 데이터를 쿼리하거나, 추가하거나, 삭제하려는 경우 어떤 속성 이름을 사용해야 하는지 알고 있다. 또한 잠재적인 클래스가 무엇인지 알고 있으며 해당 클래스의 인스턴스를 쿼리할 수 있다. 이러한 모든 기능은 여러 사람이 상호 운용 가능한 데이터와 어플리케이션을 개발하려는 경우 큰 도움이 된다.

## 스키마에 추가
동일한 스키마의 다음 버전에서는 클래스와 속성에 대한 자세한 정보를 제공하여 조금 더 발전했다.

```t
# Employee schema version 2
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> . 
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> . 
@prefix vcard: <http://www.w3.org/2006/vcard/ns#> .
@prefix emp:   <http://www.snee.com/schema/employees/> .

emp:Person rdf:type rdfs:Class ;
           rdfs:label "person" . 

emp:Employee a rdfs:Class ; 
             rdfs:label "employee" ;
             rdfs:comment "A full-time, non-contractor employee." .

vcard:given-name  rdf:type rdf:Property ;
                  rdfs:label "given name".

vcard:family-name rdf:type rdf:Property ;
                  rdfs:label "family name" ;
                   rdfs:label "apellido"@es . 

emp:hireDate a rdf:Property ;
             rdfs:label   "hire date" ;
             rdfs:comment "The first day an employee was on the payroll." .

emp:reportsTo a rdf:Property ; 
              rdfs:label  "reports to" .
```

이 버전에는 `rdfs:comment`와 `rdfs:label` 속성이 포함되어 있다. 전자는 설명하는 대상에 대한 문서 역할을 한다. `emp:Employee` 리소스에 대한 `rdfs:comment` 값과 같이 설명된 리소스가 정확히 무엇을 의미하는지에 대한 명확성을 제공해야 한다. 즉, “A full-time, non-contractor employee.”

`rdfs:label` 속성은 설명하는 리소스에 대해 사람이 읽을 수 있는 이름을 제공한다. 이 속성은 이 데이터를 사용하는 보고서와 어플리케이션에 특히 유용하다. 예를 들어 어플리케이션에서 사람들이 직원에 대한 데이터를 편집할 수 있는 양식을 표시하는 경우, 해당 필드에 "vcard:given-name" 또는 "emp:hireDate"같은 실제 속성 이름으로 레이블을 지정하면 이러한 최종 사용자가 양식을 읽기 어려울 수 있다. 반면에 어플리케이션 코드에 "hire date"과 "family name"같이 읽기 쉬운 양식 필드 이름을 하드코딩해서는 안 된다.

실제 [모델 중심 개발](https://martinfowler.com/bliki/ModelDrivenSoftwareDevelopment.html)의 경우 스키마에 의해 인코딩된 모델이 발전함에 따라 어플리케이션이 가능한 한 이러한 발전에 자동으로 적응하도록 설정하고 싶을 수 있다. 모델의 일부로 표시 이름을 제공하면 어플리케이션이 이 목표를 향해 나아가는 데 도움이 된다. 개정된 버전의 샘플 스키마를 사용하는 어플리케이션은 "family name"과 "given name"같은 `rdfs:label` 값을 사용하여 훨씬 더 읽기 쉬운 양식 필드 레이블을 제공할 수 있다.

또한 RDF(따라서 RDFS)를 사용하면 리터럴 값에 언어 태그를 추가할 수 있다. RDF 리소스에 여러 개의 `rdfs:label` 값을 추가하고 각 값에 언어에 따라 태그를 지정하면 위에서 설명한 모델 중심 개발이 다양한 사용자를 위한 다양한 언어로 된 양식 생성으로 확장될 수 있다. 두 번째 버전의 스키마에서 `vcard:family-name` 리소스에는 영어와 스페인어로 된 레이블이 있다. (향후 버전의 스키마에는 다른 클래스와 속성에도 스페인어 레이블이 있어야 한다.) 스키마의 데이터를 기반으로 특정 양식을 미국 영어, 영국 영어, 카스티야 스페인어, 멕시코 스페인어 등으로 표시할 수 있도록 국가별 용어 버전에 대한 언어 코드를 포함할 수도 있다.

여기서는 RDFS 스키마에서 `rdfs:label`과 `rdfs:comment` 값을 사용하고 있지만, 원하는 RDF에서도 사용할 수 있다는 점을 기억하세요. 예를 들어

```t
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> . 
@prefix vcard: <http://www.w3.org/2006/vcard/ns#> .
@prefix emp:   <http://www.snee.com/schema/employees/> .
@prefix ex:    <http://www.snee.com/example/> .

ex:id3 a emp:Employee ;
       vcard:given-name "Jane" ;
       vcard:family-name "Berger" ;
       rdfs:label "Jane Berger" ;
       rdfs:comment """Jane has taken the sales department from being only her and an assistant to the ten-person team we have today.""" .
```

(여기서 이 `rdfs:comment` 값은 긴 리터럴로 표시되며, 값에 캐리지 리턴이 포함될 수 있도록 값을 큰따옴표로 묶는다.) 마찬가지로 RDFS 스키마뿐만 아니라 원하는 모든 RDF 리터럴 값에 언어 태그를 추가할 수 있다.

## schema.org
다음 글에서는 RDFS로 할 수 있는 좀 더 멋진 모델링과 데이터 통합 및 모바일 어플리케이션같은 어플리케이션을 어떻게 도울 수 있는 지에 대해 설명하려고 한다. 또한 스키마 기반 소프트웨어 개발과 스키마 없는 개발에 대한 논쟁에서 부분 스키마를 사용하면 두 가지 장점을 모두 얻을 수 있는 방법에 대해서도 언급할 것이다.

한편, [schema.org](https://schema.org/)의 RDFS 스키마를 살펴보세요. [Schema.org for Developers](https://schema.org/docs/developers.html) 페이지의 [Vocaburary Definition Files](https://schema.org/docs/developers.html) 섹션에서 원하는 변형과 직렬화를 선택할 수 있다. 여기서 설명한 내용을 스키마가 어떻게 보여주는지 확인하기 위해 [turtle 직렬화](https://schema.org/version/latest/schemaorg-current-https.ttl)를 선택했다.

대부분 `rdfs:label` 값과 설명적인 `rdfs:comment` 값을 가진 클래스와 속성의 선언이기 때문에 turtle 버전의 schema.org 스키마를 많이 알아볼 수 있을 것이다. Schema.org는 RDFS 개발을 위한 훌륭한 역할 모델을 제공하며, 이 모든 것이 OWL 없이 가능하다! 15년 전에는 OWL이 섞이지 않은 채 RDFS가 사용된 예를 찾기가 어려웠는데, 그 이후로 Schema.org가 진정한 영감을 주었다고 생각한다.

이제부터는 특정 RDF 용어 집합이 사용되는 것을 보면 "이를 문서화한 스키마를 어디에서 찾을 수 있나?"라고 물어볼 수 있다. 그리고 모델을 설명하는 스키마(또는 OWL 온톨로지)를 발견하면 "이 스키마를 따르는 샘플 데이터는 어디에서 볼 수 있나?"라고 질문하세요. (Schema.org 샘플 데이터는 JSON-LD로 되어 있는 경우가 많지만, turtle로 충분히 쉽게 변환할 수 있다.)


<a name="footnote_1">1</a> 이 페이지는 [What is RDFS?](https://www.bobdc.com/blog/whatisrdfs/)을 편역한 것임.
