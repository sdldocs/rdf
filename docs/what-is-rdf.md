# RDF린? <sup>[1](#footnote_1)</sup>

**이 간단한 표준화 모델을 통해 얻을 수 있는 이점은 어떤 것일까?**

> **Note**
> 보통 이 글을 읽는 사람들은 RDF가 무엇인지 이미 알고 있다고 가정하고 있다. 최근 링크된(오픈) 데이터와 지식 그래프 분야에서 RDF를 접하는 사람들과 논의한 결과, 우리가 참조할 수 있는 간단한 설명이 있으면 유용할 것 같다는 생각이 들었다. 이 설명은 동영상 [SPARQL in 11 Minutes](https://www.youtube.com/watch?v=FvGndkpa4K0)의 처음 3분 동안의 자료를 기반으로 한 것이다.

RDF(Resource Description Framework)는 간단하고 유연한 데이터 모델을 위한 (HTML, CSS와 XML과 같은) [W3C 표준](https://www.w3.org/RDF/)이다. RDF를 사용하면 “employee 3 has a title of ‘Vice President’.” 와 같이 세 부분으로 구성된 문을 사용하여 데이터를 설명할 수 있다. 이 세 부분을 subject, predicate와 object고 부른다. 이를 엔터티 식별자(entity identofier), 속성 이름(attribute name), 속성 값(attrobute value)으로 생각할 수 있다.

![](./images/what-is-rdf/emp3TitleVP.png)

subject와 predicate는 실제로 URI(Uniform Resource Identifier)를 사용하여 표현되므로 어떤 내용을 말하는지 명확하게 알 수 있다. URI는 URL(Uniform Resource Locator)과 유사하며 종종 URL과 비슷하게 보이지만 위치나 주소가 아니라 식별자에 불과하다.

다음의 URI가 이를 보인다.

- 특정 회사의 "employee 3"을 의미한다.
- 책, 영화 또는 기타 창작물에 대한 레이블이 아닌 직책의 의미로 "title"을 의미하며, 이는 W3C가 발표한 버전의 [vCard business card](https://www.w3.org/TR/vcard-rdf/)에 정의된 제목에 대한 URI를 사용하기 때문이다.

![](./images/what-is-rdf/emp3TitleVPURIs.png)

object 또는 triple의 세 번째 부분도 URI일 수 있다.

![](./images/what-is-rdf/emp8reportsToemp3.png)

이렇게 하면 동일한 리소스가 일부 triple의 object가 되기도 하고 다른 triple의 subject가 되기도 하여 triple을 그래프라는 데이터 네트워크로 연결할 수 있다.

![](./images/what-is-rdf/emp3ReportsToemp8Graph.png)

RDF에서 널리 사용되는 Turtle 구문은 종종 마지막 부분 앞에 약식 접두사가 URI의 모든 부분을 대신하도록 하여 URI를 단축한다. 이렇게 하면 URI를 더 쉽게 읽고 쓸 수 있다.

```t
@prefix vcard: <http://www.w3.org/2006/vcard/ns#> .
@prefix sn:    <http://www.snee.com/hr/> .

sn:emp3 vcard:title "Vice President" . 
```

거의 모든 데이터를 triple의 컬렉션으로 표현할 수 있다. 예를 들어, 일반적으로 행 식별자를 subject로, 열 이름을 predicate로, 값을 object로 사용하여 테이블의 각 항목을 나타낼 수 있다.

![](./images/what-is-rdf/tableWithTriple.png)

이렇게 하면 테이블에 있는 모든 사실에 대해 triple를 얻을 수 있다.

```t
@prefix vcard: <http://www.w3.org/2006/vcard/ns#> .
@prefix sn: <http://www.snee.com/hr/> .

sn:emp1   vcard:given-name   "Heidi" .
sn:emp1   vcard:family-name   "Smith" .
sn:emp1   vcard:title   "CEO" .
sn:emp1   sn:hireDate   "2015-01-13" .
sn:emp1   sn:completedOrientation   "2015-01-30" .

sn:emp2   vcard:given-name   "John" .
sn:emp2   vcard:family-name   "Smith" .
sn:emp2   sn:hireDate   "2015-01-28" .
sn:emp2   vcard:title   "Engineer" .
sn:emp2   sn:completedOrientation   "2015-01-30" .
sn:emp2   sn:completedOrientation   "2015-03-15" .

sn:emp3   vcard:given-name   "Francis" .
sn:emp3   vcard:family-name   "Jones" .
sn:emp3   sn:hireDate   "2015-02-13" .
sn:emp3   vcard:title   "Vice President" .

sn:emp4   vcard:given-name   "Jane" .
sn:emp4   vcard:family-name   "Berger" .
sn:emp4   sn:hireDate   "2015-03-10" .
sn:emp4   vcard:title   "Sales" .
```

여기에 있는 속성 이름 중 일부는 vcard 표준 어휘에서 가져온 것이다. vcard 또는 다른 표준 어휘에서 사용할 수 없는 속성의 경우, 자신의 도메인 이름을 사용하여 직접 속성 이름을 만들었다. [schema.org](https://schema.org/docs/schemas.html), [geonames](https://www.geonames.org/ontology/documentation.html)와 [Dublin Core](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/)같은 다른 많은 표준 어휘는 용어의 정확한 의미를 명확하게 파악하는 데 도움이 되는 URI를 제공한다. (한 가지 예로, [책을 지칭하기](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/title) 위해 "title"이라는 용어를 사용하려면 Dublin Core를 사용했을 것이다.) RDF를 사용하면 표준 어휘와 커스텀 어휘를 쉽게 혼합하고 일치시킬 수 있다.

위 예의 데이터는 표시된 표에 깔끔하게 들어맞는다. 이 데이터가 관계형 테이블에 있고 Heidi Smith의 대학 학위에 대한 정보를 추가하고 싶다고 가정해 보겠다. 관계형 테이블을 사용하면 테이블에 새 열을 추가해야 하므로 데이터베이스 관리자가 데이터베이스 자체에 구조적 변경을 해야 할 것이다. RDF를 사용하면 triple 하나를 더하면 된다.

```T
sn:emp1 sn:degree "MFA University of Iowa 2015" .
```

데이터베이스 관리자가 관계형 테이블에 학위 열을 추가했는데 이제 Heidi가 데이터에 설명할 학위가 하나 더 추가되었다고 가정해 보자. 학위 열에는 하나의 학위 설명만 저장할 수 있으므로 관계형 데이터베이스에서 직원이 두 개 이상의 학위를 보유할 수 있도록 하려면 데이터베이스 관리자는 해당 테이블에서 새 학위 열을 제거한 다음 완전히 새로운 두 테이블을 만들어 직원과 학위 간의 관계를 추적해야 할 것이다. RDF에서는 triple이 하나 더 추가되는 것이다.

```T
sn:emp1 sn:degree "MBA Wharton 2019" .
```

URI가 아닌 triple object를 *리터럴*이라고 한다. 지금까지 살펴본 예에서 리터럴은 모두 문자열이지만 다른 데이터 타입일 수도 있다. 리터럴은 boolean, integer, float 등의 [XSD data types](https://www.w3.org/TR/xmlschema-2/)일 수도 있고, 사용자가 직접 정의한 데이터 타입일 수도 있다.

```T
@prefix sn:  <http://www.snee.com/hr/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

sn:emp1 sn:startDate "2021-03-04"^^xsd:date . 
sn:emp1 sn:empCode   "D1"^^sn:myCustomDataType . 
```

## RDF 구문
앞서 RDF는 표준화된 데이터 모델이라고 설명하였다. 이를 기록하기 위한 다양한 구문이 있었다. 원래 [RDF/XML](https://www.w3.org/TR/rdf-syntax-grammar/)이라고 불렀는데, 표준화되고 유연하다는 점과 원래 RDF 사용 사례 중 하나가 웹 페이지에 임의의 메타데이터를 추가하는 것이었기 때문에 XML을 사용했다. 이는 XML의 추가 블록이 HTML 파일의 헤드 요소에 잘 맞을 것이라는 생각이었다. 하지만 XML을 사용하여 임의의 관계 모음을 표현하는 것은 장황하고 지저분해질 수 있었다. 이 때문에 지금은 아무도 RDF/XML을 사용하지 않지만, 안타깝게도 초창기에는 이 특정 구문으로 인해 RDF 자체가 나쁜 평판을 얻었다. (저자 생각에는 RDF/XML 파일에 ".rdf"라는 확장자를 부여하는 파일 명명 규칙 때문에 사람들이 RDF가 진짜라고 생각하게 되었다고 생각한다.)

이제 대부분의 사람들이 훨씬 간단하고 [W3C 표준](https://www.w3.org/TR/turtle/)이기도 한 turtle을 사용한다. 점점 인기를 얻고 있는 JSON-LD를 비롯한 [다른 구문](https://en.wikipedia.org/wiki/Resource_Description_Framework#Serialization_formats)도 사용할 수 있다. 이 글에서 모든 예는 turtle 구문을 사용한다.

## SPARQL
SPARQL("SPARQL Protocol and RDF Query Language")은 또 다른 W3C 표준입니다. 프로토콜 부분은 일반적으로 서로 다른 컴퓨터 간에 SPARQL 쿼리를 주고받는 프로그램을 작성하는 사람들에게만 문제가 된다.

SPARQL 쿼리는 일반적으로 데이터 세트에서 어떤 종류의 triple을 검색할 것인지에 대한 패턴을 설명하기 위해 turtle과 유사한 구문을 사용한다. 이 패턴은 종종 turtle triple과 유사하지만, 일치 패턴에 유연성을 더하고 일치 결과 값을 저장하기 위해 와일드카드 역할을 하는 변수를 사용한다. 다음 쿼리는 직책(job title)이 “Vice President”인 모든 사람의 이름과 성을 요청한다.

```t
PREFIX  vcard: <http://www.w3.org/2006/vcard/ns#>
PREFIX  sn:    <http://www.snee.com/hr/>

SELECT ?givenName ?familyName
WHERE
  { ?employee vcard:title "Vice President" .
    ?employee vcard:given-name  ?givenName .
    ?employee vcard:family-name ?familyName .
  }
```

동영상 [SPARQL in 11 Minutes](https://www.youtube.com/watch?v=FvGndkpa4K0)에서 간단한 SPARQL 쿼리의 더 많은 예를 볼 수 있다.

## Triplestores
Triplestore는 RDF triple을 위한 데이터베이스 관리자이다. 다양한 오픈 소스와 상용 triplestore가 있으며, 그중 일부는 수십억 개의 triple을 저장할 수 있다. 일반적으로 웹 기반 그래픽 사용자 인터페이스와 프로그래밍 방식으로 데이터를 추가, 편집, 검색할 수 있는 방법을 모두 제공한다.

"SPARQL"에서 "프로토콜"의 "P"는 일부 프로그래밍 인터페이스의 기초이다. 이는 RDF로 작업하기 위한 도구가 모두 공개적으로 공개된 표준을 기반으로 하며 광범위한 구현에서 지원된다는 것을 보여주는 또 다른 예이다.

## 데이터 통합
[W3C RDF 개요](https://www.w3.org/RDF/) 페이지의 두 번째 문장에 따르면 "RDF에는 기본 스키마가 다르더라도 데이터를 쉽게 병합할 수 있는 기능이 있으며, 특히 모든 데이터 소비자가 변경할 필요 없이 시간이 지남에 따라 스키마의 진화를 지원한다."라고 설명되어 있다. 가장 간단한 수준에서는 서로 다른 두 개의 RDF 데이터 집합을 서로 연결하기만 하면 통합할 수 있으며, 두 데이터 집합이 모두 turtle 또는 [N-triples](https://www.w3.org/TR/n-triples/)같은 구문을 사용한다고 가정하면 된다. 데이터 세트가 동일한 구문을 사용하는지 여부에 관계없이 여러 데이터 세트를 동일한 triplestore 데이터 세트에 로드하는 것도 쉽고 널리 사용된다.

이러한 데이터 통합의 용이성은 사람들이 다양한 다른 형식과 모델의 데이터를 RDF로 변환하여 쉽게 조합하여 사용할 수 있기 때문에 RDF가 성공할 수 있었던 큰 원동력이 되었다. ("[RDFS란?](./what-is-rdfs.md)"에서 RDF 스키마가 어떻게 이 작업을 더욱 쉽게 만들어주는 선택적 모델을 정의할 수 있는지 설명한다.)

## Semantic Web
RDF 초창기에는 기계가 읽을 수 있는 데이터를 World Wide Web에서 "Semantic Web"이라는 이름으로 공유한다는 아이디어가 기계가 사물(things)을 "이해"한다는 모호한 구식 인공 지능 아이디어와 혼동되어 과대 포장되었다고 할 정도로 인기가 있었다. 위에서 "title"이 책의 레이블이 아닌 "job title"이라는 의미로 사용되는 것을 보았는데, 이는 유용하고 기계가 읽을 수 있는 방식으로 단어의 일부 의미, 즉 시맨틱을 나타낸다.

"RDFS란?"에서는 triple을 통해 Heidi Smith가 Employee 클래스의 인스턴스임을 표시하는 방법과 Employee가 Person의 하위 클래스인 경우 Heidi도 Person의 인스턴스이며 관련 속성을 가지고 있음을 추론할 수 있는 방법을 살펴볼 것이다. OWL을 사용하면 더 많은 작업을 수행할 수 있다. 이러한 작은 의미론은 매우 유용할 수 있지만, 이러한 의미론으로 연결된 웹의 가능성과 최종 사용자 어플리케이션을 위한 플랫폼으로서의 웹의 잠재적 운명에 대한 과대 광고로 인해 '시맨틱 웹'이라는 용어가 유행에서 멀어졌다.

## Linked (Open) 데이터
무엇이 Linked Data로 간주되는지에 대한 표준 사양은 없다. 많은 사람들이 웹 발명가이자 W3C 디렉터인 Tim Berners-Lee가 "개인적인 견해일 뿐"이라는 경고와 함께 작성한 [디자인 이슈 문서](https://www.w3.org/DesignIssues/LinkedData.html)를 지적한다. 이 문서에는 여러 플랫폼에서 기계 판독 가능한 데이터를 공유하기 위한 몇 가지 규칙과 모범 사례가 요약되어 있다.

문서의 네 가지 Linked Data 규칙 아래에는 이 용어가 널리 사용되는 방식을 반영하는 “5 Stars of Linked Data”이 열거되어 있다. 여기에는 별 다섯 개짜리 Linked Data는 아니더라도 웹 서버에서 사용할 수 있는 CSV 파일도 Linked Data로 간주될 수 있다는 가능성이 포함되어 있으며, 이는 Linked Data의 아이디어에는 감탄하지만 어떤 구문, 특히 RDF/XML의 RDF를 반드시 좋아하지는 않는 많은 사람들에게 어필했다. 일반적으로 Linked Data는 데이터 자체의 구문보다는 URI와 URL을 사용하여 기계가 읽을 수 있는 데이터를 공유하는 데 더 중점을 둔다.

많은 조직에서 플랫폼 간에 데이터를 공유하기 위한 Linked Data 원칙이 방화벽 뒤에서 데이터 통합을 사용하는 데 도움이 된다는 사실을 발견했다. Linked Open Data는 이러한 원칙을 전 세계와 공유되는 데이터에 적용한다. Berners-Lee의 문서에서는 Linked Open Data를 "무료로 재사용하는 데 방해가 되지 않는 오픈 라이선스에 따라 공개된 링Linked Data"라고 설명하며, 일반적으로 모든 사람이 액세스할 수 있는 공용 인터넷에서 공유되는 데이터를 의미한다.

Linked Data의 공개 여부와 관계없이, Linked Data 원칙을 사용하여 데이터를 공유하는 모범 사례에 대해 배울 수 있는 온라인 책인 Leigh Dodds와 Ian Davis의 [Linked Data Patterns](https://patterns.dataincubator.org/book/)을 참고하시기 바란다. Jonathan Blaney의 [Introduction to the Principles of Linked Open Data](https://programminghistorian.org/en/lessons/intro-to-linked-data)도 좋은 배경 지식을 제공한다.

## 지식 그래프 (Knowledge Graph)
RDF 삼각형이 어떻게 그래프로 결합되는지 살펴보았다. 그래프 데이터 구조는 컴퓨터 과학 그 자체보다 더 오래되었다. "지식 그래프"라는 용어는 몇 년 전부터 사용되어 왔지만, 특히 2012년 Google의 엔지니어링 수석 부사장이 [Introducing the Knowledge Graph: things, not strings](https://blog.google/products/search/introducing-knowledge-graph-things-not/)이라는 글을 발표하면서 널리 알려지게 되었다. 그 후 다양한 종류의 그래프 데이터 도구로 작업하는 많은 사람들이 "Google은 데이터를 지식 그래프에 저장한다고?"라고 묻기 시작했다. 우리도 그렇게 하고, 여러분도 할 수 있다!"라고 말하기 시작했다. RDF 기반 시스템은 데이터를 그래프로 저장하고 의미를 저장하기 위한 다양한 옵션을 포함하므로 지식 그래프를 저장하는 데 매우 적합하다. 또한 데이터 통합의 용이성은 여러 개의 그래프를 부분의 합보다 더 큰 전체로 병합하려는 지식 그래프에 관심이 있는 사람들에게 매력적이다. 이에 대한 자세한 내용은 [지식 그래프!](http://www.bobdc.com/blog/knowledgegraphs/)에 작성하였다.

## RDF와 여러분
위에서 설명한 접근 방식 중 하나를 통해 RDF에 대해 처음 알게 되었다면, 이 글을 통해 RDF가 해왔고 할 수 있는 일에 대해 더 넓은 맥락을 이해하였기를 바란다. RDF와 SPARQL은 상용과 오픈 소스 세계에서 많은 구현이 이루어진 개방형 표준이라는 점을 기억하는 것이 중요하다. 학계에서 인기가 높기 때문에 많은 사람들이 이러한 표준이 학계에만 국한되어 있다고 비난하지만, 이는 사실이 아니다. 전 세계의 [유명 기업](http://sparql.club/)들이 이러한 표준의 가치를 알아보고 그 사용을 늘리고 있다.

끝으로 [Dan Brickley](https://twitter.com/danbri)와 [Libby Miller](https://twitter.com/libbymiller)의 저서 [RDF 데이터 유효성 검사](http://book.validatingrdf.com/bookHtml005.html)의 한 구절을 인용하고자 한다.

> *사람들은 RDF가 복잡하기 때문에 힘들다고 생각한다. 사실은 훨씬 더 복잡하다. RDF는 고통스러울 정도로 단순하지만, 끔찍할 정도로 복잡한 실제 데이터와 문제를 다룰 수 있게 해준다. RDF를 피할 수는 있지만 복잡한 데이터와 복잡한 컴퓨터 문제를 피하기는 더 어렵다.*


<a name="footnote_1">1</a> 이 페이지는 [What is RDF?](https://www.bobdc.com/blog/whatisrdf/)을 편역한 것임.