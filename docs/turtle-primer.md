# RDF Primer - Turtle Version<sup>[1](#footnote_1)</sup>

## 요약
RDF(Resource Description Framework)는 World Wide Web 상의 리소스에 대한 정보를 표현하는 언어이다. 이 문서는 RDF를 효과적으로 사용하기 위하여 필요한 기본 지식을 제공한다. RDF Vocabulary Description Language를 사용하여 RDF 어휘를 정의하는 방법을 설명한다. 또한, RDF의 기본 개념을 소개하고 Turtle 직렬화(serialization) 구문을 설명한다.

[이 문서의 원 버전](rdf-primer.md)은 2004년 2월에 발표되었고, RDF 권장 사항의 일부였으며 [RDF의 RDF/XML 직렬화 구문](http://www.w3.org/TR/rdf-syntax-grammar/)을 기반으로 작성되었다. 원 문서의 텍스트는 이 문서를 위해 Turtle을 사용하였으며 (RDF/XML으로 외부 기관에서 정의한) 일부 어플리케이션 예들을 삭제하였다.

> **NOTE**<br>
> 본 문서에서는 URI(Uniform Resource Indentifier)를 사용하고 있으나 최근의 문서들에서는 IRI(International Resource Identifier)를 사용하고 있다. 두 용어는 비슷한 개념으로 동일하게 해석할 수 있다. IRI의 개념은 URI(Uniform Resource Identifier)를 일반화한 것으로 IRI 문자열에서는 ASCII 문자가 아닌 문자의 사용을 허용한다. RFC 3987에서 IRI를 명시하고 있다. 단 문서에서는 원본에 표시한 대로 번역하였으나 대체하여 해석하여도 큰 무리가 없을 것이다.

## 서론 <a id="introduction"></a>
RDF(Resource Description Framework)는 World Wide Web 상의 리소스에 대한 정보를 표현하는 언어이다. 특히 웹 페이지의 제목, 작성자, 수정 날짜, 웹 문서에 대한 저작권과 라이선스 정보 또는 일부 공유 리소스의 가용 일정같은 웹 리소스에 대한 메타데이터를 나타내기 위한 것이다. 그러나 "웹 리소스"의 개념을 일반화함으로써 RDF는 웹에서 직접 검색할 수 없는 경우에도 웹에서 식별할 수 있는 정보를 나타내기 위하여 사용할 수도 있다. 그 예로는 온라인 쇼핑 사이트에서 나타난 품목에 대한 정보(즉 사양, 가격과 재고에 대한 정보) 또는 정보 전달을 위한 웹 사용자의 선호도에 대한 기술 등이다.

RDF는 이 정보를 사람에게 보여주기 보다는 어플리케이션에서 처리해야 하는 상황을 위한 것이다. RDF는 이 정보를 표현하기 위한 공통 프레임워크를 제공하여 의미 손실 없이 어플리케이션 간에 정보를 교환할 수 있다. 공통 프레임워크이기 때문에 애플리케이션 설계자는 공통 RDF 파서와 처리 도구의 가용성을 극대화할 수 있다. 서로 다른 어플리케이션 간에 정보를 교환하는 기능은 정보가 원래 생성된 어플리케이션이 아닌 다른 어플리케이션에서 해당 정보를 사용할 수 있음을 의미하는 것이다.

RDF는 웹 식별자(*Uniform Resource Identifier* 또는 *URI*라고 함)를 사용하여 사물을 식별하고, 단순한 속성과 속성 값으로 리소스를 기술한다는 아이디어에서 시작되었다. 이를 통해 RDF는 리소스를 표현하는 간단한 문장을 리소스, 해당 속성과 값을 나타내는 노드와 호(arc)의 그래프로 나타낼 수 있다. 가능한 한 빨리 이 논의를 좀 더 구체화하기 위해 "`http://www.w3.org/People/EM/contact#me`로 식별되는 사람이 있으며 이름은 `Eric Miller`이고 이메일 주소는 `em@w3.org`이며 직함은 `Dr.`"를 RDF 그래프 [Fig. 1](#fig-1)로 나타낼 수 있다.

<center>![fig1](images/turtle-primer/fig1.png)<br>
Fig. 1 Eric Miller를 기술하는 RDF 그래프</center> <a id="fig-1"></a>

[Fig. 1](#fig-1)은 RDF가 URI를 사용하여 다음을 식별하는 것을 보여준다.

- 개인, `Eric Miller`, `http://www.w3.org/People/EM/contact#me`로 식별
- 사물, `Person`, `http://www.w3.org/2000/10/swap/pim/contact#Person`으로 식별
- 사물의 속성, `mailbox`, `http://www.w3.org/2000/10/swap/pim/contact#mailbox`로 식별
- 해당 속성의 값, `mailto:em@w3.org`를 `mailbox` 속성 값으로 사용(RDF는 "Eric Miller"와 같은 문자열과 정수 및 날짜와 같은 다른 데이터 타입의 값도 속성 값으로 사용 가능)

RDF는 또한 이러한 그래프를 기록하고 교환하기 위한 텍스트 기반 구문(Turtle이라고 함)을 제공한다. EXAMPLE 1은 [Fig. 1](#fig-1)의 그래프에 해당하는 Turtle 구문이다.

> EXAMPLE 1: Eric Mill를 기술하는 Turtle
```
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix contact: <http://www.w3.org/2000/10/swap/pim/contact#>.

<http://www.w3.org/People/EM/contact#me> 
  rdf:type contact:Person;
  contact:fullName "Eric Miller";
  contact:mailbox <mailto:em@w3.org>;
  contact:personalTitle "Dr.".
```

이 예에는 URI뿐 아니라 이메일 주소와 성명(약어 형식)과 같은 속성과 해당 값 `em@w3.org`와 `Eric Miller`도 포함되어 있다.

HTML과 마찬가지로 이 Turtle 구문을 컴퓨터로 처리할 수 있으며 URI를 사용하여 웹에서 정보를 연결할 수 있다. 그러나 기존의 하이퍼텍스트와 달리 RDF URI는 웹에서 직접 검색할 수 없었던 항목(예: `Eric Miller`)을 포함하여 식별 가능한 모든 항목을 참조할 수 있다. 그 결과 웹 페이지를 기술하는 것 외에도 RDF는 자동차, 비즈니스, 사람, 뉴스 이벤트 등도 기술할 수 있다. 또한 RDF 속성 자체에는 URI가 있어 링크된 항목 간의 관계를 정확하게 식별한다.

다음 문서들은 RDF 사양에 기여하고 있다.

- [RDF Concepts and Abstract Syntax](http://www.w3.org/TR/rdf-concepts/)
- [RDF/XML Syntax Specification](http://www.w3.org/TR/rdf-syntax-grammar)
- [RDF Vocabulary Description Language 1.0: RDF Schema](http://www.w3.org/TR/rdf-schema/)
- [RDF Semantics](http://www.w3.org/TR/rdf-mt/)
- [RDF Test Cases](http://www.w3.org/TR/rdf-testcases/)
- [RDF Primer](rdf-primer.md)

이 문서는 정보 시스템 설계자와 어플리케이션 개발자에게 RDF의 기능과 사용 방법에 대한 이해를 돕기 위해 RDF에 대하여 소개하고 기존의 일부 RDF 어플리케이션을 설명하기 위한 것이다. 특히 문서는 다음과 같은 사항을 설명한다.

- RDF는 어떻게 보이는가?
- RDF는 어떤 정보를 나타내는가?
- RDF 정보를 어떻게 생성, 액세스 그리고 처리할 수 있는가?
- 기존 정보를 어떻게 RDF와 결합할 수 있는가?

이 문서는 규범 문서가 아니므로 RDF의 최종 사양에 대하여 기술하지 않는다. 문서의 예와 기타 설명 자료는 RDF를 이해하는 데 도움을 주기 위해 제공되지만 항상 결정적이거나 완전한 답변을 주는 것은 아니다. 이러한 경우 RDF 사양에 관련된 규범 부분을 참조해야 한다. 이를 돕기 위해 입문서는 RDF의 전체 사양에서 이러한 다른 문서가 수행하는 역할을 설명하고 토론의 적절한 위치에서 규범 사양의 관련 부분을 가리키는 링크를 제공한다.

이 문서는 [RDF/XML 버전 문서](rdf-primer.md)을 최대한 따른다. RDF/XML은 XML으로 직렬화를 제공한다. XML은 이 문서에서 사용하는 Turtle 구문과는 다르다. 두 직렬화는 동일한 RDF 정보를 표현할 수 있다는 점에서 *동일*하며 둘 중 하나의 선택은 전적으로 개인의 취향, 파서의 가용성 등에 좌우된다. 표현력의 손실 없이 두 직렬화 형식을 변환하는 도구도 존재한다.

## 리소스에 대한 문장 생성(Making Statements About Resources) <a id="making-statements"></a>
RDF는 웹 페이지와 같은 웹 리소스에 대한 설명을 기술하는 간단한 방법을 제공하기 위한 것이다. 이 절에서는 RDF가 이러한 기능을 제공하는 방식에 대한 기본 아이디어를 설명한다 (이러한 개념을 기술하는 표준 사양은 [RDF Concepts and Abstract Syntax](http://www.w3.org/TR/rdf-concepts/)이다).

### 기본 개념 <a id="basic-concepts"></a>
John Smith라는 이름의 사람이 특정 웹 페이지를 만들었다고 가정해 보자. 이것을 영어와 같은 자연어로 표현하는 직접적인 방법은 다음과 같은 간단한 문장의 형태일 것이다.

```
'http://www.example.org/index.html' has a 'creator' whose value is 'John Smith'
```

위 문장의 일부는 무언가의 속성을 기술하기 위해 다음과 같이 여러 사물을 명명하거나 식별하는 방법이 필요하다는 것을 설명하기 위해 ' '로 표시하였다.

- 문장이 서술하는 사물(이 경우 웹 페이지)
- 문장이 서술하는 사물의 특정 속성(이 경우 생성자)
- 문장이 서술하는 사물의 속성 값(작성자가 누구인지)

이 문장에서 웹 페이지의 URL(Uniform Resource Locator)을 사용하여 웹 페이지를 식별한다. 또한 "creator"라는 단어는 속성을 식별하는 데 사용되며, "John Smith"라는 두 단어는 이 속성의 값인 사물(사람)을 식별한다.

이 웹 페이지의 다른 속성은 URL을 사용하여 페이지를 식별하고 단어(또는 기타 표현)를 사용하여 속성과 해당 값을 식별하는 동일한 일반 형식의 영어 문장을 추가하여 서술할 수 있다. 예를 들어, 페이지를 생성한 날짜와 페이지가 작성된 언어를 아래와 같이 문장으로 추가하여 기술할 수 있다.

```
`http://www.example.org/index.html` has a `creation-date` whose value is `August 16, 1999`
`http://www.example.org/index.html` has a `language` whose value is `English`
```

RDF는 서술하는 사물이 값을 갖는 [속성](http://www.w3.org/TR/rdf-concepts/#dfn-property)을 가지고 있으며, 리소스는 위와 유사한 속성과 값을 명세하는 문장을 작성하여 서술할 수 있다는 아이디어에서 출발한다. RDF는 문장의 다양한 부분에 대해 설명하기 위해 특정 용어를 사용한다. 구체적으로 문장의 내용을 식별하는 부분(이 예에서는 웹 페이지)을 [subject](http://www.w3.org/TR/rdf-concepts/#dfn-subject)라고 한다. 문장이 명세하는 subject의 속성 또는 특성(이 예에서는 `creator`, `creation-date` 또는 `language`)을 식별하는 부분을 [predicate](http://www.w3.org/TR/rdf-concepts/#dfn-predicate)이라 하고 해당 속성의 값을 식별하는 부분을 [object](http://www.w3.org/TR/rdf-concepts/#dfn-object)라고 한다. 그래서 영어 문장은 아래와 같다.

```
`http://www.example.org/index.html` has a `creator` whose value is `John Smith`
```

위 문장의 각 부분에 대한 RDF 용어는 다음과 같다.

- *subject*는 URL `http://www.example.org/index.html`이다.
- *predicate*는 "creator"라는 단어이다.
- *object*는 "John Smith"라는 문구이다.

그러나 영어는 (영어를 사용하는) 인간 간의 의사 소통에는 좋은 반면, RDF는 컴퓨터가 처리할 수 있는 언어를 만드는 것이다. 이러한 종류의 문장을 컴퓨터가 처리할 수 있도록 적합하게 하려면 두 가지가 필요하다.

- 웹에서 다른 사람이 사용할 수 있는 유사한 모양의 식별자와 혼동되지 않도록 문장의 subject, predicate 또는 object를 식별하기 위한 컴퓨터에서 처리 가능한 식별자 시스템.
- 이러한 문장을 표현하고 컴퓨터 간 교환을 위한 컴퓨터가 처리 가능한 언어.

다행스럽게도 기존 웹 아키텍처는 이러한 필수 기능을 모두 제공하고 있다.

앞서 설명한 것처럼 웹은 이미 한 가지 형태의 식별자인 *URL(Uniform Resource Locator)*을 제공하고 있다. John Smith가 만든 웹 페이지를 식별하기 위해 앞선 예에서 URL을 사용하였다. URL은 기본 액세스 메커니즘(기본적으로 해당 네트워크 "위치")을 이용하여 웹 리소스를 식별하는 문자열이다. 그러나 웹 페이지와 달리 네트워크 위치나 URL이 없는 많은 것들에 대한 정보를 기록할 수 있는 것 또한 중요하다.

웹은 이러한 목적을 위해 [URI(Uniform Resource Identifier)](http://www.isi.edu/in-notes/rfc2396.txt)라는 보다 일반적인 형식의 식별자를 제공한다. URL은 URI의 한 종류이다. 다른 사람이나 조직을 독립적으로 생성하고 사물을 식별하는 데 사용할 수 있는 속성을 모든 URI는 공유한다. 그러나 URI는 네트워크 위치 또는 컴퓨터 액세스 메커니즘을 사용하는 것으로 사물을 식별하는 것으로 제한하지 않는다. 실제로 URI는 다음을 포함하여 문장에서 참조해야 하는 모든 것을 참조할 수 있도록 만들 수 있다.

- 전자 문서, 이미지, 서비스(예: "오늘의 로스앤젤레스 일기 예보") 또는 기타 리소스 그룹과 같이 네트워크에서 액세스할 수 있는 항목
- 인간, 기업, 도서관의 제본된 책과 같이 네트워크로 접근할 수 없는 것
- "creator"의 개념과 같이 물리적으로 존재하지 않는 추상적인 개념

이러한 일반성 때문에 RDF는 문장의 subjects, predicate와 object의 식별을 위한 메커니즘의 기초로 URI를 사용한다. 더 정확하게 말하면 RDF는 [URI references](http://www.isi.edu/in-notes/rfc2396.txt)를 사용한다. URI 참조(또는 *URIref*)는 궁극적으로 선택적인 *fragment 식별자*로 URI이다. 예를 들어, URI 참조 `http://www.example.org/index.html#section2`는 URI `http://www.example.org/index.html`과 ("#" 문자로 구분된) fragment 식별자 `section2`로 구성되어 있다. RDF URIref는 [유니코드](http://www.isi.edu/in-notes/rfc2396.txt) 문자([Resource Description Framework (RDF): Concepts and Abstract Syntax](http://www.w3.org/TR/rdf-concepts/) 참조)를 포함할 수 있으므로 많은 언어를 URIref에서 사용할 수 있다. RDF는 URI 참조로 식별할 수 있는 모든 것으로 *리소스*를 정의하므로 URIrefs를 사용하면 RDF로 거의 모든 것을 서술할 수 있고 그들 간의 관계도 기술할 수 있다. [부록 A](#appendix-a)와 [RDF Concepts and Abstract Syntax](http://www.w3.org/TR/rdf-concepts/)에서 URIref와 프래그먼트(fragement) 식별자를 자세히 설명한다.

RDF 문을 컴퓨터가 처리할 수 있도록 표현하기 위해 RDF는 표준으로 [XML(Extensible Markup Language)](http://www.w3.org/TR/2000/REC-xml-20001006)을 사용하지만 다른 구문도 가능합니다. XML 직렬화 구문(*RDF/XML*이라고 함)을 별도의 문서 [RDF/XML Syntax Specification](http://www.w3.org/TR/rdf-syntax-grammar)에서 설명하고 있다. 이 문서에서는 [Turtle 구문](#rdf-turtle)를 사용한다. Turtle 예는 [서론](#introduction)에 있다. Turtle 콘텐츠는 [유니코드](http://www.isi.edu/in-notes/rfc2396.txt) 문자를 포함할 수 있으므로 많은 언어의 정보를 직접 표현할 수 있다. [다음 절](#rdf-turtle)에서 특정 Turtle 구문을 정의하고 있다.

### RDF 모델 <a id="rdf-model"></a>
[이전 섹션](#basic-concepts)에서 RDF의 기본 문장 개념, URI 참조를 사용하여 RDF 문에서 참조되는 항목을 식별하는 아이디어, 그리고 RDF 문을 표현하기 위해 컴퓨터가 처리할 수 있는 방법인 Turtle을 소개했다. 이러한 배경을 바탕으로 이 섹션에서는 RDF가 URI를 사용하여 리소스에 대한 문장을 만드는 방법을 설명한다. 서론에서 RDF는 리소스를 간단한 문장으로 표현하는 아이디어에 기반을 두고 있다고 밝혔다. 여기서 각 문장은 subject, predicate와 obejct로 구성된다. RDF에서 영어 문장은 아래와 같다.

```
'http://www.example.org/index.html' has a 'creator' whose value is 'John Smith'
```

위의 영어 문장을 다음과 같이 RDF 문으로 나타낼 수 있다.

- subject `http://www.example.org/index.html`
- predicate `http://purl.org/dc/elements/1.1/creator`
- object `http://www.example.org/staffid/85740`

따라서 위의 RDF 문은 [Fig. 2](#fig-2)에 표시된 그래프와 같다.

<center>![fig-2](images/turtle-primer/fig2.png)<br>
Fig. 2 간단한 RDF 문</center> <a id="fig-2"></a>

문장의 그룹을 해당하는 노드와 아크의 그룹으로 표현한다. 따라서 다음 문장은 추가로 반영할 영어 문장이다.

```
'http://www.example.org/index.html' has a 'creation-date' whose value is 'August 16, 1999'
'http://www.example.org/index.html' has a 'language' whose value is 'English'
```

속성 "creation-date"와 "language"를 위한 적절한 URIref를 사용하여 [Fig. 3](#fig-3)과 같은 RDF 그래프를 표시할 수 있다 .

<center>![fig-3](images/turtle-primer/fig3.png)<br>
Fig. 3 동일한 자원에 대한 RDF 문</center> <a id="fig-3"></a>

RDF 문의 객체는 특정 종류의 속성 값을 나타내기 위해 URIref 또는 문자열로 표현되는 상수 값([리터럴](http://www.w3.org/TR/rdf-concepts/#section-Literals)이라고 함)일 수 있음을 [Fig. 3](#fig-3)은 보인다. (predicate `http://purl.org/dc/elements/1.1/language` 의 경우 리터럴은 영어에 대한 국제 표준 2자리 코드이다.) 리터럴을 RDF 문에서 subject나 predicate로 사용할 수 없다. RDF 그래프를 작성할 때 URIref인 노드를 타원으로, 리터럴인 노드를 상자로 표시한다. (이 예에서 사용된 단순 문자열 리터럴은 [타입이 지정된 리터럴(typed literals)](#typed-literals)에서 소개할 타입이 지정된 리터럴과 구별하기 위해 [일반 리터럴(plain literal)](http://www.w3.org/TR/rdf-concepts/#dfn-plain-literal)이라 한다. RDF 문에서 사용할 수 있는 다양한 종류의 리터럴을 [Resource Description Framework (RDF): Concepts and Abstract Syntax](http://www.w3.org/TR/rdf-concepts/)에서 정의하고 있다. 일반 리터럴과 타입이 지정된 리터럴 모두 [유니코드](http://www.unicode.org/unicode/standard/versions/) 문자를 포함할 수 있으므로 많은 언어로 정보를 직접 표현할 수 있다.)

때로는 토론할 때 그래프를 그리는 것이 불편하므로 문장을 작성하는 대체 방법인 트리플(triple)도 사용한다. triple 표기법에서는 subject, predicate와 object 순으로 단순한 triple의 그래프로 각 문장을 작성한다. 예를 들어, [Fig. 3](#fig-3)에 표시된 세 문장을 트리플 표기법으로 다음과 같이 작성할 수 있다.

```
<http://www.example.org/index.html> <http://purl.org/dc/elements/1.1/creator> <http://www.example.org/staffid/85740> .
<http://www.example.org/index.html> <http://www.example.org/terms/creation-date> "August 16, 1999" .
<http://www.example.org/index.html> <http://purl.org/dc/elements/1.1/language> "en" .
```

각 triple은 그래프의 단일 링크에 해당하며 링크의 시작과 끝 노드(문의 subject와 object)를 연결한다. 그려진 그래프와 달리(그러나 원래 문장과 마찬가지로) triple 표기법에서는 노드가 나타나는 각 문장에서 노드를 별도로 식별해야 한다. 따라서 예를 들어 `http://www.example.org/index.html`이 (각 triple에서 한 번씩) 세 번 나타난다. 그러나 그래프의 triple 표현에서 triple들은 작성된 그래프와 정확히 같은 정보를 나타내며 이것이 핵심이다. RDF의 기본은 문장들의 그래프 모델이다. 부차적으로 그래프로 나타내거나 묘사하는 것이다.

전체 triple 표기법에서 URI 참조는 `< >`를 사용하여 완전히 작성해야 한다. 이 경우 위의 예에서 볼 수 있듯이 페이지에 줄이 매우 길어질 수 있다. 편의를 위해 여기에서는 triple을 작성할 때 속기 방식을 사용한다 (다른 RDF 사양에서도 동일한 속기를 사용). 이 속기 방법은 전체 URI 참조에 대하여 `< >`를 사용하지 않고 약어를 *정규화된 이름(또는 QName)*으로 대체한다. QName을 네임스페이스 URI를 지정하는 *접두사(prefix)*, 콜론과 *로컬 이름* 순으로 사용한다. 접두사에 할당된 네임스페이스 URI에 로컬 이름을 추가한 QName으로 전체 URIref를 구성한다. 예를 들어 네임스페이스 URI `http://example.org/somewhere/`를 QName 접두사 `foo`로 지정한 경우 QName `foo:bar`는 URIref `http://example.org/somewhere/bar`의 약어이다. 예에서는 다음과 같이 정의된 여러 "잘 알려진" QName 접두사(매번 명시적으로 지정하지 않고)를 사용한다.

```
prefix rdf:, namespace URI: http://www.w3.org/1999/02/22-rdf-syntax-ns#
prefix rdfs:, namespace URI: http://www.w3.org/2000/01/rdf-schema#
prefix dc:, namespace URI: http://purl.org/dc/elements/1.1/
prefix owl:, namespace URI: http://www.w3.org/2002/07/owl#
prefix ex:, namespace URI: http://www.example.org/ (or http://www.example.com/)
prefix xsd:, namespace URI: http://www.w3.org/2001/XMLSchema#
```

"example" 접두사 `ex:`의 명백한 변형도 예에서 필요에 따라 사용된다. 예를 들어,

```
prefix exterms:, namespace URI: http://www.example.org/terms/ (example 조직에서 사용하는 용어),
prefix exstaff:, namespace URI: http://www.example.org/staffid/ (example 조직의 직원 식별자),
prefix ex2:, namespace URI: http://www.domain2.example.org/ (두번째 example 조직 등).
```

이 새로운 약어를 사용하여 이전 triple들을 다음과 같이 작성할 수 있다.

```
ex:index.html  dc:creator             exstaff:85740 .
ex:index.html  exterms:creation-date  "August 16, 1999" .
ex:index.html  dc:language            "en" .
```

RDF는 문장에서 사물의 이름을 명명하는 단어 대신 URIref를 사용하기 때문에 RDF는 URIref의 집합(특히 특정 목적을 위한 집합)을 *어휘*로 참조한다. 종종 그러한 어휘들의 URIref를 공통 접두사를 사용하여 QName 세트로 표현될 수 있도록 구성한다. 즉, 공통 네임스페이스 URIref는 어휘의 모든 용어 사용을 위하여 선택되며 일반적으로 해당 어휘를 정의하는 사람들의 제어 하에 있다. 공통 URIref 끝에 개별 로컬 이름을 추가하여 어휘에 포함된 URIref를 구성한다. 이는 공통 접두사를 사용하여 URIref 집합을 형성한다. 예를 들어, 이전 예에서 설명한 것처럼 example.org와 같은 조직은 "creation-date" 또는 "product"와 같이 비즈니스에서 사용하는 용어들을 위하여 접두사 `http://www.example.org/terms/`로 시작하는 URIref로 구성된 어휘를 정의한다. 그리고 직원을 식별하기 위해 `http://www.example.org/staffid/`로 시작하는 URIref의 또 다른 어휘도 정의하고 있다. RDF는 이와 동일한 접근 방식을 사용하여 RDF에서 특별한 의미를 가진 자체 용어를 정의하고 있다. 이 RDF 어휘의 모든 URIref는 전통적으로 `http://www.w3.org/1999/02/22-rdf-syntax-ns#`에 연결된 QName 접두사 `rdf:`으로 시작한다. RDF Vocabulary Description Language([RDF 어휘 정의: RDF 스키마](#definig-rdf-vocabularies)에서 설명)는 전통적으로 QName 접두사 `rdfs:`와 연관된 `http://www.w3.org/2000/01/rdf-schema#`로 시작하는 URIref를 갖는 추가 용어 세트를 정의한다. (특정 QName 접두사를 이러한 방식으로 주어진 용어 집합과 관련하여 일반적으로 사용하는 경우 QName 접두사 자체를 어휘의 이름으로 사용하는 경우가 있다. 예를 들어 "`rdfs:` 어휘"를 참조할 수 있다.)

공통 URI 접두사를 사용하면 관련 용어 집합에 대한 URIref를 쉽게 구성할 수 있다. 그러나 이것은 단지 관습일 뿐이다. RDF 모델은 완전한 URIref만 인식한다. URIref "내부를 들여다보거나" 그 구조에 대한 지식을 사용하지 않는다. 특히, RDF는 URIref가 공통의 선행 접두어를 가지고 있기 때문에 URIref 사이에 관계가 있다고 가정하지 않는다(추가 논의는 [부록 A](#appendix-a) 참조). 더우기, 다른 선행 접두어를 가진 URIref가 동일한 어휘의 일부로 간주될 수 있다고 말할 수 있다. 특정 조직, 프로세스, 도구 등은 어휘의 일부로 다른 많은 어휘의 URIref를 사용하여 의미있는 어휘를 정의할 수 있다.

또한 조직에서 해당 어휘에 대한 추가 정보를 제공하는 웹 리소스의 URL으로 어휘의 네임스페이스 URIref를 사용하기도 한다. 예를 들어, 앞서 언급한 것처럼 QName 접두사 `dc:`를 네임스페이스 URIref `http://purl.org/dc/elements/1.1/`와 연결하여 이 포스팅의 예에서 사용한다. 사실 이것은 [Dublin Core 메타데이터 이니셔티브](#doublin-core-metadata) 섹션에서 기술하는 Doublin Core 어휘를 가리킨다. 웹 브라우저에서 이 네임스페이스 URIref를 액세스하면 Doublin Core 어휘(특히 RDF 스키마)에 대한 추가 정보를 검색할 수 있다.

이후 자체 사용을 위해 RDF에서 정의한 URIref 집합 또는 직원을 식별하기 위해 example.org가 정의한 URIref 집합처럼 특정 목적을 위해 정의된 URIref 집합을 참조할 때 어휘라는 용어를 사용한다. 

RDF 그래프에서 다른 어휘의 URIref를 쉽게 혼합할 수 있다. 예를 들어, [Fig. 3](#fig-3)의 그래프는 `exterms:`, `exstaff:`와 `dc:` 어휘의 URIref를 사용한다. 또한 RDF는 동일한 자원을 서술하기 위해 그래프에 나타날 수 있는 주어진 URIref가 predicate인 문장의 수에 제한을 두지 않는다. 예를 들어, 리소스 `ex:index.html`이 John Smith외 여러 직원의 협력으로 생성된 경우 example.org는 문장을 다음과 같이 작성했을 수 있다.

```
ex:index.html  dc:creator  exstaff:85740 .
ex:index.html  dc:creator  exstaff:27354 .
ex:index.html  dc:creator  exstaff:00816 .
```

이러한 RDF 문의 예는 사물을 식별하는 RDF의 기본 방법으로 URIref를 사용하는 이점을 설명하기 시작하였다. 예를 들어 첫 번째 문장에서 "John Smith"라는 문자열로 웹 페이지 작성자를 식별하는 대신 이 경우에는 URIref(직원 번호를 기반으로 한 URIref 사용) `http:// www.example.org/staffid/85740`를 할당했다. 이때 URIref를 사용하는 것의 장점은 문장의 subject를 보다 명확히 식별할 수 있다는 것이다. 즉, 페이지 작성자는 문자열 "John Smith" 또는 John Smith라는 수천 명의 사람 중 한 명이 아니라 해당 URIref에 연결된 특정 John Smith(URIref를 작성한 사람이 누구인가 연결로 정의함)이다. 더우기 John Smith를 가르키는 URIref가 있기 때문에 그는 본격적인 자원이며, John의 URIref를 subject로 하는 RDF 문을 단순히 추가하는 것만으로 그에 대한 추가 정보를 기록할 수 있다. 예를 들어, [Fig. 4](#fig-4)는 John의 이름과 나이를 제공하는 몇 가지 추가 문장을 보여준다.

<center>![fig-4](images/turtle-primer/fig4.png)<br>
Fig. 4 John Smith에 대한 추가 정보</center> <a id="fig-4"></a>

또한 이 예는 RDF가 URIref를 RDF문의 *predicate*로 사용한다는 것을 보여준다. 즉, "creator" 또는 "name"과 같은 문자열(또는 단어)을 사용하여 속성을 식별하는 대신 RDF는 URIref를 사용한다. URIref를 사용하여 속성을 식별하는 것은 여러 이유에서 중요하다. 첫째, 한 사람이 사용할 수 있는 속성을 동일한 문자열로 식별할 수 있는 다른 사람에게 사용할 수 있는 다른 속성과 구별한다. 예를 들어, [Fig. 4](#fig-4)의 예에서 example.org는 "name"을 사용하여 문자열 리터럴로 작성된 사람의 전체 이름(예: "John Smith")을 의미하지만 다른 사람은 "name"을 다른 의미로 사용할 수 있다 (예: 프로그램 텍스트의 변수 이름). 웹에서 속성 식별자로 "name"을 접하는 프로그램(또는 여러 소스로 부터 데이터 병합)은 이러한 용도를 명확히 구분할 수 없다. 그러나 example.org는 "name" 속성에 대해 `http://www.example.org/terms/name`을 사용하고 다른 사람은 `http://www.domain2.example.org/genealogy/terms/name`을 사용한다면 에 대해 (프로그램이 고유한 의미를 자동으로 결정할 수 없는 경우에도) 별개의 속성이 관련되어 있음을 알 수 있다. 또한 속성을 식별하기 위해 URIref를 사용하면 속성 자체를 리소스로 취급할 수 있다. 속성은 리소스이기 때문에 속성의 URIref를 subject로 하는 RDF 문을 추가하기만 하면 속성에 대한 추가 정보가 기록될 수 있다(예: example.org가 "name"으로 의미하는 내용에 대한 영어 서술).

RDF 문에서 URIref를 subject, predicate와 object에 사용하면 웹에서 공유 어휘의 개발과 사용을 지원한다. 사람들은 다른 사람들이 이미 사물을 기술하기 위하여 사용한 어휘를 발견하고 사용할 수 있기 때문에 이러한 개념에 대한 공유된 이해를 반영한다. 예를 들어 아래 triple에서

```
ex:index.html   dc:creator   exstaff:85740 .
```

predicate `dc:creator`를 URIref로 완전히 확장하면 Dublin Core 메타데이터 속성 집합([Dublin Core 메타데이터 이니셔티브](#doublin-core-metadata)에서 자세히 설명)의 "creator" 속성에 대한 명확한 참조이며, 모든 종류의 정보 서술을 위해 널리 사용되는 애트리뷰트(속성) 집합이다. 이 triple의 작성자는 (`http://www.example.org/index.html`로 식별되는)웹 페이지와 페이지 작성자( `http://www.example.org/staffid/85740`로 식별되는 다른 사람)간의 관계를 효과적으로 기술하고 있다. 정확히 http://purl.org/dc/elements/1.1/creator에 의해 식별된 개념이다. Dublin Core 용어에 익숙하거나  `dc:creator`가 무엇을 의미하는지 아는 사람(예: 웹에서 정의를 검색하여)은 이 관계가 의미하는 바를 알 것이다. 또한 이러한 이해를 바탕으로 개발지들은 predicate `dc:creator`를 포함하는 triple을 처리할 때 그 의미에 따라 동작하도록 프로그램을 작성할 수 있는 것이다.

물론 이는 리터럴을 사용하는 대신 사물을 참조하는 URIref의 일반적인 사용을 늘리는 데 달려 있다. 예를 들어 `John Smith`와 `creator` 같은 문자열 리터럴 대신 URIref `exstaff:85740`와 `dc:creator`를 사용한다. 그럼에도 불구하고 RDF의 URIref 사용은 모든 식별 문제를 해결하지 못한다. 예를 들어 사람들이 여전히 동일한 것을 참조하기 위해 다른 URIref를 사용할 수 있기 때문이다. 이러한 이유로 다른 어휘와 중복될 수 있는 새로운 용어를 만드는 것보다 가능하면 (Doblin Core 같은) 기존 어휘의 용어를 사용하는 것이 바람직하다. [RDF 어플리케이션](#rdf-application)]에서 설명하는 것처럼 특정 어플리케이션 분야에서 사용하기 위한 적절한 어휘는 항상 개발되고 있다. 그러나 동의어가 생성될 때에도 일반적으로 액세스할 수 있는 "웹 공간"에서 이러한 다른 URIref가 사용된다는 사실은 이렇게 서로 다른 참조 간의 동등성을 식별하고 공통 참조를 사용할 수 있도록 마이그레이션하는 기회를 제공한다.

또한 RDF *자체*가 RDF 문에서 (이전 예에서의 `dc:creator`같이) 사용된 용어와 연관시키는 의미와 사람들이 (또는 해당 사람들이 작성한 프로그램이) 해당 용어와 연관시킬 수 있는 *외부에서 정의*된 추가 의미를 구별하는 것은 중요하다. 언어로서 RDF는 subject, predicate와 object triple의 그래프 구문, `rdf:` 어휘의 URIref와 관련된 특정 의미와 나중에 기술할 특정 개념만을 직접 정의한다. 이러한 사물들은 [Resource Description Framework (RDF): Concepts and Abstract Syntax](http://www.w3.org/TR/rdf-concepts/)와 [RDF Semantics](http://www.w3.org/TR/rdf-mt/)에 규범적으로 정의되어 있다. 그러나 RDF는 RDF 문에서 사용될 수 있는 `dc:creator`와 같은 다른 어휘의 용어 의미를 정의하지 않는다. 정의된 URIref에 특정 의미를 할당함과 함께 RDF 외부에서 특정 어휘를 생성한다. 이러한 어휘의 URIref를 사용하는 RDF 문은 이러한 어휘에 익숙한 사람들 또는 이러한 어휘를 처리하기 위해 작성된 임의의 RDF 어플리케이션에 이러한 의미를 전달하지 않고 이러한 어휘를 처리하기 위해 작성된 임의의 RDF 어플리케이션에 해당 용어와 관련된 특정 의미를 전달할 수 있다

예를 들어, 의미를 다음과 같은 triple에 연관시킬 수 있다.

```
ex:index.html  dc:creator  exstaff:85740 .
```

URIref `dc:creator`의 일부로 "creator"라는 단어의 출현과 연관시키는 의미 또는 Doublin Core 어휘에서 `dc:creator`의 특정 정의에 대한 이해에 기반을 둔다. 그러나 임의의 RDF 어플리케이션에 관한 triple은 내재된 의미에 관한 한 다음과 같을 수도 있다.

```
fy:joefy.iunm  ed:dsfbups  fytubgg:85740 .
```

마찬가지로 웹에서 찾을 수 있는 `dc:creator`의 의미를 설명하는 자연어 텍스트는 임의의 RDF 어플리케이션에서 직접 사용할 수 있는 추가 의미를 제공하지 않는다.

물론 특정 어휘의 URIref가 주어진 어플리케이션에 특별한 의미를 연관시킬 수 없더라도 RDF 문에서 사용할 수 있다. 예를 들어, 일반 RDF 소프트웨어는 위의 표현식이 RDF 문이고 `ed:dsfbups`가 predicate임을 인식한다. 어휘 개발자가 `ed:dsfbups`와 같은 URIref와 연관시킬 수 있는 특별한 의미를 triple에 단순히 연관시키지 않을 것이다. 게다가, 비록 그런 방식으로 작성되지 않은 RDF 어플리케이션에서 그 의미를 액세스할 수 없을지라도 주어진 어휘에 대한 이해를 바탕으로 해당 어휘의 URIref에 할당된 특별한 의미에 따라 동작하도록 RDF 어플리케이션을 작성할 수 있다.

이 모든 것의 결과는 RDF가 어플리케이션이 더 쉽게 처리할 수 있는 문장을 작성하는 방법을 제공한다는 것이다. 이미 언급한 바와 같이 `SELECT NAME FROM EMPLOYEE WHERE SALARY > 35000`과 같은 쿼리를 처리할 때 데이터베이스 시스템이 "employee" 또는 "salary"와 같은 용어를 "이해"하는 것만큼 어플리케이션은 이러한 문장을 실제로 "이해"할 수 없다. 어플리케이션이 적절히 작성되었다면 데이터베이스 시스템과 어플리케이션이 "employee" 및 "payroll"를 이해하지 않고도 직원과 급여 정보를 처리하는 데 유용한 작업을 수행할 수 있는 것처럼 RDF 문을 이해하는 것처럼 처리할 수 있다. 예를 들어, 사용자는 웹에서 모든 서평을 검색하고 각 책에 대한 평균 등급을 검색할 수 있다. 그런 다음 사용자는 해당 정보를 웹에 다시 게시할 수 있다. 다른 웹 사이트는 해당 도서 등급 평균 목록을 가져와 "높은 등급 상위 10 도서" 페이지를 만들 수 있다. 여기에서 등급에 대한 공유 어휘의 가용성과 사용 및 해당 도서를 식별하는 공유 URIref 그룹을 통해 상호 이해되고 점점 더 강력해지는 (추가 기여가 이루어짐에 따라) 웹상의 책에 대한 "정보 기반"을 구축할 수 있다. 웹에서 매일 수천 개의 주제를에 대하여 생성되는 방대한 양의 정보에도 동일한 원칙을 적용할 수 있다.

RDF 문은 다음과 같이 정보 기록을 위한 여러 다른 형식들과 유사하다.

- 데이터 처리 시스템의 리소스을 기술하는 단순 레코드 또는 카탈로그 목록의 항목
- 간단한 관계형 데이터베이스의 행
- 형식 논리의 단순 선언(assertion)

이러한 형식의 정보는 RDF 문으로 처리될 수 있으므로 RDF를 사용하여 여러 소스의 데이터를 통합할 수 있다.

### 구조화된 속성 값과 빈 노드(Structured Property Values and Blank Nodes) <a id="structured-property-values"></a>
사물에 대해 기록할 유일한 타입의 정보는 지금까지 설명한 간단한 RDF 문장 형식을 명백하게 따른다면 사물은 매우 간단할 것입니다. 그러나 대부분의 실제 데이터는 적어도 표면적으로는 그보다 더 복잡한 구조를 갖고 있다. 예를 들어, 원래 예에서 웹 페이지가 생성된 날짜는 그 값으로 일반 리터럴을 사용하여 단일 `exterms:creation-date` 속성으로 기록된다. 그러나 `exterms:creation-date` 속성의 값 월, 일과 연도를 별도의 정보로 기록하고자 하는 상황을 가정해 보자. 또는 John Smith의 개인 정보의 경우 John의 주소가 기술되고 있다고 가정한다. 전체 주소를 triple로 아래와 같이 일반 리터럴로 작성될 수 있다.

```
exstaff:85740   exterms:address   "1501 Grant Avenue, Bedford, Massachusetts 01730" .
```

그러나 John의 주소를 거리, 도시, 주 및 우편 번호 값으로 분리하여 *구조*로 기록해야 한다고 가정해 보자. 이를 RDF에서 어떻게 처리할까?

이와 같은 구조화된 정보는 기술할 복합적인 사물(John Smith's addreess와 같은)을 리소스로 고려하여 새로운 리소스에 대하여 서술함으로써 RDF로 표현될 수 있다. 따라서 RDF 그래프에서 John Smith's addreess를 구성 요소 부분으로 나누기 위해 이를 식별하기 위한 새 URIref(예: http://www.example.org/addressid/85740 (줄여 exaddressid:85740로))와 함께 John Smith's addreess 개념을 나타내는 새 노드를 생성한다. 그런 다음 추가 정보를 표현할 수 있도록 RDF 문(추가 노드와 링크)을 해당 노드를 subject로 작성하여 [Fig. 5](#fig-5) 그래프를 생성할 수 있다.

<center>![fig-5](images/turtle-primer/fig5.png)<br>
Fig. 5 John의 주소 분리</center> <a id="fig-5"></a>

또는 triple로 표현할 수 있다.

```
exstaff:85740       exterms:address    exaddressid:85740 .
exaddressid:85740   exterms:street     "1501 Grant Avenue" .
exaddressid:85740   exterms:city       "Bedford" .
exaddressid:85740   exterms:state      "Massachusetts" .
exaddressid:85740   exterms:postalCode "01730" .
```

RDF에서 구조화된 정보를 표현하는 이 방법은 John의 주소와 같은 복합적 개념을 나타내기 위해 `exaddressid:85740`와 같은 수많은 "intermediate" URIref를 생성하는 것을 포함할 수 있다. 이러한 개념은 특정 그래프 외부에서 직접 참조할 필요가 없으므로 "보편적인" 식별자가 필요하지 않을 수 있다. 또한 [Fig. 5](#fig-5) 표시된 문장 그룹을 나타내는 그래프의 그림에서 "John Smith's addreess"를 식별하기 위해 할당된 URIref는 실제로 필요하지 않다. 그래프를 [Fig. 6](#fig-6)과 같이 쉽게 그릴 수 있기 때문이다.

<center>![fig-6](images/turtle-primer/fig6.png)<br>
Fig. 6 빈 노드 사용</center> <a id="fig-6"></a>

완벽하게 좋은 RDF 그래프인 [Fig. 6](#fig-6)은 URIref가 없는 노드를 사용하여 "John Smith's addreess"라는 개념을 나타낸다. 노드 자체가 그래프의 다양한 다른 부분 사이에 필요한 연결을 제공하기 때문에 이 빈 노드는 URIref가 필요 없이 그림에서 목적을 달성할 수 있다. (빈 노드는 [Resource Description Framework (RDF) Model and Syntax Specification](http://www.w3.org/TR/REC-rdf-syntax/)에서 *익명 리소스*라고 한다.) 그러나 이 그래프를 triple로 나타내기 위해서는 해당 노드에 대한 명시적 식별자 형식이 필요하다. 이를 보이기 위해 [Fig. 6](#fig-6)에 표시된 항목에 해당하는 triple을 작성하면 다음과 같은 결과를 생성한다.

```
exstaff:85740   exterms:address      ??? .
???             exterms:street       "1501 Grant Avenue" .
???             exterms:city         "Bedford" .
???             exterms:state        "Massachusetts" .
???             exterms:postalCode   "01730" .
```

위에서 `???`는 공백 노드가 있음을 표시하는 의미이다. 복잡한 그래프에는 둘 이상의 공백 노드가 있을 수 있으므로 그래프의 triple 표현에서 이러한 다른 공백 노드를 구별하는 방법도 필요하다. 결과적으로 triple에서 `_:name` 형식의 [공백 노드 식별자](http://www.w3.org/TR/rdf-concepts/#dfn-blank-node-id)를 사용하여 공백 노드의 존재를 나타낸다. 예를 들어, 이 예에서 빈 노드 식별자 `_:johnaddress`를 빈 노드를 참조하는 데 사용할 수 있으며, 이 경우 결과 triple은 다음과 같다.

```
exstaff:85740   exterms:address     _:johnaddress .
_:johnaddress   exterms:street      "1501 Grant Avenue" .
_:johnaddress   exterms:city        "Bedford" .
_:johnaddress   exterms:state       "Massachusetts" .
_:johnaddress   exterms:postalCode  "01730" .
```

그래프의 triple 표현에서 그래프의 고유한 공백 노드에 각가 다른 공백 노드 식별자가 제공된다. URIref와 리터럴과 달리 공백 노드 식별자는 RDF 그래프의 실제 부분으로 간주되지 않는다 ([Fig. 6](#fig-6) 그래프를 보면 공백 노드에 공백 노드 식별자가 없음을 알 수 있다). 공백 노드 식별자는 그래프가 triple 형식을 작성할 때 그래프에서 공백 노드를 나타내는 (그리고 하나의 공백 노드를 다른 공백 노드와 구별하는) 방법일 뿐이다. 공백 노드 식별자는 *단일* 그래프를 나타내는 triple 내에서만 의미가 있다 (같은 수의 공백 노드를 가진 두 개의 서로 다른 그래프는 독립적으로 동일한 공백 노드 식별자를 사용하여 공백 노드를 구별할 수 있으며, 다른 그래프에서 동일한 공백 노드 식별자를 동일한 공백 노드로 가정하지 않는다). 그래프의 노드를 그래프 외부에서 참조해야 할 것으로 예상되는 경우 이를 식별하기 위해 URIref를 할당해야 한다. 마지막으로, 공백 노드 식별자는 RDF 그래프의 triple 형태에서 링크가 아닌 (공백) 노드를 나타내기 때문에 공백 노드 식별자는 triple의 subject 또는 object로만 나타날 수 있다. triple에서 공백 노드 식별자를 predicate로 사용할 수 없다.

이 섹션의 시작 부분에서 별도의 리소스로 기술될 복합 사물에 대한 고려와 새로운 리소스에 대한 설명을 John Smith의 주소와 같은 복합 구조로 표현할 수 있다고 언급했다. 이 예는 RDF의 중요한 측면을 보인다. 즉 RDF는 *이진* 관계만을 직접적으로 나타낸다. 예를 들면 John Smith와 그의 주소를 나타내는 리터럴 간의 관계이다. John과 이 주소의 개별 구성 요소 그룹 간의 관계는 John과 거리, 도시, 주 및 우편 번호 구성 요소 간의 n-ary(n-way) 관계(이 경우 n=5)를 처리하는 것을 포함한다. RDF에서 이러한 구조를 직접 나타내기 위해(예: 주소를 거리, 도시, 주 및 우편 번호 구성 요소 그룹으로), 이 n-way 관계를 별도의 이진 관계 그룹으로 분할해야 한다. 빈 노드는 이를 수행하는 방법중 하나를 제공한다. 각 n-ary 관계에 대해 구성 요소중 하나(이 경우 John)를 subject로 선택하고 나머지 관계(이 경우 John의 주소)를 나타내기 위해 빈 노드를 생성한다. 그런 다음 관계의 나머지 구성 요소(이 예의 도시)를 빈 노드로 표시되는 새 리소스의 별도 속성으로 표시한다.

빈 노드는 URI가 없을 수 있지만 URI가 있는 다른 리소스와의 관계 측면에서 서술하려는 리소스에 대해 더 정확하게 설명하는 방법을 제공한다. 예를 들어, Jane Smith라는 사람에 대하여 문장을 생성할 때 그 사람의 이메일 주소를 기반으로 한 URI를 그의 URI로 사용하는 것이 자연스러워 보일 수 있다(예: `mailto:jane@example.org`). 그러나 이 접근 방식은 문제를 일으킬 수 있다. 예를 들어 *Jane의 이메일 주소*(예: 주소의 서버)와 *Jane 자신*(예: 현재 실제 주소)에 대한 정보를 모두 기록하고 이메일 주소를 기반으로 Jane에 대한 URIref를 사용하는 것은 설명하고 있는 것이 Jane인지 그녀의 이메일 주소인지 구별하기 어렵다. 회사의 웹 페이지 URL(예: http://www.example.com/)을 회사의 URI로 사용하는 경우에도 동일한 문제가 발생한다. 다시 한 번, 웹 페이지 자체에 대한 정보(예: 누가, 언제 생성했는지)뿐만 아니라 회사에 대한 정보를 기록해야 할 수 있으며 `http://www.example.com/`을 식별자로 사용하면 이 중 어느 것이 실제 주제인지 구별하기 어렵다.

근본적인 문제는 Jane의 이메일을 Jane을 대신하여 사용하는 것은 실제로 정확하지 않다는 것이다. Jane과 그녀의 이메일함은 같은 사물이 아니므로 서로 다르게 식별해야 한다. Jane 자신의 URI가 없을 때 빈 노드는 이 상황을 모델링하는 정확한 방법을 제공한다. Jane은 공백 노드로 나타낼 수 있으며, 해당 공백 노드는 `exterms:mailbox` 속성을 갖으며 URIref `mailto:jane@example.org`를 값을 갖는 RDF 문의 subject로 사용할 수 있다. 또한, 다음 triple에 표시된 것처럼 `exterms:Person` 값을 갖는 `rdf:type` 속성, "Jane Smith" 값을 갖는 `exterms:name` 속성과 유용할 수 있는 다른 설명 정보로 공백 노드를 서술할 수 있다.

```
_:jane   exterms:mailbox   <mailto:jane@example.org> .
_:jane   rdf:type          exterms:Person .
_:jane   exterms:name      "Jane Smith" .
_:jane   exterms:empID     "23748"  .
_:jane   exterms:age       "26" .
```

> **NOTE**
> `mailto:jane@example.org`는 첫 번째 triple의 `<>` 안에 작성되어야 한다. `mailto:jane@example.org`는 QName 약어가 아니라 `mailto` URI 표기법에서 완전한 URIref이며 이를 triple 표기법에서 `<>`로 묶어야 하기 때문이다.

이것은 정확히 "`exterms:Person` 타입의 리소스가 있으며, 이메일은 `mailto:jane@example.org`이고, 이름은 `Jane Smith` 등이다."라고 할 수 있다. 즉, 빈 노드는 "자원이 있다"로 해석할 수 있다. 그런 다음 해당 공백 노드가 subject인 RDF 문은 그 자원의 특성에 대한 정보를 제공한다.

실제로 이러한 경우에 URIref 대신 공백 노드를 사용하는 것은 이러한 종류의 정보 처리 방식을 크게 변경하지 않는다. 예를 들어, 이메일 주소가 example.org에서 누군가를 고유하게 식별하는 것으로 알려진 경우(특히 주소가 재사용될 가능성이 낮은 경우), 이메일 주소가 그 사람의 URI가 아닐지라도 여전히 여러 출처에서 그 사람에 대한 정보를 연결하는 데 사용될 수 있다. 이 경우 책을 소개하는 웹에서 저자의 연락처 정보를 `mailto:jane@example.org`로 표현한 일부 RDF가 있다면 이 정보를 이전 triple 세트와 결합하여 저자의 이름이 Jane Smith라고 하는 것이 합리적일 수 있다. 요점은 "책의 저자는 mailto:jane@example.org입니다"는 일반적으로 "책 저자의 이메일 주소는 `mailto:jane@example.org`입니다"의 축약형이라는 것이다. 이 "누군가"를 나타내기 위해 빈 노드를 사용하는 것은 실제 상황을 표현하는 더 정확한 방법일 뿐이다. (추가로, 일부 RDF 기반 스키마 언어는 특정 속성을 기술하는 리소스의 *고유 식별자*임을 명시할 수 있다. 이는 [보다 풍부한 스키마 언어](#richer-schema-languages)에서 자세히 설명한다.)

이러한 방식으로 빈 노드를 사용하면 부적절한 상황에서 리터럴을 사용하는 것을 방지할 수도 있다. 예를 들어 Jane의 책을 설명할 때 저자를 식별하기 위한 URIref가 없는 경우 출판사는 (출판사 고유의 ex2terms: 어휘를 사용하여) 다음과 같이 작성했을 수 있다.

```
ex2terms:book78354   rdf:type          ex2terms:Book .
ex2terms:book78354   ex2terms:author   "Jane Smith" .
```

그러나 이 책의 저자는 실제로 문자열 "Jane Smith"이 아니라 이름이 Jane Smith인 사람이다. 다음과 같이 빈 노드를 사용하여 게시자가 동일한 정보를 더 정확하게 제공할 수 있다.

```
ex2terms:book78354   rdf:type         ex2terms:Book .
ex2terms:book78354   ex2terms:author  _:author78354 .
_:author78354        rdf:type         ex2terms:Person .
_:author78354        ex2terms:name    "Jane Smith" .
```

이것은 본질적으로 "자원 `ex2terms:book78354`는 `ex2terms:Book` 타입이고 저자는 이름이 `Jane Smith`인 `ex2terms:Person` 타입의 자원입니다."라고 하는 것이다. 물론 이 특별한 경우 게시자는 저자에 대한 외부 참조를 장려하기 위해 공백 노드를 사용하여 작성자를 식별하는 대신 자체 URIref를 저자에게 할당했을 수 있다.

마지막으로 Jane의 나이를 26으로 지정한 위의 예는 때때로 속성 값이 단순해 보이지만 실제로는 더 복잡할 수 있다는 사실을 보여준다. 이 경우 제인의 실제 나이는 26세이지만 단위 (년)를 명시적으로 주어지지 않았다. 속성 값을 액세스하는 모든 사람이 사용 중인 단위를 알고 있다는 가정할 수 있는 상황에서 종종 이러한 정보는 생략된다. 그러나 웹의 더 넓은 맥락에서 이러한 가정을 하는 것은 일반적으로 안전하지 않다. 예를 들어, 미국 사이트는 무게 값을 파운드로 제공할 수 있지만 미국 외부에서 해당 데이터에 액세스하는 누군가는 무게가 킬로그램으로 주어졌다고 가정할 수 있다. 일반적으로 단위와 유사한 정보를 명시적으로 나타내는 데 신중해야 한다. 이 문제는 [구조화된 값에 대한 추가 정보](#more-structured-values)에서 더 논의하도록 한다. 이러한 정보를 구조화된 값으로 표현하기 위한 RDF 기능뿐만 아니라 그러한 정보를 표현하기 위한 기타 기술에서 설명할 것이다.

### 타입이 지정된 리터럴(Typed Literals) <a id="typed-literals"></a>
이전 섹션에서는 일반 리터럴로 표현되는 속성 값이 해당 리터럴의 개별 부분을 나타내기 위해 구조화된 값으로 분할되어야 하는 상황을 처리하는 방법에 대하여 설명했다. 이 접근 방식을 사용하면 웹 페이지가 생성된 날짜를 단일 일반 리터럴을 사용하는 값으로 단일 `exterms:creation-date` 속성을 기록하는 대신, 월, 일과 연도로 구조화하여 일반 리터럴로 해당 값을 나타낼 수 있도록 별도의 정보 구조를 사용한다. 그러나 지금까지 RDF 문에서 객체로 사용되는 모든 상수 값은 이러한 (타입이 지정되지 않은) 일반 리터럴로 표현되었다. 속성 값이 수(예를 들면 `age` 속성의 `year` 값) 또는 다른 종류의 보다 전문화된 값에 대하서도 마찬가지였다.

예를 들어, [Fig. 4](#fig-4)는 John Smith에 대한 정보를 기록한 RDF 그래프를 보였다. 이 그래프는 [Fig. 7](#fig-7)과 같이 John Smith의 `exterms:age` 속성 값을 일반 리터럴 "27"로 기록했다.

<center>![fig-7](images/turtle-primer/fig7.png)<br>
Fig. 7 Jonh Smith의 age 표현</center> <a id="fig-7"></a>

이 경우 가상의 조직인 example.org는 아마도 "27"이 문자 "2" 다음에 문자 "7"로 구성된 문자열이 아니라 (리터럴은 "age" 속성의 값을 나타내므로) 수로 해석되도록 한다. 그러나 [Fig 7](#fig-7)의 그래프에 "27"이 숫자로 해석되어야 함을 명시적으로 나타내는 정보가 없다. 마찬가지로 example.org는 "27"이 8진수, 즉 23이 아니라 10진수 27로 해석되도록 할 수도 있다. 그러나 다시 한 번 [Fig 7](#fig-7)의 그래프에는 이를 명시적으로 나타내는 정보가 없다. 특정 어플리케이션에서 `exterms:age` 속성 값을 십진수로 해석해야 함을 작성할 수 있지만, 이는 RDF의 적절한 해석이 RDF 그래프에 명시적으로 제공되지 않은 정보에 의존한다는 것을 의미한다. 이 RDF를 해석해야 하는 다른 어플리케이션에서 반드시 사용할 수 있는 정보는 아니다.

프로그래밍 언어 또는 데이터베이스 시스템의 일반적인 관행은 리터럴에 이 경우 `decimal` 또는 `integer`에 *데이터 타입*을 연결하여 해석하는 방법으로 추가 정보를 제공한다. 데이터 타입을 이해하는 어플리케이션은 지정된 데이터 타입이 `integer`, `binary` 또는 `string`에 따라, 예를 들어 리터럴 "10"은 숫자 *ten*, 숫자 *2* 또는 문자 "1" 이 문자 "0" 앞에 나타난 문자열인지 여부를 알 수 있다. ([구조화된 속성 값과 빈 노드](#structured-property-values)의 끝에 언급된 단위 정보를 포함하기 위해 더 전문화된 데이터 타입을 사용할 수도 있다. 예를 들어 데이터 타입 `integerYears`, 이 포스팅에서는 이 방법에 대해 자세히 설명하지 않는다.) RDF에서 이러한 종류의 정보를 제공하는 데 [타입이 지정된 리터럴](http://www.w3.org/TR/rdf-concepts/#dfn-typed-literal)을 사용한다.

RDF 타입 리터럴은 특정 데이터 타입을 식별하는 URIref와 문자열 쌍으로 구성된다. 그 결과 이같은 쌍으로 RDF 그래프의 단일 리터럴 노드를 생성한다. 타입이 지정된 리터럴이 나타내는 값은 지정된 문자열에 연결된 지정된 데이터 타입 값이다. 예를 들어, 타입이 지정된 리터럴을 사용하여 John Smith의 age를 triple을 사용하여 정수 27을 다음과 같이 표시할 수 있다.

```
<http://www.example.org/staffid/85740>  <http://www.example.org/terms/age> "27"^^<http://www.w3.org/2001/XMLSchema#integer> .
```

또는 긴 URI를 QName 사용하여 다음과 같이 단순화할 수 있다.

```
exstaff:85740  exterms:age  "27"^^xsd:integer .
```

또는 [Fig. 8](#fig-8)과 같이 그래프로 나타낼 수 있다.

<center>![fig-8](images/turtle-primer/fig8.png)<br>
Fig. 8 Jonh Smith의 타입이 지정된 리터럴</center> <a id="fig-8"></a>

마찬가지로 웹 페이지에 대한 정보를 서술하는 [Fig. 3](#fig-3)의 그래프에서 페이지의 `exterms:creation-date 속성` 값은 일반 리터럴 "August 16, 1999"로 작성되었다. 그러나 타입이 지정된 리터럴을 사용하면 웹 페이지의 생성 날짜를 다음 triple을 사용하여 *August 16, 1999*으로 기술할 수 있다.

```
ex:index.html  exterms:creation-date  "1999-08-16"^^xsd:date .
```

또는 [Fig. 9](#fig-9)과 같이 그래프로 나타낼 수 있다.

<center>![fig-9](images/turtle-primer/fig9.png)<br>
Fig. 9 웹 페이지 생성일의 타입이 지정된 리터럴</center> <a id="fig-9"></a>

일반적인 프로그래밍 언어와 데이터베이스 시스템과 달리 RDF에는 정수, 실수, 문자열 또는 날짜와 같은 자체 데이터 타입들을 내장하지 않고 있다. 대신, RDF 타입이 지정된 리터럴은 주어진 리터럴에 대해 데이터 타입을 해석하는 데 사용해야 하는 방법을 명시적으로 제공한다. 타입이 지정된 리터럴에 사용되는 데이터 타입은 RDF 외부에서 정의되며 해당 [datatype URI](http://www.w3.org/TR/rdf-concepts/#dfn-datatype-URI)로 식별된다. (한 가지 예외가 있다. RDF는 XML 내용을 리터럴 값으로 나타내기 위해 URIref `rdf:XMLLiteral`을 사용하여 내장 데이터 타입을 정의한다. 이 데이터 타입을 [Resource Description Framework (RDF): Concepts and Abstract Syntax](http://www.w3.org/TR/rdf-concepts/)에서 정의하고 있으며 그 사용법을 [여기](https://www.w3.org/2007/02/turtle/primer/#xmlliterals)에서 설명한다.) 예를 들어, [Fig. 8](#fig-8)과 [Fig. 9](#fig-9)의 예는 [XML Schema Part 2: Datatypes](http://www.w3.org/TR/xmlschema-2/)에 정의된 XML 스키마 데이터 타입의 `integer`와 `date` 데이터 타입을 사용한다. 이 접근 방식의 장점은 이러한 소스와 RDF 데이터 타입의 기본 세트 간에 타입 변환을 수행할 필요 없이 다른 소스에서 오는 정보를 직접 표현할 수 있는 유연성을 RDF에 제공한다는 것이다. (타입 변환은 지원하는 데이터 타입 집합이 다른 시스템으로 정보를 이동할 때 여전히 필요하지만 RDF는 기본 RDF 데이터 집합 안팎으로 추가 변환을 부과하지는 않는다.)

RDF 데이터 타입 개념은 [RDF Concepts and Abstract Syntax](http://www.w3.org/TR/rdf-concepts/)에 설명된 대로 [XML Schema Part 2: Datatypes](http://www.w3.org/TR/xmlschema-2/)의 개념적 프레임워크에 기반을 두고 있다. 이 개념적 프레임워크는 다음과 같이 구성된 데이터 타입을 정의한다.

- 데이터 타입의 리터럴이 나타내려는 *값 공간(value space)*이라고 하는 값의 집합. 예를 들면 XML 스키마 데이터 타입 `xsd:date`의 경우 이 값의 집합은 날짜 집합이다.
- 데이터 타입이 값을 나타내는 데 사용하는 *어휘 공간(lexical space)*이라고 하는 문자열 집합. 이 집합은 이 데이터 타입의 리터럴을 나타내는 데 규칙으로 사용할 수 있는 문자열을 결정한다. 예를 들어, (`August 16, 1999`와는 반대로) `1999-08-16`을 이 타입의 리터럴로 작성하는 규칙으로 데이터 타입 `xsd:date`을 정의한다 . [Resource Description Framework (RDF): Concepts and Abstract Syntax](http://www.w3.org/TR/rdf-concepts/)에 정의된 대로 데이터 타입의 어휘 공간은 [유니코드](http://www.unicode.org/unicode/standard/versions/) 문자열의 집합으로, 여러 언어의 정보를 직접 표현할 수 있다.
- 어휘 공간에서 값 공간으로의 *어휘-값 매핑(lexical-to-value mapping)*. 이는 어휘 공간에서 주어진 문자열이 이 특정 데이터 타입에 대해 나타내는 값을 결정한다. 예를 들면, 데이터 타입 `xsd:date`에 대한 어휘-값 매핑은 이 데이터 타입의 경우 문자열 `1999-08-16`이 `August 16, 1999`을 나타내는 것으로 결정한다. 어휘-값 매핑이 요인이 되어 같은 문자열이 다른 데이터 타입에 매핑되어 다른 값을 나타낼 수도 있다.

모든 데이터 타입이 RDF에서 사용하기에 적합한 것은 아니다. 데이터 타입이 RDF에서 사용하기에 적합하려면 방금 설명한 개념적 프레임워크를 따라야 한다. 이는 기본적으로 문자열이 주어지면 데이터 타입이 문자열이 어휘 공간에 있는지 여부와 문자열이 값 공간의 값을 명확하게 정의해야 함을 의미한다. 예를 들어 `xsd:string`, `xsd:boolean`, `xsd:date` 등과 같은 기본 XML 스키마 데이터 타입은 RDF에서 사용하기에 적합하다. 그러나 기본 제공 XML 스키마 데이터 타입 중 일부는 RDF에서 사용하기에 부적절하다. 예를 들어, `xsd:duration`에는 잘 정의된 값 공간이 없고 `xsd:QName`에는 둘러싸고 있는 XML 문서 컨텍스트가 필요하다. 현재 RDF에서 사용하기에 적합하거나 부적합하다고 여겨지는 XML 스키마 데이터 타입의 목록은 [RDF Semantics](http://www.w3.org/TR/rdf-mt/)에 수록되어 있다.

타입이 주어진 리터럴이 나타내는 값은 `rdf:XMLLiteral`을 제외하고 타입이 주어진 리터럴의 데이터 타입에 의해 정의되므로 RDF는 데이터 타입을 정의하지 않고, RDF 그래프에 나타나는 타입이 주어진 리터럴의 실제 해석(예: 그것이 나타내는 값을 결정하는)은 RDF뿐만 아니라 타입이 주어진 리터럴의 데이터 타입을 정확히 처리할 수 있도록 작성된 소프트웨어에 의해 수행되어야 한다. 효과적으로, RDF뿐만 아니라 데이터 타입을 기본 제공 어휘의 일부로 포함하는 확장 언어를 처리할 수 있도록 작성되어야 한다. 이는 RDF 소프트웨어에서 일반적으로 사용할 수 있는 데이터 타입의 문제를 제기한다. 일반적으로 [RDF Semantics](http://www.w3.org/TR/rdf-mt/) 내의 RDF에서 사용하기에 적합한 것으로 나열된 XML 스키마 데이터 타입은 RDF에서 "동등한 것중 첫 번째" 상태를 갖는다. 이미 언급했듯이 [Fig. 8](#fig-8)과 [Fig. 9](#fig-9)의 예에서 이러한 XML 스키마 데이터 타입중 일부를 사용했으며 여기서는 타입이 지정된 리터럴의 대부분의 다른 예에서도 이러한 데이터 타입을 사용할 것이다 (한 가지 이유로 XML 스키마 데이터 타입을 [XML Schema Part 2: Datatypes](http://www.w3.org/TR/xmlschema-2/)에 지정되어 참조하는 데 사용할 수 있는 URIrefs에 이미 할당하였다.). 이 XML 스키마 데이터 타입은 다른 데이터 타입과 다르지 않게 취급되지만 가장 널리 사용될 것으로 예상되므로 다른 소프트웨어 간에 상호 운용할 가능성이 가장 높다. 결과적으로 이러한 데이터 타입을 처리하기 위해 많은 RDF 소프트웨어도 작성될 것으로 예상된다. 그러나 RDF 소프트웨어는 이미 설명된 것처럼 RDF와 함께 사용하기에 적합하다고 판단되면 다른 데이터 타입들도 처리할 수 있도록 작성되어야 한다.

일반적으로 소프트웨어가 처리할 수 있도록 작성되지 않은 데이터 타입에 대한 참조가 포함된 RDF 데이터를 처리하기 위해 RDF 소프트웨어가 호출될 수 있으며, 이 경우 소프트웨어가 수행할 수 없는 몇 가지 작업이 있다. 우선 `rdf:XMLLiteral`을 제외하고 RDF 자체는 데이터 타입을 식별하는 URIref를 정의하지 않는다. 결과적으로 특정 URIref를 인식하도록 작성되지 않았을 경우 RDF 소프트웨어는 타입이 지정된 리터럴로 작성된 URIref가 실제로 데이터 타입을 식별하는지 여부를 결정할 수 없다. 게다가 URIref가 데이터 타입을 식별할 수 있다 하더라도 RDF 자체는 해당 데이터 타입을 특정 리터럴과 페어링하는 유효성을 정의하지 않는다. 특정 데이터 타입을 정확히 처리할 수 있도록 작성된 소프트웨어만 이 유효성을 결정할 수 있다.

예를 들어 triple에서 입력된 리터럴은 다음과 같다.

```
exstaff:85740  exterms:age  "pumpkin"^^xsd:integer .
```

또는 [Fig. 10](#fig-10)의 그래프와 같다.

<center>![fig-10](images/turtle-primer/fig10.png)<br>
Fig. 10 Jonh Smith의 age의 유효하지 않은 타입이 지정된 리터럴</center> <a id="fig-10"></a>

유효한 RDF이지만 "pumpkin"이 `xsd:integer`의 어휘 공간에 있는 것으로 정의되지 않았기 때문에 `xsd:integer` 데이터 타입에 관한 한 분명히 오류이다. `xsd:integer` 데이터 타입을 처리할 수 있도록 작성되지 않은 RDF 소프트웨어는 이 오류를 인식할 수 없다.

그러나 RDF 타입으로 주어진 리터럴을 적절하게 사용하면 리터럴 값이 의도하는 해석에 대한 더 많은 정보를 제공하므로 RDF 문을 어플리케이션 간의 정보 교환을 위한 보다 나은 수단으로 제공할 수 있다.

### 개념 요약(Concepts Summary)
전체적으로 RDF는 기본적으로 단순하다. 즉, URIref로 식별되는 항목에 대한 문장으로 해석되는 노드와 아크 다이어그램이다. 이 절에서는 이러한 개념에 대한 소개를 하였다. 앞서 언급한 바와 같이, 이러한 개념을 기술하는 규범적(즉, 단정적) RDF 사양은 [RDF Concepts and Abstract Syntax](http://www.w3.org/TR/rdf-concepts/)이며 추가 정보를 위해 참조할 수 있다. 이러한 개념의 형식적 시멘틱(의미)은 (규범적) [RDF Semantics](http://www.w3.org/TR/rdf-mt/) 문서에 정의되어 있다.

그러나 지금까지 논의된 RDF 문을 사용하여 사물을 서술하는 기본 기술 외에도 사람이나 조직을 해당 문에서 사용하려는 *어휘*(용어), 특히 다음 어휘를 기술하는 방법이 필요함이 분명하다.

- 사물의 타입 기술(`exterms:Person`)
- 속성 기술(`exterms:age`, `exterms:creation-date` 등)
- (`exterms:age` 속성의 값이 항상 `xsd:integer`여야 함을 명시하듯이) 그러한 속성을 포함하는 문장의 subject 또는 object로 작용할 수 있는 사물 타입을 기술

RDF에서 이러한 어휘를 기술하기 위한 기초는 [RDF Vocabulary Description Language 1.0: RDF Schema](http://www.w3.org/TR/rdf-schema/)이며, 이를 [RDF 어휘 정의: RDF 스키마](#definig-rdf-vocabularies)에서 설명한다.

RDF의 기초가 되는 기본 아이디어에 대한 추가 배경과 웹 정보를 기술하기 위한 일반 언어를 제공하는 역할은 [Web Architecture: Describing and Exchanging Data](http://www.w3.org/1999/04/WebData)에서 볼 수 있다. RDF는 개념 그래프, 논리 기반 지식 표현, 프레임 및 관계형 데이터베이스를 포함하고, 지식 표현, 인공 지능, 데이터 관리 등에 아이디어를 활용한다. 이러한 주제에 대한 배경 정보를 [Knowledge Representation: Logical, Philosophical and Computational Foundations](?), [Conceptual Graphs](http://users.bestweb.net/~sowa/cg/cgstand.htm), [Knowledge Interchange Format](http://logic.stanford.edu/kif/dpans.html), [In Defense of Logic](?), [Artificial Intelligence: Structures and Strategies for Complex Problem Solving (3rd ed.)](?), [Logic, Algebra and Databases](?) 등에서 찾을 수 있다.

## RDF의 Turtle 텍스트 구문(A textual syntax for RDF: Turtle) <a id="rdf-turtle"></a>
[리소스에 대한 문장 생성](#making-statments)에서 설명한 바와 같이 RDF의 개념적 모델은 그래프이다. Turtle은 RDF 그래프를 기록하고 교환하기 위한 텍스트 구문을 제공한다. 이 구문은 지금까지 이 문서에서 사용된 triple 표기법을 포함하지만 RDF 그래프의 사양을 단순화하기 위한 몇 가지 추가 구문을 포함하고 있다. 여기에서는 이 구문에 대해 설명한다.

### 기본 원칙(Basic Principles) 
Turtle 구문의 기본 아이디어는 이미 제시된 몇 가지 예를 사용하여 설명할 수 있다. 영어 문장을 예로 들어보자.

```
`http://www.example.org/index.html` has a `creation-date` whose value is `August 16, 1999`
```

`creation-date` 속성에 URIref를 할당한 다음 위 문장에 대한 RDF 그래프는 [Fig. 11](fig-11)과 같다.

<center>![fig-11](images/turtle-primer/fig11.png)<br>
Fig. 11 웹 페이지의 Creation Date 서술</center> <a id="fig-11"></a>

triple 표현은 다음과 같다.

```
ex:index.html   exterms:creation-date   "August 16, 1999" .
```

> **NOTE**
> 이 예에서는 날짜 값에 지정된 타입 리터럴을 사용하지 않았다. Turtle에서 지정된 타입 리터럴의 표현을 이 절의 뒷부분에서 설명한다.

[EXAMPLE 2](#ex-2)는 [Fig. 11](#fig-11)의 그래프에 해당하는 Turtle 구문이다.

> EXAMPLE 2 <a id="ex-2"></a>

```
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix exterms: <http://www.example.org/terms/>.

<http://www.example.org/index.html> exterms:creation-date "August 16, 1999".
```

지금까지 사용된 것에 대한 유일한 추가 구문 요소는 `@prefix` 키워드를 사용하는 첫 두 줄이다. 이것들은 네임스페이스 선언이다. 즉, 접두어당 한 줄씩 문서에서 사용하는 접두어와 URI의 연관이다.

Turtle 구문은 일반적인 용도를 보다 쉽게 작성할 수 있도록 여러 약어를 제공한다. 예를 들어 동일한 리소스에 대해 여러 속성과 값을 동시에 기술하는 것이 일반적이다. 이러한 경우를 처리하기 위해 Turtle은 해당 속성을 나타내는 여러 속성 요소가 subject 리소스를 식별하는 기호 요소 내에 중첩되도록 허용한다. 예를 들어 `http://www.example.org/index.html`에 대한 다음 문장들을 다음과 같아 표현할 수 있다.

```
ex:index.html   dc:creator              exstaff:85740 .
ex:index.html   exterms:creation-date   "August 16, 1999" .
ex:index.html   dc:language             "en" .
```

그래프([Fig. 3](#fig-3)과 동일)는 [Fig. 12](#fig-12)와 같다.

<center>![fig-12](images/turtle-primer/fig3.png)<br>
Fig. 12 동일한 리소스에 대한 여러 문장</center> <a id="fig-12"></a>

[Fig. 12](#fig-12)의 그래프를 Turtle 버전으로 다음과 같이 작성할 수 있다.

> EXAMPLE 3: 다수의 속성의 약어 <a id="ex-3"></a>

```
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix dc:      <http://purl.org/dc/elements/1.1/#>.
@prefix exterms: <http://www.example.org/terms/>.

<http://www.example.org/index.html> 
    exterms:creation-date "August 16, 1999";
    dc:language "en";
    dc:creator <http://www.example.org/staffid/85740>.
```

이전 예와 비교하여 [EXAMPLE 3](#ex-3)는 동일한 subject에 `dc:language`와 `dc:creator` 속성을 추가하였다. ";"의 사용법에 유의하시오. 이 문자는 동일한 주제에 대한 추가 속성-object 쌍을 분리한다. 이러한 쌍의 시리즈는 "."로 끝낸다. 또한 더 읽기 쉬운 형식을 보장하기 위해 공백 문자를 어디서도 사용할 수 있다.

[EXAMPLE 3](#ex-3)에서는 약어를 이해하는 것이 중요하다. 각 문장을 별도로 작성한 [EXAMPLE 4](#ex-4)는 정확히 동일한 RDF 그래프를 기술한다([Fig. 12](#fig-12)의 그래프).

> EXAMPLE 4: [EXAMPLE 3](#ex-3)를 별도 문장으로 작성 <a id="ex-4"></a>

```
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix dc:      <http://purl.org/dc/elements/1.1/#>.
@prefix exterms: <hhttp://www.example.org/terms/>.

<http://www.example.org/index.html> exterms:creation-date "August 16, 1999".
<http://www.example.org/index.html> dc:language           "en".
<http://www.example.org/index.html> dc:creator            <http://www.example.org/staffid/85740>.
```

[RDF URIref 축약 및 구성](#abbrevating-and-organizing)에서는 몇 가지 추가 Turtle 약어에 대해 설명합니다. [Turtle 사양 문서](turtle-primer.md)는 사용 가능한 약어에 대한 자세한 설명을 하고 있다.

Turtle은 URIref가 없는 노드, 즉 [구조화된 속성 값과 빈 노드](#structured-property-values)에 설명된 빈 노드를 포함하는 그래프를 나타낼 수도 있다. 예를 들어, [Fig. 13](#fig-13)([RDF/XML Syntax Specification (Revised)](http://www.w3.org/TR/rdf-syntax-grammar/)에서 가져옴)은 "문서 'http://www.w3.org/TR/rdf-syntax-grammar'의 제목이 'RDF/XML Syntax Specification (Revised)'이고, 편집자는 이름이 'Dave Beckett'이며 그리고 그의 홈페이지는 'http://purl.org/net/dajobe/'이다.

<center>![fig-13](images/turtle-primer/fig13.png)<br>
Fig. 13 빈 노드를 포함한 그래프</center> <a id="fig-13"></a>

위 그림은 [구조화된 속성 값과 빈 노드](#structured-property-values)에서 논의된 아이디어를 보여준다. URIref는 없지만 다른 정보의 관점에서 설명될 수 있는 것을 나타내기 위해 공백 노드를 사용한다. 이 경우 공백 노드는 문서의 편집자인 사람을 나타내며 이름과 홈페이지로 기술된다.

Turtle은 빈 노드가 포함된 그래프를 나타내는 여러 가지 방법을 제공한다. 가장 직접적인 접근 방식으로 여기서 설명하는 방식은 (이전 예에서 직접 triple의 경우와 마찬가지로) 각 공백 노드에 *공백 노드 식별자*를 할당하는 것이다. 공백 노드 식별자는 특정 Turtle 문서 내에서 공백 노드를 식별하는 역할을 하며 URIref와 달리 할당된 문서 외부에서는 알 수 없다. Turtle에서는 공백 노드 식별자가 값으로 포함된 특수 네임스페이스 접두사 "_"를 사용하여 공백 노드를 참조하며, 그렇지 않으면 리소스의 URIref로 취급한다. [EXAMPLE 5](#ex-5)는 이 기능을 사용한 [Fig. 13](#fig-13)에 해당하는 Turtle 문이다.

> EXAMPLE 5: 빈 노드를 기술하는 Turtle <a id="ex-5"></a>

```
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix dc:      <http://purl.org/dc/elements/1.1/#>.
@prefix exterms: <http://www.example.org/terms/>.

<http://www.w3.org/TR/rdf-syntax-grammar> 
    dc:title "RDF/XML Syntax Specification (Revised)";
    exterm:editor _:abc.
        
_:abc 
    exterms:fullName "Dave Beckett";
    exterms:homePage <http://purl.org/net/dajobe/>.
```

[EXAMPLE 5](#ex-5)에서 공백 노드 식별자 `abc`는 공백 노드를 여러 명령문의 subject로 식별하는 데 사용되며, 자원의 `exterms:editor` 속성 값임을 나타내기 위해 공백 노드를 별도로 사용한다. 공백 노드 식별자를 사용하는 이점은 동일한 Turtle 문서내 여러 위치에서 동일한 공백 노드를 참조할 수 있다는 것이다.

그러나 Turtle 문서 내에서 빈 노드를 꼭 재사용할 필요는 없다. Turtle은 또한 지역 이름을 정의할 필요 없이 익명의 공백 노드를 사용하여 축약된 구문을 제공할 수 있다. [EXAMPLE 6](#ex-6)은 [Fig. 13](#fig-13)의 그래프 직렬화를 위한 Turtle의 다른 접근 방식을 보여주고 있다.

> EXAMPLE 6: 익명 빈 노드를 서술하는 Turtle <a id="ex-6"></a>

```
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix dc:      <http://purl.org/dc/elements/1.1/#>.
@prefix exterms: <http://www.example.org/terms/>.

<http://www.w3.org/TR/rdf-syntax-grammar> 
    dc:title "RDF/XML Syntax Specification (Revised)";
    exterm:editor [
        exterms:fullName "Dave Beckett";
        exterms:homePage <http://purl.org/net/dajobe/>
    ].   
```

[EXAMPLE 6](#ex-6)에서 `[`와 `]` 쌍으로 시스템(기본적으로 파서)에게 식별이 필요한 빈 노드를 알린다. 이 접근 방식은 동일한 Turtle 파일의 다른 부분에서 이 빈 노드에 액세스할 수 있는 가능성을 제공하지 않지만 많은 빈 노드 많은 사용 사례가 있다.

나머지 부분 예에서 리터럴 값의 의도된 해석에 보다 많은 정보를 전달할 때 주어진 타입의 리터럴 값을 강조하기 위해 일반(타입이 지정되지 않은) 리터럴 보다는 적절한 데이터 타입에서 타입이 지정된 리터럴을 사용한다. (예외는 해당 어플리케이션 사용법을 정확하게 반영하기 위하여 현재 타입이 지정된 리터럴을 사용하지 않는 실제 어플리케이션에서 가져온 예에서 일반 리터럴을 계속 사용할 경우이다.) Turtle에서는 일반 및 지정된 타입 리터럴(과 특정 예외를 제외하고 태그)는 [유니코드](http://www.unicode.org/unicode/standard/versions/) 문자를 포함할 수 있으므로 많은 언어의 정보를 직접 표현할 수 있다.

### RDF URIref 축약 및 구성(Abbreviating and Organizing RDF URIrefs) <a id="abbrevating-and-organizing"></a>

지금까지 예에서는 기술된 리소스에 이미 URIref가 주워졌다고 가정했다. 예를 들어, 초기 예제는 URIref가 `http://www.example.org/index.html`인 example.org의 웹 페이지에 대한 서술 정보를 제공했다. 이 리소스는 전체 URIref를 사용하여 Turtle에서 식별되었다. RDF는 URIref가 리소스에 할당되는 방식을 지정하거나 제어하지 않지만 때로는 조직화된 리소스 그룹의 일부인 리소스에 대하여도 URIref를 할당하는 것이 바람직하다. 예를 들어, 스포츠 용품 회사인 example.com이 텐트, 하이킹 부츠 등과 같은 제품의 RDF 기반 카탈로그를 `http://www.example.com/2002/04/product`로 식별되고(에 위치한) RDF/XML 문서로 제공하려고 한다고 가정하자. 이 리소스에서, 각 제품에 별도의 RDF 서술을 제공할 수 있다. 이 카탈로그는 이러한 설명 중 하나와 함께 "Overnighter"라는 텐트 모델에 대한 카탈로그 항목을 [EXAMPLE 7](#ex-7)과 같이 Turtle로 작성할 수 있다.

> EXAMPLE 7: example.com의 카탈로그를 위한 Turtle <a id="ex-7" ></a>

```
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix xsd:     <http://www.w3.org/2001/XMLSchema#>.
@prefix exterms: <hhttp://www.example.org/terms/>.

:item10245 
   exterm:models     "Overnighter"^^xsd:string;
   exterm:sleeps     "2"^^xsd:integer;
   exterm:weight     "2.4"^^xsd:decimal;
   exterm:packedSize "784"^^xsd:integer.

  ...other product descriptions...
```

[EXAMPLE 7](#ex-7)에서 기슬한 리소스(텐트)의 속성(모델, 수용 용량, 무게)을 나타내는 방식은 이전 예와 유사하다. 또한 다양한 속성 값과 연결된 데이터 타입을 명시적으로 제공하지만 이러한 정보가 값을 올바르게 해석하는 데 사용할 수 있어야 함에도 불구하고 이러한 속성 값 중 일부와 연결된 단위는 그렇지 않다. 속성 값과 연관될 수 있는 단위 및 유사한 정보의 표현을 [구조화된 값에 대한 추가 정보](#more-structured-values)에서 논의한다. 이 예에서 `exterms:sleeps`의 값은 텐트가 잠을 잘 수 있는 사람의 수로 `exterms:weight`의 값은 킬로그램으로, `exterms:packedSize`의 값은 텐트가 백팩에서 차지하는 면적인 평방 센티미터로 주어진다.

이전 예와 중요한 *차이점*은 리소스를 식별하기 위해 `:item10245`를 사용하는 것이다. *접두사 없이* 네임스페이스를 사용하면 서술되는 리소스의 전체 URIref의 약어로 *조각 식별자(fragment identifier)*임을 명시한다. *기본 URI*(이 경우 카탈로그를 포함하는 문서의 URI)를 기준으로 조각 식별자 `item10245`로 해석한다. 텐트에 대한 전체 URIref는 기본 URI(카탈로그)를 취하고 (다음이 조각 식별자임을 나타내기 위한) 문자 "#"을 더한 다음 여기에 `item10245`를 추가하여 절대 URIref `http://www.example.com/2002/04/products#item10245`를 제공함으로써 형성된다. 

이 형식은 현재 기본 URI(이 예에서는 카탈로그의 URI)내에서 상대적으로 고유해야 하는 이름을 정의한다는 점에서 XML과 HTML의 `id` 속성과 다소 유사하다. 이 카탈로그 내의 다른 Turtle 문에서 절대 URIref `http://www.example.com/2002/04/products#item10245` 또는 상대 URIref `:item10245`를 사용하여 텐트를 참조할 수 있다. 상대 URIref는 카탈로그의 기본 URIref에서 부터 정의된 URIref로 이해된다. 상대 URIref의 이러한 사용은 RDF와 독립적으로 텐트에 할당된 전체 URIref의 약어 또는 카탈로그 내의 텐트에 대한 URIref 할당으로 여겨질 수 있다.

카탈로그 *외부*에 위치한 RDF에서는 전체 URIref를 사용하여 이 텐트를 참조할 수 있다. 즉, 텐트의 상대 URIref #item10245를 카탈로그의 기본 URI에 연결하여 절대 URIref `http://www.example.com/2002/04/products#item10245`을 만든다. 예를 들어, 아웃도어 스포츠 웹 사이트 `exampleRatings.com`은 RDF를 사용하여 다양한 텐트의 등급을 제공할 수 있다. [EXAMPLE 7](#ex-7)에 서술된 텐트에 부여된 (별 5개) 등급을 부여하여 [EXAMPLE 8](#ex-8)과 같이 exampleRatings.com의 웹 사이트에 표시될 수 있다.

> EXAMPLE 8: 텐트의 exampleRatings.com의 등급 <a id="ex-8"></a>

```
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix sportex: <http://www.exampleRatings.com/terms/>.
@prefix xsd:     <http://www.w3.org/2001/XMLSchema#>.

<http://www.example.com/2002/04/products#item10245>
     sportex:ratingBy    "Richard Roe"^^xsd:string;
     sportex:numberStars "5"^^xsd:integer.
```

[EXAMPLE 8](#ex-8)은 텐트의 전체 URIref를 사용한다. 이 URIref를 사용하면 등급에서 참조되는 텐트를 정확하게 식별할 수 있다.

이 예는 몇 가지 점을 설명하고 있다. 첫째, RDF가 URIref를 리소스에 할당하는 방식을 지정하거나 제어하지 않더라도 (이 경우 카탈로그의 다양한 텐트와 기타 항목), 해당 리소스에 대한 서술의 소스로 식별하는 단일 문서(이 경우 카탈로그)를 해당 문서 내의 프로세스(RDF 외부)를 해당 리소스에 대한 서술에 상대 URIref 사용과 결합하여 URIref를 RDF의 자원에 할당하는 효과를 얻을 수 있다. 예를 들어, 제품의 항목 번호가 카탈로그의 항목에 없으면 example.com에 알려진 제품이 아니라는 관점에서 example.com은 제품을 설명하는 중앙 소스로 이 카탈로그를 사용할 수 있다. (RDF는 URIref가 동일한 베이스을 갖거나 유사하기 때문에 두 리소스 간에 특정 관계가 존재한다고 가정하지 않는다. 이 관계는 example.com에 알려질 수 있지만 RDF에 의해 직접 정의되지는 않는다.)

이러한 예는 또한 누구나 원하는 어휘를 사용하여 기존 리소스에 대한 정보를 자유롭게 추가할 수 있어야 한다는 웹의 기본 아키텍처 원칙 중 하나를 보여준다 ([What the Semantic Web can represent](http://www.w3.org/DesignIssues/RDFnot.html). 더우기 이 예는 특정 리소스를 서술하는 RDF가 모두 한 곳에 있을 필요는 없음을 보인다. 대신 웹을 통해 배포될 수도 있다. 이것은 한 조직이 다른 조직이 정의한 리소스에 대해 평가하거나 논평하는 이와 같은 상황뿐만 아니라 리소스의 원래 정의자(또는 다른 사람)가 해당 서술을 확대하려는 상황에도 적용할 수 있다. 리소스에 대한 추가 정보를 제공합니다. 이는 추가 정보를 서술하는 데 필요한 속성과 값을 추가하기 위해 리소스를 원래 서술한 RDF 문서를 수정하여 수행할 수 있다. 또는 이 예에서 설명하는 것처럼 URIref를 통해 원본 리소스를 참조하여 추가 속성과 값을 제공하는 별도의 문서를 만들 수 있다.

위의 논의는 `:item10245`와 같은 상대 URIref가 기본 URI를 기준으로 해석될 것임을 나타낸다. 기본적으로 기본 URI는 상대 URIref가 사용하는 리소스의 URI이다. 그러나 어떤 경우에는 기본 URI를 명시적으로 지정할 수 있는 것이 바람직하다. 예를 들어, `http://www.example.com/2002/04/products`에 있는 카탈로그 외에도 example.org가 `http://mirrorexample.com/2002/04/products`와 같은 미러 사이트의 중복 카탈로그를 제공하려고 한다고 가정하자. 미러 사이트에서 카탈로그를 액세스한 경우 예제 텐트에 대한 URIref는 `http://www.example.com/2002/04/products#item10245`이 아닌 포함 문서의 URI로부터 `http://mirror.example.com/2002/04/products`으로 생성되므로 문제가 발생할 수 있다. 따라서 의도한 것과 다른 리소스를 분명히 참조할 것이다. 또는 example.org는 기본을 정의하는 단일 소스 문서 사이트를 게시하지 *않고* 제품 URIref들에 기본 URIref를 할당하기를 원할 수 있다.

이러한 경우를 처리하기 위해 Turtle은 Turtle 문서가 문서 자체의 URI가 아닌 기본 URI를 지정할 수 있는 `@base` 기능을 지원한다. [EXAMPLE 9](#ex-9)는 XML Base를 사용하여 카탈로그를 서술하는 방법을 보인다.

> EXAMPLE 9: exmaple.com의 카탈로그에서 Base 사용 <a id="ex-9"></a>

```
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix exterms: <hhttp://www.example.org/terms/>.
@prefix xsd:     <http://www.w3.org/2001/XMLSchema#>.
@base            <http://www.example.com/2002/04/products>.

:item10245 
   exterm:models     "Overnighter"^^xsd:string;
   exterm:sleeps     "2"^^xsd:integer;
   exterm:weight     "2.4"^^xsd:decimal;
   exterm:packedSize "784"^^xsd:integer.

  ...other product descriptions...
```

[EXAMPLE 10](#ex-10)에서 @base 선언은 파일 요소 내의 콘텐츠에 대한 기본 URI가 `http://www.example.com/2002/04/products`이고, 해당 콘텐츠 내에서 인용된 모든 상대 URIref가 포함하는 문서의 URI가 무엇이든 상관없이 해당 기반을 기준으로 해석됨을 지정한다. 결과적으로 텐트의 상대 URIref `#item10245`는 카탈로그의 실제 URI가 무엇이든 또는 기본 URIref가 실제로 특정 문서를 식별하는지 여부에 상관없이 동일한 절대 URIref `http://www.example.com/2002/04/products#item10245`로 해석된다. 

지금까지 예제에서는 example.com 카탈로그의 특정 텐트 모델인 단일 제품 서술을 사용했다. 그러나 example.com은 아마도 배낭, 등산화 등과 같은 다른 범주의 제품뿐만 아니라 여러 가지 다른 모델의 텐트를 제공할 것이다. 사물이 다른 *종류*나 *범주*로 분류된다는 이 아이디어는 다른 *타입*이나 *클래스*를 가진 프로그래밍 언어의 객체 개념과 유사하다. RDF는 미리 정의된 속성인 `rdf:type`을 제공하여 이 개념을 지원한다. RDF 리소스를 `rdf:type` 속성으로 서술할 때 해당 속성의 값은 사물의 범주 또는 *클래스*를 나타내는 리소스로 간주되고 해당 속성의 subject는 해당 범주 또는 클래스의 *인스턴스*로 간주된다. `rdf:type`을 사용하여 [EXAMPLE 10](#ex-10)은 example.com이 제품 설명이 텐트 설명임을 나타내는 방법을 보인다.

> EXAMPLE 10: `rdf:type`으로 텐트를 기술 <a id="ex-10"></a>

```
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix exterms: <http://www.example.org/terms/>.
@prefix xsd:     <http://www.w3.org/2001/XMLSchema#>.
@base <http://www.example.com/2002/04/products>.

:item10245 
   rdf:type          <http://www.example.com/terms/Tent>;
   exterm:models     "Overnighter"^^xsd:string;
   exterm:sleeps     "2"^^xsd:integer;
   exterm:weight     "2.4"^^xsd:decimal;
   exterm:packedSize "784"^^xsd:integer.
```

[EXAMPLE 10](#ex-10)에서 `rdf:type` 속성은 기숳하는 리소스가 URIref `http://www.example.com/terms/Tent`에 의해 식별되는 클래스의 인스턴스임을 나타낸다. 이는 example.com이 다른 용어(예: 속성 `exterms:weight`)를 기술하는 데 사용하는 것과 동일한 어휘의 일부로 해당 클래스를 기술했다고 가정하므로 이를 참조하는 데 클래스의 절대 URIref를 사용한다. example.com이 이러한 클래스를 제품 카탈로그 자체의 일부로 기술했다면 상대 URIref `:Tent`를 사용하여 참조했을 수 있다.

RDF 자체는 이 예제의 Tent와 같은 사물의 어플리케이션별 클래스 또는 `exterms:weight`와 같은 속성을 정의하기 위한 기능을 제공하지 않는다. 대신, 그러한 클래스는 [RDF 어휘 정의: RDF 스키마](#definig-rdf-vocabularies)에서 다룰 RDF 스키마 언어를 사용하여 RDF 스키마에서 설명할 것이다. 클래스를 설명하기 위한 기타 기능도 정의할 수 있다 (예: [보다 풍부한 스키마 언어](#richer-schema-languages)에서 설명한 DAML+OIL과 OWL 언어).

리소스가 특정 타입 또는 클래스의 인스턴스로 리소스를 서술하는 `rdf:type` 속성을 갖는 것은 RDF에서 상당히 일반적이다. Turtle은 키워드를 사용하여 `rdf:type` 속성에 대한 특별한 약어를 도입했다. 이 약어를 사용하여 [EXAMPLE 10](#ex-10)의 example.com의 텐트를 [EXAMPLE 11](#ex-11)과 같이 기술할 수도 있다.

> EXAMPLE 11: 텐트 타입의 축약 <a id="ex-11"></a>

```
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix exterms: <hhttp://www.example.org/terms/>.
@prefix xsd:     <http://www.w3.org/2001/XMLSchema#>.
@base <http://www.example.com/2002/04/products>.

:item10245 a <http://www.example.com/terms/Tent>;
   exterm:models     "Overnighter"^^xsd:string;
   exterm:sleeps     "2"^^xsd:integer;
   exterm:weight     "2.4"^^xsd:decimal;
   exterm:packedSize "784"^^xsd:integer.
```

## 기타 RDF 기능(Other RDF Capabilities)
RDF는 리소스와 RDF 문장 그룹을 나타내는 빌트인 타입과 속성과 같은 많은 추가 기능과 XML 조각을 속성 값으로 나타내는 기능을 제공한다. 이러한 추가 기능을 설명한다.

### RDF 컨테이너(RDF Containers) <a id="containers"></a>
예를 들어, 책을 여러 저자들이 작성하였거나, 과목의 학생들을 나열하거나, 패키지의 소프트웨어 모듈을 나열하는 것과 같이 사물의 *그룹*을 서술해야 하는 경우가 종종 있다. RDF는 이러한 그룹을 기술하는 데 사용할 수 있는 미리 정의된(내장된) 타입과 속성을 제공한다.

첫째, RDF는 사전에 정의된 세 가지 유형으로 구성된 *컨테이너 어휘*를 (사전에 정의된 속성과 함께) 제공한다. *컨테이너*는 사물을 포함하는 리소스이다. 포함된 사물을 *구성원*이라 한다. 컨테이너의 구성원은 리소스(빈 노드 포함) 또는 리터럴일 수 있다. RDF는 세 가지 유형의 컨테이너를 정의한다.

- `rdf:Bag`
- `rdf:Seq`
- `rdf:Alt`

*Bag*(`rdf:Bag` 타입의 리소스)은 리소스 또는 리터럴 그룹을 나타내며, 여기에 중복 멤버가 포함될 수 있으며 여기서 멤버 순서는 중요하지 않다. 예를 들어, 부품 번호의 입력 또는 처리 순서가 중요하지 않은 부품 번호 그룹을 설명하는 데 Bag을 사용할 수 있다.

*Sequence* 또는 *Seq*(타입이 `rdf:Seq`인 리소스)는 리소스 또는 리터럴 그룹을 나타내며, 중복 멤버가 포함될 수 있으며 멤버의 순서가 있다. 예를 들어, Sequence를 알파벳 순서로 유지해야 하는 그룹을 서술하는 데 사용할 수 있다.

*Alternative* 또는 *Alt*(타입이 `rdf:Alt`인 리소스)는 *대안(alternatives)*(일반적으로 속성의 단일 값)인 리터럴 또는 리소스 그룹을 나타낸다. 예를 들어 Alt는 책 제목에 대한 대체 언어 번역을 서술하거나 리소스를 찾을 수 있는 대체 인터넷 사이트 목록을 설명하는 데 사용할 수 있다. 값이 Alt 컨테이너인 속성을 사용하는 어플리케이션은 그룹의 구성원 중 하나를 적절하게 선택할 수 있음을 알고 있어야 한다.

리소스를 이러한 타입의 컨테이너 중 하나로 서술하기 위해 해당 리소스에는 사전에 정의된 리소스 `rdf:Bag`, `rdf:Seq` 또는 `rdf:Alt`(중 적절한 것) 중 하나를 rdf:type 속성의 값으로 지정해야 한다. 컨테이너 리소스(빈 노드 또는 URIref가 있는 리소스일 수 있음)는 그룹 전체를 나타낸다. 컨테이너의 구성원은 컨테이너 리소스를 주체로, 구성원을 개체로 사용하여 각 구성원에 대한 *컨테이너 멤버십 속성*을 정의하여 기술할 수 있다. 이러한 컨테이너 멤버십 속성은 `rdf:_n` 타입의 이름을 가지며, 여기서 *n*은 0 보다 큰 10진수 정수이며 0으로 시작하지 않는다 (예: rdf:_1, rdf:_2, rdf:_3 등). 특히 컨테이너의 구성원을 기술한다. 컨테이너 리소스에는 컨테이너 구성원 속성과 `rdf:type` 속성 외에도 컨테이너를 기술하는 다른 속성이 있을 수 있다.

컨테이너의 일반적인 용도는 속성 값이 사물의 그룹임을 나타내는 것이다. 예를 들어, "Course 6.001 has Students Amy, Mohamed, Johann, Maria, and Phuong" 문장을 나타내기 위한 해당 과정을 값에 `rdf:Bag`(학생 그룹을 나타냄) 타입의 컨테이너인 `s:students` 속성(적절한 어휘로부터)을 부여하여 설명할 수 있다. 그런 다음 [Fig. 14](#fig-14)의 RDF 그래프에서와 같이 컨테이너 구성원 속성을 사용하여 개별 학생을 해당 그룹의 구성원으로 나타낼 수 있다.

<center>![fig-14](images/turtle-primer/fig14.png)<br>
Fig. 14 간단한 Bag 컨테이너 서술</center> <a id="fig-14"></a>

이 예에서 `s:students` 속성 값을 Bag으로 서술하므로 그래프의 구성원 속성 이름에 정수가 있더라도 학생의 URIref에 대해 지정된 순서에는 의미가 없다. `rdf:Bag` 컨테이너를 포함하는 그래프를 생성하고 처리하는 어플리케이션은 멤버십 속성 이름 (명백한) 순서를 무시한다.

[EXAMPLE 12](#ex-12)는 [Fig. 14](#fig-14) 그래프를 서술한다.

> EXAMPLE 12: Student들의 Bag에 대한 RDF/XML <a id="ex-12"></a>

```
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix s:   <http://example.org/students/vocab#>.

<http://example.org/courses/6.001>
    s:students [
        a rdf:Bag;
        rdf:_1 <http://example.org/students/Amy>;
        rdf:_2 <http://example.org/students/Mohamed>;
        rdf:_3 <http://example.org/students/Johann>;
        rdf:_4 <http://example.org/students/Maria>;
        rdf:_5 <http://example.org/students/Phuong>.
    ].
```

`rdf:Seq` 컨테이너의 그래프 구조와 해당 Turtle은 `rdf:Bag`의 그래프 구조와 유사하다 (유일한 차이점은 rdf:Seq 타입이다). 다시 한 번 말하지만 `rdf:Seq` 컨테이너는 시퀀스를 기술하기 위한 것이지만 정수 값 속성 이름의 시퀀스를 적절하게 해석하는 것은 그래프를 만들고 처리하는 어플리케이션에 달려 있다.

> **NOTE** <br>
> 이 예에서는 특수 predicate 이름 "a" 형식으로 속성 `http://www.w3.org/1999/02/22-rdf-syntax-ns#type`의 Turtle 약어도 보여준다. rdf를 자주 타이핑하기 때문에 실제로 이 약어는 매우 유용하다.
> Alt 컨테이너를 설명하기 위해 "X11의 소스 코드는 ftp.example.org, ftp1.example.org 또는 ftp2.example.org에서 찾을 수 있다."라는 문장을 [Fig. 15](#fig-15)의 RDF 그래프로 표현할 수 있다.

<center>![fig-15](images/turtle-primer/fig15.png)<br>
Fig. 15 간단한 Alt 컨테이너 서술</center> <a id="fig-15"></a>

[EXAMPLE 13](#ex-13)은 [Fig. 15](#fig-15)의 그래프를 Turtle로 작성하는 방법을 보인다.

> EXAMPLE 13: Alt 컨테이너의 Turtle <a id="ex-13"></a>

```
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix s:   <http://example.org/packages/vocab#>.

<http://example.org/packages/X11>
    s:DistributionSite [
        a rdf:Alt;
        rdf:_1 <ftp://ftp.example.org>;
        rdf:_2 <ftp://ftp1.example.org>;
        rdf:_3 <ftp://ftp2.example.org>.
    ].
```

Alt 컨테이너에는 `rdf:_1` 속성으로 식별되는 구성원이 적어도 하나 이상 있어야 한다. 이 구성원을 기본값 또는 선호 값(prefereed value)으로 간주한다. `rdf:_1`로 식별된 구성원외 나머지 요소의 순서는 중요하지 않다.

[Fig. 15](#fig-15)의 RDF는 단순히 `s:DistributionSite` 사이트 속성 값이 Alt 컨테이너 리소스 자체임을 명시하고 있다. 이 그래프에서 읽어야 하는, 예를 들어 Alt 컨테이너의 *구성원* 중 하나가 `s:DistributionSite` 사이트 속성의 값으로 간주되거나 `ftp://ftp.example.org`가 기본값 또는 선호 값이라는 추가적 의미는 Alt 컨테이너의 의도된 의미에 대한 어플리케이션의 이해 및/또는 특정 속성(이 경우 `s:DistributionSite`)에 대해 정의된 의미에서 구축되어야 하며, 어플리케이션이 이를 이해해야 한다.

Alt 컨테이너는 언어 태깅과 함께 자주 사용된다. (Turtle은 요소 콘텐츠가 지정된 언어로 되어 있음을 나타내기 위해 언어 태그를 사용하는 것을 허용한다. 이 태그의 사용은 [TURTLE](./turtle-primer.md)에 설명되어 있고, [PRISM](#prism)에 설명되어 있다.) 예를 들어, 제목이 여러 언어로 번역된 작품은 각 언어로 표현된 제목을 나타내는 리터럴을 포함하는 Alt 컨테이너를 가리키는 `title` 속성을 갖을 수 있다.

Bag과 Alt가 뜻하는 의미의 구별은 "Huckleberry Finn"이라는 책의 저자 예를 들어 더 자세히 설명할 수 있다. 이 책의 저자는 정확히 한 명이지만, 저자는 이름이(Mark Twain과 Samuel Clemens) 둘이다. 두 이름 모두로 저자를 지정할 수 있다. 따라서 작성자 이름에 Alt 컨테이너를 사용하는 것이 Bag을 사용하는 것(두 명의 다른 저자가 있음을 암시할 수 있음)보다 관계를 더 정확하게 나타낼 수 있다.

사용자는 RDF 컨테이너 어휘를 사용하는 대신 리소스 그룹을 서술하는 방법을 선택할 수 있다. 일반적으로 사용되는 경우 리소스 그룹과 관련된 데이터를 보다 상호 운용할 수 있도록 만드는 데 도움이 될 수 있는 공통 정의로서 이 RDF 컨테이너를 제공한다.

때때로 이러한 RDF 컨테이너 타입을 사용하는 것에 대한 명확한 대안이 있다. 예를 들어, 특정 리소스와 다른 리소스 그룹 간의 관계를 첫 번째 리소스의 동일한 속성을 사용하여 여러 명령문의 subject로 지정하여 표시할 수 있다. 이는 단일 문장으로 여러 구성원을 포함하는 컨테이너를 object로 표현하는 것과 구조적으로 다르다. 어떤 경우에는 이 두 구조가 동등한 의미를 가질 수 있지만 다른 경우에는 그렇지 않을 수 있다. 주어진 상황에서 어떤 것을 사용할 것인가의 선택은 이것을 염두에 두고 이루어져야 한다.

다음 문장과 같이 작가와 출판물 간의 관계를 예로 들어 보자.

```
Sue has written "Anthology of Time", "Zoological Reasoning", and "Gravitational Reflections".
```

이 경우 동일한 작자가 각각 독립적으로 작성한 세 개의 리소스가 있다. 이것은 반복되는 속성을 사용하여 다음과 같이 표현할 수 있다.

```
exstaff:Sue   exterms:publication   ex:AnthologyOfTime .
exstaff:Sue   exterms:publication   ex:ZoologicalReasoning .
exstaff:Sue   exterms:publication   ex:GravitationalReflections .
```

이 예에서는 동일한 사람이 작성한 것 외에는 출판물 사이에 명시된 관계가 없다. 각 문장은 독립적인 사실이므로 반복되는 속성을 사용하는 것이 합리적인 선택일 것이다. 그러나 이것을 Sue가 작성한 리소스 그룹에 대한 한 문장으로 합리적으로 표현할 수 있다.

```
exstaff:Sue   exterms:publication _:z
_:z           rdf:type            rdf:Bag .
_:z           rdf:_1              ex:AnthologyOfTime .
_:z           rdf:_2              ex:ZoologicalReasoning .
_:z           rdf:_3              ex:GravitationalReflections .
```

한편, 문장

```
The resolution was approved by the Rules Committee, having members Fred, Wilma, and Dino.
```

위원회 전체가 결의안을 승인했다고 한다. 각 위원회 구성원이 결의에 찬성 투표했다고 반드시 명시하지는 않았다. 이 경우 이 문장을 아래와 같이 각 위원회 구성원 한 명씩 각자 `exterms:approvedBy` 문으로 모델링하면 잠재적으로 오도될 수 있다.

```
ex:resolution   exterms:approvedBy   ex:Fred .
ex:resolution   exterms:approvedBy   ex:Wilma .
ex:resolution   exterms:approvedBy   ex:Dino .
```

이 성명서에는 각 회원이 결의안을 개별적으로 승인했다고 나와 있기 때문이다.

이 경우, 주제가 결의안이고 목적이 위원회 자체인 단일 `exterms:approvedBy` 문으로 문장을 모델링하는 것이 바람직하다. 위원회 리소스는 다음 세 구성원이 위원회 구성원인 Bag으로 설명할 수 있다.

```
ex:resolution      exterms:approvedBy   ex:rulesCommittee .
ex:rulesCommittee  rdf:type             rdf:Bag .
ex:rulesCommittee  rdf:_1               ex:Fred .
ex:rulesCommittee  rdf:_2               ex:Wilma .
ex:rulesCommittee  rdf:_3               ex:Dino .
```

RDF 컨테이너를 사용할 때 프로그래밍 언어의 데이터 구조에서와 같이 문장이 컨테이너를 *구성하지* 않는다는 것을 이해하는 것이 중요하다. 대신, 문장은 존재하는 컨테이너(사물 그룹)를 *서술*하고 있다. 예를 들어, 방금 주어진 Rules Committee의 예에서 규칙 위원회는 RDF에 위에서와 같은 식으로 기술되어 있든 그렇지 않든 관계없이 사람들의 순서가 없는 그룹이다. 리소스 `ex:rulesCommittee`가 `rdf:Bag` 타입을 갖는다고 말하는 것은 Rules Committee가 데이터 구조이거나 그룹의 구성원을 보유하기 위해 특정 데이터 구조로 구성되어 있다는 것을 의미하지 않는다 (Rules Committee는 구성원에 대한 기술없이 Bag으로 설명될 수 있다.) 대신 Rules Committee는 Bag 컨테이너와 관련된 특성, 즉 구성원이 있으며 나열 순서가 중요하지 않은 특성을 갖는 것으로 설명된다. 마찬가지로 컨테이너 구성원 속성을 사용하면 컨테이너 리소스가 특정 항목을 구성원으로 갖는 것으로 서술된다. 그렇다고 해서 구성원으로 서술된 것이 존재하는 유일한 구성원이라는 것은 아니다. 예를 들어, Rules Committee를 기술하기 위해 위에 주어진 triple은 Fred, Wilma 및 Dino가 위원회의 유일한 구성원이 아니라 오직 위원회의 구성원임을 나타낸다.

또한 [EXAMPLE 12](#ex-12)와 [EXAMPLE 13](#ex-13)은 관련된 컨테이너 타입(예: 컨테이너 자체를 나타내기 위해 적절한 `rdf:type` 속성이 있는 빈 노드 사용과 순차적으로 번호가 매겨진 컨테이너 멤버십 속성을 생성하는 `rdf:_n` 사용)에 관계없이 컨테이너를 기술하는 일반적인 "패턴"을 보여준다. 그러나 RDF는 RDF 컨테이너 어휘를 사용하는 특정 방식을 *강제하지* 않으므로 이 어휘를 다른 방식으로 사용할 수 있음을 이해하는 것이 중요하다. 예를 들어, 어떤 경우에는 빈 노드를 사용하는 것보다 URIref가 있는 컨테이너 리소스를 사용하는 것이 적절할 수 있다. 게다가, 이전 예에서 보여진 "잘 구성된" 구조로 그래프를 기술할 수 없는 방식으로 컨테이너 어휘를 사용하는 것이 가능하다. 예를 들어, [EXAMPLE 14](#ex-14)는 [Fig. 15](#fig-15)에 표시된 Alt 컨테이너와 유사한 그래프에 대한 Turtle 구문을 보여준다.

> EXAMPLE 14: 나쁘게 구성된 Alt 컨테이너에 대한 Turtle 구문 <a id="ex-14"></a>

```
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix s:   <http://example.org/packages/vocab#>.

<http://example.org/packages/X11>
    s:DistributionSite [
        a rdf:Alt;
        a rdf:Bag;
        rdf:_2 <ftp://ftp.example.org>;
        rdf:_2 <ftp://ftp1.example.org>;
        rdf:_5 <ftp://ftp2.example.org>
    ].
```

[RDF Semantics](http://www.w3.org/TR/rdf-mt/)에서 언급한 바와 같이, RDF는 컨테이너 어휘의 사용에 대해 "정형화" 조건을 부과하지 않으므로 [EXAMPLE 14](#ex-14)는 컨테이너가 Bag과 Alt *모두* 기술되더라도 구문적으로 틀린 것은 아니다. 또한, 그것은 `rdf:_2` 속성이 고유한 두 값을 갖는 것으로 기술되고, `rdf:_1`, `rdf:_3`와 `rdf:_4` 속성을 갖지 않는다.

결과적으로 컨테이너가 "잘 구성된" 상태여야 하는 RDF 어플리케이션이 완전히 견고하기 위해 컨테이너 어휘가 적절하게 사용되고 있는지 확인할 수 있도록 작성되어야 한다.

### RDF 컬렉션(RDF Collections) <a id="rdf-collections"></a>
[RDF 컨테이너](#containers)에서 설명한 컨테이너의 한계는 *닫을* 수 있는 방법이 없다는 것이다. [RDF 컨테이너](#containers)에서 언급했듯이 컨테이너는 식별된 특정 리소스가 구성원이라고만 표시한다. 다른 구성원이 존재하지 않는다고 말하지 않는다. 또한 하나의 그래프가 일부 구성원을 설명할 수 있지만 추가 구성원을 설명하는 다른 그래프가 어딘가에 있을 가능성을 배제할 수 없다. RDF는 지정된 구성원만 포함하는 그룹을 RDF *컬렉션* 형식으로 기술할 수 있도록 지원한다. RDF 컬렉션은 RDF 그래프에서 리스트 구조로 표현되는 사물의 그룹이다. 미리 정의된 타입 `rdf:List`, 미리 정의된 속성 `rdf:first`과 `rdf:rest`, 미리 정의된 리소스 `rdf:nil`로 구성된 미리 정의된 *컬렉션 어휘*를 사용하여 이 리스트 구조를 만든다.

이를 설명하기 위해 "과목 6.001의 학생은 Amy, Mohamed, Johann이다"라는 문장은 [Fig. 16](#fig-16)의 그래프로 나타낼 수 있다.

<center>![fig-16](images/turtle-primer/fig16.png)<br>
Fig. 16 RDF 콜렉션(리스트 구조)</center> <a id="fig-16"></a>

이 그래프에서 `s:Amy`는 컬렉션의 각 구성원은 목록을 나타내는 리소스(이 예에서는 빈 노드)로 subject인 `rdf:first` 속성의 object이다. 이 리스트 리소스는 `rdf:rest` 속성에 의해 목록의 나머지 부분을 연결한다. object로 리소스 `rdf:nil`을 갖는 `rdf:rest` 속성은 목록의 끝을 표시한다 (리소스 `rdf:nil`은 빈 리스트를 나타내며 `rdf:List` 타입으로 정의된다). 이 구조는 Lisp 프로그래밍 언어를 아는 사람들에게는 익숙할 것이다. Lisp에서와 같이 `rdf:first`와  `rdf:rest` 속성을 사용하면 어플리케이션에서 구조를 탐색할 수 있다. 그래프에 명시적으로 표시되어 있지는 않지만. 이 리스트 구조를 형성하는 각 공백 노드는 암묵적으로 `rdf:List` 타입이다 (즉, 이러한 각 노드는 값이 미리 정의된 타입 `rdf:List`인 `rdf:type` 속성을 암묵적으로 갖는다). RDF 스키마 언어([RDF Vocabulary Description Language 1.0: RDF Schema](http://www.w3.org/TR/rdf-schema/))는 속성 `rdf:first`와 `rdf:rest`를 `rdf:List` 타입의 subject로 정의하여 언제나 해당 `rdf:type` triple보다는 일반적으로 이러한 노드에 대한 정보를 유추할 수 있다.

Turtle은 이 형식의 그래프를 사용하여 컬렉션을 쉽게 설명할 수 있도록 특별한 표기법을 제공한다. 이 표기법이 작동하는 방식을 설명하기 위해 [EXAMPLE 15](#ex-15)의 Turtle 문은 [Fig. 16](#fig-16)과 같은 RDF 그래프를 생성한다.

> EXAMPLE 15: 학생 컬랙션의 Turtle 문 <a id="ex-15"></a>

```
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix s:   <http://example.org/students/vocab#>.

<http://example.org/courses/6.001>
    s:students (
        <http://example.org/students/Amy>
        <http://example.org/students/Mohamed>
        <http://example.org/students/Johann>
    ).
```

리스트에 대한 Turtle 약어의 사용은 항상 [Fig. 16](#fig-16)에 표시된 것과 같은 리스트 구조 즉, 주어진 길이와 `rdf:nil`로 끝나는 고정된 유한 항목의 리스트를 정의하고, 리스트 구조 자체에 고유한 "새" 공백 노드를 사용한다. 그러나 RDF는 RDF 모음 어휘를 사용하는 이 특정 방법을 강제하지 않으므로 이 어휘를 다른 방식으로 사용할 수 있으며 그 중 일부는 리스트나 닫힌 모음을 서술하지 않을 수 있다. 그 이유를 알아보기 위해 [Fig. 16](#fig-16)의 그래프는 [EXAMPLE 16](#ex-16)과 같이 컬렉션 어휘를 사용하여 (특별한 Turtle 표기법을 사용하지 않고) "장기적으로" 동일한 triple을 작성하여 Turtle로 작성할 수도 있다.

> EXAMPLE 16: 학생 컬랙션의 장기적 Turtle 문 <a id="ex-16"></a>

```
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix s:   <http://example.org/students/vocab#>.

<http://example.org/courses/6.001> s:student _:sch1.

_:sch1 
    rdf:first <http://example.org/students/Amy>;
    rdf:rest  _:sch2.
        
_:sch2 
    rdf:first <http://example.org/students/Mohamed>;
    rdf:rest  _:sch3.

_:sch3 
    rdf:first <http://example.org/students/Johann>;
    rdf:rest  rdf:nil.
```

[RDF Semantics](http://www.w3.org/TR/rdf-mt/)에서 언급한 바와 같이 ([RDF 컨테이너](#containers)에 설명된 컨테이너 어휘의 경우와 마찬가지로) RDF는 컬렉션 어휘 사용에 대해 "정형화" 조건을 부과하지 않는다. Turtle의 리스트 표기법을 사용하여 자동으로 생성되는 잘 구조화된 그래프 이외의 구조로 RDF 그래프를 정의할 수도 있다. 예를 들어, 주어진 노드에 `rdf:first` 속성의 두 고유한 값이 있다고 주장하거나, 분기되거나 목록이 아닌 꼬리가 있는 구조를 생성하거나, 단순히 컬렉션 설명의 일부를 생략하는 것도 가능하다. 또한, 컬렉션 어휘를 길게 사용하여 정의된 그래프는 리스트 구조에 고유한 공백 노드 대신 URIref를 사용하여 리스트의 구성 요소를 식별할 수 있다. 이 경우 컬렉션에 요소를 효과적으로 추가하여 닫히지 않게 만드는 triple을 다른 그래프에서 생성할 수 있다.

결과적으로 컬렉션을 잘 구성해야 하는 RDF 어플리케이션은 컬렉션 어휘가 적절하게 사용되는지 확인할 수 있도록 견고하고 완전하게 작성되어야 한다. 또한, RDF 그래프의 구조에 추가적인 제약을 정의할 수 있는 [OWL](http://www.w3.org/TR/owl-ref/)과 같은 언어는 이러한 경우 중 일부를 배제할 수 있다.

### RDF 구체화(RDF Reification)
RDF 어플리케이션은 때때로 RDF를 사용하여 다른 RDF 문을 서술해야 하는 경우가 있다. 예를 들어, 언제 문장이 작성되었는지, 누가 작성했는지 또는 기타 유사한 정보(이것을 "출처" 정보라고도 함)에 대한 정보를 기록하기 위해서이다. 예를 들어, [RDF URIref 축약 및 구성](#abbrevating-and-organizing)의 [EXAMPLE 9](#ex-9)는 example.com에서 판매되는 URIref `exproducts:item10245`인 특정 텐트를 기술했다. 텐트의 무게를 기술하는 triple 중 하나는 다음과 같다.

```
exproducts:item10245   exterms:weight   "2.4"^^xsd:decimal .
```

그리고 example.com이 그 특정 정보를 제공한 사람을 기록하는 것이 유용할 수 있다.

RDF는 RDF 문을 기술하기 위한 내장 어휘를 제공한다. 이 어휘를 사용하여 문장에 대한 기술을 문장의 구체화라고 합니다. RDF 구체화 어휘는 `rdf:Statement` 타입과 `rdf:subject`, `rdf:predicate`과 `rdf:object` 속성으로 구성됩된다. 그러나 RDF가 이러한 구체화된 어휘를 제공하고 있지만, 실제로 정의되지 않은 것들을 그 어휘가 정의하고 있다고 상상하기 쉽기 때문에 사용에 주의해야 한하다. 이 점을 이 섹션의 뒷부분에서 더 논의할 것이다.

구체화된 어휘를 사용하여 텐트의 무게에 대한 문장의 *구체화*는 문장에 `exproducts:triple12345`와 같은 URIref를 할당하고 (그러므로 문장을 기술하는 문장을 작성할 수 있음) 다음 문장을 사용하여 문장을 기술한다.

```
exproducts:triple12345  rdf:type        rdf:Statement .
exproducts:triple12345  rdf:subject     exproducts:item10245 .
exproducts:triple12345  rdf:predicate   exterms:weight .
exproducts:triple12345  rdf:object      "2.4"^^xsd:decimal .
```

이 문장은 URIref `exproducts:triple12345`에 의해 식별된 리소스가 RDF 문이고, 문의 subject가 `exproducts:item10245`에 의해 식별된 리소스을 참조하고, presdicate는 `exterms:weight`에 의해 식별된 리소스을 참조한다고 한다. object는 입력된 리터럴 `"2.4"^^xsd:decimal`로 식별되는 10진수 값을 참조한다. 원래 문장이 실제로 `exproducts:triple12345`에 의해 식별된다고 가정하면, 원래 문장과 구체화된 것를 비교하면 구체화된 것이 실제로 그것을 더 명확히 기술해야 한다. RDF 구체화 어휘의 전통적인 사용은 항상 이 패턴의 네 가지 문장을 사용하여 문장을 기술하는 것을 포함한다. 이러한 이유로 네 문장을 때때로 "구체화 쿼드"라고 한다.

이 규칙에 따라 구체화를 하면 example.com에 John Smith가 원래 문장에 URIref(예: 이전과 같이 `exproducts:triple12345`)를 할당하여 텐트의 무게에 대한 원래 문장을 만들었다는 사실을 기록할 수 있다. 다음 John Smith(어떤 John Smith를 참조하는지 식별하기 위해 URIref를 사용하여)가 `exproducts:triple12345`를 작성하였다는 기술을 추가한다. 결과 문장은 다음과 같다.

> EXAMPLE 17: <a id="ex-17"></a>
```
exproducts:triple12345   rdf:type        rdf:Statement .
exproducts:triple12345   rdf:subject     exproducts:item10245 .
exproducts:triple12345   rdf:predicate   exterms:weight .
exproducts:triple12345   rdf:object      "2.4"^^xsd:decimal .
exproducts:triple12345   dc:creator      exstaff:85740 . 
```

John Smith에 대한 문장의 구체화와 속성과 함께 원래 문장은 [그림 17](#럏-17)의 그래프를 형성한다.

<center>![fig-17](images/turtle-primer/fig17.png)<br>
Fig. 17 빈 노드를 포함한 그래프</center> <a id="fig-17"></a>

[RDF URIref 축약 및 구성](#abbrevating-and-organizing)에서는 URIref에 대한 Turtle 약어의 사용을 소개했다. 약어를 속성 요소가 생성하는 triple의 구체화를 자동으로 생성하기 위해 속성 요소에서 사용할 수도 있다. [EXAMPLE 17](#ex-17)은 동일한 그래프를 생성하는 데 약어를 어떻게 사용할 수 있는지 보여준다.

이 경우 `exterms:weight` 요소에 속성 `:triple12345`를 지정하면 텐트의 무게를 설명하는 원래 triple이 생성된다.

```
exproducts:item10245   exterms:weight   "2.4"^^xsd:decimal .
```

추가로 구체화된 triple.

```
exproducts:triple12345   rdf:type        rdf:Statement .
exproducts:triple12345   rdf:subject     exproducts:item10245 .
exproducts:triple12345   rdf:predicate   exterms:weight .
exproducts:triple12345   rdf:object      "2.4"^^xsd:decimal .
```

이러한 구체화된 triple의 subject는 (@base 선언에서 제공한) 문서의 기본 URI, (뒤에 오는 것이 단편 식별자임을 나타내기 위한) 문자 "#"와 약어 값을 연결하여 형성된 URIref이다. 즉, 이전 예와 동일한 subject `exproducts:triple12345`를 갖는 triple들이다.

구체화를 주장하는 것이 원래 문장이 주장하는 것과 동일하지 않으며 다른 것을 암시하지 않는다. 즉, 누군가 John이 천막의 무게에 대해 말했다고 할 때, 그들은 스스로 천막의 무게에 대해 이야기하는 것이 아니라 John이 말한 것에 대해 이야기하는 것이다. 반대로 누군가가 텐트의 무게를 기술할 때 자신이 문장에 대해서도 문장을 작성하지 않는다 (“문장”이라고 하는 사물에 대해 이야기할 의도가 없을 수 있기 때문이다).

위의 텍스트는 의도적으로 여러 곳에서 "구체화의 관습적 사용"을 언급했다. 앞에서 언급했듯이 RDF 구체화 어휘를 사용할 때는 실제로 정의되지 않은 것들을 어휘가 정의한다고 상상하기 쉽기 때문에 주의가 필요하다. 구체화를 성공적으로 사용하는 어플리케이션이 있지만 RDF가 구체화 어휘에 대해 정의하는 실제 의미와 RDF가 이를 지원하기 위해 제공하는 실제 기능에 추가하여 몇 가지 규칙을 따르고 몇 가지 가정을 한다.

우선, 구체화의 관례적인 사용에서, 구체화 triple의 subject는 동일한 subject, predicate와 obejct를 갖는 임의의 triple이 아니라 특정 RDF 문서에서 triple의 *특정 인스턴스*를 식별하는 것이다는 점에 유의하는 것이 중요하다. 이 특정 규칙은 구체화가 이미 주어진 예에서와 같이 구성 날짜 및 출처 정보와 같은 속성을 표현하기 위한 것이며 이러한 속성은 triple의 특정 인스턴스에 적용되어야 하기 때문에 사용된다. 동일한 subject, predicate와 object를 갖는 triple이 여러 개 있을 수 있으며, 그래프는 triple의 집합으로 정의되지만 동일한 triple 구조를 가진 여러 인스턴스가 다른 문서에서도 발생할 수 있다. 따라서 이 관례를 완전히 뒷받침하려면 구체화 triple의 subject를 *어떤 문서에서 개별 triple*과 연관시키는 어떤 수단이 필요하다. 그러나 RDF는 이를 수행할 방법을 제공하지 않는다.

예를 들어, 위의 예에서 텐트의 무게를 서술하는 원래 문장이 네 가지 구체화 문장과 John Smith가 그것을 생성했다는 문장의 subject인 리소스인 리소스 `exproducts:triple12345`라는 것을 실제로 나타내는 triple 또는 Turtle 코드에 명시적 정보가 없다. 이것은 [Fig. 17](#fig-17)의 그래프를 보면 알 수 있다. 원래 설명은 확실히 이 그래프의 일부이지만 그래프의 정보에 관한 한 `exproducts:triple12345`는 해당 부분을 식별하는 것이 아니라 별도의 리소스이다. RDF는 `exproducts:triple12345`와 같은 URIref를 특정 문장 또는 그래프와 연결시키는 방법을 나타내는 빌트인 방법을 제공하지 않고, `exproducts:item10245`와 같은 URIref가 실제 텐트와 연결되는 방식을 나타내는 빌트인 방법을 제공한다. 특정 URIref를 특정 리소스(이 경우 문장)와 연결시키는 것은 RDF 외부 메커니즘을 사용하여 수행해야 한다.

[EXAMPLE 17](#ex-17)에서와 같이 URIref를 약어를 사용하면 구체화가 자동으로 생성되고 구체화에서 문장의 subject로 사용될 URIref를 표시하는 편리한 방법을 제공한다. 더우기, 구체화된 triple의 subject에 대한 URIref를 생성하는 데 약어의 값 `triple12345`를 사용하기 때문에, 그것은 생성의 원인이 된 Turtle 구문의 조각과 구체화의 triple을 관련시키는 부분 "hook"를 제공한다. 그러나 결과 triple에는 원래 triple에 URIref `exproducts:triple12345`가 있었다고 명시적으로 서술하는 것이 없기 때문에 이 관계는 다시 한 번 RDF 외부에 있다 (RDF는 URIref와 triple이 사용되거나 축약된 Turtle 코드 사이에 어떤 관계가 있다고 가정하지 않는다).

URIref를 명령문에 할당하는 내장 수단이 없다는 것은 이러한 종류의 "출처" 정보가 RDF에서 표현될 수 없다는 것을 의미하는 것이 아니라 RDF가 구체화 어휘와 연관시키는 의미만을 사용하여 수행될 수 없다는 것이다. 예를 들어, RDF 문서(예: 웹 페이지)에 URI가 있는 경우 해당 URI로 식별되는 리소스에 대한 서술을 만들 수 있으며, 이러한 문장이 어떻게 해석되어야 하는지에 대한 일부 어플리케이션에 종속적인 이해를 기반으로 어플리케이션이 마치 그러한 문장이 문서의 모든 문장에 "배포"(동일하게 적용)되는 것처럼 동작할 수 있다. 또한 URI를 개별 RDF 문에 할당하는 메커니즘의(RDF 외부에 있는) 경우 문장을 식별하는 URI를 사용하여 개별 문에 대한 문장을 확실히 만들 수 있다. 그러나 이러한 경우에도 기존의 방식으로 구체화 어휘를 엄격하게 사용할 필요는 없다.

이를 보이기 위하여 아래와 같은 원래 문장을 가정하자.

```
exproducts:item10245  exterms:weight  "2.4"^^xsd:decimal .
```

`exproducts:triple12345`의 URIref를 갖는 경우, 간단히 속성에 John Smith을 표현하는 다음과 같은 문장을 작성할 수 있다.

```
exproducts:triple12345  dc:creator  exstaff:85740 .
```

구체화 어휘를 사용하지 않았다 (`exproducts:triple12345`이 `rdf:type rdf:Statement`을 갖는다는 서술이 도움을 줄 수 있다).

또한 구체화 어휘는 특정 triple을 해당 구체화와 연관시키는 방법에 대한 어플리케이션에 종속적인 이해와 함께 위에서 설명한 관례에 따라 직접 사용할 수 있다. 그러나 이 RDF를 수신한 다른 어플리케이션은 반드시 이러한 어플리케이션에 종속적 이해를 공유하지 않을 것이며 따라서 그래프를 적절하게 해석할 필요는 없다.

여기에 설명된 구체화에 대한 해석은 일부 언어에서 볼 수 있는 "인용"과 동일하지 않다는 점에 유의하는 것도 중요하다. 대신, 구체화는 triple의 특정 인스턴스와 triple이 참조하는 리소스 간의 관계를 설명한다. "이 RDF triple을 이러한 형태인 것"이 아니라 "이 RDF triple은 이러한 사물들에 대해 서술한다"라고 하는 것으로 구체화를 직관적으로 읽을 수 있다. 예를 들어, 이 섹션에서 사용된 구체화 예에서 triple인

```
exproducts:triple12345  rdf:subject  exproducts:item10245 .
```

원래 문장의 `rdf:subject`에 대한 서술을 문장의 subject가 URIref `exproducts:item10245`로 식별되는 리소스(텐트)라고 한다. 인용과 같이 문장의 주제가 URIref 자체(즉, 특정 문자로 시작하는 문자열)라고 하지 *않는다*.

### 구조화된 값에 대한 추가 정보(More on Structured Values: rdf:value) <a id="more-structured-values"></a>
[구조화된 속성 값과 빈 노드](#structured-property-values)에서 RDF 모델이 본질적으로 이진 관계만을 지원한다고 언급했다. 즉, 문장은 두 리소스 간의 관계를 명시한다. 예를 들어 다음과 같은 문장은

```
exstaff:85740   exterms:manager   exstaff:62345 .
```

`exterms:manager`라는 두 직원 사이에 유지되는 관계를 나타낸다 (아마도 한 사람이 다른 직원을 관리함).

그러나 어떤 경우에는 더 높은 차원의 관계(두 개 이상의 리소스 사이의 관계)를 포함하는 정보를 RDF로 표현해야 한다. [구조화된 속성 값과 빈 노드](#structured-property-values)에서는 John Smith와 그의 주소 정보 간의 관계를 나타내는 것이 문제이고 John의 주소 값이 그의 street, city, state 및 postal code으로 이루어진 구조화된 값인 경우에 대한 한 가지 예를 논의했다. 이것을 관계로 쓰면 이 주소가 다음 형식의 5-ary 관계임을 알 수 있다.

```
address(exstaff:85740, "1501 Grant Avenue", "Bedford", "Massachusetts", "01730")
```

[구조화된 속성 값과 빈 노드](#structured-property-values)에서는 이러한 종류의 구조화된 정보가 별도의 리소스로 기술된 집합적 사물(여기서는 John의 주소를 나타내는 구성 요소 그룹)을 고려하여 다음과 같이 새 리소스에 대해 별도의 설명을 작성함으로써 RDF로 표현될 수 있다고 언급했다. triple:

```
exstaff:85740   exterms:address        _:johnaddress .
_:johnaddress   exterms:street         "1501 Grant Avenue" .
_:johnaddress   exterms:city           "Bedford" .
_:johnaddress   exterms:state          "Massachusetts" .
_:johnaddress   exterms:postalCode     "01730" .
```

(여기서 `_:johnaddress`는 John의 주소를 나타내는 공백 노드의 식별자이다.)

이것은 RDF에서 n-ary 관계를 나타내는 일반적인 방법이다. 참가자 중 한 명(이 경우 John)을 선택하여 원래 관계(이 경우 주소)의 주체 역할을 한 다음 (URI를 할당하거나 할당하지 않고) 관계의 나머지 부분에 관계의 나머지 구성 요소를 나타내는 새 리소스 속성을 제공한다.

John의 주소의 경우 구조화된 값의 각 부분은 `exterms:address` 속성의 "주요" 값으로 간주될 수 없다. 모든 부분이 가치에 동등하게 기여한다. 그러나 어떤 경우에는 구조화된 값의 부분 중 하나가 종종 "주요" 값으로 생각되고 관계의 다른 부분은 주요 값을 한정하는 추가 컨텍스트 또는 기타 정보를 제공한다. 예를 들어, [RDF URIref 축약 및 구성](#abbrevating-and-organizing)의 [EAMPLE 9](#ex-9)에서 특정 텐트의 무게는 형식화된 리터럴을 사용하여 십진수 값 2.4로 주어졌다.

```
exproduct:item10245   exterms:weight   "2.4"^^xsd:decimal .
```

사실, 무게에 대한 더 완전한 설명은 십진수 2.4가 아니라 *2.4 kilograms*이었을 것이다. 이를 위해 `exterms:weight` 속성의 값은 두 가지 구성요소, 즉 십진수 값을 위한 타입이 지정된 리터럴과 측정 단위(kilogram) 표시를 포함해야 한다. 명시되지 않은 단위 정보를 채울 컨텍스트에 의존하여 값이 (위의 triple에서와 같이) 입력된 리터럴로 간단히 기록되기 때문에 이 상황에서 십진수 값은 `exterms:weight` 속성의 "주요" 값으로 간주될 수 있다. 

RDF 모델에서 이러한 종류의 한정된 속성 값은 단순히 다른 종류의 구조화된 값으로 간주될 수 있다. 이를 표현하기 위해 구조화된 값 전체(이 경우 무게)를 표현하고 원래 문장의 대상으로 사용하기 위해 별도의 리소스를 사용할 수 있다. 그러면 해당 리소스에 구조화된 값의 개별 부분을 나타내는 속성을 제공할 수 있다. 이 경우 10진수 값을 나타내는 타입이 지정된  리터럴에 대한 속성과 단위에 대한 속성이 있어야 한다. RDF는 구조화된 값의 주요 값(있는 경우)을 설명하기 위해 미리 정의된 `rdf:value` 속성을 제공한다. 따라서 이 경우 `rdf:value` 속성의 값으로 타입이 지정된 리터럴을 제공할 수 있고 `exterms:units` 속성의 값으로 리소스 `exunits:kilograms`을 제공할 수 있다 (`exunits:kilograms` 리소스를 example.org의 어휘 일부로 정의하였다고 가정한다). 결과 triple은 다음과 같다.

```
exproduct:item10245   exterms:weight   _:weight10245 .
_:weight10245         rdf:value        "2.4"^^xsd:decimal .
_:weight10245         exterms:units    exunits:kilograms .
```

주요 값을 부여하기 위하여 `rdf:value` 속성과 분류 체계 또는 값을 더 서술하는 다른 정보를 식별하기 위한 추가 속성을 사용하여 다른 분류 체계 또는 등급 시스템에서 가져온 값뿐만 아니라 모든 측정 단위를 사용하여 수량을 나타내는 데 동일한 접근 방식을 사용할 수 있다. 

이러한 목적을 위해 `rdf:value`를 사용할 필요는 없다(예: `exterms:amount`와 같은 사용자 정의 속성을 `rdf:value` 대신 사용할 수 있다). 그리고 RDF는 `rdf:value`에 어떤 특별한 의미도 연관시키지 않는다. `rdf:value`는 이러한 일반적으로 발생하는 상황에서 간단한 사용으로 편리함을 제공한다.

그러나 데이터베이스와 웹 (및 이후의 예)에 있는 많은 기존 데이터가 무게, 비용 등과 같은 속성에 대한 간단한 값의 형태를 취하더라도 이러한 간단한 값은 종종 이러한 속성을 적절하게 서술하기에는 불충분하다는 원칙은 중요하다. 웹과 같은 글로벌 환경에서 속성 값을 액세스하는 모든 사람이 사용하는 단위(또는 기타 컨텍스트 종속적인 정보를 포함하는)를 이해할 것이라고 가정하는 것은 일반적으로 *안전하지 않다*. 예를 들어, 미국 사이트는 무게 값을 pound로 제공할 수 있지만 미국 외부에서 해당 데이터를 액세스하는 누군가는 무게는 kilograms으로 주어졌다고 가정할 수 있다. 웹 환경에서 데이터를 올바르게 해석하려면 추가 정보(예: 단위 정보)가 명시적으로 기록되어야 한다. 이는 `rdf:value`에 속성 이름에 단위 정보를 포함하거나 (예: `exterms:weightInKg`), 단위 정보를 포함하는 특수 데이터 타입을 정의하거나 (예: `extypes:kilograms`), 또는 이 정보를 명시하는 사용자 정의 속성을 추가하는 (예: `exterms:unitOfWeight`) 등 다양한 방법으로 개별 항목 또는 제품 기술, 데이터 세트 기술 (예: 카탈로그 또는 사이트의 모든 데이터) 또는 스키마([RDF 어휘 정의: RDF 스키마](#definig-rdf-vocabularies) 참조)에서 수행할 수 있다.  

## RDF 어휘 정의: RDF 스키마(Defining RDF Vocabularies: RDF Schema) <a id="definig-rdf-vocabularies"></a>
RDF는 명명된 속성과 값을 사용하여 리소스에 대한 간단한 문장을 표현하는 방법을 제공한다. 그러나 RDF 사용자 커뮤니티는 리소스의 특정 종류 또는 클래스를 서술하고 해당 리소스를 서술하는 특정 속성을 사용한다는 것을 나타내기 위해 해당 문장에서 사용하려는 *용어(terms)(어휘(vocabulary))*를 정의하는 기능도 필요하다고 한다. 예를 들어, [RDF URIref 축약 및 구성](#abbrevating-and-organizing)의 예에서 example.com 회사는 `exterms:Tent`와 같은 클래스를 서술하하는 데 `exterms:model`, `exterms:weightInKg`와 `exterms:packedSize`와 같은 속성을 사용하려고 한다 (다양한 "example" 네임스페이스 접두사 QName는 [기본 개념](#basic-concepts)에서 논의된 것처럼 RDF에서 이러한 이름이 실제로 *URI 참조*임을 상기시키기 위해 여기에서 클래스와 속성의 이름으로 사용된다. 유사하게, 서지 리소스 기술에 관심 있는 사람들은 `ex2:Book` 또는 `ex2:MagazineArticle`과 같은 클래스를 서술하고, `ex2:author`, `ex2:title`과 `ex2:subject`와 같은 속성을 사용하여 서술하기를 원할 것이다. 다른 어플리케이션에서는 `ex3:Person`과 `ex3:Company`와 같은 클래스와 `ex3:age`, `ex3:jobTitle`, `ex3:stockSymbol` 및 `ex3:numberOfEmployees`와 같은 속성을 서술해야 할 수 있다. RDF 자체에서 이러한 어플리케이션별 클래스와 속성을 정의하는 수단을 제공하지 않는다. 대신, 여기에서는 RDF 스키마라고 하는 [ RDF Vocabulary Description Language 1.0: RDF Schema](http://www.w3.org/TR/rdf-schema/)가 제공하는 RDF 확장 어휘로 이러한 클래스와 속성을 기술한다.

RDF 스키마는 `exterms:Tent`, `ex2:Book` 또는 `ex3:Person`과 같은 어플리케이션별 클래스의 어휘나 `exterms:weightInKg`, `ex2:author` 또는 `ex3:JobTitle`과 같은 속성을 제공하지 않는다. 대신 이러한 클래스와 속성을 서술하고 함께 사용할 클래스와 속성을 나타내는 데 필요한 기능을 제공한다 (예: `ex3:jobTitle` 속성이 `ex3:Person`을 서술하는 데 사용). 즉, RDF 스키마는 RDF에 대한 *타입 시스템*을 제공한다. RDF 스키마 타입 시스템은 Java와 같은 객체 지향 프로그래밍 언어의 타입 시스템과 몇 가지 면에서 유사하다. 예를 들어, RDF 스키마를 사용하면 리소스를 하나 이상의 클래스 인스턴스로 정의할 수 있다. 또한 클래스를 계층적으로 구성할 수 있다. 예를 들어 `ex:Dog` 클래스를 `ex:Animal`의 하위 클래스인 `ex:Mammal`의 하위 클래스로 정의할 수 있다. 즉, `ex:Dog` 클래스에 속한 리소스는 암묵적으로 `ex:Animal` 클래스에도 속한다. 그러나 **RDF 클래스와 속성은 어떤 면에서 프로그래밍 언어 타입과 매우 다르다**. RDF 클래스와 속성 기술은 정보를 강제로 입력해야 하는 스트레이트재킷(straightjacket)을 만들지 않고 대신 기술하는 RDF 리소스에 대한 추가 정보를 제공한다. [RDF 스키마 선언 해석](#interpreting-rdf-schema)에서 논의될 다양한 방식으로 이 정보를 사용할 수 있다.

RDF 스키마 기능은 자체적으로 RDF 어휘의 형태로 제공된다. 즉, 고유한 의미를 갖는 사전 정의된 RDF 리소스의 특별한 집합이다. RDF 스키마 어휘의 리소스는 접두사가 `http://www.w3.org/2000/01/rdf-schema#`인 URIref를 갖는다 (통상적으로 QName 접두사 `rdfs:`를 사용). RDF 스키마 언어로 작성된 어휘 서술(스키마)은 합법적인 RDF 그래프이다. 따라서 추가 RDF 스키마 어휘를 처리하도록 작성되지 않은 RDF 소프트웨어는 여전히 스키마를 다양한 리소스와 속성으로 구성된 합법적인 RDF 그래프로 해석할 수 있지만 RDF 스키마 용어에 추가된 의미를 "이해"하지 못한다. 이러한 추가 의미를 이해하려면 RDF 소프트웨어는 `rdf:` 어휘뿐만 아니라 `rdfs:` 어휘의 빌트인 의미를 포함하는 확장된 구문를 처리할 수 있도록 작성되어야 한다. 이를 다음 섹션에서 설명하도록 한다.

다음 섹션들에서 RDF 스키마의 기본 리소스와 속성을 설명한다.

### 클래스 서술(Describing Classes) <a id="describing-classes"></a>
모든 종류의 서술 프로세스의 기본 단계는 서술할 다양한 종류를 식별하는 것이다. RDF 스키마는 이러한 "사물의 종류"를 *클래스*라고 한다. RDF 스키마의 *클래스*는 *타입(Type)* 또는 *범주(Category)*의 일반 개념에 해당하며, Java와 같은 객체 지향 프로그래밍 언어의 클래스 개념과 비슷하다. RDF 클래스는 웹 페이지, 사람, 문서 유형, 데이터베이스 또는 추상 개념과 같은 거의 모든 범주의 사물을 나타내는 데 사용할 수 있다. 클래스는 RDF 스키마 리소스 `rdfs:Class`와 `rdfs:Resource` 그리고 `rdf:type`와 `rdfs:subClassOf` 속성을 사용하여 기술된다.

예를 들어 `example.org` 조직에서 RDF를 사용하여 다양한 종류의 자동차에 대한 정보를 제공하려고 한다고 가정하자. RDF 스키마에서 먼저 `example.org`는 자동차인 사물의 범주를 나타내는 클래스가 필요하다. 클래스에 속하는 리소스를 해당 리소스의 인스턴스라고 한다. 이 경우 `example.org`는 이 클래스의 인스턴스가 자동차인 리소스가 되도록 하려 한다.

RDF 스키마에서 *클래스*는 `rdf:type` 속성 값이 리소스 `rdfs:Class`인 리소스이다. 따라서 클래스에 `ex:MotorVehicle`라는 URIref를 할당하고 (`ex:`는 URIref `http://www.example.org/schemas/vehicles`를 뜻하도록 사용되여, `ex:`는 example.org의 어휘에서 URIref의 접두사로 사용된다), `rdf:type` 속성 값이 리소스 `rdfs:Class`인 리소스로 기술하여 자동차 클래스를 서술한다. 즉, example.org는 아래와 같은 RDF 문을 작성한다.

```
ex:MotorVehicle   rdf:type   rdfs:Class .
```

[RDF URIref 축약 및 구성](#abbrevating-and-organizing)에 설명한 것처럼 리소스가 클래스의 인스턴스임을 나타내는 데 속성 `rdf:type`을 사용한다. 따라서 `ex:MotorVehicle`가 클래스라면 리소스 `exthings:companyCar`는 아래 RDF 문장으로 자동차로 기술된다.

```
exthings:companyCar   rdf:type   ex:MotorVehicle .
```

(이 문장은 클래스 이름을 대문자로 시작하여 작성하고 속성과 인스턴스 이름를 소문자로 작성하는 일반적인 규칙을 사용한다. 그러나 이 규칙은 RDF 스키마에서 필요하지 않다. 또한 이 문장은 example.org가 사물의 클래스와 사물의 인스턴스에 대해 별도의 어휘 사용을 결정했다고 가정한다.)

리소스 `rdfs:Class` 자체는 `rdfs:Class`의 `rdf:type`을 갖는다. 리소스는 여러 클래스의 인스턴스일 수 있다.

`ex:MotorVehicle` 클래스를 기술한 후 `example.org`는 승용차, 밴, 미니밴 등과 같은 다양한 종류의 자동차를 나타내는 클래스를 추가할 수 있다. 새로운 클래스 각각에 대해 URIref를 할당하고, 이 리소스를 클래스로 기술하는 RDF 문장을 작성하여 `ex:MotorVehicle` 클래스와 동일한 방식으로 정의할 수 있다. 예:

```
ex:Van     rdf:type   rdfs:Class .
ex:Truck   rdf:type   rdfs:Class .
...
```

그러나 이러한 문장 자체는 개별 클래스만을 기술한다. `example.org`는 또한 클래스 `ex:MotorVehicle`과 특별한 관계, 즉 그들이 `ex:MotorVehicle`의 툭수한 `종류`임을 나타내기를 원할 수 있다.

이 두 클래스를 연결하기 위해 미리 정의된 속성 `rdfs:subClassOf`을 사용하여 두 클래스 간의 이러한 종류의 특수화 관계를 서술할 수 있다. 예를 들어, `ex:Van`이 `ex:MotorVehicle`의 특수한 종류라고 명시하기 위해 `example.org`는 아래 RDF 문장을 작성한다.

```
ex:Van   rdfs:subClassOf   ex:MotorVehicle .
```

`rdfs:subClassOf` 관계의 의미는 `ex:Van` 클래스의 모든 인스턴스가 `ex:MotorVehicle` 클래스의 인스턴스이기도 하다는 것이다. 따라서 리소스 `exthings:companyVan`이 `ex:Van`의 인스턴스이면, `rdfs:subClassOf` 관계를 기반으로, RDF 스키마 어휘를 이해하도록 작성된 RDF 소프트웨어는 `exthings:companyVan` 또한 `ex:MotorVehicle`의 인스턴스라는 추가 정보를 추론할 수 있다.

`exthings:companyVan`의 예는 확장된 언어를 정의하는 RDF 스키마에 대해 앞서 설명한 것을 보인다. RDF 자체는 `rdfs:subClassOf`와 같은 RDF 스키마 어휘에서 용어의 특별한 의미를 정의하지 않는다. 따라서 RDF 스키마가 `ex:Van`과 `ex:MotorVehicle` 간의 `rdfs:subClassOf` 관계를 정의하는 경우 RDF 스키마 용어를 이해하지 못하는 RDF 소프트웨어는 이를 predicate `rdfs:subClassOf`를 갖는 triple로 인식하지만 `rdfs:subClassOf`의 특수한 의미를 이해하지 못하며, `exthings:companyVan`도 `ex:MotorVehicle`의 인스턴스라는 추가적인 추론을 이끌어낼 수 없다.

`rdfs:subClassOf` 속성은 전이적(transitive)이다. 이는 예를 들어 다음과 같은 RDF 문장을 의미한다.

```
ex:Van       rdfs:subClassOf   ex:MotorVehicle .
ex:MiniVan   rdfs:subClassOf   ex:Van .
```

RDF 스키마는 `ex:MiniVan`을 `ex:MotorVehicle`의 하위 클래스로 정의한다. 결과적으로 `ex:MiniVan` 클래스의 인스턴스인 리소스는 (`ex:Van` 클래스의 인스턴스이기도 하며) `ex:MotorVehicle` 클래스의 인스턴스이기도 하다고 RDF 스키마는 정의한다. 클래스는 둘 이상의 클래스의 하위 클래스일 수 있다 (예: `ex:MiniVan`은 `ex:Van`과 `ex:PassengerVehicle`의 하위 클래스일 수 있음). RDF 스키마는 (모든 클래스에 속하는 인스턴스가 리소스이기 때문에) 모든 클래스를 `rdfs:Resource` 클래스의 하위 클래스로 정의한다.

[Fig. 18](#fig-18)은 이 예에서 논의하는 전체 클래스 계층 구조를 보인다.

<center>![fig-18](images/turtle-primer/fig18.png)<br>
Fig. 18 Vehicle 틀래스 계층 구조</center> <a id="fig-18"></a>

(그림을 단순화하기 위해 각 클래스를 `rdfs:Class`에 연결하는 `rdf:type` 속성을 [Fig. 18](#fig-18)에서 생략하였다. 사실, RDF 스키마는 `rdfs:subClassOf` 속성을 사용하는 문장의 subject와 object를 타입 `rdfs:Class`의 리소스로 정의하고, 이 정보를 유추할 수 있다. 그러나 실제로 스키마를 작성할 때 이 정보를 명시적으로 제공하는 것이 바람직하다.)

이 스키마를 triple로도 아래와 같이 작성할 수 있다.

```
ex:MotorVehicle       rdf:type          rdfs:Class .
ex:PassengerVehicle   rdf:type          rdfs:Class .
ex:Van                rdf:type          rdfs:Class .
ex:Truck              rdf:type          rdfs:Class .
ex:MiniVan            rdf:type          rdfs:Class .

ex:PassengerVehicle   rdfs:subClassOf   ex:MotorVehicle .
ex:Van                rdfs:subClassOf   ex:MotorVehicle .
ex:Truck              rdfs:subClassOf   ex:MotorVehicle .

ex:MiniVan            rdfs:subClassOf   ex:Van .
ex:MiniVan            rdfs:subClassOf   ex:PassengerVehicle .
```

### 속성 서술(Describing Properties) <a id="describing-properties"></a>
서술하려는 사물의 특정 클래스를 기술하는 것 외에도 사용자 커뮤니티는 해당 클래스의 특징을 서술하는 특정 속성(예: 승용차를 설명하는 rearSeatLegRoom)을 기술할 수 있어야 한다. RDF 스키마에서 속성을 RDF 클래스 `rdf:Property`와 RDF 스키마 속성 `rdfs:domain`, `rdfs:range`와 `rdfs:subPropertyOf`를 사용하여 기술한다.

RDF의 모든 속성은 `rdf:Property` 클래스의 인스턴스로 정의된다. 따라서 `exterms:weightInKg`와 같은 새 속성은 속성에 URIref를 할당하고 값이 리소스 `rdf:Property`인 `rdf:type` 속성을 사용하여 해당 리소스를 정의하여 기술한다 (예: RDF 문 작성).

```
exterms:weightInKg   rdf:type   rdf:Property .
```

RDF 스키마는 또한 RDF 데이터와 함께 속성과 클래스를 사용하는 방법을 설명하기 위한 어휘를 제공한다. RDF 스키마 속성 `rdfs:range`과 `rdfs:domain`을 사용하여 어플리케이션별 속성을 추가로 설명함으로써 이러한 종류의 가장 중요한 정보를 제공한다.

`rdfs:range` 속성은 특정 속성의 값이 지정된 클래스의 인스턴스임을 나타내는 데 사용된다. 예를 들어 `example.org`가 `ex:author` 속성이 `ex:Person` 클래스 인스턴스 값임을 나타내려면 다음과 같아 RDF 문장을 작성한다.

```
ex:Person   rdf:type     rdfs:Class .
ex:author   rdf:type     rdf:Property .
ex:author   rdfs:range   ex:Person .
```

위의 문장들은 `ex:Person`이 클래스이고, `ex:author`가 속성이며, `ex:author` 속성을 사용하는 RDF 문이 `ex:Person`의 인스턴스를 object로 가지고 있음을 나타낸다.

예를 들어 `ex:hasMother`와 같은 속성은 0, 하나 또는 둘 이상의 range 속성을 가질 수 있다. `ex:hasMother`에 range 속성이 없으면 `ex:hasMother` 속성의 값에 대해 아무 것도 하지 않는다. `ex:hasMother`에 하나의 range 속성이 있는 경우, 예를 들어 `ex:Person`을 range로 지정하는 경우 `ex:hasMother` 속성의 값이 `ex:Person` 클래스의 인스턴스임을 나타낸다. `ex:hasMother`에 둘 이상의 range 속성이 있는 경우, 예를 들어 하나는 range로 `ex:Person`을 지정하고 다른 하나는 range로 `ex:Female`을 지정하는 경우 `ex:hasMother` 속성의 값은 범위로 지정된 모든 클래스의 인스턴스인 리소스임을 나타낸다. 즉 `ex:hasMother`의 모든 값은 `ex:Female`과 `ex:Person`의 인스턴스여야 한다.

이 마지막 사항은 분명하지 않을 수 있다. 그러나 속성 `ex:hasMother`가 `ex:Female`과 `ex:Person`의 두 가지 range를 갖는다는 것은 두 개의 별도 문장을 만드는 것과 관련이 있다.

```
ex:hasMother   rdfs:range   ex:Female .
ex:hasMother   rdfs:range   ex:Person .
```

이 속성을 사용하는 다음과 같은 문장이 있을 때,

```
exstaff:frank   ex:hasMother   exstaff:frances .
```

두 `rdfs:range` 문이 모두 정확하려면 `exstaff:frances`는 `ex:Female`과 `ex:Person`의 인스턴스여야 한다.

`rdfs:range` 속성을 [타입이 지정된 리터럴](#typed-literals)에서 설명한 것처럼 타입이 지정된 리터럴으로 속성 값을 제공함을 나타내는 데 사용할 수도 있다. 예를 들어 `example.org`가 속성 `ex:age`에 XML 스키마 데이터 타입 `xsd:integer` 값이 있음을 나타내려면 아래와 같이 RDF 문을 작성한다.

```
ex:age   rdf:type     rdf:Property .
ex:age   rdfs:range   xsd:integer .
```

데이터 타입 `xsd:integer`는 URIref(URIref `http://www.w3.org/2001/XMLSchema#integer`)로 식별된다. 이 URIref는 데이터 타입을 식별하는 스키마에 명시적인 언급없이 사용할 수 있다. 그러나 주어진 URIref가 데이터 타입을 식별한다고 명시하는 것이 종종 유용하다. 이는 RDF 스키마 클래스 `rdfs:Datatype`을 사용하여 명시할 수 있다. `xsd:integer`가 데이터 타입임을 나타내기 위해 `example.org`는 다음과 같이 RDF 문을 작성한다.

```
xsd:integer   rdf:type   rdfs:Datatype .
```

이 문장은 `xsd:integer`가 데이터 타입의 URIref임을 나타낸다 ([Resource Description Framework (RDF): Concepts and Abstract Syntax](http://www.w3.org/TR/rdf-concepts/)에 설명된 RDF 데이터 타입에 대한 요구 사항을 준수한다고 가정함). 그러한 문장은 예를 들어 `example.org`가 새로운 데이터 타입을 정의한다는 의미에서 데이터 타입의 *정의*를 *구성*하지 않는다. RDF 스키마에서 데이터 타입을 정의하는 방법은 없다. [타입이 지정된 리터럴](#typed-literals)에서 언급했듯이 RDF(및 RDF 스키마) 외부에서 데이터 타입을 정의하며 URIref으로 RDF 문에서 참조된다. 이 문장은 단순히 데이터 타입의 존재를 알리고 이 스키마에서 사용 중임을 명시적으로 나타낸다.

`rdfs:domain` 속성은 특정 속성이 지정된 클래스에 적용됨을 나타내는 데 사용된다. 예를 들어 `example.org`가 `ex:author` 속성이 `ex:Book` 클래스의 인스턴스에 적용됨을 나타내려면 다음과 같이 RDF 문을 작성한다.

```
ex:Book     rdf:type      rdfs:Class .
ex:author   rdf:type      rdf:Property .
ex:author   rdfs:domain   ex:Book .
```

위 문장들은 `ex:Book`이 클래스이고, `ex:author`가 속성이며, `ex:author` 속성을 사용하는 RDF 문이 `ex:Book`의 인스턴스를 subject로 가지고 있음을 나타낸다.

주어진 속성 `exterms:weight`은 0개, 하나 또는 둘 이상의 domain 속성을 가질 수 있다. `exterms:weight`에 domain 속성이 없으면 `exterms:weight` 속성과 함께 사용할 수 있는 리소스에 대해 아무 것도 하지 않는다 (모든 리소스에는 `exterms:weight` 속성이 있을 수 있음). `exterms:weight`에 하나의 domain 속성이 있는 경우, 예를 들어 `ex:Book`을 domain으로 지정하는 속성은 `exterms:weight` 속성이 `ex:Book` 클래스의 인스턴스에 적용됨을 의미한다. `exterms:weight`에 하나 이상의 domain 속성이 있는 경우, 예를 들어 하나는 domain으로 `ex:Book`을 지정하고 다른 하나는 domain으로 `ex:MotorVehicle`을 지정하는 경우 `exterms:weight` 속성이 있는 모든 리소스는 모든 domain으로 지정된 클래스, 즉 `exterms:weight` 속성이 있는 모든 리소스는 `ex:Book`과 `ex:MotorVehicle` 모두이다 (domain과 range 지정 시 주의해야 함을 나타냄).

`rdfs:range`의 경우와 마찬가지로 이 마지막 사항이 명확하지 않을 수 있다. 그러나 `exterms:weight` 속성이 `ex:Book`과 `ex:MotorVehicle`의 두 domain을 가진다는 것은 두 개의 별도 문장을 포함한다.

```
exterms:weight   rdfs:domain   ex:Book .
exterms:weight   rdfs:domain   ex:MotorVehicle .
```

이 속성을 사용하는 주어진 문장을 다음과 같이 작성할 수 있다.

```
exthings:companyCar   exterms:weight   "2500"^^xsd:integer .
```

두 `rdfs:domain` 문이 모두 정확하려면 `exthings:companyCar`가 두 클래스 `ex:Book`과 `ex:MotorVehicle`의 인스턴스여야 한다.

차량 스키마를 확장하여 `ex:registeredTo`와 `ex:rearSeatLegRoom` 속성과 새로운 클래스 `ex:Person`을 추가하고, 그리고 데이터 타입 `xsd:integer`를 데이터 타입으로 명시적으로 기술하여 이러한 range와 domain의 사용을 서술할 수 있다. `ex:registeredTo` 속성은 모든 `ex:MotorVehicle`에 적용되며 그 값은 `ex:Person`이다. 이 예에서 `ex:rearSeatLegRoom`은 `ex:PassengerVehicle` 클래스의 인스턴스에만 적용된다. `xsd:integer`는 뒷좌석 레그룸의 센티미터로 그 값을 제공한다. 이러한 기술을 [EXAMPLE 18](#ex-18)에서 볼 수 있다.

> EXAMPLE 18: Vehiclae 스키마의 속성 기술 <a id="ex-18"></a>

```
:registeredTo a rdf:Property;
    rdfs:domain :MotorVehicle;
    rdfs:range  :Person.

:rearSeatLegRoom a rdf:Property;
    rdfs:domain :PassengerVehicle;
    rdfs:range  xsd:integer.
 
:Person a rdfs:Class.
xsd:integer a rdfs:Datatype.                 
```

RDF 스키마는 속성과 클래스를 세분화하는 방법을 제공한다. 두 속성 간의 이러한 특수화 관계를 미리 정의된 `rdfs:subPropertyOf` 속성을 사용하여 기술한다. 예를 들어 `ex:primaryDriver`와 `ex:driver`가 둘 모두 속성인 경우 `example.org`는 RDF 문에서 이들 속성과 `ex:primaryDriver`가 `ex:driver`의 세분화라는 사실을 설명할 수 있다.

```
ex:driver          rdf:type             rdf:Property .
ex:primaryDriver   rdf:type             rdf:Property .
ex:primaryDriver   rdfs:subPropertyOf   ex:driver .
```

이 `rdfs:subPropertyOf` 관계의 의미는 인스턴스 `exstaff:fred`가 인스턴스 `ex:companyVan`의 `ex:primaryDriver`인 경우 RDF 스키마는 `exstaff:fred`가 `ex:companyVan`의 `ex:driver`도 되는 것으로 정의된다.

속성은 0 또는 하나 이상의 속성의 하위 속성일 수 있다. RDF 속성에 적용되는 모든 RDF 스키마 `rdfs:range`와 `rdfs:domain` 속성은 각 하위 속성에도 적용된다. 따라서 위의 예에서 RDF 스키마는 `ex:primaryDriver`가 `ex:driver`의 하위 속성 관계가 있으므로 `ex:primaryDriver`도 `ex:MotorVehicle`의 `rdfs:domain`을 갖는 것으로 정의한다.

[EXAMPLE 19](#ex-19)는 지금까지 보인 모든 서술을 포함하는 전체 차량 스키마에 대한 Turtle을 보인다.

> EXAMPLE 19: Vehicle 전체 스키마 <a id="ex-19"></a>

```
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
@base <http://example.org/schemas/vehicles>

:MotorVehicle a rdfs:Class.

:PassengerVehicle a rdfs:Class;
   rdfs:subClassOf :MotorVehicle.

:Truck a rdfs:Class;
   rdfs:subClassOf :MotorVehicle.
    
:Van a rdfs:Class;
   rdfs:subClassOf :MotorVehicle.

:MiniVan a rdfs:Class;
   rdfs:subClassOf :Van.
   rdfs:subClassOf :PassengerVehicle;

:Person a rdfs:Class.

xsd:integer a rdfs:Datatype.

:registeredTo a rdf:Property;
   rdfs:domain :MotorVehicle;
   rdfs:range  :Person.
    
:rearSeatLegRoom a rdf:Property;
   rdfs:domain rdf:resource :MotorVehicle;
   rdfs:range xsd:integer.

:driver a rdf:Property;
   rdfs:domain :MotorVehicle.

:primaryDriver a rdf:Property;
   rdfs:subPropertyOf :driver.
```

RDF 스키마를 사용하여 클래스와 속성을 설명하는 방법을 보였으므로 이제 이러한 클래스와 속성을 사용하는 인스턴스를 설명할 수 있다. 예를 들어, [EXAMPLE 19](#ex-19)예 기술된 ex:PassengerVehicle 클래스의 인스턴스를 해당 속성에 대한 몇 가지 가상 값을 사용하여 [EXAMPLE 20](#ex-20)에서 기술한다.

> EXAMPLE 20: `ex:PassengerVehicle`의 인스턴스 <a id="ex-20"></a>

```
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
@prefix ex:  <http://example.org/schemas/vehicles#>
@base <http://example.org/things>

:johnSmithsCar a ex:PassengerVehicle;
    ex:registeredTo    <http://www.example.org/staffid/85740>;
    ex:rearSeatLegRoom "127"^^xsd:integer;
    ex:primaryDriver   <http://www.example.org/staffid/85740>.
```
위의 예에서는 인스턴스가 스키마와 별도로 기술되어 있다고 가정한다. 스키마에는 `http://example.org/schemas/vehicles`를 `xml:base`로 사용하여 네임스페이스 선언 `@prefix ex: <http://example.org/schemas/vehicles#>`을 인스턴스 데이터의 QName `ex:registeredTo`을 그 스키마에 정의된 클래스와 속성의 URIref로 적절히 확장하여 사용할 수 있도록 제공한다. @base 선언 또한 `:johnSmithsCar`가 실제 위치와 무관하게 적절한 URIref로 확장할 수 있도록 ​인스턴스에 제공된다.

`ex:PassengerVehicle`은 `ex:MotorVehicle`의 서브클래스이기 때문에 `ex:registeredTo` 속성은 이 `ex:PassengerVehicle` 인스턴스를 설명하는 데 사용할 수 있다. 또한 이 경우에는 일반 리터럴이 아닌 타입이 지정된 리터럴이 `ex:rearSetLegRoom` 속성 값으로 사용된다 (예: 값을 `ex:rearSeatLegRoom 127;`로 지정하는 대신). 스키마가 이 속성의 범위를 `xsd:integer`로 설명하기 때문에 속성 값은 range 기술과 일치하도록 해당 데이터 타입으로 지정된 리터럴이어야 한다 (즉, range 선언이 데이터 타입을 자동으로 "할당"하지 않는다. 일반 리터럴이므로 적절한 데이터 타입으로 지정된 리터럴을 명시적으로 제공하여야 함). 스키마 또는 추가 인스턴스 데이터의 추가 정보는 [구조화된 값에 대한 추가 정보](#more-structured-values)에서 설명한 바와 같이 `ex:rearSetLegRoom` 속성의 단위(센티미터)를 명시적으로 지정하기 위해 제공될 수도 있다.

### RDF 스키마 선언 해석(Interpreting RDF Schema Declarations) <a id="interpreting-rdf-schema"></a>
앞서 언급했듯이 RDF 스키마 타입 시스템은 Java같은 객체 지향 프로그래밍 언어의 타입 시스템과 몇 가지 면에서 유사하다. 그러나 RDF는 몇 가지 중요한 측면에서 대부분의 프로그래밍 언어 타입 시스템과 구별된다.

한 가지 중요한 차이점은 클래스를 특정 속성 모음이 있는 것으로 기술하는 대신 RDF 스키마는 속성을 *domain*과 *range* 속성을 사용하여 리소스의 특정 클래스에 적용하는 것으로 기술한다는 것이다. 예를 들어, 일반적인 객체 지향 프로그래밍 언어는 `Person` 타입의 값을 갖는 `author`라는 애트리뷰트가 있는 `Book` 클래스를 정의할 수 있다. 그러나 해당 RDF 스키마는 `ex:Book` 클래스를 정의하고 별도로 `ex:Book` domain과 `ex:Person` range를 갖는 속성 `ex:author`를 정의한다.

이러한 접근 방식의 차이점이 구문론적으로만 보일 수 있지만 실제로는 중요한 차이점이 있다. 프로그래밍 언어 클래스 정의에서 애트리뷰트 `author`는 `Book` 클래스 정의의 일부이며 `Book` 클래스의 *인스턴스에만* 적용된다. 다른 클래스(예: softwareModule)에도 `author`라는 애트리뷰트가 있을 수 있지만 이는 *다른* 애트리뷰트로 간주된다. 즉, 대부분의 프로그래밍 언어에서 속성 서술의 *range*는 속성이 정의된 클래스 또는 타입으로 제한된다. 반면에 RDF에서 속성 정의는 기본적으로 클래스 정의와 무관하게 기본적으로 전역 범위를 갖는다 (선택적으로 domain 사양을 사용하여 특정 클래스에만 적용되도록 선언할 수 있음).

결과적으로 RDF 스키마는 domain을 지정하지 않고도 `exterms:weight` 속성을 기술할 수 있다. 그런 다음 이 속성을 사용하여 무게가 있는 것으로 간주될 수 있는 클래스의 인스턴스를 기술할 수 있다. RDF 속성 기반 접근 방식의 한 가지 이점은 속성 정의의 사용을 원래 정의에서 예상하지 못한 방황으로 확장하는 것이 더 쉬워진다는 것이다. 동시에 이는 속성이 부적절한 상황에서 잘못 적용되지 않도록 주의해서 사용해야 하는 "이점(?)"이다.

RDF 속성 기술의 전역 범위에 대한 또 다른 결과는 RDF 스키마에서 특정 속성에 적용되는 리소스 클래스에 따라 지역적으로 다른 범위를 갖는 것으로 정의하는 것이 불가능하다는 것이다. 예를 들어, 속성 `ex:hasParent`를 정의할 때 속성이 `ex:Human` 클래스의 리소스 기술에 사용되는 경우 속성 범위도 `ex:Human` 클래스 리소스라고 하는 것이 바람직하다. 반면 속성이 `ex:Tiger` 클래스 리소스 기술에 사용되는 경우 속성 범위도 `ex:Tiger` 클래스 리소스이다. RDF Schema에서는 이러한 정의가 불가능하다. 대신, RDF 속성에 대해 정의된 모든 범위는 속성의 모든 용도에 적용되므로 범위를 주의해서 정의해야 한다. 그러나 지역적으로 다른 범위를 RDF 스키마에서 정의할 수 없지만 [보다 풍부한 스키마 언어](#richer-schema-languages)의 설명 중 일부에서는 정의할 수 있다.

또 다른 중요한 차이점은 RDF 스키마 기술이 프로그래밍 언어 타입 선언의 일반적인 방식처럼 반드시 규범적이지 않다는 것이다. 예를 들어, 프로그래밍 언어가 `Person` 타입의 `author` 애트리뷰트를 갖는 클래스 `Book`을 선언하는 경우, 이는 일반적으로 *제약 조건*으로 해석된다. 언어는 `author` 애트리뷰트가 없는 `Book` 인스턴스의 생성을 허용하지 않으며 `author` 애트리뷰트 값으로 `Person`이 아닌 `Book` 인스턴스를 허용하지 않는다. 게다가 `author`가 `Book` 클래스에 정의된 *유일한* 애트리뷰트라면 언어는 다른 애트리뷰트를 가진 `Book` 인스턴스를 허용하지 않는다.

반면에 RDF 스키마는 리소스에 대한 추가 *기술*로 스키마 정보를 제공하지만 어플리케이션에서 이러한 기술에 대한 사용 방법을 규정하지 않는다. 예를 들어, RDF 스키마가 `ex:author` 속성에 `ex:Person` 클래스의 `rdfs:range`가 있다고 명시하였다고 가정하자. 이것은 단순히 `ex:author` 속성을 포함하는 RDF 문장에 `ex:Person`의 인스턴스가 object로 있다는 RDF 문장이다.

이 스키마 제공 정보를 다른 방식으로 사용할 수 있다. 한 어플리케이션은 이 문장을 생성 중인 RDF 데이터의 템플릿 일부로 해석하고 모든 `ex:author` 속성이 표시된 클래스(`ex:Person`)의 값을 갖도록 하는 데 사용할 수 있다. 즉, 이 어플리케이션은 프로그래밍 언어와 동일한 방식으로 스키마 정의를 제약 조건으로 해석한다. 그러나 다른 어플리케이션은 이 문장을 수신 중인 데이터에 대한 추가 정보를 제공하는 것으로 해석할 수 있다. 원래 데이터에 명시적으로 이 정보를 제공하지 않을 수 있다. 예를 들어, 이 두 번째 어플리케이션은 값이 지정되지 않은 클래스의 리소스인 `ex:author` 속성을 포함하는 일부 RDF 데이터를 수신하고 이 스키마 제공 문장을 사용하여 리소스가 클래스 `ex:Person`의 인스턴스여야 한다는 결론을 내릴 수 있다. 세 번째 어플리케이션은 값이 `ex:Corporation` 클래스의 리소스인 `ex:author` 속성을 포함하는 일부 RDF 데이터를 수신하고, "여기에는 불일치가 있을 수 있지만 다른 하나에는 없을 수도 있다."는 경고로 이 스키마 정보를 사용한다. 다른 곳에는 명백한 불일치를 해결하는 선언이 있을 수 있다 (예: "회사는 (법적인) 사람"이라는 취지의 선언).

또한 어플리케이션이 속성 기술을 해석하는 방법에 따라 스키마 지정 속성 중 일부가 없거나 추가 속성이 있는 경우 (`ex:Book` 클래스를 기술하는 스키마가 이러한 속성을 기술하지 않더라도 `ex:technicalEditor` 속성이 있는 `ex:Book` 인스턴스가 있을 수 있음) 인스턴스 기술이 유효한 것으로 간주될 수 있다 (예: `ex:author`가 `ex:Book` domain을 갖는 것으로 정의되어도 `ex:author` 속성이 없는 `ex:Book` 인스턴스가 있을 수 있다).

즉, RDF 스키마의 문장은 항상 서술이다. 그것들은 또한 (제약 조건 도입으로) 규범적일 수도 있지만, 해당 명령문을 해석하는 어플리케이션이 해당 문장을 그렇게 처리하기를 원하는 경우에만 가능하다. **RDF 스키마가 하는 모든 것은 이 추가 정보를 기술하는 방법을 제공하는 것이다**. 이 정보가 명시적으로 지정된 인스턴스 데이터와 충돌하는지 여부는 어플리케이션이 결정하고 조치를 취하는 데 달려 있다.

### 기타 스키마 정보(Other Schema Information) <a id="other-schema-information"></a>
RDF 스키마는 RDF 스키마 또는 인스턴스에 대한 문서화와 기타 정보를 제공하는 데 사용할 수 있는 기타 여러 기본 제공 속성을 제공한다. 예를 들어 `rdfs:comment` 속성은 사람이 읽을 수 있는 리소스 설명을 제공하는 데 사용할 수 있다. `rdfs:label` 속성은 사람이 읽을 수 있는 리소스 이름 버전을 제공하는 데 사용할 수 있다. `rdfs:seeAlso` 속성은 주제 리소스에 대한 추가 정보를 제공할 수 있는 리소스를 나타내는 데 사용할 수 있다. `rdfs:isDefinedBy` 속성은 `rdfs:seeAlso`의 하위 속성이며 (RDF에 의해 지정되지 않은 의미에서, 예를 들어 리소스가 RDF 스키마사 아닐 수 있음) 주제 리소스를 "정의"하는 리소스를 나타내는 데 사용할 수 있다. [RDF Vocabulary Description Language 1.0: RDF 스키마](http://www.w3.org/TR/rdf-schema/)는 이러한 속성에 대하여 추가로 설명한다.

`rdf:value`와 같은 여러 기본 RDF 속성과 마찬가지로 이러한 RDF 스키마 속성의 용도에 대한 설명은 의도된 것일 뿐이다. [RDF Semantics](http://www.w3.org/TR/rdf-mt/)은 이러한 속성에 대한 특별한 의미를 정의하지 않으며 RDF 스키마는 이러한 의도된 사용에 기반한 제약을 정의하지 않는다. 예를 들어, `rdfs:seeAlso` 속성의 object가 해당 속성이 나타나는 문장의 subject에 대한 추가 정보를 *제공해야* 한다는 제약 조건을 지정하지 않는다.

### 보다 풍부한 스키마 언어(Richer Schema Language) <a id="richer-schema-languages"></a>
RDF 스키마는 RDF 어휘를 설명하기 위한 기본 기능을 제공하지만 추가 기능도 가능하며 유용할 수 있다. 이러한 기능은 RDF 스키마의 추가 개발을 통해 또는 RDF를 기반으로 하는 다른 언어로 제공될 수 있다. RDF 스키마에서 제공하지 않지만 유용하고 더 풍부한 스키마 기능은 다음과 같다.

- 속성에 대한 *카디널리티 제약*, 예를 들어 Person에는 *오직* 한 명의 생물학적 아버지가 있다.
- 주어진 속성(예: `ex:hasAncestor`)이 전이적임을 지정한다. 예를 들어 `A ex:hasAncestor B`와 `B ex:hasAncestor C`이면 `A ex:hasAncestor C`이다.
- 주어진 속성이 특정 클래스의 인스턴스에 대한 고유 식별자(또는 *키*)임을 지정한다.
- 두 개의 다른 클래스(다른 URIref를 가짐)가 실제로 동일한 클래스를 나타내도록 지정한다.
- 두 개의 다른 인스턴스(서로 다른 URIref를 가짐)가 실제로 동일한 인스턴스를 나타냄을 지정한다.
- 속성이 적용되는 리소스 클래스에 따라 속성의 range 또는 카디널리티에 대한 제약 조건 지정, 예를 들어 축구팀의 경우 `ex:hasPlayers` 속성이 11 값을 갖는 반면 농구팀의 경우 동일한 속성에는 5 값이어야 한다.
- 다른 클래스의 조합(예: 합집합 및 교집합)의 관점에서 새 클래스를 설명하거나 두 클래스가 disjoint되어 한다고 지정할 수 있는 능력 (즉, 한 리소스가 두 클래스의 인스턴스가 될 수 없음).

위에서 언급한 추가 기능은 다른 기능과 함께 [DAML+OIL](http://www.w3.org/TR/daml+oil-reference)과 [OWL](http://www.w3.org/TR/owl-ref/)과 같은 *온톨로지* 언어의 목표이다. 이 두 언어 모두 RDF와 RDF 스키마를 기반으로 한다 (현재 둘 다 위에서 언급한 모든 추가 기능을 제공함). 이러한 언어의 의도는 리소스에 대해 컴퓨터가 처리 가능한 의미 체계를 추가로 제공하는 것이다. 이러한 기능이 RDF를 사용하여 유용한 어플리케이션을 구축하는 데 반드시 필요한 것은 아니지만 (여러 기존 RDF 어플리케이션에 대한 설명은 [RDF 어플리케이션](#rdf-applications) 참조), 이러한 언어의 개발은 [시맨틱 웹](http://www.w3.org/2001/sw/Activity) 개발의 일부로서 매우 활발한 연구 주제이다.

## RDF 어플리케이션(RDF Applications: RDF in Field) <a id="rdf-applications"></a>
이전 섹션에서는 RDF와 RDF 스키마의 일반적인 기능에 대해 설명했다. 이러한 기능을 설명하기 위해 해당 섹션에서 예제가 사용되었으며 이러한 예제 중 일부는 잠재적인 RDF 어플리케이션을 제안했을 수 있지만 해당 섹션에서는 실제 어플리케이션에 대해서는 논의하지 않았다. 이 섹션에서는 RDF가 다양한 사물에 대한 정보를 표현하고 조작하기 위해 다양한 실제 요구 사항을 지원하는 방법을 보여주는 실제 배포된 RDF 어플리케이션에 대해 설명한다.

### Dublin Core 메타데이터 이니셔티브 <a id="doublin-core-metadata"></a>
*메타데이터는 데이터에 대한 데이터이다*. 특히 이 용어는 정보 자원이 물리적이든 전자적이든 정보 자원을 식별, 기술 또는 찾는 데 사용되는 데이터를 나타낸다. 컴퓨터에 의해 처리되는 구조화된 메타데이터는 비교적 새로운 것이지만 메타데이터의 기본 개념은 많은 정보 컬렉션의 관리와 사용에 도움을 주어 예전부터 사용되어 왔다. 도서관 카드 카탈로그는 그러한 메타데이터의 친숙한 예이다.

Dublin Core는 문서를 기술하기 위한 (따라서 메타데이터를 기록하기 위한) "요소"(속성) 집합이다. 요소 집합은 1995년 3월 오하이오주 더블린에서 열린 메타데이터 워크샵에서 처음 개발되었다. Dublin Core는 이후 Dublin Core Metadata Workshop을 기반으로 수정되었으며 현재 [Dublin Core Metadata Initiative](http://dublincore.org/)가 유지 관리한다. Dublin Core의 목표는 도서관 카드 카탈로그와 유사한 방식으로 문서와 같은 연결된(networked) 객체의 자동 색인과 서술을 용이하게 하는 최소한의 서술 요소 집합을 제공하는 것이다. Dublin Core 메타데이터 세트는 널리 사용되는 World Wide Web 검색 엔진에서 사용하는 "Webcrawlers"와 같은 인터넷의 리소스 검색 도구에서 사용하기에 적합하도록 고안되었다. 또한 Dublin Core는 인터넷에 정보를 제공하는 광범위한 저자와 일반 발행인이 이해하고 사용할 수 있을 만큼 충분히 단순해야 한다. Dublin Core 요소는 인터넷 리소스를 문서화하는 데 널리 사용되었다 (Dublin Core 작성자 요소는 이전 예에서 이미 사용되었다). [Dublin Core Metadata Element Set, Version 1.1: Reference Description](http://dublincore.org/documents/2003/06/02/dces/)는 다음 속성에 대한 정의를 포함하는 더블린 코어의 현재 요소를 정의하고 있다.

- **Title**: 리소스 이름.
- **Creator**: 리소스의 컨텐츠 작성의 주 책임 엔티티.
- **Subject**: 리소스 컨텐츠의 주제.
- **Description**: 리소스 컨텐츠에 대한 기술.
- **Publisher**: 리소스를 사용 가능케 만드는 엔티티.
- **Contributor**: 리소스 컨텐츠에 기여한 엔티티.
- **Date**: 리소스 생명주기의 이벤트 일시.
- **Type**: 리소스 컨텐츠의 종류 또는 장르.
- **Format**: 리소스의 물리적 또는 디지털 표현.
- **Identifier**: 주어진 컨텍스트 내의 리소스에 대한 명확한 참조.
- **Source**: 현재 리소스가 파생된 리소스에 대한 참조.
- **Language**: 리소스의 지적 내용 언어.
- **Relation**: 관련 리소스에 대한 참조.
- **Coverage**: 리소스 내용의 범위.
- **Rights**: 리소스에 대해 보유하고 있는 권리에 대한 정보.

Dublin Core 요소를 사용하는 정보는 적절한 언어(예: HTML 메타 요소)로 표현될 수 있다. RDF는 Dublin Core 정보에 대한 이상적인 표현이다. 아래 예는 Dublin Core 어휘를 사용하여 RDF의 리소스 집합에 대한 간단한 기술을 보인다. 여기에 표시된 특정 Dublin Core RDF 어휘는 공식적인(authoritative) 것이 아니다. [Dublin Core Metadata Element Set, Version 1.1: Reference Description](http://dublincore.org/documents/dces/)이 공식적인 참조 문서이다.

첫 번째 예인 [EXAMPLE 21](#ex-21)은 Dublin Core 속성을 사용하는 웹 사이트 홈 페이지를 기술한다.

> EXAMPLE 21: 더블린 코어 속성을 사용하여 기술된 웹 페이지 <a id="ex-21"></a>

```
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.               
@prefix dc:  <http://purl.org/dc/elements/1.1/>.        

<http://www.dlib.org>
    dc:title       "D-Lib Program - Research in Digital Libraries";
    dc:description """The D-Lib program supports the community of people
       with research interests in digital libraries and electronic
       publishing.""";
    dc:publisher:  "Corporation For National Research Initiatives";
    dc:date        "1995-01-07";
    dc:subject [
         a rdf:Bag;
         rdf:_1 "Research; statistical methods";
         rdf:_2 "Education, research, related topics";
         rdf:_3 "Library use Studies".
    ];
    dc:type        "World Wide Web Home Page";
    dc:format      "text/html";
    dc:language    "en".
```

RDF와 Dublin Core는 모두 "Description"이라는 (XML) 요소를 정의한다 (Dublin Core 요소 이름은 소문자로 작성됨). 초기 문자가 동일하게 대문자인 경우에도 XML 네임스페이스 메커니즘을 통해 이 두 요소를 구별할 수 있다 (하나는 rdf:Description이고 다른 하나는 dc:description임). 또한 관심 사항으로 웹 브라우저에서 `http://purl.org/dc/elements/1.1/` (이 예에서 더블린 코어 어휘를 식별하는 데 사용되는 네임스페이스 URI)를 액세스하면 (현재 작성 기준) [Dublin Core Metadata Element Set, Version 1.1: Reference Description](http://dublincore.org/documents/dces/)에 대한 RDF 스키마 선언을 검색할 수 있다.

두 번째 예인 [EXAMPLE 22](#ex-22)는 발행된 잡지에 대해 기술한다.

> EXAMPLE 22: Dublin Core를 사용한 잡지 기술 <a id="ex-22"></a>

```
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.               
@prefix dc: <http://purl.org/dc/elements/1.1/>.        
@prefix dcterms: <http://purl.org/dc/terms/>.        

<http://www.dlib.org/dlib/may98/05contents.html>
      dc:title         "DLIB Magazine - The Magazine for Digital Library Research - May 1998";
      dc:description   """D-LIB magazine is a monthly compilation of
       contributed stories, commentary, and briefings.""";
      dc:contributor   "Amy Friedlander";
      dc:publisher     "Corporation for National Research Initiatives";
      dc:date          "1998-01-05";
      dc:type          "electronic journal";
      dc:subject [
        a rdf:Bag;
        rdf:_1 "library use studies";
        rdf:_2 "magazines and newspapers".
      ];
      dc:format        "text/html";
      dc:identifier    <urn:issn:1082-9873>;
      dcterms:isPartOf <http://www.dlib.org>.
```

[EXAMPLE 22](#ex-22)에서 (마지막 줄에서) Dublin Core 한정자(qulaifier) `isPartOf`(별도의 어휘에서)를 사용하여 이 잡지가 이전에 기술된 웹 사이트의 "일부"임을 나타낸다.

세 번째 예인 [EXAMPLE 23](#ex-23)은 [EXAMPLE 22](#ex-22)에 기술된 잡지의 특정 기사를 기술한다.

> EXAMPLE 23: 잡지 기사에 대한 기술 <a id="ex-23"></a>

```
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.               
@prefix dc: <http://purl.org/dc/elements/1.1/>.        
@prefix dcterms: <http://purl.org/dc/terms/>.        

<http://www.dlib.org/dlib/may98/miller/05miller.html>
   dc:title         "An Introduction to the Resource Description Framework";
   dc:creator       "Eric J. Miller";
   dc:description 
      """The Resource Description Framework (RDF) is an
       infrastructure that enables the encoding, exchange and reuse of
       structured metadata. rdf is an application of xml that imposes needed
       structural constraints to provide unambiguous methods of expressing
       semantics. rdf additionally provides a means for publishing both
       human-readable and machine-processable vocabularies designed to
       encourage the reuse and extension of metadata semantics among
       disparate information communities. the structural constraints rdf
       imposes to support the consistent encoding and exchange of
       standardized metadata provides for the interchangeability of separate
       packages of metadata defined by different resource description
       communities.""";
    dc:publisher     "Corporation for National Research Initiatives";
    dc:subject [
        a rdf:Bag;
        rdf:_1 "machine-readable catalog record formats";
        rdf:_2 "applications of computer file organization and access methods".
    ];
    dc:rights        "Copyright © 1998 Eric Miller";
    dc:type          "Electronic Document";
    dc:format        "text/html";
    dc:language      "en";
    dcterms:isPartOf <http://www.dlib.org/dlib/may98/05contents.html>.
```

[EXAMPLE 23](#ex-23)에서 또한 `isPartOf` 한정자를 사용하여 이 기사가 이전에 기술된 잡지의 "일부"임을 나타낸다.

### PRISM <a id="prism"></a>
[PRISM: Publishing Requirements for Industry Standard Metadata](http://www.prismstandard.org/)은 출판 산업에서 개발된 메타데이터 사양이다. 잡지 발행인과 그 공급업체는 메타데이터에 대한 업계의 요구를 식별하고, 이를 충족하는 사양을 정의하기 위해 PRISM Working Group을 구성했다. 발행인는 기존 콘텐츠를 제작하는 데 투자한 수익을 더 많이 얻기 위해 다양한 방법으로 기존 콘텐츠를 사용하기를 원한다. 웹에 게시하기 위해 잡지 기사를 HTML로 변환하는 것이 한 예이다. [LexisNexis](http://www.lexisnexis.com/)와 같은 애그리게이터에 라이선스를 부여하는 것은 또 다른 문제이다. 이 모든 것은 콘텐츠의 "first use"이다. 일반적으로 잡지가 스탠드에 도착할 때 모두 라이브로 전환된다. 발행인은 또한 자신의 콘텐츠가 "evergreen"이기를 원한다. 회고 기사처럼 새로운 이슈에서 사용될 수 있다. 잡지의 사진, 요리법 등으로 편집한 책처럼 회사의 다른 부서에서 사용할 수 있다. 또 다른 용도로는 제품 리뷰의 재판이나 다른 발행인이 제작한 회고록에서처럼 외부인에게 라이선스를 부여하는 것이다. 이 전반적인 목표에 *discovery*, *rights tracking*과 *end-to-end metadata*를 강조하는 메타데이터 접근 방식이 필요하다.

*Discovery*: 검색은 검색, 브라우징, 콘텐츠 라우팅 및 기타 기술을 포함하는 콘텐츠를 찾는 일반적인 용어이다. 검색에 대한 논의는 공개 웹 사이트를 검색하는 소비자를 중심으로 자주 이루어진다. 그러나 콘텐츠를 검색하는 것은 그보다 훨씬 광범위하다. 사용자는 컨텐츠 소비자로 구성되거나 연구원, 디자이너, 사진 편집자, 라이선스 에이전트 등 내부 사용자로도 구성될 수 있다. 검색을 돕기 위해 PRISM은 리소스의 주제, 형식, 장르, 출처와 컨텍스트를 기술하는 속성을 제공한다. 또한 여러 주제 기술 분류를 사용하여 리소스를 분류하는 수단을 제공한다.

*Rights Tracking*: 잡지에는 다른 사람의 라이선스를 받은 자료가 포함되어 있는 경우가 많다. 스톡 사진 에이전시의 사진이 가장 일반적인 유형의 라이선스 자료이지만 기사, 사이드바 및 기타 모든 유형의 콘텐츠에 라이선스가 부여될 수 있다. 콘텐츠에 일회성 사용을 위해 라이선스가 부여되었는지, 로열티 지불이 필요한지 또는 발행인이 완전히 콘텐츠를 소유했는지 여부를 단순히 아는 것은 어려운 일이다. PRISM은 이러한 권리의 기본적인 추적을 위한 요소를 제공한다. PRISM 사양에 정의된 별도의 용어는 콘텐츠가 사용되거나 사용되지 않을 수 있는 장소, 시간과 산업에 대한 기술을 지원한다.

*End-to-end metadata*: 게시된 대부분의 콘텐츠에는 이미 생성된 메타데이터가 있습니다. 불행히도 콘텐츠가 시스템 간에 이동할 때 메타데이터가 자주 삭제되고 나중에 제작 과정에서 다시 생성하면 상당한 비용이 든다. PRISM은 콘텐츠 제작 파이프라인의 여러 단계에서 사용할 수 있는 사양을 제공하여 이 문제를 줄이는 것을 목표로 한다. PRISM 사양의 중요한 특징은 다른 기존 사양을 사용한다는 것이다. 그룹에서 완전히 새로운 것을 만들기보다는 기존 사양을 최대한 활용하고 필요한 경우에만 새로운 것을 정의하기로 결정했다. 이러한 이유로 PRISM 사양은 XML, RDF, Dublin Core과 다양한 ISO의 형식 및 어휘를 사용한다.

PRISM 기술은 일반 리터럴 값을 가진 몇몇 Dublin Core 속성만큼 간단할 수 있다. [EXAMPLE 24](#ex-24)는 제목, 사진사, 형식 등에 대한 기본 정보를 제공하는 사진을 기술한다.

> EXAMPLE 24: 사진에 대한 PRISM 기술 <a id="ex-24"></a>

```
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix dc: <http://purl.org/dc/elements/1.1/>.

<http://travel.example.com/2000/08/Corfu.jpg>
    dc:title       "Walking on the Beach in Corfu";
    dc:description "Photograph taken at 6:00 am on Corfu with two models";
    dc:creator     "John Peterson";
    dc:contributor "Sally Smith, lighting";
    dc:format      "image/jpeg".
```

PRISM은 또한 더 자세한 기술을 하기 위해 Dublin Core를 확장한다. 일반적으로 접두사 `prism:`, `pcv:`과 `prl:`을 사용하여 인용되는 세 가지 새로운 어휘로 확장을 정의한다.

`prism`: 이 접두사는 URI 접두사 `http://prismstandard.org/namespaces/basic/1.0/`을 사용하는 주요 PRISM 어휘를 나타낸다. 이 어휘에 있는 속성의 대부분은 Dublin Core의 속성에 대한 보다 구체적인 버전이다. 예를 들어 `dc:date`의 보다 구체적인 버전으로 `prism:publicationTime`, `prism:releaseTime`, `prism:expirationTime` 등과 같은 속성을 제공한다.

`pcv`: 이 접두사는 URI 접두사 `http://prismstandard.org/namespaces/pcv/1.0/`를 사용하는 PRISM Controlled Vocabulary(pcv) 어휘를 나타낸다. 현재 기사의 주제를 기술하는 일반적인 관행은 서술하는 키워드를 제공하는 것이다. 불행히도 다른 사람들이 다른 키워드를 사용한다는 사실로 인하여 단순한 키워드는 검색 성능에 큰 차이를 만들지 못한다([Indexing and Access for Digital Libraries and the Internet: Human, Database, and Domain Factors](http://is.gseis.ucla.edu/research/mjbates.html)). 모범 사례는 "제어 어휘"를 주제 용어로 사용하여 기사를 코딩하는 것이다. 어휘에서 해당 용어에 대해 가능한 한 많은 동의어를 제공해야 한다. 이러한 방식으로 제어된 용어는 검색자와 인덱서가 제공하는 키워드에 대한 만남의 장을 제공한다. `pcv` 어휘는 어휘의 용어, 용어 간의 관계와 용어의 대체 이름을 지정하기 위한 속성을 제공한다.

`prl`: 이 접두사는 URI 접두사 `http://prismstandard.org/namespaces/prl/1.0/`을 사용하는 PRISM Rights Language 어휘를 나타낸다. 디지털 권한 관리(Digital Rights Management)는 상당한 격변을 겪고 있는 영역이다. 권한 관리 언어에 대한 제안이 많이 있지만 업계 전체에서 명확하게 선호되는 것은 없다. 추천할 명확한 선택지가 없었기 때문에 PRISM Rights Language(PRL)를 임시로 정의했다. 시간, 지리과 업계의 조건에 따라 항목을 "사용"할 수 있는지 여부를 기술할 수 있는 속성을 제공한다. 이는 발행인이 권리를 추적할 때 비용을 절약할 수 있도록 도움을 줄 수 있는 80/20 트레이드오프로 여겨진다. 일반 권리 언어이거나 발행인이 콘텐츠의 사용에 대한 제한이 자동 시행되도록 허용하는 것을 뜻하지 않는다.

PRISM은 다양한 복잡성에 대한 기술을 처리하는 능력 때문에 RDF를 사용한다. 현재 많은 메타데이터가 다음과 같은 간단한 문자열(일반 리터럴) 값을 아래와 같이 사용하고 있다.

```
dc:coverage "Greece";
```

시간이 지남에 따라 PRISM 개발자는 PRISM 사양의 사용이 단순한 리터럴 값에서 보다 구조화된 값으로 변화하며 더욱 정교해지기를 기대한다. 사실, 그 값의 범위가 지금 직면하고 있는 상황이다. 일부 발행인은 이미 정교하게 제어된 어휘를 사용하고 다른 발행인 수동으로 제공되는 키워드를 거의 사용하지 않는다. 이를 설명하기 위해 dc:coverage 속성에 제공할 수 있는 다양한 종류의 값에 대한 몇몇 예는 다음과 같다.

```
dc:coverage "Greece";
dc:coverage <http://prismstandard.org/vocabs/ISO-3166/GR>;
```

(즉, 일반 리터럴 또는 URIref를 사용하여 국가를 식별) 그리고

```
dc:coverage <http://prismstandard.org/vocabs/ISO-3166/GR>.
<http://prismstandard.org/vocabs/ISO-3166/GR> rdf:type pcv:Descriptor;
<http://prismstandard.org/vocabs/ISO-3166/GR> pcv:label "Greece"@en;
<http://prismstandard.org/vocabs/ISO-3166/GR> pcv:label "Grèce"@fr.
```

(구조화된 값을 사용하여 다양한 언어로 URIref와 이름을 모두 제공).

의미가 유사한 속성이나 다른 속성의 하위 집합도 있다. 예를 들어, 자원의 지리적 subject는 다음과 같이 주어질 수 있다.

```
prism:subject "Greece";
dc:coverage   "Greece";
```

또는

```
prism:location "Greece";
```

이러한 속성은 단순한 리터럴 값 또는 보다 복잡한 구조화된 값을 사용할 수 있다. 이러한 가능성의 범위는 DTD 또는 최신 XML 스키마로도 적절하게 설명할 수 없다. 처리해야 할 다양한 구문 변형이 있지만 RDF의 그래프 모델은 triple 세트라는 단순한 구조를 가지고 있다. triple domain의 메타데이터를 처리하면 이전 소프트웨어에서 새로운 확장이 있는 콘텐츠를 훨씬 쉽게 수용할 수 있다.

이 섹션은 두 개의 마지막 예로 끝을 맺는다. [EXAMPLE 25](#ex-25)는 이미지(.../Corfu.jpg)를 담배 산업 (SIC의 코드 21, 표준 산업 분류)에서 사용할 수 없다(#none)고 서술한다.

> EXAMPLE 25: 이미지에 대한 PRISM 기술 <a id="ex-25"></a>

```
@prefix prism: <http://prismstandard.org/namespaces/basic/1.0/>.
@prefix prl:   <http://prismstandard.org/namespaces/prl/1.0/>.
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix dc:    <http://purl.org/dc/elements/1.1/>.

<http://travel.example.com/2000/08/Corfu.jpg>
    dc:rights [
      prl:usage    <http://prismstandard.org/vocabularies/1.0/usage.xml#none>;
      prl:industry <http://prismstandard.org/vocabs/SIC/21>.
    ].
```

[EXAMPLE 26](#ex-26)은 Corfu 이미지의 사진사가 John Peterson으로 알려진 직원 3845라고 기술한다. 또한 사진의 지리적 범위는 그리스라고 한다. 이는 제어된 어휘의 코드뿐만 아니라 어휘에서 해당 용어에 대한 정보의 캐시된 버전을 제공하여 수행한다.

> EXAMPLE 26: [EXAMPLE 25](#ex-25)의 이미지에 대한 추가 정보

```
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix pcv: <http://prismstandard.org/namespaces/pcv/1.0/>.
@prefix dc:  <http://purl.org/dc/elements/1.1/>.

</2000/08/Corfu.jpg>
   dc:identifier <http://travel.example.com/content/2357845> ;
   dc:creator    <http://travel.example.com/content/2357845> ;
   dc:coverage   <http://prismstandard.org/vocabs/ISO-3166/GR>.
  
</content/2357845> a pcv:Descriptor;
   pcv:label "John Peterson".
 
<http://prismstandard.org/vocabs/ISO-3166/GR>
   pcv:label "Greece"@en;
   pcv:label "Grèce"@fr.
```

## Other Parts of the RDF Specification
[서론](#introduction)에서 RDF 사양이 다음과 같은 여러 문서로 구성되어 있음을 보였다.

- [RDF Concepts and Abstract Syntax](http://www.w3.org/TR/rdf-concepts/)
- [RDF/XML Syntax Specification](http://www.w3.org/TR/rdf-syntax-grammar/) 
- [RDF Vocabulary Description Language 1.0: RDF Schema]( http://www.w3.org/TR/rdf-schema/)
- [RDF Semantics](http://www.w3.org/TR/rdf-mt/)
- [RDF Test Cases](http://www.w3.org/TR/rdf-testcases/) 
- [RDF Primer](../rdf-primer/) (RDF/XML Version)

이 페이지에서 이미 이러한 문서의 주제, 기본 RDF 개념([리소스에 대한 문장 생성](#making-statements))과 RDF 스키마([RDF 어휘 정의: RDF 스키마](definig-rdf-vocabularies))에 대해 논의했다. 이 절에서는 ([RDF Semantics](http://www.w3.org/TR/rdf-mt/)에 대한 많은 참조가 이미 있음에도 불구하고) RDF의 전체 사양에서 해당 역할을 설명하기 위해 페이지의 나머지 부분에서 간략하게 기술한다.

### RDF 시멘틱(RDF Semantics)
이전 절에서 논의한 바와 같이 RDF는 특정 어휘(자원의 이름, 속성, 클래스 등)를 사용하여 그래프 형태로 리소스에 대한 문장을 표현하는 데 사용된다. RDF는 또한 [보다 풍부한 스키마 언어](#richer-schema-languages)에서 논의한 것과 같은 고급 언어의 기반이 되도록 하였다. 이러한 목적을 달성하기 위해 RDF 그래프의 "의미"는 매우 정확한 방식으로 정의되어야 한다.

매우 일반적인 의미에서 RDF 그래프의 "의미"의 구성은 사용자 커뮤니티 내에서 특정 방식으로 사용자 정의 RDF 클래스와 속성을 해석하는 규칙, 자연어로 된 주석 또는 내용이 포함된 다른 문서에 대한 링크를 포함하는 여러 요인에 의존한다. [RDF 모델](#rdf-model)에서 간략하게 언급했듯이 이 의미는 RDF 정보의 사람이나 다양한 종류의 처리를 수행하는 소프트웨어를 작성하는 프로그래머가 사용할 수 있을 지라도 이러한 형식으로 전달되는 의미의 대부분은 기계 처리에 직접 액세스할 수 없다. 그러나 RDF 문장은 또한 기계가 주어진 RDF 그래프에서 도출할 수 있는 결론(또는 함의)을 수학적 정밀도로 결정하는 형식적 의미를 갖는다. [RDF Semantics](http://www.w3.org/TR/rdf-mt/) 문서는 형식 언어의 의미를 지정하기 위한 *모델 이론*이라는 기술을 사용하여 이 형식적 의미를 정의한다. [RDF Semantics](http://www.w3.org/TR/rdf-mt/)은 또한 RDF 스키마와 개별 데이터 타입으로 표현되는 RDF 언어에 대한 의미 확장을 정의한다. 즉, RDF 모델 이론은 모든 RDF 개념에 대한 형식적 토대를 제공한다. 모델 이론에서 정의된 의미론을 기반으로 RDF 그래프를 본질적으로 동일한 의미를 갖는 논리적 표현으로 변환하는 것은 간단하다.

### 테스트 케이스(Test Cases)
[RDF 테스트 케이스](http://www.w3.org/TR/rdf-testcases/)는 RDF 핵심 워킹 그룹에서 다루는 특정 기술 문제에 해당하는 테스트 케이스(예)로 텍스트 RDF 사양을 보완한다. 이러한 예를 설명하는 데 도움이 되도록 테스트 사례 문서는 이 페이지 전체에서 사용되는 triple 표기법의 기초를 제공하는 N-Triples라는 표기법을 소개한다. 테스트 사례는 테스트 사례 문서에서 참조하는 웹 위치에 기계 판독 가능 형식으로 게시되므로 개발자는 이를 RDF 소프트웨어의 자동화된 테스트를 위한 기초로 사용할 수 있다.

테스트 케이스는 여러 카테고리로 나누어 진다.

- 포지티브 및 네거티브 파서 테스트: 이 테스트는 RDF/XML 파서가 올바른 RDF/XML 입력 문서로부터 정확한 N-Triples 출력 그래프를 생성하는지 또는 입력 문서가 올바른 RDF/XML이 아닌 경우 오류를 정확히 보고하는지 테스트한다.
- 포지티브 및 네거티브 포함 테스트: 이 테스트는 적절한 함의(결론)가 지정된 RDF 문장 세트로부터 도출되었는지 여부를 테스트한다.
- 데이터 타입 인식 포함 테스트: 데이터 타입 사용을 포함하는 포지티브 또는 네가티브 포함 테스트이므로 테스트와 관련된 특정 데이터 타입에 대한 추가 지원이 필요하다.
- 기타 테스트: 위의 카테고리에 속하지 않는 테스트이다.

테스트 케이스는 RDF의 완전한 사양이 아니며 다른 사양 문서보다 우선하지 않는다. 그러나 이는 RDF 설계와 관련하여 RDF Core Working Group의 의도를 설명하기 위한 것이며 개발자는 이러한 테스트 사례가 세부 사항의 사양의 문구가 명확하지 않은 경우 도움이 될 수 있다.

## Appendix A: More on Uniform Resource Identifiers (URIs) <a id="appendix-a"></a>
> **Notes**
> 이 섹션은 URI에 대한 간략한 소개를 위한 것이다. URI의 최종 사양은 [RFC 2396 - Uniform Resource Identifiers (URI): Generic Syntax](http://www.isi.edu/in-notes/rfc2396.txt.)이며 자세한 내용을 참조해야 한다. URI에 대한 추가 논의는 [Naming and Addressing: URIs, URLs, ...](http://www.w3.org/Addressing/)에서도 찾을 수 있다.

[기본 개념](#basic-concepts)에서 설명한 바와 같이 웹은 웹상의 리소스를 식별(이름 지정)하기 위해 [URI(Uniform Resource Identifier)](http://www.isi.edu/in-notes/rfc2396.txt)라고 하는 일반적인 형식의 식별자를 제공한다. URL과 달리 URI는 네트워크 위치가 있는 항목을 식별하거나 다른 컴퓨터 액세스 메커니즘을 사용하는 것으로 제한되지 않는다. 다양한 *URI 체계*(URI 형식)가 이미 개발되어 다양한 목적으로 사용되고 있다. 예는 다음과 같다.

- `http`: (웹 페이지용 HTTP)
- `mailto`: (이메일 주소), 예: `mailto:em@w3.org`
- `ftp`: (파일 전송 프로토콜)
- `urn`: (Uniform Resource Names, 영구적인 위치 독립적 리소스 식별자가 되도록 의도됨), 예: `urn:isbn:0-520-02356-0`(책의 경우)

기존 URI 체계의 목록은 [Addressing Schemes](http://www.w3.org/Addressing/schemes.html)에서 찾을 수 있으며, 새로운 체계를 발명하려고 하기보다 특수한 식별 목적을 위해 기존 체계 중 하나를 적용하는 것을 고려하는 것이 바람직하다.

URI를 만드는 사람이나 URI를 사용하는 방법을 제어하는 사람이나 조직은 없다. URL의 `http:`와 같은 일부 URI 체계는 DNS와 같은 중앙 집중식 시스템에 의존하지만 `freenet:`과 같은 다른 체계는 완전히 분산되어 있다. 이것은 다른 종류의 이름과 마찬가지로 어떤 것에 대한 URI를 만드는 데 특별한 권한이나 권한이 필요하지 않다는 것을 뜻한다. 또한 누구나 자신이 소유하지 않은 것에 대해 원하는 이름을 사용할 수 있는 것처럼 일반 언어에서 누구나 자신이 소유하지 않은 것을 참조하는 URI를 만들 수 있다.

[기본 개념](#basic-concepts)에서도 언급했듯이 RDF는 *URI 참조*[URI](http://www.isi.edu/in-notes/rfc2396.txt)를 사용하여 RDF 문에서 subject, predicate와 object의 이름을 지정한다. URI 참조(또는 *URIref*)는 끝에 선택적 *단편 식별자(fragment identifier)*가 있는 URI이다. 예를 들어, URI 참조 `http://www.example.org/index.html#section2`는 URI `http://www.example.org/index.html`과 ("#" 문자로 구분된) 단편 식별자 `#section2`으로 구성된다. RDF URIref는 Unicode([The Unicode Standard, Version 3](http://www.unicode.org/unicode/standard/versions/)) 문자([RDF Concepts and Abstract Syntax](http://www.w3.org/TR/rdf-concepts/) 참조)를 포함할 수 있으므로 많은 언어가 URIref에 반영될 수 있다.

URIref는 *절대적*이거나 *상대적*일 수 있다. *절대* URIref는 URIref가 나타나는 컨텍스트와 독립적인 리소스를 나타낸다 (예: URIref `http://www.example.org/index.html`). *상대* URIref는 URIref의 일부 접두사가 누락된 절대 URIref의 축약형이며 누락된 정보를 채우기 위해 URIref가 나타나는 컨텍스트의 정보가 필요하다. 예를 들어 상대 URIref `otherpage.html`은 리소스 `http://www.example.org/index.html`에 나타날 때 절대 URIref `http://www.example.org/otherpage.html`로 채워진다. URI 부분이 없는 URIref는 현재 문서(그것이 나타나는 문서)에 대한 참조로 간주된다. 따라서 문서 내의 빈 URIref는 문서 자체의 URIref와 동일한 것으로 간주된다. 단편 식별자으로만 구성된 URIref는 단편 식별자가 추가된 문서의 URIref와 동일한 것으로 간주된다. 예를 들어 `http://www.example.org/index.html` 내에서 `#section2`가 URIref로 나타나면 절대 URIref `http://www.example.org/index.html#section2`와 동일한 것으로 간주된다.

[RDF Concepts and Abstract Syntax](http://www.w3.org/TR/rdf-concepts/)는 RDF 그래프(추상 모델)가 상대 URIref를 사용하지 않는다는 점을 주목하고 있다. 그러나 RDF/XML 또는 Turtle과 같은 구체적인 RDF 구문은 특정 상황에서 상대 URIref를 절대 URIref의 약어로 사용할 수 있도록 허용할 수 있다. Turtle은 상대 URIref의 사용을 허용하며 이 페이지의 Turtle 예 중 일부는 그러한 사용을 보이고 있다. 자세한 내용은 [TURTLE](turtle-primer.md)을 참고한다.

RDF와 웹 브라우저는 모두 URIref를 사용하여 사물을 식별한다. 그러나 RDF와 브라우저는 URIref를 약간 다른 방식으로 해석한다. 이는 RDF가 사물을 식별하기 위해서만 URIref를 사용하는 반면 브라우저는 URIref를 사용하여 사물을 검색하기 때문이다. 실제적인 차이가 없는 경우가 많지만 경우에 따라 차이가 상당할 수 있다. 한 가지 분명한 차이점은 URIref가 브라우저에서 사용될 때 실제로 검색할 수 있는 리소스를 식별할 것이라는 기대가 있다는 것이다. 그러나 RDF에서 URIref는 웹에서 검색할 수 없는 사람과 같은 것을 식별하는 데 사용될 수 있다. 사람들은 때때로 URIref가 RDF 리소스를 식별하는 데 사용될 때 해당 리소스에 대한 설명 정보를 포함하는 페이지가 해당 URI "at" 웹에 배치되어 URIref가 다음에서 사용될 수 있도록 하는 규칙과 함께 RDF를 사용한다. 브라우저를 사용하여 해당 정보를 검색한다. 이것은 원본 리소스의 식별자를 그것을 기술하는 웹 페이지의 식별자([구조화된 속성 값과 빈 노드](#structured-property-values)에서 논의한 주제)와 구별하는 데 어려움을 일으키지만 어떤 상황에서는 유용한 관례가 될 수 있다. 그러나 이 규칙은 RDF 정의의 명시적인 부분이 아니며 RDF 자체는 URIref가 검색할 수 있는 것을 식별한다고 가정하지 않는다.

또 다른 차이점은 조각 식별자가 있는 URIref가 처리되는 방식에 있다. 조각 식별자는 종종 HTML 문서를 식별하는 URL에서 볼 수 있으며, 여기서 URL로 식별되는 문서 내의 특정 위치를 식별하는 역할을 한다. URI 참조가 표시된 리소스를 검색하는 데 사용되는 일반적인 HTML 사용에서 두 URIref는 다음과 같다.

```
http://www.example.org/index.html
http://www.example.org/index.html#Section2
```

두 URIref는 관련되어 있다 (둘 다 동일한 문서를 참조하고 두 번째 문서는 첫 번째 문서 내의 위치를 식별함). 그러나 이미 언급했듯이 RDF는 리소스를 검색하는 것이 아니라 순수하게 식별하기 위해 URI 참조를 사용하며 RDF는 이 두 URIref 사이에 특별한 관계가 없다고 가정한다. RDF에 관한 한 그들은 구문적으로 다른 URI 참조를 참조하므로 관련 없는 것을 참조할 수 있다. 이것은 HTML 정의 포함 관계가 존재하지 않을 수 있다는 것을 의미하지 않으며, 단지 RDF가 URI 참조의 URI 부분이 동일하다는 사실에만 기초하여 관계가 존재한다고 가정하지 않는다는 것이다.

이 점에서 더 나아가, RDF는 단편 식별자가 있든 없든 공통 선행 문자열을 공유하는 URI 참조 사이에 관계가 있다고 가정하지 않는다. 예를 들어, RDF에 관한 한 두 URIref는 다음과 같다.

```
http://www.example.org/foo.html
http://www.example.org/bar.html
```

둘 다 문자열 `http://www.example.org/`로 시작하더라도 특별한 관계가 없다. RDF의 경우 URIref가 다르기 때문에 단순히 다른 리소스이다. (실제로 같은 디렉토리에 있는 두 개의 파일일 수 있지만 RDF는 이 관계나 다른 관계가 존재한다고 가정하지 않는다.)

<a name="footnote_1">1</a>: 이 페이지는 [RDF Primer — Turtle version](https://www.w3.org/2007/02/turtle/primer/)을 편역한 것임.

last edited on 2023-10-16
