이 페이지는 RDF(Resource Description Frmawork)에 대한 문서들을 모아 편역한 것으로 [DCAT](https://sdldocs.gitlab.io/dcat/))에 적용하기 위한 것이다. 특히 [RDF Primer -  Turtle version](./turtle-primer.md)은 대부분의 W3C DCAT 문서에서 RDF를 Turtle로 작성하고 있어 이를 보다 잘 이해하기 위하여 편역한다.

현재 문서 리스트는 다음과 같다.

- [RDF at a Glance](./rdf-glance.md)
- [RDF Primer](./rdf-primer.md)
- [RDF란?](./what-is-rdf.md)
- [RDFS란?](./what-is-rdfs.md)
- [RDFS로 무엇을 할 수 있나?](https://www.bobdc.com/blog/whatisrdfspart2/)
- [RDF Primer -  Turtle version](./turtle-primer.md)
- [rdflib](rdflib/index.md)

Todo:

- [RDF 1.2 Concepts and Abstract Syntax](https://www.w3.org/TR/rdf-concepts/)
- [RDF Vocabulary Description Language 1.0: RDF 스키마](http://www.w3.org/TR/rdf-schema/)
- [RDF Semantics](http://www.w3.org/TR/rdf-mt/)
- [RDF and SPARQL: Using Semantic Web Technology to Integrate the World's Data](https://www.w3.org/2007/03/VLDB/)
- [Using OWL and SKOS](https://www.w3.org/2006/07/SWD/SKOS/skos-and-owl/master.html)
- [Validating RDF data with SHACL](https://www.bobdc.com/blog/validating-rdf-data-with-shacl/)
- [You probably don't need OWL](https://www.bobdc.com/blog/dontneedowl/)
- [Differing Definitions of Ontologies](https://accidental-taxonomist.blogspot.com/2020/12/differing-definitions-of-ontologies.html)
- [the leading introduction to taxonomy development](https://www.hedden-information.com/accidental-taxonomist/)
- [Differing Definitions of Ontologies](https://accidental-taxonomist.blogspot.com/2020/12/differing-definitions-of-ontologies.html)
