본 문서는 [Resource Description Framework(RDF)](https://www.techtarget.com/searchapparchitecture/definition/Resource-Description-Framework-RDF)를 편역한 것이다. 

## RDF란 ?

RDF(Resource Description Framework)는 웹에서 상호 연결된 데이터를 표현하기 위한 일반적인 프레임워크이다. RDF 문은 메타데이터를 설명하고 교환하는 데 사용되며, 이는 관계를 기반으로 한 표준화된 데이터 교환을 가능하게 한다.

RDF를 여러 소스의 데이터를 통합하는 데 사용할 수 있다. 이 접근법의 예로 제조업체의 온라인 카탈로그 목록을 표시하고 제품을 다른 웹 사이트의 리뷰와 제품을 판매하는 상인에게 연결하는 웹 사이트를 들 수 있다, 시맨틱 웹은 의미에 따라 정보를 구성하기 위해 RDF 프레임워크를 사용하는 것을 기반으로 리고 있다.

RDF 문은 다음과 같은 리소스 간의 관계를 나타낸다.

* 문서
* 물리적 물체
* 사람
* 추상적 개념
* 데이터 객체

관련 RDF 문 집합은 엔티티들 간의 관계를 매핑하는 방향 그래프를 구성한다. 관련 엔티티에 대한 RDF 문을 사용하여 해당 엔티티들이 어떻게 관련되어 있는지 보여주는 RDF 그래프를 구성할 수 있다.

W3C(World Wide Web Consortium)은 RDF에 대한 표준을 유지하며, 표준은 기본 개념, 의미론 및 다양한 포맷에 대한 사양을 포함하고 있다. RDF에 대해 정의된 첫 번째 구문은 XML(Extensible Markup Language)를 기반으로 한다. Turtle( Terse RDF Triple Language), JSON-LD(JavaScript Object Notation for Linked Data), N-Triples 등과 같은 다른 구문들이 더 일반적으로 사용된다.

![components_of_an_rdf_graph-h.png](images/rdf/a_simple_rdf_graph-h.png)

## RDF 작동 방법

RDF는 리소스(resource)를 기술하는 표준 방법이다. RDF 문장은 세 가지 요소로 구성되어 있으며, 이를 *triple*이라 한다.

1. **Subject**는 triple이 기술하는 리소스이다.
2. **Predicate**는 subject와 object 사이의 관계를 설명한다.
3. **Object**는 subject를 기술하는 리소스이다.

subject와 object는 사물을 나타내는 노드이다. predicate는 노드 간의 관계를 나타내기 때문에 link(arc)이다.

RDF 표준은 세 가지 유형의 노드를 제공한다.

* **URI(Uniform Resource Identifier)** 는 추상적이든 물리적이든 리소스를 식별하기 위한 표준 형식이다. URI의 한 유형인 URL(Uniform Resource Locator)은 RDF 문에 일반적으로 사용되고 있다. 2014년 W3C가 RDF 규격을 버전 1.1로 업데이트할 때 노드 유형으로 IRI(Internationalized Resource Identifier)를 추가하였다. IRI는 URI와 유사하며 상호 보완적이어서 국제 문자 집합을 사용할 수 있다.
* **리터럴(Literal)** 은 특정 데이터 값이며 문자열, 날짜 또는 숫자 값이 될 수 있다. 리터럴 값은 URI 또는 IRI 형식을 사용하여 표현될 수 있다.
* **Blank node identifier(빈 노드 식별자)** 는 *익명 리소스(anonymous node)* 또는 *b노드(bnode)* 라고도 한다. 그것은 관계를 기술하는 것외에는 아무것도 나타내지 않은 subject를 나타낸다. 빈 노드 식별자는 특수 구문을 사용하여 식별한다.

RDF 트리플의 모든 구성 요소(subject, predicate, object)는 URI 또는 IRI로 표현될 수 있다. URI는 웹 리소스를 가리키는 URL이거나 임의 데이터를 포함할 수 있다.

동일한 엔티티에 대한 여러 RDF 문장들은 모두 RDF 트리플이다. 그들은 같은 주제를 가지고 있지만, 다른 술어(predicate)와 목적어(obejct)를 가지고 있다. 이러한 트리플에서 RDF 그래프를 만들 때 subject를 한 번만 표시하고 subject에서 여러 화살표를 사용하여 다른 predicate와 다른 obejct로 연결하어 표시할 수 있다.

![a_simple_rdf_graph-h.png](images/rdf/components_of_an_rdf_graph-h.png)

## RDF를 어떻게 사용할까?

RDF 문장은 HTML 웹 페이지에 통합되거나 별도의 파일에 저장될 수 있으며 웹 콘텐츠의 데이터에 연결될 수 있다. RDF가 처음 지정되었을 때, RDF 문은 웹 콘텐츠에 연결된 XML 문서에 통합되었었다.

처음에는 XML 구문이 유일한 옵션이었지만, 이후 RDF 문을 인코딩하기 위한 표준은 다음과 같은 세 가지 구문을 포함한다.

* **Turtle** 는 RDF 문장으로 가장 많이 쓰이는 텍스트 구문입니다. W3C는 일반적으로 사용되는 패턴에 대한 약어를 포함하는 "콤팩트하고 자연스러운 텍스트 양식"이라고 설명한다.
* **JSON-LD** 는 RDF 문에 JSON 구문을 사용한다.
* **N-Triples** 는 Turtle 문법의 하위 집합으로, 문장을 더 쉽게 작성할 수 있도록 RDF 문을 위한 단순한 텍스트 기반 형식으로 설계되었다. 단순한 형식은 프로그램이 RDF 문을 쉽게 생성하고 구문 분석할 수 있도록 한다.

RDF 쿼리 언어는 RDF 그래프에 저장된 정보를 액세스하고 관리하는 데 사용된다. RDF 쿼리 언어는 RDF 트리플을 구문 분석하고 트리플의 내용 및 트리플 간의 관계에 관련된 결과를 해석하고 생성할 수 있어야 한다.

[SPARQL](https://sdldocs.gitlab.io/w3c-related/spqrql/)은 SPARQL Protocol and RDF Query Language의 재귀적 약자로 W3C가 개발한 RDF 쿼리 언어의 표준이다. SPARQL 호환 쿼리 언어는 RDF 트리플에서만 작동하며 RDF 스키마에 대한 쿼리를 지원하지 않는다. RDF 스키마는 표준화된 자원과 관계를 정의하는 기본 RDF 어휘의 확장이다.

## RDF의 이점

시맨틱 웹은 데이터와 그들의 메타데이터 교환을 위한 개방적이고 상호 운용 가능한 표준을 보유하는 데 필요하다. RDF가 이를 제공하는 것이고, 처음 표준화한 이유이다. RDF의 이점은 다음과 같다.

* 일관된 프레임워크는 인터넷 자원에 대한 메타데이터의 공유를 장려한다.
* 데이터를 설명하고 쿼리하기 위한 RDF의 표준 구문은 메타데이터를 사용하는 소프트웨어를 쉽게 작동할 수 있도록 한다.
* 표준 구문과 쿼리 기능을 통해 어플리케이션간 정보 교환이 용이하다.
* 검색자는 전체 텍스트 수집에서 파생된 색인보다 메타데이터를 기반으로 검색하여 더 정확한 결과를 얻을 수 있다.
* 지능형 소프트웨어 에이전트는 더 정확한 데이터를 가지고 작업할 수 있어 사용자가 원하는 바를 보다 정확히 전달할 수 있다.

관계형 데이터베이스와 달리, RDF 데이터는 엔티티와 다른 엔티티와의 관계에 대한 훨씬 더 흥미로운 정보를 제공할 수 있다.

## RDF의 한계

개방형 사양과 광범위한 적용성으로 RDF는 시맨틱 웹 데이터(Semantic Web Data)를 게시하고 의미론적 정보 구성에 성공적인 표준이 되었다. RDF에는 몇 가지 제한이 있는데, 그 중 일부는 대중성에 기인한다.

RDF와 관련하여 발생할 수 있는 다음과 같은 몇 가지 점에서 어려울 수 있다.

* RDF 리소스를 설명하기 위한 **어휘의 표준화**는 어려울 수 있다. [Dublin Core Metadata Initiative](https://www.dublincore.org/)는 공통 엔티티와 속성에 대한 RDF 요소와 용어의 표준화를 시도하는 중요한 노력중 하나이다.
* RDF 문에 가장 적합한 구문 형식을 선택하려면 연습이 필요하다. 특히 RDF 문을 생성하는 시스템의 경우 특정 구현에 다른 형식을 사용하는 것이 더 쉬울 수 있다.
* 어플리케이션에 가장 적합한 RDF 쿼리 언어를 선택하는 것은 쿼리 언어의 기능과 어플리케이션의 요구에 따라 달라진다.

관계를 인코딩하는 메타데이터로 더 많은 웹 컨텐츠와 리소스를 사용할 수 있게 되면 사용자는 필요한 정보와 필요한 정보만 얻을 수 있는 옵션이 많아진다.
